<?php

return [
    'gameTitle' => env('GAME_TITLE', 'taishi-pk'),
    'oldCountry' => [
        [
            "id" => 1,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "近江",
            "kana" => "おうみ",
            "old_kana" => "ちかつあふみ",
            "harvest" => 78
        ],
        [
            "id" => 2,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "美濃",
            "kana" => "みの",
            "old_kana" => "みの",
            "harvest" => 54
        ],
        [
            "id" => 3,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "飛騨",
            "kana" => "ひだ",
            "old_kana" => "ひだ",
            "harvest" => 4
        ],
        [
            "id" => 4,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "信濃",
            "kana" => "しなの",
            "old_kana" => "しなの",
            "harvest" => 41
        ],
        [
            "id" => 5,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "上野",
            "kana" => "こうづけ",
            "old_kana" => "かみつけの",
            "harvest" => 50
        ],
        [
            "id" => 6,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "下野",
            "kana" => "しもつけ",
            "old_kana" => "しもつけの",
            "harvest" => 37
        ],
        [
            "id" => 7,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "出羽",
            "kana" => "でわ",
            "old_kana" => "いでは",
            "harvest" => 32
        ],
        [
            "id" => 8,
            "region" => "東山道",
            "region_slug" => "touzandou",
            "name" => "陸奥",
            "kana" => "むつ",
            "old_kana" => "みちのをく",
            "harvest" => 167
        ],
        [
            "id" => 9,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "若狭",
            "kana" => "わかさ",
            "old_kana" => "わかさ",
            "harvest" => 9
        ],
        [
            "id" => 10,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "越前",
            "kana" => "えちぜん",
            "old_kana" => "こしのみちのくち",
            "harvest" => 50
        ],
        [
            "id" => 11,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "加賀",
            "kana" => "かが",
            "old_kana" => "かが",
            "harvest" => 36
        ],
        [
            "id" => 12,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "能登",
            "kana" => "のと",
            "old_kana" => "のと",
            "harvest" => 21
        ],
        [
            "id" => 13,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "越中",
            "kana" => "えっちゅう",
            "old_kana" => "こしのみちのなか",
            "harvest" => 38
        ],
        [
            "id" => 14,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "越後",
            "kana" => "えちご",
            "old_kana" => "こしのみちのしり",
            "harvest" => 39
        ],
        [
            "id" => 15,
            "region" => "北陸道",
            "region_slug" => "hokurikudou",
            "name" => "佐渡",
            "kana" => "さど",
            "old_kana" => "さど",
            "harvest" => 2
        ],
        [
            "id" => 16,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "伊賀",
            "kana" => "いが",
            "old_kana" => "いが",
            "harvest" => 10
        ],
        [
            "id" => 17,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "伊勢",
            "kana" => "いせ",
            "old_kana" => "いせ",
            "harvest" => 57
        ],
        [
            "id" => 18,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "志摩",
            "kana" => "しま",
            "old_kana" => "しま",
            "harvest" => 2
        ],
        [
            "id" => 19,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "尾張",
            "kana" => "おわり",
            "old_kana" => "おはり",
            "harvest" => 57
        ],
        [
            "id" => 20,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "三河",
            "kana" => "みかわ",
            "old_kana" => "みかは",
            "harvest" => 29
        ],
        [
            "id" => 21,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "遠江",
            "kana" => "とおとおみ",
            "old_kana" => "とほたあふみ",
            "harvest" => 26
        ],
        [
            "id" => 22,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "駿河",
            "kana" => "するが",
            "old_kana" => "するが",
            "harvest" => 15
        ],
        [
            "id" => 23,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "伊豆",
            "kana" => "いず",
            "old_kana" => "いづ",
            "harvest" => 7
        ],
        [
            "id" => 24,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "甲斐",
            "kana" => "かい",
            "old_kana" => "かひ",
            "harvest" => 23
        ],
        [
            "id" => 25,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "相模",
            "kana" => "さがみ",
            "old_kana" => "さかみ",
            "harvest" => 19
        ],
        [
            "id" => 26,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "武蔵",
            "kana" => "むさし",
            "old_kana" => "むさし",
            "harvest" => 67
        ],
        [
            "id" => 27,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "安房",
            "kana" => "あわ",
            "old_kana" => "あは",
            "harvest" => 9
        ],
        [
            "id" => 28,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "上総",
            "kana" => "かずさ",
            "old_kana" => "かみつふさ",
            "harvest" => 38
        ],
        [
            "id" => 29,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "下総",
            "kana" => "しもうさ",
            "old_kana" => "しもつふさ",
            "harvest" => 39
        ],
        [
            "id" => 30,
            "region" => "東海道",
            "region_slug" => "toukaidou",
            "name" => "常陸",
            "kana" => "ひたち",
            "old_kana" => "ひたち",
            "harvest" => 53
        ],
        [
            "id" => 31,
            "region" => "畿内",
            "region_slug" => "kinai",
            "name" => "大和",
            "kana" => "やまと",
            "old_kana" => "やまと",
            "harvest" => 45
        ],
        [
            "id" => 32,
            "region" => "畿内",
            "region_slug" => "kinai",
            "name" => "山城",
            "kana" => "やましろ",
            "old_kana" => "やましろ",
            "harvest" => 23
        ],
        [
            "id" => 33,
            "region" => "畿内",
            "region_slug" => "kinai",
            "name" => "摂津",
            "kana" => "せっつ",
            "old_kana" => "つ",
            "harvest" => 36
        ],
        [
            "id" => 34,
            "region" => "畿内",
            "region_slug" => "kinai",
            "name" => "河内",
            "kana" => "こうち",
            "old_kana" => "かふち",
            "harvest" => 24
        ],
        [
            "id" => 35,
            "region" => "畿内",
            "region_slug" => "kinai",
            "name" => "和泉",
            "kana" => "いづみ",
            "old_kana" => "いずみ",
            "harvest" => 14
        ],
        [
            "id" => 36,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "丹波",
            "kana" => "たんば",
            "old_kana" => "たんば",
            "harvest" => 26
        ],
        [
            "id" => 37,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "丹後",
            "kana" => "たんご",
            "old_kana" => "たにはのみちのしり",
            "harvest" => 11
        ],
        [
            "id" => 38,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "但馬",
            "kana" => "たじま",
            "old_kana" => "たちま",
            "harvest" => 11
        ],
        [
            "id" => 39,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "因幡",
            "kana" => "いなば",
            "old_kana" => "いなは",
            "harvest" => 9
        ],
        [
            "id" => 40,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "伯耆",
            "kana" => "ほうき",
            "old_kana" => "ははき",
            "harvest" => 10
        ],
        [
            "id" => 41,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "出雲",
            "kana" => "いずも",
            "old_kana" => "いづも",
            "harvest" => 19
        ],
        [
            "id" => 42,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "石見",
            "kana" => "いわみ",
            "old_kana" => "いはみ",
            "harvest" => 11
        ],
        [
            "id" => 43,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "隠岐",
            "kana" => "おき",
            "old_kana" => "をき",
            "harvest" => 5
        ],
        [
            "id" => 44,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "播磨",
            "kana" => "はりま",
            "old_kana" => "はりま",
            "harvest" => 36
        ],
        [
            "id" => 45,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "美作",
            "kana" => "みまさか",
            "old_kana" => "みまさか",
            "harvest" => 19
        ],
        [
            "id" => 46,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "備前",
            "kana" => "びぜん",
            "old_kana" => "きびのみちのくち",
            "harvest" => 22
        ],
        [
            "id" => 47,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "備中",
            "kana" => "びっちゅう",
            "old_kana" => "きびのみちのなか",
            "harvest" => 18
        ],
        [
            "id" => 48,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "備後",
            "kana" => "びんご",
            "old_kana" => "きびのみちのしり",
            "harvest" => 19
        ],
        [
            "id" => 49,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "安芸",
            "kana" => "あき",
            "old_kana" => "あき",
            "harvest" => 19
        ],
        [
            "id" => 50,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "周防",
            "kana" => "すおう",
            "old_kana" => "すはう",
            "harvest" => 17
        ],
        [
            "id" => 51,
            "region" => "山陰道",
            "region_slug" => "sanninndou",
            "name" => "長門",
            "kana" => "ながと",
            "old_kana" => "ながと",
            "harvest" => 13
        ],
        [
            "id" => 52,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "紀伊",
            "kana" => "きい",
            "old_kana" => "き",
            "harvest" => 24
        ],
        [
            "id" => 53,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "淡路",
            "kana" => "あわじ",
            "old_kana" => "あはぢ",
            "harvest" => 6
        ],
        [
            "id" => 54,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "阿波",
            "kana" => "あわ",
            "old_kana" => "あは",
            "harvest" => 18
        ],
        [
            "id" => 55,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "讃岐",
            "kana" => "さぬき",
            "old_kana" => "さぬき",
            "harvest" => 13
        ],
        [
            "id" => 56,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "伊予",
            "kana" => "いよ",
            "old_kana" => "いよ",
            "harvest" => 37
        ],
        [
            "id" => 57,
            "region" => "南海道",
            "region_slug" => "nannkaidou",
            "name" => "土佐",
            "kana" => "とさ",
            "old_kana" => "とさ",
            "harvest" => 10
        ],
        [
            "id" => 58,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "筑前",
            "kana" => "ちくぜん",
            "old_kana" => "ちくしのみちのくち",
            "harvest" => 34
        ],
        [
            "id" => 59,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "筑後",
            "kana" => "ちくご",
            "old_kana" => "ちくしのみちのしり",
            "harvest" => 27
        ],
        [
            "id" => 60,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "豊前",
            "kana" => "ぶぜん",
            "old_kana" => "とよくにのみちのくち",
            "harvest" => 14
        ],
        [
            "id" => 61,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "豊後",
            "kana" => "ぶんご",
            "old_kana" => "とよくにのみちのしり",
            "harvest" => 42
        ],
        [
            "id" => 62,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "肥前",
            "kana" => "びぜん",
            "old_kana" => "ひのみちのくち",
            "harvest" => 31
        ],
        [
            "id" => 63,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "肥後",
            "kana" => "びご",
            "old_kana" => "ひのみちのしり",
            "harvest" => 34
        ],
        [
            "id" => 64,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "日向",
            "kana" => "ひゅうが",
            "old_kana" => "ひうか",
            "harvest" => 12
        ],
        [
            "id" => 65,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "大隅",
            "kana" => "おおすみ",
            "old_kana" => "おほすみ",
            "harvest" => 18
        ],
        [
            "id" => 66,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "薩摩",
            "kana" => "さつま",
            "old_kana" => "さつま",
            "harvest" => 28
        ],
        [
            "id" => 67,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "壱岐",
            "kana" => "いき",
            "old_kana" => "ゆき",
            "harvest" => 2
        ],
        [
            "id" => 68,
            "region" => "西海道",
            "region_slug" => "seikaidou",
            "name" => "対馬",
            "kana" => "つしま",
            "old_kana" => "つしま",
            "harvest" => 1
        ],
    ],

];