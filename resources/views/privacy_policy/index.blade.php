@extends('layouts.layout')

@section('title', 'プライバシーポリシー | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="信長の野望 徹底攻略のプライバシーポリシーです。">
    <meta property="og:description" content="信長の野望 徹底攻略のプライバシーポリシーです。" />
    <meta property="og:title" content="プライバシーポリシー | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "プライバシーポリシー | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <time>
                        <i class="far fa-calendar-check"></i><span class="calender">2021年12月23日</span>
                        {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2021年12月12日</span> --}}
                    </time>
                    <h1 class="mt-8">プライバシーポリシー</h1>
        
                    <div class="main_contents">
                        <h2 class="mt-24 rich_h2">個人情報の利用目的</h2>
                        <p class="mt-12">
                            当ブログでは、メールでのお問い合わせ、メールマガジンへの登録などの際に、名前（ハンドルネーム）、メールアドレス等の個人情報をご登録いただく場合がございます。<br>
                        
                            これらの個人情報は質問に対する回答や必要な情報を電子メールなどをでご連絡する場合に利用させていただくものであり、個人情報をご提供いただく際の目的以外では利用いたしません。
                        </p>
                        <h2 class="mt-24 rich_h2">個人情報の第三者への開示</h2>
                        <p class="mt-12">
                            当サイトでは、個人情報は適切に管理し、以下に該当する場合を除いて第三者に開示することはありません。
                            <br>
                            ・本人のご了解がある場合<br>
                            ・法令等への協力のため、開示が必要となる場合
                        </p>
                        <h2 class="mt-24 rich_h2">個人情報の開示、訂正、追加、削除、利用停止</h2>
                        <p class="mt-12">
                            ご本人からの個人データの開示、訂正、追加、削除、利用停止のご希望の場合には、ご本人であることを確認させていただいた上、速やかに対応させていただきます。
                        </p>                        
                        <h2 class="mt-24 rich_h2">免責事項</h2>
                        <p class="mt-12">
                            当サイトからリンクやバナーなどによって他のサイトに移動された場合、移動先サイトで提供される情報、サービス等について一切の責任を負いません。<br>
                        
                            当サイトのコンテンツ・情報につきまして、可能な限り正確な情報を掲載するよう努めておりますが、誤情報が入り込んだり、情報が古くなっていることもございます。<br>
                            
                            当サイトに掲載された内容によって生じた損害等の一切の責任を負いかねますのでご了承ください。
                        </p>
                        <h2 class="mt-24 rich_h2">プライバシーポリシーの変更について</h2>
                        <p class="mt-12">
                            当サイトは、個人情報に関して適用される日本の法令を遵守するとともに、本ポリシーの内容を適宜見直しその改善に努めます。<br>
                        
                            修正された最新のプライバシーポリシーは常に本ページにて開示されます。
                        </p>        
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'privacy'])
    </div>
@endsection