<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {{-- 固定ページ --}}
    <url>
        <loc>{{ url('/') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ url('/privacy_policy') }}</loc>
        <changefreq>yearly</changefreq>
        <priority>0.2</priority>
    </url>
    <url>
        <loc>{{ url('/about') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ url('/release') }}</loc>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ url('/administrator') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>{{ url('/contact') }}</loc>
        <changefreq>yearly</changefreq>
        <priority>0.2</priority>
    </url>

    {{-- 新信長の野望 --}}
    {{-- 攻略 --}}
    <url>
        <loc>{{ url('/shinnobunaga/strategy/beginner') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>


    {{-- 武将名鑑 --}}
    <url>
        <loc>{{ url('/taishi-pk/database/samurai') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    @foreach ($TaishiSamurais as $TaishiSamurai)
        <url>
            <loc>{{ url('/taishi-pk/database/samurai/' . $TaishiSamurai->id) }}</loc>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach
    <url>
        <loc>{{ url('/taishi/database/will') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/taimei') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi/strategy') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/strategy') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi/tactics') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/tactics') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/resource') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    {{-- 天守 --}}
    <url>
        <loc>{{ url('/taishi/tensyu') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/tensyu') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    {{-- 設備 --}}
    <url>
        <loc>{{ url('/taishi-pk/facility') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    {{-- 施設 --}}
    <url>
        <loc>{{ url('/taishi-pk/building') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>

    {{-- シナリオ --}}
    <url>
        <loc>{{ url('/taishi/scenario') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/shinsei/scenario') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/scenario') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/taishi/scenario/recommend') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>

    {{-- 攻略 --}}
    <url>
        <loc>{{ url('/taishi-pk/capture/military') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/march') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
        <url>
        <loc>{{ url('/taishi-pk/capture/decisive-battle') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/siege') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/plot') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/general-contract') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/human-resources') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/will') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/domestic-affairs') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/commercial') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/agriculture') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    <url>
        <loc>{{ url('/taishi-pk/capture/diplomacy') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>
    {{-- 記事 --}}
    <url>
        <loc>{{ url('/article') }}</loc>
        <changefreq>always</changefreq>
        <priority>0.5</priority>
    </url>
    @foreach ($Articles as $Article)
        <url>
            <loc>{{ url('/article/' .  $Article->id) }}</loc>
            <lastmod>{{ $Article->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach
    @foreach ($ArticleCategories as $ArticleCategory)
        <url>
            <loc>{{ url('/article/category/' .  $ArticleCategory->slug) }}</loc>
            <changefreq>weekly</changefreq>
            <priority>0.5</priority>
        </url>
    @endforeach

    {{-- 投票 --}}
    <url>
        <loc>{{ url('/vote-campaign') }}</loc>
        <changefreq>daily</changefreq>
        <priority>0.6</priority>
    </url>
    @foreach ($VoteCampaigns as $VoteCampaign)
        <url>
            <loc>{{ url('/vote-campaign/' .  $VoteCampaign->id) }}</loc>
            <lastmod>{{ $now->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
        <url>
            <loc>{{ url('/vote-campaign/' .  $VoteCampaign->id . '/vote') }}</loc>
            <changefreq>monthly</changefreq>
            <priority>0.3</priority>
        </url>
    @endforeach

    {{-- コンテンツ --}}
    <url>
        <loc>{{ url('/contents/timeline') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ url('/contents/game_title') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ url('/contents/real') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>{{ url('/contents/war') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.9</priority>
    </url>
    @foreach ($Disturbances as $Disturbance)
        <url>
            <loc>{{ url('/contents/war/' . $Disturbance->id) }}</loc>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    <url>
        <loc>{{ url('/contents/vassals') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ url('/contents/word') }}</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>
    {{-- 新生 --}}
    <url>
        <loc>{{ url('/shinsei/info') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ url('/shinsei/tensyou') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{ url('/shinsei/beginner') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>{{ url('/shinsei/policy') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.5</priority>
    </url>
    @foreach ($ShinseiSamurais as $ShinseiSamurai)
        <url>
            <loc>{{ url('/shinsei/database/samurai/' . $ShinseiSamurai->id) }}</loc>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach

    {{-- 新生PK --}}
    <url>
        <loc>{{ url('/shinsei-pk/database/samurai') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    @foreach ($ShinseiPkSamurais as $ShinseiPkSamurai)
        <url>
            <loc>{{ url('/shinsei-pk/database/samurai/' . $ShinseiPkSamurai->id) }}</loc>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach

    {{-- 覇道 --}}
    <url>
        <loc>{{ url('/hadou/database/samurai') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>
    @foreach ($HadouSamuraiParameters as $HadouSamuraiParameter)
        <url>
            <loc>{{ url('/hadou/database/samurai/' . $HadouSamuraiParameter->samurai->id) }}</loc>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach
    <url>
        <loc>{{ url('/hadou/database/samurai/rarity/ssr') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/hadou/database/samurai/rarity/ssr') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/hadou/database/samurai/rarity/r') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('/hadou/database/samurai/rarity/n') }}</loc>
        <changefreq>monthly</changefreq>
        <priority>0.6</priority>
    </url>
</urlset>