@extends('layouts.layout')

@section('title', '検索 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="「信長の野望 徹底攻略」の検索ページです。指定の文字列で検索ができます。">
    <meta property="og:description" content="「信長の野望 徹底攻略」の検索ページです。指定の文字列で検索ができます。" />
    <meta property="og:title" content="検索 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "このサイトについて | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <h1 id="title" class="h1">
                        「{{ $q }}」の検索結果
                    </h1>
                    <livewire:search />
                    <p class="font_small mt-12">全 {{ count($Samurais) + count($Words) + count($Articles) }} 件</p>
                    @if (count($Samurais))
                        <section>
                            <h2 class="rich_h2 mt-24">部将</h2>
                            <ul class="samurai_ul">
                                @foreach ($Samurais as $Samurai)
                                    <li class="samurai_li active" data-kana="{{ mb_substr($Samurai->furigana, 0, 1) }}">
                                        <a href="{{ route('database.samurais.show', [$GameTitle->slug, $Samurai->id]) }}" title="{{ $Samurai->name }}" class="samurai_anchor">
                                            <div class="img_area">
                                                <img src="{{ My_func::return_min_samurai_img_url($Samurai->id, $GameTitle->slug) }}" alt="{{ $Samurai->name }}" width="80px" height="80px" class="samurai_img">
                                            </div>
                                            <div class="text_area">
                                                <p class="hover_white">{{ $Samurai->name }} <span class="font_small hover_white">（{{ $Samurai->furigana }}）</span></p>
                                                <p class="mt-8 font_small hover_white">{!! $Samurai->retuden !!}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </section>
                    @endif

                    @if (count($Words))
                        <section>
                            <h2 class="rich_h2 mt-24">用語</h2>
                            <table class="font_small ta-c mt-24 w-100">
                                <tr>
                                    <th style="width: 60px;">用語</th>
                                    <th style="width: 80px;">読み</th>
                                    <th>説明</th>
                                </tr>
                                @foreach ($Words as $Word)
                                    <tr>
                                        <td class="back_cloud_blue">{{ $Word->name }}</td>
                                        <td>{{ $Word->name_kana }}</td>
                                        <td class="ta-l">{!! $Word->body !!}</td>
                                    </tr>                
                                @endforeach
                            </table>
                        </section>
                    @endif
                    @if (count($Articles))
                        <section>
                            <h2 class="rich_h2 mt-24">記事</h2>
                            <ul class="card_ul">
                                @foreach ($Articles as $Article)
                                    <li class="card_list narrow_card mt-24">
                                        <a href="{{ route('article.single', $Article->id) }}" class="card_anchor shadow shadow_hover" title="{{ $Article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($Article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($Article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $Article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info">
                                                    <time>
                                                        @if ($Article->is_updated)
                                                            {{ $Article->additioned_at->format('Y年m月d日') }}
                                                        @else
                                                            {{ $Article->created_at->format('Y年m月d日') }}
                                                        @endif
                                                    </time>
                                                    <p class="card_category">
                                                        @foreach ($Article->articleCategorySearches as $articleCategorySearch)
                                                            @if ($articleCategorySearch->is_main_category)
                                                                {{ $articleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $Article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </section>
                    @endif
                </article>
                @include('components.breadcrumbs', ['slug' => 'search', 'argument1' => $q])
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection