@extends('layouts.layout')

@section('title', $Article->title)

@section('meta')
    <meta name="description" content="{{ $Article->description }}">
    <meta property="og:description" content="{{ $Article->description }}" />
    <meta property="og:title" content="{{ $Article->title }}">
    <meta property="og:image" content="{{ My_func::return_article_main_img_url($Article->id) }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Article",
            "name": "{{ $Article->title }}",
            "url": "{{ url()->current() }}",
            "headline": "{{ $Article->title }}",
            "image": "{{ My_func::return_article_main_img_url($Article->id) }}",
            "datePublished": "{{ $Article->created_at }}",
            @if ($Article->is_updated)
                "dateModified": "{{ $Article->additioned_at }}",
            @endif
            "articleBody" : "{{ $Article->body }}",
            "author": {
              "@type": "Person",
              "url": "{{ route('administrator') }}",
              "name": "taka"
            }
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
    {{-- "description": "記事の説明文です。" --}}
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">{{ $Article->created_at->format('Y年m月d日') }}</span>
                            @if ($Article->is_updated)
                                <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">{{ $Article->additioned_at->format('Y年m月d日') }}</span>
                            @endif
                        </time>
                        <div>
                            <p>views : {{ $Article->view_count }}</p>
                        </div>
                    </div>

                    {{-- カテゴリー --}}
                    <ul class="category_ul d-f mt-12">
                        @foreach ($ArticleCategorySearches as $ArticleCategorySearch)
                            <li class="category_li"><a href="{{ route('article.category.index', $ArticleCategorySearch->articleCategory->slug) }}" class="category_anchor shadow shadow_hover" title="{{ $ArticleCategorySearch->articleCategory->name }}">{{ $ArticleCategorySearch->articleCategory->name }}</a></li>
                        @endforeach
                    </ul>
                    <h1 class="mt-36">{{ $Article->title }}</h1>
                    <picture>
                        <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($Article->id) }}" class="main_visual mt-24">
                        <img src="{{ My_func::return_article_main_img_url($Article->id) }}" width="100%" height="100%" class="main_visual mt-24" alt="{{ $ArticleCategorySearch->article->title }}">
                    </picture>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            {!! $Article->body !!}
                        </div>
                    </div>
                </article>
                {{-- SNSシェア --}}
                <div class="mt-24">
                    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="{{ $Article->title }}" data-url="{{ url()->current() }}" data-hashtags="信長の野望徹底攻略" data-size="large" data-show-count="false">ツイート</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                {{-- 関連武将 --}}
                <section class="mt-48">
                    <h2 class="rich_h2">記事に登場する武将</h2>
                    <ul class="samurai_ul mt-12">
                        @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                            <li class="samurai_li active">
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $SamuraiRelatedArticle->Samurai->id]) }}" title="{{ $SamuraiRelatedArticle->Samurai->name }}" class="samurai_anchor">
                                    <div class="img_area">
                                        <img src="{{ My_func::return_min_samurai_img_url($SamuraiRelatedArticle->Samurai->id, $GameTitle->slug) }}" alt="{{ $SamuraiRelatedArticle->Samurai->name }}" width="80px" height="80px" class="samurai_img">
                                    </div>
                                    <div class="text_area">
                                        <p class="hover_white">{{ $SamuraiRelatedArticle->Samurai->name }} <span class="font_small hover_white">（{{ $SamuraiRelatedArticle->Samurai->furigana }}）</span></p>
                                        <p class="mt-8 font_small hover_white">{!! $SamuraiRelatedArticle->Samurai->retuden !!}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </section>
                {{-- 著者のプロフィール --}}
                <div class="author_area d-f mt-24">
                    <div class="author_img_area">
                        <img src="{{ asset('img/common/taka.png')}}" class="profile_img shadow" alt="信長の野望 徹底攻略の管理人 taka" width="100%" height="100%">
                    </div>
                    <div class="author_text">
                        <p class="author fw-b">taka</p>
                        <p class="author_description">信長の野望シリーズを「革新」の頃の2005年からプレイ。大河ドラマや歴史小説を読みまくる。明治維新・元寇も好きだが戦国時代が一番好き。<br>
                            現在フリーランスとしてwebサイトの開発をやっています！よろしく！<br>
                        </p>
                        <p>公式Twitterは<a href="https://twitter.com/nobutettei" target="_blank" title="信長の野望 徹底攻略の公式Twitter">こちら</a></p>
                        <p>プロフィールは<a href="{{ route('administrator') }}" title="管理人情報">こちら</a></p>
                    </div>
                </div>
                <div class="mt-48">
                    {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                        crossorigin="anonymous"></script> --}}
                    {{-- 記事終わり --}}
                    <ins class="adsbygoogle"
                        style="display:block"
                        data-ad-client="ca-pub-2958289145034418"
                        data-ad-slot="6356312135"
                        data-ad-format="auto"
                        data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <section class="mt-48">
                    <h2 class="rich_h2 d-f ai-c">
                        <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                        <p>[{{ $MainArticleCategorySearch->articleCategory->name }}]関連記事一覧</p>
                    </h2>
                    @if (isset($RelatedArticleCategorySearches))
                        <ul class="card_ul">
                            @foreach ($RelatedArticleCategorySearches as $RelatedArticleCategorySearch)
                                @if (isset($RelatedArticleCategorySearch->article_id))
                                    <li class="card_list narrow_card mt-24">
                                        <a href="{{ route('article.single', $RelatedArticleCategorySearch->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $RelatedArticleCategorySearch->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($RelatedArticleCategorySearch->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($RelatedArticleCategorySearch->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $RelatedArticleCategorySearch->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>
                                                        @if ($RelatedArticleCategorySearch->article->is_updated)
                                                            {{ $RelatedArticleCategorySearch->article->additioned_at->format('Y年m月d日') }}
                                                        @else
                                                            {{ $RelatedArticleCategorySearch->article->created_at->format('Y年m月d日') }}
                                                        @endif
                                                    </time>
                                                    <p class="card_category">
                                                        @foreach ($RelatedArticleCategorySearch->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $RelatedArticleCategorySearch->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @elseif (isset($RelatedArticleCategorySearch->disturbance_id))
                                    <li class="card_list narrow_card mt-24">
                                        <a href="{{ route('other.war.single', $RelatedArticleCategorySearch->disturbance->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $RelatedArticleCategorySearch->disturbance->name }}">
                                            <div class="card_image" style="background-image: url('{{ Storage::disk('s3')->url('img/disturbances/' . $RelatedArticleCategorySearch->disturbance->id . '.png') }}');"></div>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>
                                                        {{ $RelatedArticleCategorySearch->disturbance->created_at->format('Y年m月d日') }}
                                                    </time>
                                                    <p class="card_category">
                                                        @foreach ($RelatedArticleCategorySearch->disturbance->articleCategorySearches as $mainDisturbanceCategorySearch)
                                                            @if ($mainDisturbanceCategorySearch->is_main_category)
                                                                {{ $mainDisturbanceCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $RelatedArticleCategorySearch->disturbance->name }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                    <div class="mt-36 ta-c">
                        <a href="{{ route('article.category.index', $MainArticleCategorySearch->articleCategory->slug) }}" title="一覧を見る">一覧を見る</a>
                    </div>
                </section>
                @include('components.contents_bottom_ad')
                @include('components.recommend_articles')
                @include('components.breadcrumbs', ['slug' => 'article.single', 'argument1' => $ArticleCategorySearches[0]->articleCategory, 'argument2' => $Article])
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection