@extends('layouts.layout')

@section('title', '[' . $ArticleCategory->name . '] の記事一覧 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="カテゴリー[{{ $ArticleCategory->name }}] の記事一覧ページです。「信長の野望 徹底攻略」では歴史、特に戦国時代の{{ $ArticleCategory->name }}に関する記事を扱っています。">
    <meta property="og:description" content="カテゴリー[{{ $ArticleCategory->name }}] の記事一覧ページです。「信長の野望 徹底攻略」では歴史、特に戦国時代の{{ $ArticleCategory->name }}に関する記事を扱っています。" />
    <meta property="og:title" content="[{{ $ArticleCategory->name }}] の記事一覧 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "【{{ $ArticleCategory->name }}】の記事一覧 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>[{{ $ArticleCategory->name }}] の記事一覧</h1>
                    <ul class="card_ul">
                        @foreach ($ArticleCategorySearches as $ArticleCategorySearch)
                            @if (isset($ArticleCategorySearch->article_id))
                                <li class="card_list narrow_card mt-24">
                                    <a href="{{ route('article.single', $ArticleCategorySearch->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $ArticleCategorySearch->article->title }}">
                                        <picture>
                                            <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($ArticleCategorySearch->article->id) }}" class="card_image">
                                            <img src="{{ My_func::return_article_main_img_url($ArticleCategorySearch->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $ArticleCategorySearch->article->title }}">
                                        </picture>
                                        <div class="card_bottom">
                                            <div class="d-f j-sb card_info">
                                                <time>
                                                    @if ($ArticleCategorySearch->article->is_updated)
                                                        {{ $ArticleCategorySearch->article->additioned_at->format('Y年m月d日') }}
                                                    @else
                                                        {{ $ArticleCategorySearch->article->created_at->format('Y年m月d日') }}
                                                    @endif
                                                </time>
                                                <p class="card_category">
                                                    @foreach ($ArticleCategorySearch->article->articleCategorySearches as $mainArticleCategorySearch)
                                                        @if ($mainArticleCategorySearch->is_main_category)
                                                            {{ $mainArticleCategorySearch->articleCategory->name }}
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                            <p class="card_title">{{ $ArticleCategorySearch->article->title }}</p>
                                        </div>
                                    </a>
                                </li>
                            @elseif (isset($ArticleCategorySearch->disturbance_id))
                                <li class="card_list narrow_card mt-24">
                                    <a href="{{ route('other.war.single', $ArticleCategorySearch->disturbance->id) }}" class="card_anchor shadow shadow_hover" title="{{ $ArticleCategorySearch->disturbance->name }}">
                                        <div class="card_image" style="background-image: url('{{ Storage::disk('s3')->url('img/disturbances/' . $ArticleCategorySearch->disturbance->id . '.png') }}');"></div>
                                        <div class="card_bottom">
                                            <div class="d-f j-sb card_info">
                                                <time>
                                                    {{ $ArticleCategorySearch->disturbance->created_at->format('Y年m月d日') }}
                                                </time>
                                                <p class="card_category">
                                                    @foreach ($ArticleCategorySearch->disturbance->articleCategorySearches as $mainDisturbanceCategorySearch)
                                                        @if ($mainDisturbanceCategorySearch->is_main_category)
                                                            {{ $mainDisturbanceCategorySearch->articleCategory->name }}
                                                        @endif
                                                    @endforeach
                                                </p>
                                            </div>
                                            <p class="card_title">{{ $ArticleCategorySearch->disturbance->name }}</p>
                                        </div>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="mt-24">
                        {{ $ArticleCategorySearches->links() }}
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'article.category.index', 'argument1' => $ArticleCategory])
                </div>
                {{-- 次の記事 --}}
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection