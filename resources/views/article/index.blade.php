@extends('layouts.layout')

@section('title', '記事一覧 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="記事一覧ページです。「信長の野望 徹底攻略」では歴史、特に戦国時代に関する記事を扱っています。">
    <meta property="og:description" content="記事一覧ページです。「信長の野望 徹底攻略」では歴史、特に戦国時代に関する記事を扱っています。" />
    <meta property="og:title" content="記事一覧 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "記事一覧 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>記事一覧</h1>
                    <ul class="card_ul">
                        @foreach ($Articles as $Article)
                            <li class="card_list narrow_card mt-24">
                                <a href="{{ route('article.single', $Article->id) }}" class="card_anchor shadow shadow_hover" title="{{ $Article->title }}">
                                    <picture>
                                        <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($Article->id) }}" class="card_image">
                                        <img src="{{ My_func::return_article_main_img_url($Article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $Article->title }}">
                                    </picture>
                                    <div class="card_bottom">
                                        <div class="d-f j-sb card_info">
                                            <time>
                                                @if ($Article->is_updated)
                                                    {{ $Article->additioned_at->format('Y年m月d日') }}
                                                @else
                                                    {{ $Article->created_at->format('Y年m月d日') }}
                                                @endif
                                            </time>
                                            <p class="card_category">
                                                @foreach ($Article->articleCategorySearches as $articleCategorySearch)
                                                    @if ($articleCategorySearch->is_main_category)
                                                        {{ $articleCategorySearch->articleCategory->name }}
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                        <p class="card_title">{{ $Article->title }}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="mt-24">
                        {{ $Articles->links() }}
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'article.index'])
                </div>
                {{-- 次の記事 --}}
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection