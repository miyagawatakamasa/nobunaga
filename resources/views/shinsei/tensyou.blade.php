@extends('layouts.layout')

@section('title', '天正猿芝居のシナリオ予想【信長の野望 新生】')

@section('meta')
	<meta name="description" content="7月21日販売のゲーム「信長の野望 新生」の限定シナリオ「天正猿芝居」の予想を行いました。著者の予想は清洲会議の1582年です。">
    <meta property="og:description" content="7月21日販売のゲーム「信長の野望 新生」の限定シナリオ「天正猿芝居」の予想を行いました。著者の予想は清洲会議の1582年です。" />
    <meta property="og:title" content="天正猿芝居のシナリオ予想【信長の野望 新生】">
    <meta property="og:image" content="{{ asset('img/shinsei/6.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Article",
            "image": "{{ asset('img/shinsei/6.png')}}",  
            "name": "天正猿芝居のシナリオ予想【信長の野望 新生】",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <time>
                        <i class="far fa-calendar-check"></i><span class="calender">2022年05月17日</span>
                        {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年05月03日</span> --}}
                    </time>
                    <h2>天正猿芝居のシナリオ予想【信長の野望 新生】</h2>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            <picture>
                                <source type="image/webp" srcset="{{ asset('img/shinsei/6.webp')}}">
                                <img src="{{ asset('img/shinsei/6.png')}}" alt="信長の野望 新生 最新情報まとめ" width="100%" height="100%">
                            </picture>
<p>こんにちは、歴史大好き管理人のtakaです。</p>
<p>とうとう「信長の野望 新生」の発売日が<span class="color_red">2022年7月21日</span>と決定しました！</p>
<p>おめでとうございます！！待ち遠しいです。</p>
<p>信長の野望 新生の豪華版には、シナリオ「<span class="color_red">天正猿芝居</span>」限定ダウンロードシリアルがありますが、この「天正猿芝居」についての情報が出ていないので、今回の記事では「天正猿芝居」のシナリオの予想をしていきます！</p>

<h2><span id="i">信長の野望 新生の商品情報</span></h2>
<p>どうすれば「天正猿芝居」シナリオを手に入れられるかについてまず書いていきます。</p>
<p>「信長の野望 新生」購入の種類はゲームソフトのみの<span class="color_red">通常版</span>と、さまざまな特典がついている<span class="color_red">豪華版</span>の2つあります。</p>
<p><span class="yellow_marker">ゲームソフトのみの通常版には「天正猿芝居」はついてきません！</span></p>
<p>豪華版のひとつ「<span class="color_red">TREASURE BOX</span>」パッケージ版の特典がこちら。</p>
<ul>
<li>オリジナルサウンドトラックCD（2枚組）</li>
<li>シブサワ・コウ秘伝攻略データ＆武将アートブック</li>
<li>戦国 日めくり万年カレンダー</li>
<li>シナリオ「天正猿芝居」限定ダウンロードシリアル</li>
</ul>
<p>希望小売価格は16,280円（税込）！</p>
<div class="mt-24">
<p>TREASURE BOXの購入は<a href="https://amzn.to/3wtSeE0" target="_blank">こちら</a>！</p>
</div>
<p class="mt-24">また豪華版のもうひとつの「<span class="color_red">Digital Deluxe Edition</span>」の特典が</p>
<ul>
<li>デジタルオリジナルサウンドトラック</li>
<li>デジタルシブサワ・コウ秘伝攻略データ＆武将アートブック</li>
<li>シナリオ「天正猿芝居」限定ダウンロードシリアル</li>
</ul>
<p>希望小売価格は14,080円（税込）でTREASURE BOX パッケージ版より約2,000円安いですね。</p>

<p>ということで、<span class="yellow_marker">どちらの豪華版にも「天正猿芝居」がついています</span>。</p>
<p>みなさんはどれを購入しますか？</p>
<p>著者は日めくり万年カレンダーはいらないので少し安いDigital Deluxe Editionにしようかと思っています。</p>
<h2><span id="i-2">天正猿芝居のシナリオ予想</span></h2>
<p>それでは「天正猿芝居」のシナリオ予想をしていきます。</p>
<h3><span id="i-3">天正とは？</span></h3>
<p>天正猿芝居の「<span class="color_red">天正</span>」とは年号ですね。</p>
<p>天正は1573年から1592年までの期間なので、この間のシナリオと予想できます。</p>
<h3><span id="i-4">猿芝居とは？</span></h3>
<p>猿はもちろん信長から猿と呼ばれた<span class="color_red">豊臣秀吉</span>のことでしょう。</p>
<p>猿芝居なので秀吉が画策した何かでしょうが、猿芝居と比喩される歴史事象はありません。<br />
何なんでしょうか？</p>
<p>天正の時代に秀吉が行った大きい事柄がこちら</p>
<ul>
<li>播磨・但馬平定 天正5年（1577年）</li>
<li>中国攻め 天正9年（1581年）</li>
<li>中国大返し 天正10年（1582年）</li>
<li>山﨑の戦い 天正10年（1582年）</li>
<li>清洲会議 天正10年（1582年）</li>
<li>賤ヶ岳の戦い 天正11年（1583年）</li>
<li>小牧長久手の戦い 天正12年（1584年）</li>
<li>関白任官と紀伊・四国・越中攻略 天正13年（1585年）</li>
<li>九州平定 天正15年（1587年）</li>
<li>小田原征伐から奥羽仕置 天正18年（1590年）</li>
<li>天下統一 天正19年（1591年）</li>
</ul>
<p>こう見てみると秀吉の激動の時代ですね&#8230;</p>
<h3><span id="i-5">天正猿芝居はいつの時代？</span></h3>
<p>天正猿芝居という秀吉がメインの時代ということは、本能寺の変（1582年）の後と予想します。</p>
<p>秀吉の勢力が大きいとゲームとしてつまらないので、おそらく紀伊・四国・越中攻略（1585年）より以前の時代でしょう。</p>
<p>というと、この辺りか。<br />
中国大返し 天正10年（1582年）<br />
山﨑の戦い 天正10年（1582年）<br />
清洲会議 天正10年（1582年）<br />
賤ヶ岳の戦い 天正11年（1583年）<br />
小牧長久手の戦い 天正12年（1584年）</p>
<p>山﨑の戦い、賤ヶ岳の戦い、小牧長久手の戦いは芝居という感じではないので、中国大返しか清洲会議でしょう。<br />
「1582年5月　夢幻の如く」というシナリオがデフォルトで存在しているので、それとより遠い<span class="yellow_marker">清洲会議 天正10年（1582年）が天正猿芝居の年代</span>と著者は予想します！</p>
<h2><span id="i-6">清洲会議とは</span></h2>
<p>清洲会議を知らない方にちょっと説明します。<br />
本能寺の変で織田信長は亡くなりましたが、嫡男の織田信忠も同時に亡くなっています。<br />
とすると次の当主は誰にしようかとなりますよね。</p>
<p>事態の収拾を図るため各地に散らばっていた柴田勝家をはじめとする織田家の重臣達は愛知県の清洲城に集まりました。</p>
<p>次の当主として序列は<span class="color_red">次男 織田信雄</span>（おだのぶかつ）が妥当ですが、うつけ者で評判が良くありません。</p>
<p>そこで<span class="color_red">三男 織田信孝</span>（おだのぶたか）も後継ぎの候補に上がったのです。</p>
<p>三男 信孝は人心を集め、戦でも功を上げています。</p>
<p>秀吉のことが大嫌いだった<span class="yellow_marker">筆頭家老・柴田勝家は三男の信孝を次期当主に推薦しました</span>。</p>
<p>そして<span class="yellow_marker">羽柴秀吉は対抗するように次男の信雄を推しました</span>。</p>
<p>宿老の一人・滝川一益の代理として池田恒興（いけだつねおき）を含む、柴田勝家・丹羽長秀・羽柴秀吉・池田恒興の4人で次期当主を決める清洲会議を行います。</p>
<p>柴田勝家・丹羽長秀が三男 織田信孝を羽柴秀吉・池田恒興が次男 織田信雄を推薦すると事前に予想されていましたが、蓋を開けてみると柴田勝家が三男 織田信孝を<span class="yellow_marker">丹羽長秀・羽柴秀吉・池田恒興の3人が三法師を推薦します</span>。</p>
<p>ここで出てきた<span class="color_red">三法師</span>というのが信忠の嫡男で当時わずか3歳でした。</p>
<p>清洲会議でこの三法師が次期当主と決定し、その後見人として羽柴秀吉がなりました。</p>
<img src="{{ asset('img/shinsei/kiyosu.png')}}" alt="清洲会議" width="100%" height="100%" class="mt-24">
<p class="mt-24">この絵は、秀吉が三法師を抱き抱えているところです。</p>
<p>清洲会議を期に織田家臣内での勢力が柴田勝家から羽柴秀吉に移ったということですね。</p>
<h2><span id="i-7">まとめ</span></h2>
<p>というわけで、今回は天正猿芝居のシナリオ予想を行いました。<br />
著者の予想は、<span class="yellow_marker">羽柴秀吉が清洲会議を行う1582年</span>です。</p>
<p>また公式の情報が更新され内容がわかったら記事修正しますね。</p>
<p>それでは。</p>

{{-- <div class="mt-24 ta-c d-f j-sb">
    <iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=tfile08-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B09WW1B5G8&linkId=f9ed3cfb5b876253bf4b353320e9ea69"></iframe>
    <iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=tfile08-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B09WXNWR64&linkId=a470706b173f418a7af6b22ec617172b"></iframe>
</div> --}}
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinsei.tensyou'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection