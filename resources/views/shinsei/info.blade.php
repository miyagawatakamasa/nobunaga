@extends('layouts.layout')

@section('title', '信長の野望 新生 最新情報まとめ（5/20 の最新情報もあるよ） | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="7月21日販売のゲーム「信長の野望 新生」の最新情報をまとめています。3/30日の信長の日に生配信された時に公開された情報も記載しています。">
    <meta property="og:description" content="7月21日販売のゲーム「信長の野望 新生」の最新情報をまとめています。3/30日の信長の日に生配信された時に公開された情報も記載しています。" />
    <meta property="og:title" content="信長の野望 新生 最新情報まとめ（5/20 の最新情報もあるよ） | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/shinsei/6.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Article",
            "image": "{{ asset('img/shinsei/6.png')}}",  
            "name": "信長の野望 新生 最新情報まとめ（5/20 の最新情報もあるよ） | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <time>
                        <i class="far fa-calendar-check"></i><span class="calender">2022年04月04日</span>
                        <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年05月20日</span>
                    </time>
                    <h2>信長の野望 新生 最新情報まとめ<br>（5/20 の最新情報もあるよ）</h2>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            <picture>
                                <source type="image/webp" srcset="{{ asset('img/shinsei/6.webp')}}">
                                <img src="{{ asset('img/shinsei/6.png')}}" alt="信長の野望 新生 最新情報まとめ" width="100%" height="100%">
                            </picture>
<p>こんにちは、歴史大好き管理人のtakaです。</p>
<p>とうとう<span class="color_red">「信長の野望 新生」の発売日が2022年7月21日</span>と決定しました！</p>
<p>おめでとうございます！！待ち遠しいです。</p>
<p>今回の記事では、<a href="https://www.gamecity.ne.jp/shinsei/#top" target="_blank" title="信長の野望 新生公式HP">信長の野望 新生公式HP</a>や3/30の信長の野望の日に行われた公式生放送で分かった情報をまとめていきます。</p>

<p>2022/04/23 追記: 4/22にYouTubeで公開された「具申・助言」について更新しました。</p>
<p>2022/05/20 追記: 4/22にYouTubeで公開された「戦の準備」について更新しました。</p>

<p>それでは見ていきましょう！！</p>
<h2>製品情報</h2>
<h3>通常版</h3>
<p>ゲームソフトのみのパッケージ版・ダウンロード版は共に価格は10,780円（税込）です。</p>
<p>PlayStation®4 / Nintendo Switch™ / Windows® お持ちのハードに対応したソフトを購入しよう。</p>

<h3>豪華版</h3>
<p>続いてゲームソフトに加えて特典が入っている豪華版はというと、</p>
<p><span class="color_red">TREASURE BOX パッケージ版</span>の希望小売価格は16,280円（税込）！</p>
<p>なかなかなお値段ですが、気になるのは特典です。それがこちら。</p>
<ul>
    <li>オリジナルサウンドトラックCD（2枚組）</li>
    <li>シブサワ・コウ秘伝攻略データ＆武将アートブック</li>
    <li>戦国 日めくり万年カレンダー</li>
    <li>シナリオ「天正猿芝居」限定ダウンロードシリアル</li>
</ul>
<p>シナリオ「天正猿芝居」が欲しい！</p>
<p>「天正猿芝居」のシナリオ予想は<a href="{{ route('shinsei.tensyou') }}">こちら</a></p>

<p>また<span class="color_red">Digital Deluxe Edition</span>というデジタルの豪華版もあります。</p>
<p>希望小売価格は14,080円（税込）でTREASURE BOX パッケージ版より約2,000円安いですね。</p>
<p>こちらの特典は</p>
<ul>
    <li>デジタルオリジナルサウンドトラック</li>
    <li>デジタルシブサワ・コウ秘伝攻略データ＆武将アートブック</li>
    <li>シナリオ「天正猿芝居」限定ダウンロードシリアル</li>
</ul>
<p>TREASURE BOX パッケージ版は、<br>
    ・ダウンロードではなくゲームソフトが来る<br>
    ・日めくり万年カレンダーがある</p>
<p>Digital Deluxe Editionは、<br>
・ダウンロード版<br>
・日めくり万年カレンダーがない</p>
<p>共にシブサワ・コウ秘伝攻略データ＆武将アートブック、シナリオ「天正猿芝居」限定ダウンロードシリアルがもらえるということですね。</p>
<p></p>

<p>シナリオ「天正猿芝居」は両方入っているのか...</p>
<p>ちょっとお財布に優しい、Digital Deluxe Editionを管理人は買います！</p>
<h2>発売日</h2>
<p>「信長の野望 新生」の発売日は冒頭でも触れたように<span class="color_red">2022年7月21日</span>に決定しました。</p>
<p>発売時期は当初「桜開花に負けない」や「2022年early」公式サイトと出ていましたが、7月ということでちょっと伸びた感じですね。</p>
<p>発売が伸びた代わりに、シブサワコウ40周年に相応しいしっかりとしたボリュームになるということなのでこれは期待大です！</p>
<h2>購入ストア・店舗特典</h2>
<p>既に<span class="color_red">「信長の野望 新生」の予約が開始されています</span>。</p>
<p>（2022年4月4日現在）予約できる店舗と購入特典をまとめました。</p>
<table class="font_small mt-24 w-100">
    <tr>
        <th>店舗名</th>
        <th>購入特典購入</th>
        <th style="width: 70px;">リンク</th>
    </tr>
    <tr>
        <td>GAMECITYオンラインショッピング</td>
        <td>シブサワ・コウ40周年記念武将データ<br>
            項羽（項劉記）/宋江（水滸伝）/源義経（源平合戦）/チンギスカン（蒼き狼と白き牝鹿）/沖田総司（維新の嵐）
            </td>
        <td></td>
    </tr>
    <tr>
        <td>Amazon.co.jp</td>
        <td>※PC壁紙<br>Windows®版には付属しません</td>
        <td><a title="PS4版の予約" href="https://www.amazon.co.jp/%E3%80%90PS4%E3%80%91%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B%E3%83%BB%E6%96%B0%E7%94%9F-PC%E5%A3%81%E7%B4%99-%E9%85%8D%E4%BF%A1-%E6%97%A9%E6%9C%9F%E8%B3%BC%E5%85%A5%E7%89%B9%E5%85%B8%E3%82%B7%E3%83%8A%E3%83%AA%E3%82%AA%E3%80%8C%E9%95%B7%E7%AF%A0%E8%A8%AD%E6%A5%BD%E5%8E%9F%E3%81%AE%E6%88%A6%E3%81%84%E3%80%8D%E3%83%80%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%89%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%AB-%E3%80%90Amazon-co-jp%E9%99%90%E5%AE%9A%E3%80%91/dp/B09WW1B5G8?qid=1648732736&sr=8-3&linkCode=ll1&tag=tfile08-22&linkId=0a023c41dbcccb6fdeacd524be605778&language=ja_JP&ref_=as_li_ss_tl" target="_blank">PS4版の予約</a><br>
            <a title="Switch版の予約" href="https://www.amazon.co.jp/dp/B09WXQKCK3?qid=1648732736&sr=8-1&linkCode=ll1&tag=tfile08-22&linkId=b6b35c8003c16ce21e0e0cf106117cb0&language=ja_JP&ref_=as_li_ss_tl" target="_blank">Switch版の予約</a></td>
    </tr>
    <tr>
        <td>ebten</td>
        <td>B3タペストリー</td>
        <td></td>
    </tr>
    <tr>
        <td>WonderGOO</td>
        <td>湯呑</td>
        <td></td>
    </tr>
    <tr>
        <td>あみあみ</td>
        <td>アクリルスチルスタンド<br>
            ※Windows®版には付属しません<br>
            ※TREASURE BOXのみに付属します
        </td>
        <td></td>
    </tr>
    <tr>
        <td>ゲオ</td>
        <td>A4クリアファイル</td>
        <td></td>
    </tr>
    <tr>
        <td>よろずやショップびっく宝島</td>
        <td>ミニ色紙</td>
        <td></td>
    </tr>
    <tr>
        <td>楽天ブックス</td>
        <td>油取り紙</td>
        <td></td>
    </tr>
</table>
<h2>信長の野望 新生のコンセプト</h2>

<p>それでは、今回の信長の野望のコンセプトを見ていきましょう。</p>
<p>副題が「新生（しんせい）」ということで、今までとは違った新しい「信長の野望」を見ることができます。</p>
<p>シリーズ初の自ら考え行動する“生きた武将”が今作の特徴です。</p>
<p>今までは敵勢力がAIで動くことはあっても自勢力の武将は全て自分で操作してきました。</p>
<p>今作はAIの発展により、プレイ大名の家臣が自ら考え行動します。</p>
<p>委任で作業を減らすことはできていましたが、家臣が提案してきたり自ら動くことによってよりリアルに近づいたなと思います！</p>
<p>家臣がプレイヤーを支えるというのが謳い文句ですが、もしかしたら足手まといになるんじゃないかとも思います。</p>
<p>リアルでも部下が失敗したり謀反を起こしたりしたわけで、それはそれで面白いとも思います。</p>

<h2>システム</h2>
<p>それでは、具体的にどんな機能があるのか見ていきましょう。</p>

<h3>戦の準備</h3>
<img src="{{ asset('img/shinsei/12.png')}}" alt="戦の準備" width="100%" height="100%">
<p class="mt-24">5/20に信長の野望の公式YouTubeチャンネルから新しく「戦の準備」についての情報が公開されました。</p>
<div class="mt-24">
    <iframe title="ad" width="100%" height="300px" src="https://www.youtube.com/embed/ceOI9b5JLQM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p class="mt-24">軍備の要は「兵力」と「部隊編成」ということで、戦の準備についての動画になっています。</p>

<p>所領拡大のためには城の攻略が必要であり、十分な戦力を蓄えることが必要です。</p>
<img class="mt-24" src="{{ asset('img/shinsei/13.png')}}" alt="戦の準備" width="100%" height="100%">

<p class="mt-24">兵力は城の石高に応じて増加するようになっています。</p>
<p>ということは山城は兵力が少なく、平城は多くなりそうですね。</p>
<img class="mt-24" src="{{ asset('img/shinsei/11.png')}}" alt="戦の準備" width="100%" height="100%">
<p class="mt-24">郡開発や城下施設建設など内政によって石高を増やすことができます。</p>
    
<p>前線の城では石高アップを図り、兵力増強を狙うのも手。</p>
<h3>部隊編成</h3>
<p>戦で勝利するためには、兵数以外に部隊を率いる武将の選択も重要です。</p>
<p>能力や特性を生かした部隊編成が、兵力の差を覆すことも！</p>
    <p>戦力の拡充に加えて、事前に有利な状況を作ることも重要です。</p>
    <p>攻略目標を設定し、軍備を指示して部隊の能力を強化！</p>
    <p>同盟勢力への援軍要請。</p>
    <p>これは以前からありましたね。</p>
    <img class="mt-24" src="{{ asset('img/shinsei/10.png')}}" alt="破壊工作" width="100%" height="100%">

    <p class="mt-24">破壊工作による敵の弱体化ができるようです。</p>
    <p>戦国立志伝のミッションで破壊工作がありましたが、今作は金銭でできるようになっています。</p>
    <p>進軍ルートの確保</p>
    <p>戦は勢力の命運を賭けた一大事、英知を集めて必勝を期せ！</p>
    <p>敵を知り己を知れば、百戦危うからず</p>
    <p>敵味方の状況を見極め、万全の体勢で戦に挑め！</p>
    <p>というわけで、信長の野望で一番面白い合戦で勝利するためのシステム紹介でした。</p>
    <h3>具申</h3>
<p>4/22に信長の野望の公式YouTubeチャンネルから新しく「具申・助言」についての情報が公開されました。</p>
<div class="mt-24">
    <iframe title="ad" width="100%" height="300px" src="https://www.youtube.com/embed/bp-XLq9w4Ws" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p>「自ら考え、提案する武将たち具申・助言」ということで、今までの信長の野望シリーズにはなかった今回の大きな目玉機能ですね。</p>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/7.webp')}}">
    <img src="{{ asset('img/shinsei/7.png')}}" alt="具申" width="100%" height="100%">
</picture>
<p>具体的にどのような具申をしてくるかというと以下です。</p>

<ul>
    <li>丹羽長秀「日野城を急ぎ修復しましょう。戦なき今こ好機・・・次の戦に備え一気に直してしまわねば」</li>
    <li>前田慶次「長島城付近は警備が手薄な様子、出兵のための兵糧を焼き払い本願寺家の手駒を減らしましょう」</li>
    <li>滝川一益「我らが敵、本願寺家の急所は長島城と推察する次第なれば、先んじて城壁を崩しておこうかと」</li>
    <li>織田信忠「我が清洲城は人手が足りませぬ。ぜひとも牢人を登用しゆくゆくは我が城の郡を治めたく」</li>
</ul>
<p>修復や登用は言われんでもしますけど、「手駒を減らしましょう」や「城壁を崩しておこう」は、攻略しやすくしてくれるのでしょう。これはやってほしい。</p>

<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/8.webp')}}">
    <img src="{{ asset('img/shinsei/8.png')}}" alt="引抜工作" width="100%" height="100%">
</picture>
<p>調略「引抜工作」で武将を引き抜いてくれるようです。</p>

<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/9.webp')}}">
    <img src="{{ asset('img/shinsei/9.png')}}" alt="焼討" width="100%" height="100%">
</picture>
<p>調略「焼討」で兵糧を減少させ出陣を封じます。</p>

<p>時には対案が出され選択肢が増えることもあるようです。</p>
<p>どんどんリアルに近づいていますね。</p>
<p>どの家臣からの具申を優先すべきかということが今作で重要になってきます。</p>

<h3>助言</h3>
<p>家臣はプレイヤーの判断のサポートをし、自勢力がすべき当面の目標や周辺国の動向を考慮した次に取るべき行動の助言を行ってくれます。</p>
<p>君臣一体となることが天下統一の鍵になります。</p>

<h3>知行</h3>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/2.webp')}}">
    <img src="{{ asset('img/shinsei/2.png')}}" alt="知行" width="100%" height="100%">
</picture>
<p>今作では「他国から勝ち取った土地を、適材適所を見極め家臣に与える知行制」が再現されました。</p>
<p>大名の直轄地は与えられないが、他の土地や戦で奪った土地を家臣に与えることができます。</p>
<p>俸禄や家宝を与えたり、城主・軍団長に任命することは以前からありましたが、土地を与え運営させる知行制が導入されています。</p>
<p>これもまたリアルですね。</p>
<p>直轄地には、代官を置くことができます。</p>
<p>代官に置く武将は身分の縛りがないので、お気に入りの武将を代官にし、勲功をためさせることで身分をあげ、知行をあげるというプレイができます。</p>
<p>家臣の能力値によって知行での生産性が上がるようです。</p>
<p>知行を与えるには家臣が一定以上の身分になる必要があります。<br>
    より高い身分になると、2つ以上の知行を与えられるようになるので、能力高い武将にたくさん知行を与えるのが良さそうですね。</p>


<h3>戦闘</h3>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/4.webp')}}">
    <img src="{{ asset('img/shinsei/4.png')}}" alt="ルートの進言" width="100%" height="100%">
</picture>
<p>今作は、家臣が進行ルートを進言してくれます。</p>
<p>というのも、道には行軍上限が設けられているので、メインの部隊は大道で、サブ部隊を迂回されるなどの選択肢が出てきます。</p>

<p>また、大名は城主に対して、敵城攻略の準備を命じることができるのだが、命じられた城主は軍備を開始し、軍備が完了すると「臨戦状態」となって出陣する部隊の能力が上昇します。</p>
<p>臨戦状態は前作までになかったので、面白い仕組みですね。</p>
<p>しかし、臨戦状態では領地の発展は一時的に停滞してしまうのでバランス感覚が必要です。</p>

<h3>合戦</h3>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/5.webp')}}">
    <img src="{{ asset('img/shinsei/5.png')}}" alt="合戦" width="100%" height="100%">
</picture>
<p>すべての武将たちは自らの意思で戦います。</p>
<p>各個撃破や挟撃をうまくやってもらえたらいいのですが、どこまでやってくれるんでしょうね？</p>
<p>合戦の戦場には“要所”が点在しており、制圧することで戦いを有利に進めることができます。</p>
<p>また、マップには退却口が存在し、そこを敵に制圧されると総崩れに近いぐらい戦況が大いに崩れてしまいます。</p>
<p>100人の部隊が5,000人の部隊に突撃する命令を出すと、断られる場合があるようです。AIの進化です。</p>
<h3>戦略</h3>
<p>大名家の「政策」と武将が持つ「特性」がが富国強兵の要。</p>
<p>政策のレベルアップや、城主とそれを補佐する武将たちの特性で勢力の特色が大きく変化する。</p>

<p>政策と特性は馴染みの機能ですね。</p>
<p>特定の大名家専用の「政策」も存在するようです。</p>

<p>公式HPには</p>

<div class="retuden">
    <p>
    天下布武 星1 100/月<br>
    行軍による郡制圧速度 +20<br>
    全家臣の獲得勲功 +10%<br>
    最高能力60未満の武将忠誠 -2</p>
</div>

<div class="retuden">
    <p>
    天下布武 星2 2000/月<br>
    行軍による郡制圧速度 +50<br>
    全家臣の獲得勲功 +20%<br>
    最高能力60未満の武将忠誠 -2</p>
</div>
<p>必要威信 30<br>
必要武将 3<br>
期間120日</p>
<p>とあります。</p>
<p>自勢力の規模・金銭収入に合わせて、政策のレベルを自分で選べられるようですね。</p>
<p>今作の政策の実行には威信が必要です。</p>
<p>合戦で勝利することでこの威信を獲得することができます。</p>
<p>威信は、武将の忠誠度、大名家同士の敵視などにも影響します。</p>
<p>であれば、天下統一において合戦での勝利がより重要になりました。</p>

<p>武将の持つ「特性」の効果は内政や軍事など多岐にわたり、軍事系の特性を持った武将、城攻めに有利な「城乗」、特殊な調略が可能になる「策謀」などうまく使いこなすのが肝になるようです。</p>

<p>また、軍の統治に有効な「一所懸命」などの知行に関する特性もあるようです。</p>
<p>それにしても、早くやりたい。</p>

<h3>ドラマ</h3>
<p>新規武将のみならず、新勢力が登場し、武将2200人､イベント数330以上の大ボリュームとなっています。</p>

<h2>その他アップデート</h2>
<h3>リングメニュー</h3>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/3.webp')}}">
    <img src="{{ asset('img/shinsei/3.png')}}" alt="リングメニュー" width="100%" height="100%">
</picture>
<p>マップのどこでもいいので右クリックすると、リングメニューが表示され、コマンドを選択しやすくなりました。</p>
<p>パソコン版は問題ないけど、SwitchやPS4などのコントローラーでは少々手間だったので、この改修は嬉しいですね。</p>

<h3>チュートリアル</h3>
<picture class="mt-24">
    <source type="image/webp" srcset="{{ asset('img/shinsei/1.webp')}}">
    <img src="{{ asset('img/shinsei/1.png')}}" alt="チュートリアル" width="100%" height="100%">
</picture>
<p>40周年ということでチュートリアルをしっかり作成し、UIもわかりやすいようにしているため、今作が初プレイの方でも迷わない・楽しめるようになっています。</p>
<p></p>
<h3>よりリアルに</h3>
<p>評定画面では武将が瞬きをしたり、息遣いを見ることができます。</p>
<p>こういった細かいアップデートによってより没入感が味わえますね。</p>
<h2>まとめ</h2>
<p>いかがでしたか？</p>
<p>最新情報を見ると、とても楽しみになりますよね！</p>
<p>このサイトからゲームの予約をしていただくと、やる気が出ます！</p>
<p>発売されると新生の攻略情報もバンバン更新していきますので、ブックマークお願いします！</p>
<p><a href="{{ route('vote_campaign.show', 1 ) }}" title="あなたが好きな武将は誰？">投票キャンペーン</a>で好きな武将の投票募集しているのでこちらもよろしく！</p>

<div class="mt-24 ta-c">
    <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=tfile08-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B09WW1B5G8&linkId=f9ed3cfb5b876253bf4b353320e9ea69"></iframe>
</div>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinsei.info'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection