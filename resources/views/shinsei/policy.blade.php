@extends('layouts.layout')

@section('title', '政策【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望 {{$GameTitle->name}}」でのシナリオ一覧です。">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "政策【新生】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                            <p>政策【新生】</p>
                        </div>
                    </h1>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th>政策名</th>
                            <th>効果</th>
                            <th>固有大名</th>
                        </tr>
                        @foreach ($ShinseiPolicies as $ShinseiPolicy)
                            <tr>
                                <td>{{ $ShinseiPolicy->name }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinseiPolicy->description)) !!}</td>
                                <td>{{ $ShinseiPolicy->daimyou }}</td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'scenario.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection