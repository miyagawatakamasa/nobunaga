@extends('layouts.layout')

@section('title', '' . $Samurai->name . '【新生】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="このページは「信長の野望 新生」の攻略データベース内の特定の武将の詳細ページです。信長の野望 新生に登場する「{{ $Samurai->name }}」の情報、能力、スキルなどを確認することができます。">
    <meta property="og:description" content="{{ $Samurai->retuden }}" />
    <meta property="og:title" content="「{{ $Samurai->name }}」の武将パラメーター({{ $GameTitle->name }}) | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ My_func::return_min_samurai_img_url($Samurai->id, $GameTitle->slug) }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "{{ $Samurai->name }}",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/slick/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){    
            $('#slider').slick({
                arrows: false,
                centerMode: true,
            });
        });
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <p>武将名鑑【{{$GameTitle->name}}】</p>
                    <h1 class="mt-24 bb-blue">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                            <p class="h1">{{ $Samurai->name }}（{{ $Samurai->furigana }}）</p>
                        </div>
                    </h1>
                    <div class="mt-12 d-f j-sb ai-c">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['shinsei', $BeforeSamurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        <livewire:search />
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['shinsei', $NextSamurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <ul class="d-f font_small title_box mt-12">
                        @if (isset($HadouSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('hadou.database.samurais.show', $HadouSamuraiParameter->samurai->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/hadou.png')}}" alt="新信長アイコン" width="84px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei-pk.database.show', [$Samurai->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei_pk.png')}}" alt="新生PKアイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        <li class="active title_li p-8">
                            <a href="" class="title_anchor active normal_block_anchor shadow_hover shadow d-b">
                                <img src="{{ asset('img/common/shinsei.png')}}" alt="新生アイコン" width="78px" height="40px" class="title_img">
                            </a>
                        </li>
                        @if (isset($ShinnobuSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinnobu.png')}}" alt="新信長アイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($TaishiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('database.samurais.show' , ['taishi-pk', $Samurai->id] ) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/taishi_pk.png')}}" alt="大志PKアイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="d-f mt-12 parameter_img_area">
                        <table class="font_small ta-c parameter_table">
                            <tr>
                                <th colspan="6">{{ $Samurai->name }} の能力値</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n" style="min-width: 75px;">統率 
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の防御力、城の防御力に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiSamuraiParameter->leadership))
                                        <span class="status_bar" style="width: {{ $ShinseiSamuraiParameter->leadership }}px;"></span>{{ $ShinseiSamuraiParameter->leadership }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $Samurai->id }}&sort=sinsei-leadership">{{ $ShinseiSamuraiParameter->leadership_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">武勇
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の攻撃力、強攻時に敵城に与えるダメージ、<br>城の攻撃力に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiSamuraiParameter->brave))
                                        <span class="status_bar" style="width: {{ $ShinseiSamuraiParameter->brave }}px;"></span>{{ $ShinseiSamuraiParameter->brave }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $Samurai->id }}&sort=sinsei-brave">{{ $ShinseiSamuraiParameter->brave_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">知略
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の包囲時のダメージ量、城の包囲時の防御力、<br>また戦法のダメージに影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiSamuraiParameter->wisdom))
                                        <span class="status_bar" style="width: {{ $ShinseiSamuraiParameter->wisdom }}px;"></span>{{ $ShinseiSamuraiParameter->wisdom }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $Samurai->id }}&sort=sinsei-wisdom">{{ $ShinseiSamuraiParameter->wisdom_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">政務
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">城の収入に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiSamuraiParameter->affairs))
                                        <span class="status_bar" style="width: {{ $ShinseiSamuraiParameter->affairs }}px;"></span>{{ $ShinseiSamuraiParameter->affairs }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $Samurai->id }}&sort=sinsei-affairs">{{ $ShinseiSamuraiParameter->affairs_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">合計</th>
                                <td colspan="4" class="ta-l of-h">
                                    @if (isset($ShinseiSamuraiParameter->all))
                                        <span class="status_bar" style="width: {{ $ShinseiSamuraiParameter->all / 4 }}px;"></span>{{ $ShinseiSamuraiParameter->all }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $Samurai->id }}&sort=sinsei-all">{{ $ShinseiSamuraiParameter->all_rank }}</a> 位)</span><span class="f-r font_min">2201人中</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="samurai_img_area d-f">
                            @if ($imgCount == 0)
                                <div class="d-f ai-c j-c w-100">
                                    <picture>
                                        <source type="image/webp" srcset="{{ My_func::return_min_samurai_img_webp_url($Samurai->id, $GameTitle->slug) }}">
                                        <img src="{{ My_func::return_min_samurai_img_url($Samurai->id, $GameTitle->slug) }}" alt="{{ $Samurai->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img">
                                    </picture>
                                </div>
                            @else
                                <div id="slider" class="d-f ai-c">
                                    @for ($i = 0; $i < ($imgCount + 1); $i++)
                                        @if ($i == 0)
                                            <div class="d-f ai-c j-c">
                                                <picture>
                                                    <source type="image/webp" srcset="{{ Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $Samurai->id . '.webp') }}">
                                                    <img src="{{ Storage::disk('s3')->url('img/shinsei/samurai/' . $Samurai->id . '.jpg') }}" alt="{{ $Samurai->name }}のグラフィック画像1" width="80%" height="100%" class="m-a samurai_img">
                                                </picture>
                                            </div>
                                        @else
                                            <div class="d-f ai-c j-c add_img">
                                                <picture>
                                                    <source type="image/webp" srcset="{{ Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $Samurai->id . '_' . $i . '.webp') }}">
                                                    <img src="{{ Storage::disk('s3')->url('img/shinsei/samurai/' . $Samurai->id . '_' . $i . '.jpg') }}" alt="{{ $Samurai->name }}のグラフィック画像{{ $i + 1 }}" width="80%" height="100%" class="m-a samurai_img">
                                                </picture>
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                            @endif
                        </div>
                    </div>
                    @include('components.samurai_basic_data', ['Samurai' => $Samurai])

                    <div class="mt-36 ta-c pc_display">
                        <div id="im-af643d07e0944e0b8a908c2accf2d86a">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1808041,type:"banner",display:"inline",elementid:"im-af643d07e0944e0b8a908c2accf2d86a"})</script>
                        </div>
                    </div>
                    <div class="mt-36 ta-c sp_display">
                        <div id="im-7181044169e0457d9a2a4bb3b1c8337f">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1808042,type:"banner",display:"inline",elementid:"im-7181044169e0457d9a2a4bb3b1c8337f"})</script>
                        </div>
                    </div>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th colspan="6" style="width: 50%;">その他のデータ</th>
                            <th colspan="6" style="width: 50%;">列伝</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">誕生年</th>
                            <td colspan="4">{{ $Samurai->birth_year }}年</td>
                            <td colspan="6" rowspan="3" class="ta-l">{!! $Samurai->retuden_html !!}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">死亡年</th>
                            <td colspan="4">{{ $Samurai->death_year }}年</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">主義
                                <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">革新、中道、保守の3種類ある。忠誠の増減に関係する。<br>また、政策の発令条件になっている。</p>
                                    </div>
                                </span>
                            </th>
                            <td colspan="4">{{ $ShinseiSamuraiParameter->shinseiDoctrine->name }}</td>
                        </tr>
                        @if (count($ShinseiSamuraiParameter->shinseiCharacteristicSearches))
                            <tr>
                                <th colspan="12">特性
                                    <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">武将の個性。政略、軍事など、様々な場面で効果を得られる。</p>
                                        </div>
                                    </span>
                                </th>
                            </tr>
                            @foreach ($ShinseiSamuraiParameter->shinseiCharacteristicSearches as $ShinseiCharacteristicSearch)
                                <tr>
                                    <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiCharacteristicSearch->shinseiCharacteristic->name }}</th>
                                    <td colspan="10" class="ta-l">{{ $ShinseiCharacteristicSearch->shinseiCharacteristic->description }}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if (isset($ShinseiSamuraiParameter->shinseiTactic))
                            <tr>
                                <th colspan="12">戦法
                                    <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">合戦で使える特殊な技。<br>部隊を強化したり、ダメージを与えたり、<br class="sp_display">様々な効果を得られる。</p>
                                        </div>
                                    </span>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiSamuraiParameter->shinseiTactic->name }}</th>
                                <td colspan="10" class="ta-l">{{ $ShinseiSamuraiParameter->shinseiTactic->description }}</td>
                            </tr>
                        @endif
                        @if (isset($ShinseiPolicy))
                            <tr>
                                <th colspan="12">固有政策</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPolicy->name }}</th>
                                <td colspan="10" class="ta-l">{!! nl2br(e($ShinseiPolicy->description)) !!}</td>
                            </tr>
                        @endif
                    </table>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th colspan="12">シナリオ</th>
                        </tr>
                        <tr>
                            <th colspan="4" class="back_cloud_blue color_font_black fw-n">年月</th>
                            <th colspan="4" class="back_cloud_blue color_font_black fw-n">シナリオ</th>
                            <th colspan="4" class="back_cloud_blue color_font_black fw-n">年齢</th>
                        </tr>
                        <tr>
                            <td colspan="4">1546年1月</td>
                            <td colspan="4">信長元服</td>
                            <td colspan="8">{{ My_func::return_entry_status($ShinseiSamuraiParameter->nobunaga_gennpuku) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">1553年4月</td>
                            <td colspan="4">尾張統一</td>
                            <td colspan="8">{{ My_func::return_entry_status($ShinseiSamuraiParameter->owari_touitsu) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">1560年4月</td>
                            <td colspan="4">桶狭間の戦い</td>
                            <td colspan="8">{{ My_func::return_entry_status($ShinseiSamuraiParameter->okehazama) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">1570年4月</td>
                            <td colspan="4">信長包囲網</td>
                            <td colspan="8">{{ My_func::return_entry_status($ShinseiSamuraiParameter->houimou) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">1582年5月</td>
                            <td colspan="4">夢幻の如く</td>
                            <td colspan="8">{{ My_func::return_entry_status($ShinseiSamuraiParameter->yumemaboroshi) }}</td>
                        </tr>

                    </table>
                    
                    <div class="mt-24 d-f j-sb">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['shinsei', $BeforeSamurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['shinsei', $NextSamurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    @if (count($WarSamurais))
                        <div class="mt-24 ta-c">
                            <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=nobunagakou06-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0BZTP474L&linkId=aae5964576c966183bab9689019e92fd"></iframe>
                        </div>
                        <section class="mt-36 war_samurai">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する合戦</h2>
                            <ul>
                                @foreach ($WarSamurais as $WarSamurai)
                                    <li class="mt-24">
                                        <h3 class="rich_h3"><ruby>{{ $WarSamurai->war->name }}<rt>{{ $WarSamurai->war->kana }}</rt></ruby>　[<a href="{{ route('other.war.single', $WarSamurai->war->disturbance->id) }}">{{ $WarSamurai->war->disturbance->name }}</a>]</h3>
                                        @include('components.war', ['war' => $WarSamurai->war])
                                        <p class="ta-r mt-12">
                                            <a href="{{ route('other.war.single', $WarSamurai->war->disturbance_id) }}">
                                                詳細を見る
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    @if (count($SamuraiRelatedArticles))
                        <div class="mt-24 ta-c">
                            <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=nobunagakou06-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0BZTP474L&linkId=aae5964576c966183bab9689019e92fd"></iframe>
                        </div>
                        <section class="mt-36">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24 narrow_card">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($SamuraiRelatedArticle->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($SamuraiRelatedArticle->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $SamuraiRelatedArticle->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>{{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}</time>
                                                    <p class="card_category">
                                                        @foreach ($SamuraiRelatedArticle->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif
                    <div class="mt-24">
                        <ins class="adsbygoogle"
                        style="display:block"
                        data-ad-format="fluid"
                        data-ad-layout-key="-do+95-1f-g0+ye"
                        data-ad-client="ca-pub-2958289145034418"
                        data-ad-slot="5115790106"></ins>
                        <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinsei.samurais.show', 'argument1' => $GameTitle, 'argument2' => $Samurai])
                </article>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection