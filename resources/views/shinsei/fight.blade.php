@extends('layouts.layout')

@section('title', '戦闘【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での戦闘一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での戦闘一覧です。" />
    <meta property="og:title" content="施設【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "戦闘【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h2>戦闘【{{$GameTitle->name}}】</h2>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            <p>こんにちは、管理人のtakaです。</p>
                            <p>世の中にはキラキラネームがありますが、それは戦国時代も同じです。</p>
                            <p>また、名字と組み合わせることによって威力を発揮するすごい名前の武将も中にはいます。</p>

                            <p>なので今回は、変な名前な戦国武将を独断と偏見でピックアップしランキングにしました。</p>
                            <p>早速見ていきましょう！！</p>

                            <section>
                                <h2>第三位 清野清秀</h2>
                                <div class="retuden">
                                    <p>
                                        村上家臣。主家に背いた家臣を討ったり真田幸隆と戦うなど、主家の勢力維持に尽力。主君・義清の信濃退去に従い、のちに義清の子・国清の教育係を務めた。
                                    </p>
                                </div>
                                <p><a href="{{ route('database.samurais.show' , [$GameTitle->slug, 773] ) }}">清野清秀</a>が第三位！</p>
                                <p>ノミネートの理由は、名字と名前に同じ文字「清」が入っているためです。</p>
                                <p>せっかくの名前なのに漢字が被るって...真剣に考えましたか？と言いたくなりますね。</p>
                                <p>※他に同じ文字を使っている武将は「<a href="{{ route('database.samurais.show' , [$GameTitle->slug, 798] ) }}">久能宗能</a>」や「<a href="{{ route('database.samurais.show' , [$GameTitle->slug, 1102] ) }}">秦泉寺泰惟</a>」がおり、結構多いのかもしれません。</p>
                            </section>
                            {{-- ここから下書きゾーン --}}
                            規模が大きかった戦ランキング！！

                            

                        </div>
                    </div>

                    <div class="mt-36">
                        <a href="{{ route('top') }}" title="トップページに戻る">トップページに戻る</a>
                    </div>
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection