@extends('layouts.layout')

@section('title', '403エラー | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="403エラー">
@endsection

@section('css')
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <h1>403エラー</h1>
                </article>
                <p>アクセスしようとしたページは表示できませんでした。</p>
                <p>すみません。</p>
                <div class="mt-36">
                    <a href="{{ route('top') }}" title="トップページに戻る">トップページに戻る</a>
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection