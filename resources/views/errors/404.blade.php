@extends('layouts.layout')

@section('title', '404エラー | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="404エラー">
@endsection

@section('css')
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <h1>404エラー</h1>
                </article>
                <p>指定されたファイルまたはディレクトリは存在しません。</p>
                <div class="mt-36">
                    <a href="{{ route('top') }}" title="トップページに戻る">トップページに戻る</a>
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection