@extends('layouts.layout')

@section('title', '能力値ランキング【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」の武将の能力値ランキングです。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」の武将の能力値ランキングです。" />
    <meta property="og:title" content="能力値ランキング({{ $GameTitle->name }}) | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/sort.js') }}"></script>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "能力値ランキング{{ $GameTitle->name }}） | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/ranking.png')}}" alt="能力値ランキングアイコン" width="30px" height="30px" class="mr-8">
                        <p>能力値ランキング【{{$GameTitle->name}}】</p>
                    </h1>
                    <table class="font_small ta-c mt-12 w-100" id="sort-table">
                        @if ($GameTitle->slug == 'taishi-pk')
                            <thead>
                                <tr class="sort_tr">
                                    <th class="c-p sort_th" id="taishi-pk-kana">名前</th>
                                    <th class="c-p sort_th" id="taishi-pk-leadership">統率</th>
                                    <th class="c-p sort_th" id="taishi-pk-brave">武勇</th>
                                    <th class="c-p sort_th" id="taishi-pk-wisdom">知略</th>
                                    <th class="c-p sort_th" id="taishi-pk-domestic_affairs">内政</th>
                                    <th class="c-p sort_th" id="taishi-pk-foreign_affairs">外政</th>
                                    <th class="c-p sort_th" id="taishi-pk-all">合計</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($SamuraiParameters as $SamuraiParameter)
                                <tr class="samurai_id_{{ $SamuraiParameter->samurai->id }} samurai_tr">
                                    <td data-sort="{{ $SamuraiParameter->samurai->kana }}"><a href="{{ route('database.samurais.show', ['taishi-pk', $SamuraiParameter->id]) }}" title="{{ $SamuraiParameter->samurai->name }}">{{ $SamuraiParameter->samurai->name }}</a></td>
                                    <td class="@if($SamuraiParameter->leadership >= 85) back_orange @elseif($SamuraiParameter->leadership >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->leadership }}">{{ $SamuraiParameter->leadership }}</td>
                                    <td class="@if($SamuraiParameter->brave >= 85) back_orange @elseif($SamuraiParameter->brave >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->brave }}">{{ $SamuraiParameter->brave }}</td>
                                    <td class="@if($SamuraiParameter->wisdom >= 85) back_orange @elseif($SamuraiParameter->wisdom >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->wisdom }}">{{ $SamuraiParameter->wisdom }}</td>
                                    <td class="@if($SamuraiParameter->domestic_affairs >= 85) back_orange @elseif($SamuraiParameter->domestic_affairs >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->domestic_affairs }}">{{ $SamuraiParameter->domestic_affairs }}</td>
                                    <td class="@if($SamuraiParameter->foreign_affairs >= 85) back_orange @elseif($SamuraiParameter->foreign_affairs >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->foreign_affairs }}">{{ $SamuraiParameter->foreign_affairs }}</td>
                                    <td class="@if($SamuraiParameter->all >= 400) back_orange @elseif($SamuraiParameter->all >= 370) back_yellow @endif" data-sort="{{ $SamuraiParameter->all }}">{{ $SamuraiParameter->all }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        @elseif ($GameTitle->slug == 'shinsei')
                            <thead>
                                <tr class="sort_tr">
                                    <th class="c-p sort_th" id="sinsei-kana">名前</th>
                                    <th class="c-p sort_th" id="sinsei-leadership">統率</th>
                                    <th class="c-p sort_th" id="sinsei-brave">武勇</th>
                                    <th class="c-p sort_th" id="sinsei-wisdom">知略</th>
                                    <th class="c-p sort_th" id="sinsei-affairs">政務</th>
                                    <th class="c-p sort_th" id="sinsei-all">合計</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($SamuraiParameters as $SamuraiParameter)
                                <tr class="samurai_id_{{ $SamuraiParameter->samurai->id }} samurai_tr">
                                    <td data-sort="{{ $SamuraiParameter->samurai->kana }}"><a href="{{ route('database.samurais.show', ['shinsei', $SamuraiParameter->samurai->id]) }}" title="{{ $SamuraiParameter->samurai->name }}">{{ $SamuraiParameter->samurai->name }}</a></td>
                                    <td class="@if($SamuraiParameter->leadership >= 85) back_orange @elseif($SamuraiParameter->leadership >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->leadership }}">{{ $SamuraiParameter->leadership }}</td>
                                    <td class="@if($SamuraiParameter->brave >= 85) back_orange @elseif($SamuraiParameter->brave >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->brave }}">{{ $SamuraiParameter->brave }}</td>
                                    <td class="@if($SamuraiParameter->wisdom >= 85) back_orange @elseif($SamuraiParameter->wisdom >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->wisdom }}">{{ $SamuraiParameter->wisdom }}</td>
                                    <td class="@if($SamuraiParameter->affairs >= 85) back_orange @elseif($SamuraiParameter->affairs >= 70) back_yellow @endif" data-sort="{{ $SamuraiParameter->affairs }}">{{ $SamuraiParameter->affairs }}</td>
                                    <td class="@if($SamuraiParameter->all >= 330) back_orange @elseif($SamuraiParameter->all >= 290) back_yellow @endif" data-sort="{{ $SamuraiParameter->all }}">{{ $SamuraiParameter->all }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                    </table>
                </article>

                <div class="mt-36 pc_display">
                    <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                        <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                        <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                    </div>
                </div>
                <div class="mt-36 sp_display">
                    <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                        <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                        <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                    </div>
                </div>
                @include('components.breadcrumbs', ['slug' => 'database.ranking', 'argument1' => $GameTitle])
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection