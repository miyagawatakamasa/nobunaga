@extends('layouts.layout')

@section('title', '' . $Samurai->name . '【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="{{ $Samurai->retuden }}">
    <meta property="og:description" content="{{ $Samurai->retuden }}" />
    <meta property="og:title" content="「 {{ $Samurai->name }}」の武将パラメーター({{ $GameTitle->name }}) | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ My_func::return_min_samurai_img_url($Samurai->id, $GameTitle->slug) }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "{{ $Samurai->name }}",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/slick/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){    
            $('#slider').slick({
                arrows: false,
                centerMode: true,
            });
        });
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <p>武将名鑑【{{$GameTitle->name}}】</p>
                    <h1 class="mt-24 bb-blue d-f ai-c">
                        <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                        <p class="h1">{{ $Samurai->name }}（{{ $Samurai->furigana }}）</p>
                    </h1>
                    <div class="mt-12 d-f j-sb ai-c">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $BeforeSamurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        <livewire:search />
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $NextSamurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <ul class="d-f font_small title_box mt-12">
                        @if (isset($HadouSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('hadou.database.samurais.show', $HadouSamuraiParameter->samurai->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/hadou.png')}}" alt="新信長アイコン" width="84px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei-pk.database.show', [$Samurai->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei_pk.png')}}" alt="新生PKアイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei.database.samurais.show', $ShinseiSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei.png')}}" alt="新生アイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinnobuSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinnobu.png')}}" alt="新信長アイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        <li class="active title_li p-8">
                            <a href="" class="title_anchor normal_block_anchor active shadow_hover shadow d-b">
                                <img src="{{ asset('img/common/taishi_pk.png')}}" alt="大志PKアイコン" width="40px" height="40px" class="title_img">
                            </a>
                        </li>
                    </ul>
                    <div class="d-f mt-12 parameter_img_area">
                        <table class="font_small ta-c parameter_table">
                            <tr>
                                <th colspan="6">{{ $Samurai->name }} の能力値</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n" style="min-width: 75px;">統率
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">高いほど、決戦での移動速度が上がったり、籠城戦で城を奪われにくくなったりする。<br>
                                                また高いほど、評定で意見を出したとき、軍事の施策力を獲得しやすい。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($SamuraiParameter->leadership))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->leadership }}px;"></span>{{ $SamuraiParameter->leadership }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-leadership">{{ $SamuraiParameter->leadership_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">武勇
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">高いほど、決戦で敵部隊に与えるダメージが増える。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($SamuraiParameter->brave))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->brave }}px;"></span>{{ $SamuraiParameter->brave }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-brave">{{ $SamuraiParameter->brave_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">知略
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">高いほど、決戦で敵部隊から受けるダメージが減る。<br>
                                                また高いほど、評定で意見を出したとき、論議の施策力を獲得しやすい。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($SamuraiParameter->wisdom))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->wisdom }}px;"></span>{{ $SamuraiParameter->wisdom }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-wisdom">{{ $SamuraiParameter->wisdom_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">内政
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">高いほど、評定で意見を出したとき、<br class="sp_display">農業・商業の施策力を獲得しやすい。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($SamuraiParameter->domestic_affairs))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->domestic_affairs }}px;"></span>{{ $SamuraiParameter->domestic_affairs }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-domestic_affairs">{{ $SamuraiParameter->domestic_affairs_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">外政
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">「外交」で奏者に任命した際、高いと心証が上がりやすくなる。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($SamuraiParameter->foreign_affairs))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->foreign_affairs }}px;"></span>{{ $SamuraiParameter->foreign_affairs }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-foreign_affairs">{{ $SamuraiParameter->foreign_affairs_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">合計</th>
                                <td colspan="4" class="ta-l of-h">
                                    @if (isset($SamuraiParameter->all))
                                        <span class="status_bar" style="width: {{ $SamuraiParameter->all / 5 }}px;"></span>{{ $SamuraiParameter->all }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', $GameTitle->slug) }}?samurai-id={{ $Samurai->id }}&sort={{ $GameTitle->slug }}-all">{{ $SamuraiParameter->all_rank }}</a> 位)</span><span class="f-r font_min">2201人中</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="samurai_img_area d-f">
                            @if ($imgCount == 0)
                                <div class="d-f ai-c j-c w-100">
                                    <img src="{{ My_func::return_min_samurai_img_url($Samurai->id, $GameTitle->slug) }}" alt="{{ $Samurai->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img">
                                </div>
                            @else
                                <div id="slider" class="d-f ai-c">
                                    @for ($i = 0; $i < ($imgCount + 1); $i++)
                                        @if ($i == 0)
                                            <div class="d-f ai-c j-c">
                                                <img src="{{ Storage::disk('s3')->url('img/samurai/min/' . $Samurai->id . '.jpg') }}" alt="{{ $Samurai->name }}のグラフィック画像1" width="80%" height="100%" class="m-a samurai_img">
                                            </div>
                                        @else
                                            <div class="d-f ai-c j-c add_img">
                                                <img src="{{ Storage::disk('s3')->url('img/samurai/min/' . $Samurai->id . '_' . $i . '.jpg') }}" alt="{{ $Samurai->name }}のグラフィック画像{{ $i + 1 }}" width="80%" height="100%" class="m-a samurai_img">
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                            @endif
                        </div>
                    </div>
                    @include('components.samurai_basic_data', ['Samurai' => $Samurai])
                    <div class="mt-36 ta-c pc_display">
                        <div id="im-af643d07e0944e0b8a908c2accf2d86a">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1808041,type:"banner",display:"inline",elementid:"im-af643d07e0944e0b8a908c2accf2d86a"})</script>
                        </div>
                    </div>
                    <div class="mt-36 ta-c sp_display">
                        <div id="im-7181044169e0457d9a2a4bb3b1c8337f">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1808042,type:"banner",display:"inline",elementid:"im-7181044169e0457d9a2a4bb3b1c8337f"})</script>
                        </div>
                    </div>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th colspan="6" style="width: 50%;">その他のデータ</th>
                            <th colspan="6" style="width: 50%;">列伝</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">誕生年</th>
                            <td colspan="4">{{ $Samurai->birth_year }}年</td>
                            <td colspan="6" rowspan="4" class="ta-l">{!! $Samurai->retuden_html !!}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">死亡年</th>
                            <td colspan="4">{{ $Samurai->death_year }}年</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">気質</th>
                            <td colspan="4">
                                @isset($SamuraiParameter->taishi_pk_temperament)
                                    {{ $SamuraiParameter->taishi_pk_temperament }}
                                @endisset
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">野心</th>
                            <td colspan="4">{{ $SamuraiParameter->taishi_pk_ambition ?? '0' }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">志</th>
                        </tr>
                        <tr>
                            <th colspan="6" class="back_cloud_blue color_font_black fw-n">{{ $SamuraiParameter->taishiPkWill['name'] }}</th>
                            <td colspan="6">{{ $SamuraiParameter->taishiPkWill['kana'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">戦法
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">決戦で武将から献策があると実行できる。<br>青く表示されている戦法は自動で発動する。</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">戦法名</th>
                            <td colspan="4">{{ $TaishiPkTactics[$SamuraiParameter->taishi_pk_tactic_id - 1]['name'] }}</td>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">消費ゲージ</th>
                            <td colspan="4">{{ $TaishiPkTactics[$SamuraiParameter->taishi_pk_tactic_id - 1]['cost'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">効果</th>
                            <td colspan="10">{{ $TaishiPkTactics[$SamuraiParameter->taishi_pk_tactic_id - 1]['effect'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">作戦
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">決戦開始時に決める。<br>作戦によって様々な効果がある。<br>青く表示されている戦法は自動で発動する。</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">作戦名</th>
                            <td colspan="10">{{ $SamuraiParameter->taishiPkStrategy['name'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">読み</th>
                            <td colspan="10">{{ $SamuraiParameter->taishiPkStrategy['kana'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">条件</th>
                            <td colspan="10">{{ $SamuraiParameter->taishiPkStrategy['terrain'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">消費軍議</th>
                            <td colspan="4">{{ $SamuraiParameter->taishiPkStrategy['cost'] }}</td>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">必要部隊</th>
                            <td colspan="4">{{ $SamuraiParameter->taishiPkStrategy['required_units'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">効果</th>
                            <td colspan="10">{{ $SamuraiParameter->taishiPkStrategy['description'] }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">補佐</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $TaishiPkTactics[$SamuraiParameter->taishi_pk_assistant - 1]['name'] }}</th>
                            <td colspan="10">{{ $TaishiPkTactics[$SamuraiParameter->taishi_pk_assistant - 1]['effect'] }}</td>
                        </tr>
                        @if(count($TaishiPkHeirloomSamurais))
                            <tr>
                                <th colspan="12">史実保有家宝</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">家宝名</th>
                                <td colspan="10">
                                    @foreach ($TaishiPkHeirloomSamurais as $TaishiPkHeirloomSamurai)
                                    <span class="mr-8">{{ $TaishiPkHeirloomSamurai->taishiPkHeirloom->name }}</span>
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                    </table>
                    <div class="mt-24 d-f j-sb">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $BeforeSamurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $NextSamurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    @if (count($WarSamurais))
                        <section class="mt-36 war_samurai">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する合戦</h2>
                            <ul>
                                @foreach ($WarSamurais as $WarSamurai)
                                    <li class="mt-24">
                                        <h3 class="rich_h3"><ruby>{{ $WarSamurai->war->name }}<rt>{{ $WarSamurai->war->kana }}</rt></ruby>　[<a href="{{ route('other.war.single', $WarSamurai->war->disturbance->id) }}">{{ $WarSamurai->war->disturbance->name }}</a>]</h3>
                                        @include('components.war', ['war' => $WarSamurai->war])
                                        <p class="ta-r mt-12">
                                            <a href="{{ route('other.war.single', $WarSamurai->war->disturbance_id) }}">
                                                詳細を見る
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    @if (count($SamuraiRelatedArticles))
                        <section class="mt-36">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24 narrow_card">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($SamuraiRelatedArticle->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($SamuraiRelatedArticle->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $SamuraiRelatedArticle->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>{{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}</time>
                                                    <p class="card_category">
                                                        @foreach ($SamuraiRelatedArticle->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    <div class="mt-36 pc_display">
                        <div id="im-1809c2f3ba91483ea2a6348619f5e43c">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783074,type:"banner",display:"inline",elementid:"im-1809c2f3ba91483ea2a6348619f5e43c"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-1691617e8d7e45d28aa923529ab34dfa">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783182,type:"banner",display:"inline",elementid:"im-1691617e8d7e45d28aa923529ab34dfa"})</script>
                        </div>
                    </div>

                    <section class="mt-48">
                        <h2>
                            <div class="d-f ai-c">
                                <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                                <p>武将名鑑【{{$GameTitle->name}}】</p>
                            </div>
                        </h2>
                        <p>武将列伝を一覧で見る場合は<a href="{{ route('database.samurais', 'taishi-pk') }}">こちら</a></p>
                        <ul class="samurai_ul mt-24 simple">
                            @foreach ($Samurais as $Samurai)
                            <li class="samurai_li mt-12 active"><a href="{{ route('database.samurais.show', [$GameTitle->slug, $Samurai->id]) }}" title="{{ $Samurai->name }}">{{ $Samurai->name }}</a></li>
                            @endforeach
                        </ul>
                    </section>
                </article>
                @include('components.recommend_articles')
                @include('components.breadcrumbs', ['slug' => 'database.samurais.show', 'argument1' => $GameTitle, 'argument2' => $SamuraiParameter->samurai])
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection