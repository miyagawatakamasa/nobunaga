@extends('layouts.layout')

@section('title', '戦法【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での戦法一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での戦法一覧です。" />
    <meta property="og:title" content="戦法【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "戦法【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦法【{{$GameTitle->name}}】</p>
                    </h1>
                    <table class="font_small ta-c mt-24 w-100">
                        @if ($GameTitle->slug == 'taishi')
                            <tr>
                                <th>No</th>
                                <th style="width: 70px;">戦法名</th>
                                <th>発動条件</th>
                                <th>効果</th>
                            </tr>
                            @foreach ($TaishiTactics as $TaishiTactic)
                                <tr>
                                    <td class="back_cloud_blue">{{ $TaishiTactic->id }}</td>
                                    <td class="ta-l">{{ $TaishiTactic->name }}</td>
                                    <td class="ta-l">
                                        @if ($TaishiTactic->terms)
                                            {{ $TaishiTactic->terms }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="ta-l">
                                        @if ($TaishiTactic->effect)
                                            {{ $TaishiTactic->effect }}
                                        @else
                                            確認中
                                        @endif
                                    </td>
                                </tr>                
                            @endforeach
                            <tr>
                                <th>No</th>
                                <th>戦法名</th>
                                <th>発動条件</th>
                                <th>効果</th>
                            </tr>
                        @elseif ($GameTitle->slug == 'taishi-pk')
                            <tr>
                                <th style="width: 80px;">戦法</th>
                                <th>ゲージ</th>
                                <th style="width: 60px;">種類</th>
                                <th>説明</th>
                            </tr>
                            @foreach ($TaishiPkTactics as $TaishiPkTactic)
                                <tr>
                                    <td class="back_cloud_blue">{{ $TaishiPkTactic->name }}</td>
                                    <td>
                                        @if ($TaishiPkTactic->cost)
                                            {{ $TaishiPkTactic->cost }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $TaishiPkTactic->kind }}</td>
                                    <td class="ta-l">{{ $TaishiPkTactic->effect }}</td>
                                </tr>                
                            @endforeach
                            <tr>
                                <th>戦法</th>
                                <th>ゲージ</th>
                                <th>種類</th>
                                <th>説明</th>
                            </tr>
                        @endif
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'tactics.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection