
<aside class="side_bar">
    <livewire:search />
    {{-- 広告 --}}
    <div class="pc_display">
        {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
        crossorigin="anonymous"></script> --}}
    </div>
    {{-- サイドバー上 --}}
    <div class="pc_display mt-24">
        <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="ca-pub-2958289145034418"
        data-ad-slot="7229486667"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="shadow side_bar_contents back_white sp_display mt-24 section">
        <h2 class="back_green d-f ai-c">
            <img src="{{ asset('img/common/db.png')}}" alt="データベースアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">データベース【新生PK】</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('shinsei-pk.database.samurais') }}" class="side_bar_section_link hover_blue d-f ai-c" title="武将名鑑">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将名鑑</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow side_bar_contents back_white sp_display mt-24 section">
        <h2 class="back_blue d-f ai-c">
            <img src="{{ asset('img/common/db.png')}}" alt="データベースアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">データベース【覇道】</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('database.samurais', 'hadou') }}" class="side_bar_section_link hover_blue d-f ai-c" title="武将名鑑">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将名鑑</p>
                </a>
            </li>
        </ul>
        <div class="side_bar_title">
            <p class="font_small">レアリティ別</p>
        </div>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('hadou.database.samurais.show.rarity', 'ssr') }}" class="side_bar_section_link hover_blue d-f ai-c" title="SSRキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/ssr.png') }}" alt="SSR" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">SSRキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('hadou.database.samurais.show.rarity', 'sr') }}" class="side_bar_section_link hover_blue d-f ai-c" title="SRキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/sr.png') }}" alt="SR" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">SRキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('hadou.database.samurais.show.rarity', 'r') }}" class="side_bar_section_link hover_blue d-f ai-c" title="Rキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/r.png') }}" alt="R" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">Rキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('hadou.database.samurais.show.rarity', 'n') }}" class="side_bar_section_link hover_blue d-f ai-c" title="Rキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/n.png') }}" alt="R" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">Nキャラ一覧</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow side_bar_contents back_white sp_display mt-24 section">
        <h2 class="back_red d-f ai-c">
            <img src="{{ asset('img/common/kouryaku.png')}}" alt="攻略情報アイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">攻略【新信長の野望】</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.strategy.beginner') }}" class="side_bar_section_link hover_blue d-f ai-c" title="序盤の進め方">
                    <img src="{{ asset('img/common/beginner.png')}}" alt="初心者アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">序盤の進め方</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.ranking') }}" class="side_bar_section_link hover_blue d-f ai-c" title="武将最強ランキング【新信長の野望】">
                    <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将最強ランキング</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.strategy.resemara') }}" class="side_bar_section_link hover_blue d-f ai-c" title="リセマラランキング【新信長の野望】">
                    <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">リセマラランキング</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.strategy.mukakin') }}" class="side_bar_section_link hover_blue d-f ai-c" title="無課金キャラランキング【新信長の野望】">
                    <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">無課金キャラランキング</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.gacha.kokushimusou') }}" class="side_bar_section_link hover_blue d-f ai-c" title="国士無双ガチャおすすめ【新信長の野望】">
                    <img src="{{ asset('img/common/gacha.png')}}" alt="ガチャアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">国士無双ガチャおすすめ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.strategy.heirloom') }}" class="side_bar_section_link hover_blue d-f ai-c" title="家宝システム【新信長の野望】">
                    <img src="{{ asset('img/common/heirloom.png')}}" alt="家宝アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">家宝システム</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais') }}" class="side_bar_section_link hover_blue d-f ai-c" title="序盤の進め方">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="初心者アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将名鑑</p>
                </a>
            </li>
        </ul>
        <div class="side_bar_title">
            <p class="font_small">適正別</p>
        </div>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'captain') }}" class="side_bar_section_link hover_blue d-f ai-c" title="主将キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/captain.png') }}" alt="主将" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">主将キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'staff') }}" class="side_bar_section_link hover_blue d-f ai-c" title="参謀キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/staff.png') }}" alt="参謀" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">参謀キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'pioneer') }}" class="side_bar_section_link hover_blue d-f ai-c" title="先鋒キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/pioneer.png') }}" alt="先鋒" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">先鋒キャラ一覧</p>
                </a>
            </li>
        </ul>
        <div class="side_bar_title">
            <p class="font_small">兵科別</p>
        </div>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'spear-soldier') }}" class="side_bar_section_link hover_blue d-f ai-c" title="槍兵キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/spear-soldier.png') }}" alt="槍兵" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">槍兵キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'cavalry') }}" class="side_bar_section_link hover_blue d-f ai-c" title="騎兵キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/cavalry.png') }}" alt="騎兵" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">騎兵キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'ninja') }}" class="side_bar_section_link hover_blue d-f ai-c" title="忍者キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/ninja.png') }}" alt="忍者" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">忍者キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'gun') }}" class="side_bar_section_link hover_blue d-f ai-c" title="鉄砲キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/gun.png') }}" alt="鉄砲" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">鉄砲キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'odzu') }}" class="side_bar_section_link hover_blue d-f ai-c" title="大筒キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/odzu.png') }}" alt="大筒" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">大筒キャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'farmer') }}" class="side_bar_section_link hover_blue d-f ai-c" title="農兵キャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/farmer.png') }}" alt="農兵" width="30px" height="100%" class="mr-8">
                    <p class="anchor_text">農兵キャラ一覧</p>
                </a>
            </li>
        </ul>
        <div class="side_bar_title">
            <p class="font_small">レアリティ別</p>
        </div>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'ssr') }}" class="side_bar_section_link hover_blue d-f ai-c" title="SSRキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/ssr.png') }}" alt="SSR" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">SSRキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'sr') }}" class="side_bar_section_link hover_blue d-f ai-c" title="SRキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/sr.png') }}" alt="SR" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">SRキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'r') }}" class="side_bar_section_link hover_blue d-f ai-c" title="Rキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/r.png') }}" alt="R" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">Rキャラ一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'n') }}" class="side_bar_section_link hover_blue d-f ai-c" title="Nキャラ一覧">
                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/n.png') }}" alt="N" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">Nキャラ一覧</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow mt-24 side_bar_contents sp_display section">
        <h2 class="back_blue d-f ai-c">
            <img src="{{ asset('img/common/db.png')}}" alt="データベースアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">データベース【大志PK】</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('database.samurais', 'taishi-pk') }}" class="side_bar_section_link hover_blue d-f ai-c" title="武将名鑑">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将名鑑</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('scenario.index', 'taishi-pk') }}" class="side_bar_section_link hover_blue d-f ai-c" title="シナリオ">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">シナリオ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('database.will', $GameTitle->slug) }}" class="side_bar_section_link hover_blue d-f ai-c" title="志と特性">
                    <img src="{{ asset('img/common/will.png')}}" alt="志と特性アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">志と特性</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('taimei.index') }}" class="side_bar_section_link hover_blue d-f ai-c" title="大命">
                    <img src="{{ asset('img/common/taimei.png')}}" alt="大命アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">大命</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('taishi-pk.strategy.index') }}" class="side_bar_section_link hover_blue d-f ai-c" title="作戦">
                    <img src="{{ asset('img/common/strategy.png')}}" alt="作戦アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">作戦</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('tactics.index', 'taishi-pk') }}" class="side_bar_section_link hover_blue d-f ai-c" title="戦法">
                    <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦法</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('resource.index') }}" class="side_bar_section_link hover_blue d-f ai-c" title="資源">
                    <img src="{{ asset('img/common/resource.png')}}" alt="資源アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">資源</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('tensyu.index') }}" class="side_bar_section_link hover_blue d-f ai-c" title="天守">
                    <img src="{{ asset('img/common/tensyu.png')}}" alt="天守アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">天守</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('facility.index', $GameTitle->slug) }}" class="side_bar_section_link hover_blue d-f ai-c" title="設備">
                    <img src="{{ asset('img/common/facility.png')}}" alt="設備アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">設備</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('building.index', $GameTitle->slug) }}" class="side_bar_section_link hover_blue d-f ai-c" title="施設">
                    <img src="{{ asset('img/common/building.png')}}" alt="施設アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">施設</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow mt-24 side_bar_contents sp_display section">
        <h2 class="back_red d-f ai-c">
            <img src="{{ asset('img/common/kouryaku.png')}}" alt="攻略情報アイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">攻略情報<br>【大志PK】</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('database.ranking', 'taishi-pk') }}" class="side_bar_section_link hover_red d-f ai-c" title="能力値ランキング">
                    <img src="{{ asset('img/common/ranking.png')}}" alt="能力値ランキング" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">能力値ランキング</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('scenario.recommend', 'taishi-pk') }}" class="side_bar_section_link hover_red d-f ai-c" title="能力値ランキング">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="おすすめシナリオ" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">おすすめシナリオ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.military') }}" class="side_bar_section_link hover_red d-f ai-c" title="軍事のコツ">
                    <img src="{{ asset('img/common/gunshi.png')}}" alt="軍事アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">軍事のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.march') }}" class="side_bar_section_link hover_red d-f ai-c" title="行軍のコツ">
                    <img src="{{ asset('img/common/strategy.png')}}" alt="行軍アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">行軍のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.decisive_battle') }}" class="side_bar_section_link hover_red d-f ai-c" title="決戦のコツ">
                    <img src="{{ asset('img/common/strategy.png')}}" alt="決戦アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">決戦のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.siege') }}" class="side_bar_section_link hover_red d-f ai-c" title="攻城戦のコツ">
                    <img src="{{ asset('img/common/tensyu.png')}}" alt="攻城戦アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">攻城戦のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.plot') }}" class="side_bar_section_link hover_red d-f ai-c" title="調略のコツ">
                    <img src="{{ asset('img/common/makimono.png')}}" alt="調略アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">調略のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.domestic_affairs') }}" class="side_bar_section_link hover_red d-f ai-c" title="内政のコツ">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">内政のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.commercial') }}" class="side_bar_section_link hover_red d-f ai-c" title="商業のコツ">
                    <img src="{{ asset('img/common/commercial.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">商業のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.agriculture') }}" class="side_bar_section_link hover_red d-f ai-c" title="農業のコツ">
                    <img src="{{ asset('img/common/agriculture.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">農業のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.diplomacy') }}" class="side_bar_section_link hover_red d-f ai-c" title="外政のコツ">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">外交のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.general_contract') }}" class="side_bar_section_link hover_red d-f ai-c" title="普請のコツ">
                    <img src="{{ asset('img/common/tensyu.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">普請のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.human_resources') }}" class="side_bar_section_link hover_red d-f ai-c" title="人事のコツ">
                    <img src="{{ asset('img/common/people.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">人事のコツ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('capture.will') }}" class="side_bar_section_link hover_red d-f ai-c" title="志について">
                    <img src="{{ asset('img/common/will.png')}}" alt="" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">志について</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow mt-24 side_bar_contents sp_display section">
        <h2 class="back_orange d-f ai-c">
            <img src="{{ asset('img/common/new.png')}}" alt="new" width="30px" height="30px" class="mr-8">
            <p class="color_white">新生</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('database.samurais', 'shinsei') }}" class="side_bar_section_link hover_orange d-f ai-c" title="武将名鑑">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将名鑑</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('database.ranking', 'shinsei') }}" class="side_bar_section_link hover_red d-f ai-c" title="能力値ランキング">
                    <img src="{{ asset('img/common/ranking.png')}}" alt="能力値ランキング" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">能力値ランキング</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinsei.info') }}" class="side_bar_section_link hover_orange d-f ai-c" title="新生 最新情報">
                    <img src="{{ asset('img/common/new.png')}}" alt="内政アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">新生 最新情報</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('scenario.index', 'shinsei') }}" class="side_bar_section_link hover_orange d-f ai-c" title="シナリオ">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">シナリオ</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('shinsei.policy') }}" class="side_bar_section_link hover_orange d-f ai-c" title="政策">
                    <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">政策</p>
                </a>
            </li>
            {{-- <li class="side_bar_section_li">
                <a href="https://www.gamecity.ne.jp/shinsei/fight.html" target="_blank" class="side_bar_section_link hover_orange d-f ai-c" title="戦闘">
                    <img src="{{ asset('img/common/fight.png')}}" alt="戦闘アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦闘</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="https://www.gamecity.ne.jp/shinsei/strategy.html" target="_blank" class="side_bar_section_link hover_orange d-f ai-c" title="戦略">
                    <img src="{{ asset('img/common/gunshi.png')}}" alt="戦略アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦略</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="https://www.gamecity.ne.jp/shinsei/battle.html" target="_blank" class="side_bar_section_link hover_orange d-f ai-c" title="合戦">
                    <img src="{{ asset('img/common/strategy.png')}}" alt="合戦アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">合戦</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="https://www.gamecity.ne.jp/shinsei/domestic.html" target="_blank" class="side_bar_section_link hover_orange d-f ai-c" title="内政">
                    <img src="{{ asset('img/common/domestic.png')}}" alt="内政アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">内政</p>
                </a>
            </li> --}}
        </ul>
    </div> 
    <div class="shadow mt-24 side_bar_contents back_white sp_display section">
        <h2 class="bb-blue">{{ $RandomSamurai->name }}</h2>
        <div class="p-12">
            <div class="ta-c">
                <img src="{{ My_func::return_min_samurai_img_url($RandomSamurai->id, $GameTitle->slug) }}" alt="{{ $RandomSamurai->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img" style="margin: auto;">
            </div>
            <p class="font_small mt-12">{!! $RandomSamurai->retuden_html !!}</p>
            <p class="mt-12"><a href="/database/samurai/{{ $RandomSamurai->id }}">詳細を見る</a></p>
        </div>
    </div>
    <div class="shadow mt-24 side_bar_contents font_small section">
        <h2 class="back_orange">プロフィール</h2>
        <div class="p-5 profile_img_area">
            <img src="{{ asset('img/common/taka.png')}}" class="profile_img shadow" alt="信長の野望 徹底攻略の管理人 taka" width="100%" height="100%">
        </div>
        <p class="p-5">歴史大好きで、情報発信しています！<br>現在フリーランスとしてwebサイトの開発を行っています。</p>
        <ul class="mt-12 side_bar_section_ul">
            <li>住まい：福岡県</li>
            <li>好きな人物：黒田官兵衛</li>
        </ul>
        <p class="p-5"><a href="{{ route('administrator') }}" title="管理人情報">プロフィールはこちら</a></p>
    </div>
    <div class="shadow mt-24 side_bar_contents diagnose_test">
        <a href="{{ route('other.diagnose.index') }}">
            <img src="{{ asset('img/diagnose/banner.png')}}" alt="武将診断テスト" width="100%" height="100%" class="d-b">
        </a>
    </div>
    <div class="shadow mt-24 side_bar_contents">
        {{-- 人気記事 --}}
        <div class="section">
            <h2 class="back_green d-f ai-c">
                <img src="{{ asset('img/common/ranking.png')}}" alt="人気記事アイコン" width="30px" height="30px" class="mr-8">
                <p class="color_white">人気記事</p>
            </h2>
            <ul class="side_bar_section_ul">
                @foreach ($RankingArticles as $RankingArticle)
                    <li class="side_bar_section_li">
                        <a href="{{ route('article.single', $RankingArticle->id) }}" class="side_bar_section_link hover_green d-b" title="{{ $RankingArticle->title }}">
                            {{ $loop->index + 1 }}. {{ $RankingArticle->title }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="shadow mt-24 side_bar_contents section">
        <h2 class="back_green mt-12 d-f ai-c">
            <img src="{{ asset('img/common/scenario.png')}}" alt="新着記事アイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">新着記事</p>
        </h2>
        <ul class="side_bar_section_ul">
            @foreach ($NewArticles as $NewArticle)
                <li class="side_bar_section_li">
                    <a href="{{ route('article.single', $NewArticle->id) }}" class="side_bar_section_link hover_green d-b" title="{{ $NewArticle->title }}">
                        {{ $NewArticle->title }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="mt-12 next_area">
            <a href="{{ route('article.index') }}" title="一覧を見る">一覧を見る</a>
        </div>
    </div>
    <div class="shadow mt-24 side_bar_contents section">
        <h2 class="back_ocean_blue d-f ai-c">
            <img src="{{ asset('img/common/scenario.png')}}" alt="カテゴリーアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">カテゴリー一覧</p>
        </h2>
        <ul class="side_bar_section_ul">
            @foreach ($ArticleCategories as $ArticleCategory)
                @if ($ArticleCategory->id == 31)
                    <li class="side_bar_section_li">
                        <p class="side_bar_section_link hover_ocean_blue prefectures d-b color_anchor_blue" title="{{ $ArticleCategory->name }}">
                            {{ $ArticleCategory->name }}  <span class="category_arrow anchor_text"></span>
                        </p>
                    </li>
                @elseif (count($ArticleCategory->articleCategorySearches))
                    <li class="side_bar_section_li @if($ArticleCategory->parent_category_id) child @endif @if($ArticleCategory->parent_category_id == 31) prefecture @endif">
                        <a href="{{ route('article.category.index', $ArticleCategory->slug) }}" class="side_bar_section_link hover_ocean_blue d-b" title="{{ $ArticleCategory->name }}">
                            {{ $ArticleCategory->name }} （{{ count($ArticleCategory->articleCategorySearches) }}）
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    {{-- <div class="shadow mt-24 side_bar_contents section">
        <h2 class="back_ocean_blue d-f ai-c">
            <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">投票キャンペーン</p>
        </h2>
        <ul class="side_bar_section_ul">
            @foreach ($VoteCampaigns as $VoteCampaign)
                <li class="side_bar_section_li">
                    <a href="{{ route('vote_campaign.show', $VoteCampaign->id ) }}" class="side_bar_section_link hover_ocean_blue d-b" title="{{ $VoteCampaign->title }}">
                        {{ $VoteCampaign->title }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="mt-12 next_area">
            <a href="{{ route('vote_campaign.index') }}" title="一覧を見る">一覧を見る</a>
        </div>
    </div> --}}
    <div class="shadow mt-24 side_bar_contents section">
        <h2 class="back_red d-f ai-c">
            <img src="{{ asset('img/common/contents.png')}}" alt="コンテンツアイコン" width="30px" height="30px" class="mr-8">
            <p class="color_white">コンテンツ</p>
        </h2>
        <ul class="side_bar_section_ul">
            <li class="side_bar_section_li">
                <a href="{{ route('other.timeline') }}" class="side_bar_section_link hover_red d-f ai-c" title="戦国年表">
                    <img src="{{ asset('img/common/timeline.png')}}" alt="戦国年表アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦国年表</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('other.game_title') }}" class="side_bar_section_link hover_red d-f ai-c" title="ゲームタイトル一覧">
                    <img src="{{ asset('img/common/game.png')}}" alt="ゲームタイトル一覧アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">ゲームタイトル一覧</p>
                </a>
            </li>
            {{-- <li class="side_bar_section_li">
                <a href="{{ route('other.tactics') }}" class="side_bar_section_link hover_red d-f ai-c">
                    <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法一覧アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦法一覧</p>
                </a>
            </li> --}}
            <li class="side_bar_section_li">
                <a href="{{ route('other.war') }}" class="side_bar_section_link hover_red d-f ai-c" title="合戦一覧">
                    <img src="{{ asset('img/common/strategy.png')}}" alt="合戦一覧アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">合戦一覧</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('article.category.index', 'minor') }}" class="side_bar_section_link hover_red d-f ai-c" title="マイナー武将列伝">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="マイナー武将列伝アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">マイナー武将列伝</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('other.vassals') }}" class="side_bar_section_link hover_red d-f ai-c" title="家臣団総称">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="家臣団アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">家臣団総称</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('other.diagnose.index') }}" class="side_bar_section_link hover_red d-f ai-c" title="武将診断テスト">
                    <img src="{{ asset('img/common/diagnose.png')}}" alt="診断アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">武将診断テスト</p>
                </a>
            </li>
            <li class="side_bar_section_li">
                <a href="{{ route('other.word') }}" class="side_bar_section_link hover_red d-f ai-c" title="戦国時代用語集">
                    <img src="{{ asset('img/common/diagnose.png')}}" alt="用語集アイコン" width="30px" height="30px" class="mr-8">
                    <p class="anchor_text">戦国時代用語集</p>
                </a>
            </li>
        </ul>
        {{-- 戦国時代の戦・合戦一覧 --}}

        {{-- カテゴリー一覧 --}}
        {{-- アーカイブ --}}
        {{-- 広告 --}}
        {{-- <div class="mt-24 ta-c">
            <iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=tfile08-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B09WW1B5G8&linkId=f9ed3cfb5b876253bf4b353320e9ea69"></iframe>
        </div> --}}
    </div>
    <div class="bottom_ad">
        <div class="mt-24">
            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
            crossorigin="anonymous"></script> --}}
            <!-- サイドバー下 -->
            <ins class="adsbygoogle"
                style="display:block"
                data-ad-client="ca-pub-2958289145034418"
                data-ad-slot="4028608256"
                data-ad-format="auto"
                data-full-width-responsive="true"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</aside>
