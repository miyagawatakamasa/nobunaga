<p class="mt-8">{{ $war->start_year }}年@if ($war->start_month){{ $war->start_month }}月@endif
@if ($war->start_day){{ $war->start_day }}日@endif
@if ($war->end_year || $war->end_month || $war->end_day)
    ~ 
@endif
@if ($war->end_year){{ $war->end_year . '年' }}@endif
@if ($war->end_month){{ $war->end_month . '月' }}@endif
@if ($war->end_day){{ $war->end_day . '日' }}@endif
{{-- 一旦これで --}}
</p>
<p class="mt-0">{{ $war->prefectures }}{{ $war->city }}{{ $war->town }}</p>
<div class="d-f ai-c mt-12 ta-c font_small join_box">
    <div>
        <div class="d-f ai-c join_samurai_box">
            @foreach ($war->warSamurais as $WarSamurai)
                @if ($WarSamurai->position == 'l')
                    <div class="join_samurai">
                        @if (isset($WarSamurai->samurai))
                            <a href="{{ route('database.samurais.redirect_show', [$WarSamurai->samurai->id]) }}">
                                <img src="{{ My_func::return_min_samurai_img_url($WarSamurai->samurai->id, $GameTitle->slug) }}" alt="" width="65px" height="100%" style="display: block">
                                {{ $WarSamurai->samurai->name }}
                            </a><small>({{ $war->start_year - $WarSamurai->samurai->birth_year }})</small>
                        @else
                            <img src="{{ My_func::return_min_samurai_img_url(0, $GameTitle->slug) }}" alt="" width="65px" height="100%" style="display: block">
                            <p class="mt-0">{{ $WarSamurai->samurai_name }}</p>
                        @endif
                    </div>
                @endif
            @endforeach
        </div>
        <div class="w_or_l ta_c">
            @foreach ($war->warSamurais as $WarSamurai)
                @if ($WarSamurai->position == 'l' && $WarSamurai->w_or_l == 'w')
                    <p class="back_win w_or_l_txt">WIN</p>
                    @break
                @elseif ($WarSamurai->position == 'l' && $WarSamurai->w_or_l == 'l')
                    <p class="back_lose w_or_l_txt">LOSE</p>
                    @break
                @elseif ($WarSamurai->position == 'l' && $WarSamurai->w_or_l == 'd')
                    <p class="back_draw w_or_l_txt">DRAW</p>
                    @break
                @endif
            @endforeach
        </div>
    </div>

    <div class="vs_icon">
        <img src="{{ asset('img/common/strategy.png')}}" width="30px" height="30px" alt="">
    </div>
    <div>
        <div class="d-f ai-c join_samurai_box">
            @foreach ($war->warSamurais as $WarSamurai)
                @if ($WarSamurai->position == 'r')
                    <div class="join_samurai">
                        @if (isset($WarSamurai->samurai))
                            <a href="{{ route('database.samurais.redirect_show', [$WarSamurai->samurai->id]) }}">
                                <img src="{{ My_func::return_min_samurai_img_url($WarSamurai->samurai->id, $GameTitle->slug) }}" alt="" width="65px" height="100%" style="display: block">
                                {{ $WarSamurai->samurai->name }}
                            </a><small>({{ $war->start_year - $WarSamurai->samurai->birth_year }})</small>
                        @else
                            <img src="{{ My_func::return_min_samurai_img_url(0, $GameTitle->slug) }}" alt="" width="65px" height="100%" style="display: block">
                            <p class="mt-0">{{ $WarSamurai->samurai_name }}</p>
                        @endif
                    </div>
                @endif
            @endforeach
        </div>
        <div class="w_or_l ta_c">
            @foreach ($war->warSamurais as $WarSamurai)
                @if ($WarSamurai->position == 'r' && $WarSamurai->w_or_l == 'w')
                    <p class="back_win w_or_l_txt">WIN</p>
                    @break
                @elseif ($WarSamurai->position == 'r' && $WarSamurai->w_or_l == 'l')
                    <p class="back_lose w_or_l_txt">LOSE</p>
                    @break
                @elseif ($WarSamurai->position == 'r' && $WarSamurai->w_or_l == 'd')
                    <p class="back_draw w_or_l_txt">DRAW</p>
                    @break
                @endif
            @endforeach
        </div>
    </div>
</div>
<p class="mt-24">{!! $war->body_html !!}</p>
