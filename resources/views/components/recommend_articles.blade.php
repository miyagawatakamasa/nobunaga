<section class="mt-48">
    <h2 class="rich_h2 d-f ai-c">
        <img src="{{ asset('img/common/contents.png')}}" alt="" width="30px" height="30px" class="mr-8">
        <p>おすすめ記事一覧</p>
    </h2>
    <ul class="card_ul">
        @foreach ($RecommendArticles as $RecommendArticle)
            <li class="card_list narrow_card mt-24">
                <a href="{{ route('article.single', $RecommendArticle->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $RecommendArticle->title }}">
                    <picture>
                        <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($RecommendArticle->id) }}" class="card_image">
                        <img src="{{ My_func::return_article_main_img_url($RecommendArticle->id) }}" width="100%" height="100%" class="card_image" alt="{{ $RecommendArticle->title }}">
                    </picture>
                    <div class="card_bottom">
                        <div class="d-f j-sb card_info sp_block">
                            <time>
                                @if ($RecommendArticle->is_updated)
                                    {{ $RecommendArticle->additioned_at->format('Y年m月d日') }}
                                @else
                                    {{ $RecommendArticle->created_at->format('Y年m月d日') }}
                                @endif
                            </time>
                            <p class="card_category">
                                @foreach ($RecommendArticle->articleCategorySearches as $mainArticleCategorySearch)
                                    @if ($mainArticleCategorySearch->is_main_category)
                                        {{ $mainArticleCategorySearch->articleCategory->name }}
                                    @endif
                                @endforeach
                            </p>
                        </div>
                        <p class="card_title">{{ $RecommendArticle->title }}</p>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
</section>
