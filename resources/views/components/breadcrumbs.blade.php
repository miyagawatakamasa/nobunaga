<div class="mt-24">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
     crossorigin="anonymous"></script>
    <!-- パンくずリスト上 -->
    <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="ca-pub-2958289145034418"
        data-ad-slot="1768404282"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<div class="breadcrumbs mt-24">
    <div class="inner inner_wrapper breadcrumbs_inner">
        @if (isset($argument2))
            {{ Breadcrumbs::render($slug, $argument1, $argument2) }}
        @elseif (isset($argument1))
            {{ Breadcrumbs::render($slug, $argument1) }}
        @else
            {{ Breadcrumbs::render($slug) }}
        @endif
    </div>
</div>


