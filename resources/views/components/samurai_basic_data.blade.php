<div class="d-f mt-12 parameter_img_area">
    <table class="font_small ta-c parameter_table">
        <tr>
            <th colspan="6">{{ $Samurai->name }} の基礎データ</th>
        </tr>
        <tr>
            <th colspan="2" class="back_cloud_blue color_font_black fw-n" style="min-width: 75px;">性別</th>
            <td colspan="4">
                @if ($Samurai->sex == 'm')
                    男
                @elseif ($Samurai->sex == 'w')
                    女
                @else
                    不明
                @endif
            </td>
        </tr>
        @if (count($Samurai->aliases($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">別名</th>
                <td colspan="4">
                    @foreach ($Samurai->aliases($Samurai->id) as $samuraiName)
                        {{ $samuraiName->alias }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if (count($Samurai->childhoodNames($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">幼名</th>
                <td colspan="4">
                    @foreach ($Samurai->childhoodNames($Samurai->id) as $samuraiName)
                        {{ $samuraiName->childhood_name }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if (count($Samurai->spiritualNames($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">霊名</th>
                <td colspan="4">
                    @foreach ($Samurai->spiritualNames($Samurai->id) as $samuraiName)
                        {{ $samuraiName->spiritual_name }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if (count($Samurai->kanas($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">仮名・通称</th>
                <td colspan="4">
                    @foreach ($Samurai->kanas($Samurai->id) as $samuraiName)
                        {{ $samuraiName->kana }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if (count($Samurai->nicknames($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">渾名</th>
                <td colspan="4">
                    @foreach ($Samurai->nicknames($Samurai->id) as $samuraiName)
                        {{ $samuraiName->nickname }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if (count($Samurai->legalNames($Samurai->id)))
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">法号・戒名</th>
                <td colspan="4">
                    @foreach ($Samurai->legalNames($Samurai->id) as $samuraiName)
                        {{ $samuraiName->legal_name }}
                    @endforeach
                </td>
            </tr>
        @endif
        @if ($Samurai->father)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">父</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->father) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->adoptive_father)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">養父</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->adoptive_father) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->mother)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">母</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->mother) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->adoptive_mother)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">養母</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->adoptive_mother) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->spouse)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">配偶者・正室</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->spouse) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->concubine_1)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">側室1</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->concubine_1) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->concubine_2)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">側室2</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->concubine_2) !!}
                </td>
            </tr>
        @endif
        @if ($Samurai->concubine_3)
            <tr>
                <th colspan="2" class="back_cloud_blue color_font_black fw-n">側室3</th>
                <td colspan="4">
                    {!! My_func::return_samurai_name_or_string($Samurai->concubine_3) !!}
                </td>
            </tr>
        @endif
    </table>
    <div class="samurai_img_area d-f">
        <div class="d-f ai-c j-c w-100">
            {!! My_func::return_wiki_img_html_tag($Samurai) !!}
        </div>
    </div>
</div>
