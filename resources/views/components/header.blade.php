<header class="back_white">
  @if (count($TextSliderTimelines))
    <div class="back_blue font_small">
      <div class="inner_wrapper d-f inner">
        <p class="today_text">今日は何の日？</p>
        <div class="text_slider d-f">
              <ul class="text_slider_ul">
                @foreach ($TextSliderTimelines as $TextSliderTimeline)
                  <li class="text_slider_li">● {{ $TextSliderTimeline->year }}年　{{ $TextSliderTimeline->body }}</li>
                @endforeach
              </ul>
              <ul class="text_slider_ul">
                @foreach ($TextSliderTimelines as $TextSliderTimeline)
                  <li class="text_slider_li">● {{ $TextSliderTimeline->year }}年　{{ $TextSliderTimeline->body }}</li>
                @endforeach
              </ul>
        </div>
      </div>
    </div> 
  @endif
  <div class="inner inner_wrapper d-f j-sb ai-c mt-12 header_contents sp_block">
    <div class="d-f ai-c sp-j-c">
      <h1>
        <a href="{{ route('top') }}" class="img_anchor h1_anchor" title="信長の野望 徹底攻略">
          <picture>
            <source type="image/webp" srcset="{{ asset('img/h1.webp')}}">
            <img class="logo" src="{{ asset('img/h1.png') }}" alt="信長の野望 徹底攻略" width="100%" height="100%">
          </picture>
        </a>
      </h1>
      <p class="font_small ml-12 pc_display">歴史シミュレーションゲーム「信長の野望」の攻略サイト</p>
    </div>
    <div class="d-f j-e">
      <p>この人誰？</p>
      <p>「<a href="/database/samurai/{{ $RandomSamurai->id }}" title="{{ $RandomSamurai->name }}">{{ $RandomSamurai->name }}</a>」</p>
    </div>
  </div>
  <nav class="pc_display ta-c back_gray">
      <ul class="d-f inner">
        <li class="header_nav_list">
            <a href="{{ route('database.samurais', 'shinsei') }}?form_header=1" class="color_gray header_nav_anchor fw-b" title="武将名鑑">武将名鑑</a>
            <ul class="back_gray dropdown_ul">
              <li class="dropdown_li"><a href="{{ route('hadou.database.samurais') }}" class="color_gray dropdown_anchor" title="信長の野望 覇道 武将名鑑">信長の野望 覇道</a></li>
              <li class="dropdown_li"><a href="{{ route('shinsei-pk.database.samurais') }}" class="color_gray dropdown_anchor" title="信長の野望 新生PK 武将名鑑">信長の野望 新生PK</a></li>
              <li class="dropdown_li"><a href="{{ route('database.samurais', 'shinsei') }}" class="color_gray dropdown_anchor" title="信長の野望 新生 武将名鑑">信長の野望 新生</a></li>
              <li class="dropdown_li"><a href="{{ route('shinnobunaga.database.samurais') }}" class="color_gray dropdown_anchor" title="新信長の野望 武将名鑑">新信長の野望</a></li>
          </ul>
        </li>
        <li class="header_nav_list">
          <a href="{{ route('article.index') }}" class="color_gray header_nav_anchor fw-b">記事</a>
          <ul class="back_gray dropdown_ul">
              <li class="dropdown_li"><a href="{{ route('article.index') }}" class="color_gray dropdown_anchor" title="記事一覧">一覧</a></li>
              <li class="dropdown_li"><a href="{{ route('article.category.index', 'dousuru-ieyasu') }}" class="color_gray dropdown_anchor" title="どうする家康">どうする家康</a></li>
              <li class="dropdown_li"><a href="{{ route('other.war') }}" class="color_gray dropdown_anchor" title="合戦一覧">合戦一覧</a></li>
              <li class="dropdown_li"><a href="{{ route('article.category.index', 'ranking') }}" class="color_gray dropdown_anchor" title="ランキング記事一覧">ランキング</a></li>
              <li class="dropdown_li"><a href="{{ route('article.category.index', 'minor') }}" class="color_gray dropdown_anchor" title="マイナー武将列伝記事一覧">マイナー武将列伝</a></li>
          </ul>
      </li>
      <li class="header_nav_list">
          <a href="#" class="color_gray header_nav_anchor fw-b">コンテンツ</a>
          <ul class="back_gray dropdown_ul">
              <li class="dropdown_li"><a href="{{ route('other.timeline') }}" class="color_gray dropdown_anchor" title="戦国年表">戦国年表</a></li>
              <li class="dropdown_li"><a href="{{ route('other.diagnose.index') }}" class="color_gray dropdown_anchor" title="武将診断テスト">武将診断テスト</a></li>
              <li class="dropdown_li"><a href="{{ route('other.game_title') }}" class="color_gray dropdown_anchor" title="ゲームタイトル一覧">ゲームタイトル一覧</a></li>
              <li class="dropdown_li"><a href="{{ route('other.vassals') }}" class="color_gray dropdown_anchor" title="家臣団総称">家臣団総称</a></li>
              <li class="dropdown_li"><a href="{{ route('other.word') }}" class="color_gray dropdown_anchor" title="戦国時代用語集">戦国時代用語集</a></li>
          </ul>
      </li>
      <li class="header_nav_list">
        <a href="{{ route('about') }}" class="color_gray header_nav_anchor fw-b">このサイトについて</a>
        <ul class="back_gray dropdown_ul">
          <li class="dropdown_li"><a href="{{ route('contact') }}" class="color_gray dropdown_anchor" title="お問い合わせ">お問い合わせ</a></li>
          <li class="dropdown_li"><a href="{{ route('administrator') }}" class="color_gray dropdown_anchor" title="管理人情報">管理人情報</a></li>
          <li class="dropdown_li"><a href="{{ route('release') }}" class="color_gray dropdown_anchor" title="リリース情報">リリース情報</a></li>
        </ul>
      </li>
    </ul>
  </nav>
  <div class="ta-c back_white inner">
    {{-- <p class="ta-l">新着記事🎉 <a href="{{ route('article.single', 161 ) }}">【新解釈】本能寺の変の真実！光秀が謀反を決意した真の動機とは？</a></p> --}}
    {{-- <p>「信長の野望 新生 <span class="color_red">PK</span>」<span class="color_red">7/20発売</span>!!<br class="sp_display"> 購入は<a href="https://www.amazon.co.jp/%E3%82%B3%E3%83%BC%E3%82%A8%E3%83%BC%E3%83%86%E3%82%AF%E3%83%A2%E3%82%B2%E3%83%BC%E3%83%A0%E3%82%B9-%E3%80%90Windows%E3%80%91%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B%EF%BD%A5%E6%96%B0%E7%94%9F-with-%E3%83%91%E3%83%AF%E3%83%BC%E3%82%A2%E3%83%83%E3%83%97%E3%82%AD%E3%83%83%E3%83%88%E3%80%90%E6%97%A9%E6%9C%9F%E8%B3%BC%E5%85%A5%E7%89%B9%E5%85%B8%E3%80%91%E3%82%B7%E3%83%8A%E3%83%AA%E3%82%AA%E3%80%8C%E6%89%8B%E5%8F%96%E5%B7%9D%E3%81%AE%E6%88%A6%E3%81%84%E3%80%8DDLC-%E5%90%8C%E6%A2%B1/dp/B0BZTPSX87?keywords=%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B+%E6%96%B0%E7%94%9F+%E3%83%91%E3%83%AF%E3%83%BC%E3%82%A2%E3%83%83%E3%83%97%E3%82%AD%E3%83%83%E3%83%88&qid=1688633213&sr=8-2-spons&sp_csd=d2lkZ2V0TmFtZT1zcF9hdGY&psc=1&linkCode=ll1&tag=nobunagakou06-22&linkId=398782cd85ead88a1414a8a04e8d47f2&language=ja_JP&ref_=as_li_ss_tl" target="_blank" title="信長の野望 新生 PKの予約">こちら</a>から 🎉</p> --}}
  </div>
  <livewire:sp-menu />
</header>

