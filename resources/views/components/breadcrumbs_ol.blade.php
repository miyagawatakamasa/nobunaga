@unless ($breadcrumbs->isEmpty())

    <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
        @foreach ($breadcrumbs as $breadcrumb)

            @if (!is_null($breadcrumb->url) && !$loop->last)
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="{{ $breadcrumb->url }}" itemprop="item" class="breadcrumb_anchor">
                        <span itemprop="name" class="breadcrumb_span">{{ $breadcrumb->title }}</span>
                    </a>
                    <meta itemprop="position" content="{{ $loop->index + 1 }}" />
                </li>
            @else
                <li class="breadcrumb-item active">
                    {{ $breadcrumb->title }}
                </li>
            @endif
        @endforeach
    </ol>

@endunless