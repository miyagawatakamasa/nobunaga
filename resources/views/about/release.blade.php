@extends('layouts.layout')

@section('title', 'リリース情報 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望 徹底攻略」のリリース情報ページです。本番リリースから、現在に至るまでの更新情報を記載しています。">
    <meta property="og:description" content="「信長の野望 徹底攻略」のリリース情報ページです。本番リリースから、現在に至るまでの更新情報を記載しています。" />
    <meta property="og:title" content="リリース情報 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "リリース情報 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <h1>リリース情報</h1>
                    {{-- 。は使わない --}}
                    <div class="main_contents mt-36">
                        {{-- 部品 --}}
                        {{-- <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年6月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>80pxは1日目だけ
                                    <td class="p-12">
                                        画像を圧縮しました<br>
                                        URLを調整しました<br>
                                        フォルダ構成を修正しました
                                    </td>
                                </tr>
                            </table>
                        </div> --}}
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年9月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        新生PKの武将一覧ページをアップデートしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        新生PKの武将情報をアップデートしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinsei-pk.database.samurais') }}">新生PKの武将一覧</a>をアップしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">7日</td>
                                    <td class="p-12">
                                        RDBをアップデートしました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年8月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        武将アイコンの間違いを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.game_title' ) }}" title="ゲームタイトル">ゲームタイトルページ</a>を追記しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.game_title' ) }}" title="ゲームタイトル">ゲームタイトルページ</a>を追記しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        複数のcss・jsファイルをまとめました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        不要なServiceProviderを削除しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        n+1問題を一部解消しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年7月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        【PS4】信長の野望・新生 with パワーアップキット 40周年記念 TREASURE BOXの購入は<a href="https://www.amazon.co.jp/%E3%80%90PS4%E3%80%91%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B%E3%83%BB%E6%96%B0%E7%94%9F-%E3%83%91%E3%83%AF%E3%83%BC%E3%82%A2%E3%83%83%E3%83%97%E3%82%AD%E3%83%83%E3%83%88-40%E5%91%A8%E5%B9%B4%E8%A8%98%E5%BF%B5-TREASURE-%E3%80%90%E3%83%A1%E3%83%BC%E3%82%AB%E3%83%BC%E7%89%B9%E5%85%B8%E3%81%82%E3%82%8A%E3%80%91/dp/B0BZTNP9WQ?crid=1SOECDK96V9C7&keywords=%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B%2B%E6%96%B0%E7%94%9F%2B%E3%83%91%E3%83%AF%E3%83%BC%E3%82%A2%E3%83%83%E3%83%97%E3%82%AD%E3%83%83%E3%83%88&qid=1690697000&sprefix=%E4%BF%A1%E9%95%B7%2Caps%2C276&sr=8-7&th=1&linkCode=ll1&tag=nobunagakou06-22&linkId=e529f8135cb4683d1fde9b828c57c6fb&language=ja_JP&ref_=as_li_ss_tl" traget="_blank">こちら</a><br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました<br>
                                        スマホのデザインを一部修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        一部livewireを削除しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        livewireを利用し通信速度を短縮しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        livewireを利用し通信速度を短縮しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        目次を修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 161 ) }}" title="【新解釈】本能寺の変の真実！光秀が謀反を決意した真の動機とは？">【新解釈】本能寺の変の真実！光秀が謀反を決意した真の動機とは？</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinsei.policy' ) }}" title="政策一覧">政策一覧ページ</a>を作成しました<br>
                                        <a href="{{ route('article.single', 160 ) }}" title="再現度が高い信長の野望武将アイコン ランキングTOP8">再現度が高い信長の野望武将アイコン ランキングTOP8</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        戦法情報を追加しました<br>
                                        画像をs3に移行しました<br>
                                        部将パラメーターランキングの仕様を変更しました<br>
                                        固有政策を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年6月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        アフィリエイトを開始しました<br>
                                        yamをアップデートしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        検索ワード保存
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        t2.microからt2.smallに変更しました<br>
                                        投票キャンペーンをサイドバーから削除しました<br>
                                        一部画像を遅延読み込みしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        ソート機能を修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        一部n+1問題を解消しました<br>
                                        画像のheightをautoから100%に変更しました<br>
                                        HTMLminを導入しました<br>
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        サイドバーの一部を非同期で取得するように変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        過大なネットワーク ペイロードの回避を行いました<br>
                                        記事の画像をwebp対応しました<br>
                                        jsにdeferを設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        検索の範囲を広げました<br>
                                        サイトマップを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        リッチリザルト対応を行いました<br>
                                        パンクズリストに構造化データを登録しました<br>
                                        フッターを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        検索ページをGETに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        武将のデータの間違いを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年5月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        合戦情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        データの間違いを一部修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">2日</td>
                                    <td class="p-12">
                                        武将の基礎データを一部追加しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="ps back_cloud_blue p-12">
                            <p>2023年4月</p>
                        </div>
                        <table class="w-100">
                            <tr class="va-b">
                                <td class="p-12">26日</td>
                                <td class="p-12">
                                    合戦情報を更新しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">25日</td>
                                <td class="p-12">
                                    合戦情報を更新しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">21日</td>
                                <td class="p-12">
                                    武将情報にシナリオ情報を追加しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">20日</td>
                                <td class="p-12">
                                    合戦情報に武将の年齢を追加しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">19日</td>
                                <td class="p-12">
                                    合戦情報を更新しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">17日</td>
                                <td class="p-12">
                                    合戦情報を更新しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">14日</td>
                                <td class="p-12">
                                    武将列伝で一部リンクに変更しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">13日</td>
                                <td class="p-12">
                                    合戦情報を更新しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12">10日</td>
                                <td class="p-12">
                                    一部武将に別名のデータを登録しました<br>
                                    検索機能を強化しました
                                </td>
                            </tr>
                            <tr class="va-b">
                                <td class="p-12" style="width: 80px;">5日</td>
                                <td class="p-12">
                                    新生ページで情報が表示されないバグを修正しました
                                </td>
                            </tr>
                        </table>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年3月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        一部武将に別名のデータを登録しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        一部武将に別名のデータを登録しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        一部武将に別名のデータを登録しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        一部武将に別名のデータを登録しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        信長の野望 覇道のNレアキャラを追加しました<br>
                                        middlewareを利用しました<br>
                                        存在しないページが検索されるバグを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        信長の野望 覇道武将詳細ページに秘伝を追記しました<br>
                                        <a href="{{ route('database.samurais', 'hadou') }}" title="武将名鑑">信長の野望 覇道武将一覧ページ</a>をメニューに追加しました<br>
                                        フォントの種類を変更いたしました<br>
                                        信長の野望 覇道レアリティ一覧ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        信長の野望 覇道武将詳細ページにCVを追記しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        信長の野望 覇道武将詳細ページに技能を追記しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        信長の野望 覇道武将詳細ページを追記しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.diagnose.index') }}" title="武将診断テスト">武将診断テスト</a>ページに診断結果を表示しました<br>
                                        <a href="{{ route('hadou.database.samurais') }}" title="信長の野望 覇道">信長の野望 覇道武将一覧</a>ページを作成しました<br>
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        リンク先がないバグを一部修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年2月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        関連記事のロジックを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        リンク先の調整をしました<br>
                                        スマホでのデザインを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('database.ranking', 'shinsei') }}" title="新生武将ランキング">新生武将ランキング</a>ページを作成しました<br>
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        合戦にカテゴリーを追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        インフォメーションを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('database.samurais', 'shinsei') }}" title="武将名鑑【新生】">武将名鑑【新生】</a>を作成しました<br>
                                        <a href="{{ route('article.single', 159 ) }}" title="池田長正（いけだながまさ）とは？父を自害させられた摂津の豪族【マイナー武将列伝】">池田長正（いけだながまさ）とは？父を自害させられた摂津の豪族【マイナー武将列伝】</a>を公開しました<br>
                                        検索機能を修正しました<br>
                                        能力値にインフォメーションを設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 158 ) }}" title="粟屋勝久（あわやかつひさ）とは？幽閉された主君を救出した若狭武田四老の一人【マイナー武将列伝】">粟屋勝久（あわやかつひさ）とは？幽閉された主君を救出した若狭武田四老の一人【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 153 ) }}" title="南方就正（みなかたなりまさ）とは？毛利元就から「就」の字をもらった勇将【マイナー武将列伝】">南方就正（みなかたなりまさ）とは？毛利元就から「就」の字をもらった勇将【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 154 ) }}" title="本多忠純（ほんだただずみ）とは？家臣を虐待し殺した最低男は復讐にあう【マイナー武将列伝】">本多忠純（ほんだただずみ）とは？家臣を虐待し殺した最低男は復讐にあう【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 155 ) }}" title="長船定行（おさふねさだゆき）とは？宇喜多三老である父の貞親を支えた漢【マイナー武将列伝】">長船定行（おさふねさだゆき）とは？宇喜多三老である父の貞親を支えた漢【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 156 ) }}" title="岡本禅哲（おかもとぜんてつ）とは？梅を愛した佐竹家の外交僧【僧侶】">岡本禅哲（おかもとぜんてつ）とは？梅を愛した佐竹家の外交僧【僧侶】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 157 ) }}" title="犬甘政徳（いぬかいまさのり）とは？あれ、味方の部隊と思ったら敵の馬場信春じゃん！？【マイナー武将列伝】">犬甘政徳（いぬかいまさのり）とは？あれ、味方の部隊と思ったら敵の馬場信春じゃん！？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2023年1月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 152 ) }}" title="三浦貞久（みうらさだひさ）とは？尼子軍の侵攻を何度も防衛した美作の豪族【マイナー武将列伝】">三浦貞久（みうらさだひさ）とは？尼子軍の侵攻を何度も防衛した美作の豪族【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 151 ) }}" title="宗像正氏（むなかたまさうじ）とは？村上水軍を撃退した宗像水軍【マイナー武将列伝】">宗像正氏（むなかたまさうじ）とは？村上水軍を撃退した宗像水軍【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 150 ) }}" title="山内康豊（やまうちやすとよ）とは？兄の山内一豊よりも優秀！？【マイナー武将列伝】">山内康豊（やまうちやすとよ）とは？兄の山内一豊よりも優秀！？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 148 ) }}" title="宝蔵院胤栄（ほうぞういんいんえい）とは？革新的な宝蔵院流槍術の創始者！【マイナー武将列伝】">宝蔵院胤栄（ほうぞういんいんえい）とは？革新的な宝蔵院流槍術の創始者！【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 149 ) }}" title="細野藤光（ほそのふじみつ）とは？伊勢国最大級の城郭「安濃城」を築城！【マイナー武将列伝】">細野藤光（ほそのふじみつ）とは？伊勢国最大級の城郭「安濃城」を築城！【マイナー武将列伝】</a>を公開しました<br>
                                        サイドバーの記事カテゴリーの「都道府県」を開閉式に変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 147 ) }}" title="繋ぎの当主がしっかり仕事をしてバトンタッチ！！古田重治（ふるたしげはる）とは？【マイナー武将列伝】">繋ぎの当主がしっかり仕事をしてバトンタッチ！！古田重治（ふるたしげはる）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 146 ) }}" title="傀儡当主となった能登の大名・畠山義慶（はたけやまよしのり）とは？【マイナー武将列伝】">傀儡当主となった能登の大名・畠山義慶（はたけやまよしのり）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 144 ) }}" title="沖田畷合戦で散った龍造寺四天王の１人・成松信勝（なりまつのぶかつ）とは？【マイナー武将列伝】">沖田畷合戦で散った龍造寺四天王の１人・成松信勝（なりまつのぶかつ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        記事のカテゴリーを整理しました<br>
                                        <a href="{{ route('article.single', 145 ) }}" title="武田勝頼の祖父である禰津元直（ねづもとなお）とは？【マイナー武将列伝】">武田勝頼の祖父である禰津元直（ねづもとなお）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 140 ) }}" title="家臣団に追放された土佐七雄の一人である津野定勝（つのさだかつ）とは？【マイナー武将列伝】">家臣団に追放された土佐七雄の一人である津野定勝（つのさだかつ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 141 ) }}" title="美濃苗木藩２代藩主の遠山秀友（とおやまひでとも）とは？【マイナー武将列伝】">美濃苗木藩２代藩主の遠山秀友（とおやまひでとも）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 142 ) }}" title="上杉に近づき主君と対立したため戦死した大宝寺氏の重臣である土佐林禅棟（とさばやしぜんとう）とは？【マイナー武将列伝】">上杉に近づき主君と対立したため戦死した大宝寺氏の重臣である土佐林禅棟（とさばやしぜんとう）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 143 ) }}" title="上杉→織田→上杉と2度も上杉氏に仕えた越中の豪族・土肥政繁（どいまさしげ）とは？【マイナー武将列伝】">上杉→織田→上杉と2度も上杉氏に仕えた越中の豪族・土肥政繁（どいまさしげ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 135 ) }}" title="出羽の大名で家臣に暗殺された小野寺稙道（おのでらたねみち）とは？【マイナー武将列伝】">出羽の大名で家臣に暗殺された小野寺稙道（おのでらたねみち）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 136 ) }}" title="もう耐えきれない！毛利に寝返った尼子家臣の佐世清宗（させきよむね）とは？【マイナー武将列伝】">もう耐えきれない！毛利に寝返った尼子家臣の佐世清宗（させきよむね）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 137 ) }}" title="竹製のノコギリで時間をかけて首を切断する刑に処された杉谷善住坊（すぎたにぜんじゅうぼう）とは？【マイナー武将列伝】">竹製のノコギリで時間をかけて首を切断する刑に処された杉谷善住坊（すぎたにぜんじゅうぼう）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 138 ) }}" title="北条氏康の妻として、氏政・氏照・氏規・氏邦らを産んだ瑞渓院（ずいけいいん）とは？【戦国時代の女性】">北条氏康の妻として、氏政・氏照・氏規・氏邦らを産んだ瑞渓院（ずいけいいん）とは？【戦国時代の女性】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 139 ) }}" title="『元親記』を著した長宗我部家臣・高島正重（たかしままさしげ）とは？【マイナー武将列伝】">『元親記』を著した長宗我部家臣・高島正重（たかしままさしげ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 133 ) }}" title="仮病を使って敵を油断させる！加藤嘉明家臣の佃十成（つくだかずなり）とは？【マイナー武将列伝】">仮病を使って敵を油断させる！加藤嘉明家臣の佃十成（つくだかずなり）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 134 ) }}" title="義元亡き後も最後まで忠義を貫いた今川家臣・朝比奈泰朝（あさひなやすとも）とは？【マイナー武将列伝】">義元亡き後も最後まで忠義を貫いた今川家臣・朝比奈泰朝（あさひなやすとも）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 131 ) }}" title="弓馬の達人で文武両道な伊勢の大名・北畠晴具（きたばたけはるとも）とは？【マイナー武将列伝】">弓馬の達人で文武両道な伊勢の大名・北畠晴具（きたばたけはるとも）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 132 ) }}" title="ご乱心の主君・義久に殺されてしまった尼子氏の筆頭家老・宇山久兼（うやまひさかねも）とは？【マイナー武将列伝】">ご乱心の主君・義久に殺されてしまった尼子氏の筆頭家老・宇山久兼（うやまひさかねも）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 129 ) }}" title="最強の剣豪大名！！しかし織田に乗っ取られてしまう北畠具教（きたばたけとものり）とは？【マイナー武将列伝】">最強の剣豪大名！！しかし織田に乗っ取られてしまう北畠具教（きたばたけとものり）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 130 ) }}" title="おでぶ過ぎて父からも嫌われた北畠具房（きたばたけともふさ）とは？【マイナー武将列伝】">おでぶ過ぎて父からも嫌われた北畠具房（きたばたけともふさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        スマホ版のデザインを調整しました<br>
                                        <a href="{{ route('article.single', 127 ) }}" title="南部信直を擁立に貢献し右腕として南部家を支えた北信愛（きたのぶちか）とは？【マイナー武将列伝】">南部信直を擁立に貢献し右腕として南部家を支えた北信愛（きたのぶちか）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 128 ) }}" title="一向宗でない領民は全員惨殺していく北原兼孝（きたはらかねたか）とは？【マイナー武将列伝】">一向宗でない領民は全員惨殺していく北原兼孝（きたはらかねたか）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 126 ) }}" title="「無」を掲げた徳川四天王・榊原康政（さかきばらやすまさ）とは？【マイナー武将列伝】">「無」を掲げた徳川四天王・榊原康政（さかきばらやすまさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        <a href="{{ route('article.single', 124 ) }}" title="西園寺十五将の一人で謀反の巻き添えのため戦死した北之川親安（きたのがわちかやす）とは？【マイナー武将列伝】">西園寺十五将の一人で謀反の巻き添えのため戦死した北之川親安（きたのがわちかやす）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 125 ) }}" title="恩を仇で返す！主家を滅亡させた神保家臣・小島職鎮（こじまもとしげ）とは？【マイナー武将列伝】">恩を仇で返す！主家を滅亡させた神保家臣・小島職鎮（こじまもとしげ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 122 ) }}" title="北楯大堰を造り石高を10倍にした北楯利長（きただてとしなが）とは？【マイナー武将列伝】">北楯大堰を造り石高を10倍にした北楯利長（きただてとしなが）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        <a href="{{ route('article.single', 123 ) }}" title="優秀な南部家臣で北信愛の嫡男である北愛一（きたちかかず）とは？【マイナー武将列伝】">優秀な南部家臣で北信愛の嫡男である北愛一（きたちかかず）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 118 ) }}" title="怪力自慢で当主自ら最前線で戦った伝説が残る安芸武田家１０代当主の武田光和（たけだみつかず）とは？【マイナー武将列伝】">怪力自慢で当主自ら最前線で戦った伝説が残る安芸武田家１０代当主の武田光和（たけだみつかず）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 119 ) }}" title="「信濃の四大将」と呼ばれるも信玄を前に散った木曾義康（きそよしやす）とは？【マイナー武将列伝】">「信濃の四大将」と呼ばれるも信玄を前に散った木曾義康（きそよしやす）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 120 ) }}" title="北里柴三郎のルーツ！！肥後の武将・北里政義（きたざとまさよし）とは？【マイナー武将列伝】">北里柴三郎のルーツ！！肥後の武将・北里政義（きたざとまさよし）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 121 ) }}" title="上杉景虎派の重鎮で御館の乱の際に景勝に暗殺された北条景広（きたじょうかげひろ）とは？【マイナー武将列伝】">上杉景虎派の重鎮で御館の乱の際に景勝に暗殺された北条景広（きたじょうかげひろ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 117 ) }}" title="左京大夫という京職を持つやさしいおっちゃん・木曾義在（きそよしあり）とは？【マイナー武将列伝】">左京大夫という京職を持つやさしいおっちゃん・木曾義在（きそよしあり）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        <a href="{{ route('article.single', 115 ) }}" title="島左近と同様に関ヶ原の戦いで西軍についた元筒井家臣・岸田忠氏（きしだただうじ）とは？【マイナー武将列伝】">島左近と同様に関ヶ原の戦いで西軍についた元筒井家臣・岸田忠氏（きしだただうじ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 116 ) }}" title="天然要害の観音寺城（山形県）を築城した来次時秀（きすぎときひで）とは？【マイナー武将列伝】">天然要害の観音寺城（山形県）を築城した来次時秀（きすぎときひで）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        <a href="{{ route('article.single', 113 ) }}" title="大坂の陣で活躍し三河刈谷藩初代藩であった稲垣重綱（いながきしげつな）とは？【マイナー武将列伝】">大坂の陣で活躍し三河刈谷藩初代藩であった稲垣重綱（いながきしげつな）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 114 ) }}" title="日本一小さな東照宮を建立した徳川家臣・伊奈忠政（いなただまさ）とは？【マイナー武将列伝】">日本一小さな東照宮を建立した徳川家臣・伊奈忠政（いなただまさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 110 ) }}" title="父を討たれて、家康に捕らえられてしまいどうしたらいいの！？鵜殿氏長（うどのうじなが）とは？【マイナー武将列伝】">父を討たれて、家康に捕らえられてしまいどうしたらいいの！？鵜殿氏長（うどのうじなが）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 111 ) }}" title="家康への忠誠無二と評された数正の叔父・石川家成（いしかわいえなり）とは？【マイナー武将列伝】">家康への忠誠無二と評された数正の叔父・石川家成（いしかわいえなり）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 112 ) }}" title="なぜ豊臣家に出奔したのか！？徳川家康の右腕であった石川数正（いしかわかずまさ）とは？【メジャー武将列伝】">なぜ豊臣家に出奔したのか！？徳川家康の右腕であった石川数正（いしかわかずまさ）とは？【メジャー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 107 ) }}" title="家康から最も信頼された同級生・平岩親吉（ひらいわちかよし）とは？【マイナー武将列伝】">家康から最も信頼された同級生・平岩親吉（ひらいわちかよし）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 108 ) }}" title="家康につかなかったため滅ぼされた三河武士、鵜殿長照（うどのながてる）とは？【マイナー武将列伝】">家康につかなかったため滅ぼされた三河武士、鵜殿長照（うどのながてる）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 109 ) }}" title="今川義元の妹を娶った西三河の武将・鵜殿長持（うどのながもち）とは？【マイナー武将列伝】">今川義元の妹を娶った西三河の武将・鵜殿長持（うどのながもち）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 103 ) }}" title="信長の弟・信興を自害に追い込んだお坊さん、願証寺証意（がんしょうじしょうい）とは？【マイナー武将列伝】">信長の弟・信興を自害に追い込んだお坊さん、願証寺証意（がんしょうじしょうい）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 104 ) }}" title="伊勢長島願証寺の3世住持である願証寺証恵（がんしょうじしょうけい）とは？【マイナー武将列伝】">伊勢長島願証寺の3世住持である願証寺証恵（がんしょうじしょうけい）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 105 ) }}" title="「城井谷の悲劇」の犠牲者、城井鎮房の父・城井長房（きいながふさ）とは？【マイナー武将列伝】">「城井谷の悲劇」の犠牲者、城井鎮房の父・城井長房（きいながふさ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 106 ) }}" title="艾蓬（よもぎ）の射法という弓の技法を室町将軍に披露した城井正房（きいまさふささ）とは？【マイナー武将列伝】">艾蓬（よもぎ）の射法という弓の技法を室町将軍に披露した城井正房（きいまさふささ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 100 ) }}" title="障害が残るほどの苦悩、遺書を残し死を覚悟した懺悔など黒田官兵衛の大ピンチTOP3！！">障害が残るほどの苦悩、遺書を残し死を覚悟した懺悔など黒田官兵衛の大ピンチTOP3！！</a>を公開しました<br>
                                        <a href="{{ route('article.single', 101 ) }}" title="毛利元就のあまーい言葉に踊らされた秋月文種（あきづきふみたね）とは？【マイナー武将列伝】">毛利元就のあまーい言葉に踊らされた秋月文種（あきづきふみたね）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 102 ) }}" title="病弱な父の代わりに日向国2代藩主になった秋月種春（あきづきたねはる）とは？【マイナー武将列伝】">病弱な父の代わりに日向国2代藩主になった秋月種春（あきづきたねはる）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 96 ) }}" title="織田家オールスターに包囲され詰んだ神吉頼定（かんきよりさだ）とは？【マイナー武将列伝】">織田家オールスターに包囲され詰んだ神吉頼定（かんきよりさだ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 97 ) }}" title="真田幸隆の弟として真田一族を支えた、鎌原幸定（かんばらゆきさだ）とは？【マイナー武将列伝】">真田幸隆の弟として真田一族を支えた、鎌原幸定（かんばらゆきさだ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 98 ) }}" title="濡れた手拭で喉の渇きを癒しつつ防戦した蒲生茂綱（がもうしげつな）とは？【マイナー武将列伝】">濡れた手拭で喉の渇きを癒しつつ防戦した蒲生茂綱（がもうしげつな）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 99 ) }}" title="10歳で家督を継ぎ織田家をうまく立ち回った、蒲生元珍（がもうもとよし）とは？【マイナー武将列伝】">10歳で家督を継ぎ織田家をうまく立ち回った、蒲生元珍（がもうもとよし）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 93 ) }}" title="織田信長の従兄弟！？川口宗勝（かわぐちむねかつ）とは？【マイナー武将列伝】">織田信長の従兄弟！？川口宗勝（かわぐちむねかつ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 94 ) }}" title="二階堂氏から伊達氏へ！川島宗泰（かわしまむねやす）とは？【マイナー武将列伝】">二階堂氏から伊達氏へ！川島宗泰（かわしまむねやす）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 95 ) }}" title="津軽平野の歴史書「津軽郡中名字」を編纂した川原具信（かわはらとものぶ）とは？【マイナー武将列伝】">津軽平野の歴史書「津軽郡中名字」を編纂した川原具信（かわはらとものぶ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 88 ) }}" title="日本初となる鉄砲同士による合戦を行った蒲生範清（かもうのりきよ）とは？【マイナー武将列伝】">日本初となる鉄砲同士による合戦を行った蒲生範清（かもうのりきよ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 89 ) }}" title="一乗谷四奉行の１人である河合吉統（かわいよしむね）とは？【マイナー武将列伝】">一乗谷四奉行の１人である河合吉統（かわいよしむね）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 90 ) }}" title="甑島に配流された！？川上忠克（かわかみただかつ）とは？【マイナー武将列伝】">甑島に配流された！？川上忠克（かわかみただかつ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 91 ) }}" title="朝鮮の役で活躍した島津家臣・川上忠実（かわかみただざね）とは？【マイナー武将列伝】">朝鮮の役で活躍した島津家臣・川上忠実（かわかみただざね）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 92 ) }}" title="島津看経所四名臣の一人・川上久朗（かわかみひさあき）とは？【マイナー武将列伝】">島津看経所四名臣の一人・川上久朗（かわかみひさあき）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年12月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 85 ) }}" title="山中鹿之介の孫・徳川家臣として生き残った亀井政矩（かめいまさのり）とは？【マイナー武将列伝】">山中鹿之介の孫・徳川家臣として生き残った亀井政矩（かめいまさのり）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 86 ) }}" title="主家に背き撃退した岩手の武将・亀ヶ森光広（かめがもりみつひろ）とは？【マイナー武将列伝】">主家に背き撃退した岩手の武将・亀ヶ森光広（かめがもりみつひろ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 87 ) }}" title="塙団右衛門を討ち取った亀田高綱（かめだたかつな）とは？【マイナー武将列伝】">塙団右衛門を討ち取った亀田高綱（かめだたかつな）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 83 ) }}" title="北条家の馬廻衆・狩野泰光（かのうやすみつ）とは？【マイナー武将列伝】">北条家の馬廻衆・狩野泰光（かのうやすみつ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 84 ) }}" title="「源氏物語」を愛読した菊池家臣・鹿子木親員（かのこぎちかかず）とは？【マイナー武将列伝】">「源氏物語」を愛読した菊池家臣・鹿子木親員（かのこぎちかかず）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 80 ) }}" title="善政を敷き慕われた白河結城家臣の河東田清重（かとうだきよしげ）とは？【マイナー武将列伝】">善政を敷き慕われた白河結城家臣の河東田清重（かとうだきよしげ）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 81 ) }}" title="「永久に島津家はその功を忘れぬ」島津看経所四名臣の一人・鎌田政年（かまたまさとし）とは？【マイナー武将列伝】">「永久に島津家はその功を忘れぬ」島津看経所四名臣の一人・鎌田政年（かまたまさとし）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 82 ) }}" title="直江兼続と二頭体制を築いた狩野秀治（かのうひではる）とは？【マイナー武将列伝】">直江兼続と二頭体制を築いた狩野秀治（かのうひではる）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 78 ) }}" title="南部家から秋田県比内を奪還！嘉成重盛（かなりしげもり）とは？【マイナー武将列伝】">南部家から秋田県比内を奪還！嘉成重盛（かなりしげもり）とは？【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 79 ) }}" title="朝倉家臣として奉公職を務めた印牧美満（かねまきよしみつ）とは？【マイナー武将列伝】">朝倉家臣として奉公職を務めた印牧美満（かねまきよしみつ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 77 ) }}" title="木戸孝允の祖先、毛利家宿老で厳島神社の管理者・桂元澄（かつらもとずみ）とは？【マイナー武将列伝】">木戸孝允の祖先、毛利家宿老で厳島神社の管理者・桂元澄（かつらもとずみ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 76 ) }}" title="片桐且元の弟・片桐貞隆（かたぎりさだたか）とは？【マイナー武将列伝】">片桐且元の弟・片桐貞隆（かたぎりさだたか）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 75 ) }}" title="岩手の荒くれ者、柏山明長（かしやまあきなが）とは？【マイナー武将列伝】">岩手の荒くれ者、柏山明長（かしやまあきなが）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 74 ) }}" title="葛西・大崎一揆の総帥・柏山明久（かしやまあきひさ）とは？【マイナー武将列伝】">葛西・大崎一揆の総帥・柏山明久（かしやまあきひさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        グーグルタグマネージャーを導入しました<br>
                                        <a href="{{ route('article.single', 73 ) }}" title="岩手県一の実力者・柏山明吉（かしやまあきよし）とは？【マイナー武将列伝】">岩手県一の実力者・柏山明吉（かしやまあきよし）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 72 ) }}" title="私と一緒に地図に残る仕事をしませんか？伊予の土木家・足立重信（あだちしげのぶ）とは？【マイナー武将列伝】">私と一緒に地図に残る仕事をしませんか？伊予の土木家・足立重信（あだちしげのぶ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 71 ) }}" title="長尾為景の傀儡として越後守護となった上杉定実（うえすぎさだざね）とは？【マイナー武将列伝】">長尾為景の傀儡として越後守護となった上杉定実（うえすぎさだざね）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        ヘッダーのレイアウトを変更しました<br>
                                        武将詳細ページに広告を追加しました<br>
                                        スムーススクロールを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページのレイアウトを修正しました<br>
                                        戦国時代の合戦詳細ページに武将一覧を作成しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページのレイアウトを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 70 ) }}" title="揚北衆の首領・新発田綱貞（しばたつなさだ）とは？【マイナー武将列伝】">揚北衆の首領・新発田綱貞（しばたつなさだ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 69 ) }}" title="黒田孝高の祖父・明石長行（あかしながゆき）とは？【マイナー武将列伝】">黒田孝高の祖父・明石長行（あかしながゆき）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年11月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        目次を修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('top' ) }}" title="トップページ">トップページ</a>に新着記事を追加しました<br>
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました<br>
                                        <a href="{{ route('article.single', 68 ) }}" title="長宗我部元親に傀儡化された土佐の公家大名・一条内政（いちじょうただまさ）とは？【マイナー武将列伝】">長宗我部元親に傀儡化された土佐の公家大名・一条内政（いちじょうただまさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        ヘッダーに「今日は何の日？」欄を設置しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war' ) }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを大幅に修正しました<br>
                                        武将詳細ページに合戦内容を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 67 ) }}" title="神森出雲（こうのもりいずも）とは？白米を水の代わりに演出！？【マイナー武将列伝】">神森出雲（こうのもりいずも）とは？白米を水の代わりに演出！？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 66 ) }}" title="早雲時代からの由緒正しい北条家の重臣・大道寺盛昌（だいどうじもりまさ）とは？【マイナー武将列伝】">早雲時代からの由緒正しい北条家の重臣・大道寺盛昌（だいどうじもりまさ）とは？【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        <a href="{{ route('about') }}" title="このサイトについて">このサイトについて</a>を更新しました<br>
                                        <a href="{{ route('article.single', 65 ) }}" title="戦国時代の結婚について【戦国時代の暮らし】">戦国時代の結婚について【戦国時代の暮らし】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 63 ) }}" title="最も側室が多かった戦国大名は誰か？【戦国時代の暮らし】">最も側室が多かった戦国大名は誰か？【戦国時代の暮らし】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 64 ) }}" title="家康を恐怖に陥れた天才軍師・真田昌幸、九度山で果てる【メジャー武将列伝】">家康を恐怖に陥れた天才軍師・真田昌幸、九度山で果てる【メジャー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年10月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.word') }}" title="戦国時代用語集">戦国時代用語集</a>のページを作成しました<br>
                                        <a href="{{ route('article.single', 62 ) }}" title="徳川家を苦しめた妖刀「村正」【戦国時代の逸話】">徳川家を苦しめた妖刀「村正」【戦国時代の逸話】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 59 ) }}" title="なぜあと少し早く届かなかったのか？惜しくも自刃した海賊大名・九鬼嘉隆【メジャー武将列伝】">なぜあと少し早く届かなかったのか？惜しくも自刃した海賊大名・九鬼嘉隆【メジャー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 60 ) }}" title="家康はなぜ松平から徳川に改姓したのか？【メジャー武将列伝】">家康はなぜ松平から徳川に改姓したのか？【メジャー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 61 ) }}" title="家康はどんな人質時代を送っていたのか？【メジャー武将列伝】">家康はどんな人質時代を送っていたのか？【メジャー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 56 ) }}" title="夫への愛と憎悪による悲劇の死、細川ガラシャ【戦国時代の女性】">夫への愛と憎悪による悲劇の死、細川ガラシャ【戦国時代の女性】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 57 ) }}" title="戦国武将たちのセックス事情はどんなだった？【戦国時代の暮らし】">戦国武将たちのセックス事情はどんなだった？【戦国時代の暮らし】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 58 ) }}" title="戦国武将の忍者の実態は？本当に活躍したのか？">戦国武将の忍者の実態は？本当に活躍したのか？</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 53 ) }}" title="関ヶ原の合戦後八丈島へ流罪となった宇喜多秀家はどうなったのか？">関ヶ原の合戦後八丈島へ流罪となった宇喜多秀家はどうなったのか？</a>を公開しました<br>
                                        <a href="{{ route('article.single', 54 ) }}" title="この子誰の子？家康の次男ながら冷遇された結城秀康【マイナー武将列伝】">この子誰の子？家康の次男ながら冷遇された結城秀康【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 55 ) }}" title="武士の鑑、見事な死に際。その男清水宗治【メジャー武将列伝】">武士の鑑、見事な死に際。その男清水宗治【メジャー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 52 ) }}" title="戦国武将はタバコを吸っていたのか？">戦国武将はタバコを吸っていたのか？</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 51 ) }}" title="石川直経（いしかわなおつね）は何をした人？一色家の英雄【マイナー武将列伝】">石川直経（いしかわなおつね）は何をした人？一色家の英雄【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 50 ) }}" title="大月景秀（おおつきかげひで）は何をした人？戦国時代の医師で「万金丹」を開発【マイナー武将列伝】">大月景秀（おおつきかげひで）は何をした人？戦国時代の医師で「万金丹」を開発【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年8月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 49 ) }}" title="長坂信政（ながまさのぶまさ）は何をした人？その槍を血で濡らさぬ事は無かった「血槍九郎」【マイナー武将列伝】">長坂信政（ながまさのぶまさ）は何をした人？その槍を血で濡らさぬ事は無かった「血槍九郎」【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 48 ) }}" title="一条房家（いちじょうふさいえ）は何をした人？高知に「小京都」を作った男【マイナー武将列伝】">一条房家（いちじょうふさいえ）は何をした人？高知に「小京都」を作った男【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 47 ) }}" title="山田有信（やまだありのぶ）は何をした人？大友の攻撃を死守した剛勇の島津家臣【マイナー武将列伝】">山田有信（やまだありのぶ）は何をした人？大友の攻撃を死守した剛勇の島津家臣【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 46 ) }}" title="穴沢俊光（あなざわとしみつ）は何をした人？伊達の侵攻を防いだ「北の門番」【マイナー武将列伝】">穴沢俊光（あなざわとしみつ）は何をした人？伊達の侵攻を防いだ「北の門番」【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 45 ) }}" title="横山喜内（よこやまきない）は何をした人？蒲生家を何度も何度も出入りする図太い精神【マイナー武将列伝】">横山喜内（よこやまきない）は何をした人？蒲生家を何度も何度も出入りする図太い精神【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 44 ) }}" title="水心（すいしん）は何をした人？長宗我部元親を妻として支え中央政権との架け橋となった姫【戦国時代の女性】">水心（すいしん）は何をした人？長宗我部元親を妻として支え中央政権との架け橋となった姫【戦国時代の女性】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 43 ) }}" title="松倉右近（まつくらうこん）は何をした人？島左近と並び「右近左近」と称された男【マイナー武将列伝】">松倉右近（まつくらうこん）は何をした人？島左近と並び「右近左近」と称された男【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 42 ) }}" title="実は超優秀！？森蘭丸（もりらんまる）の実態【織田家臣】">実は超優秀！？森蘭丸（もりらんまる）の実態【織田家臣】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 41 ) }}" title="高屋良栄（たかやりょうえい）は何をした人？一色家の守護神【マイナー武将列伝】">高屋良栄（たかやりょうえい）は何をした人？一色家の守護神【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 40 ) }}" title="六角定頼（ろっかくさだより）は何をした人？信長より先に楽市楽座を実施した人【マイナー武将列伝】">六角定頼（ろっかくさだより）は何をした人？信長より先に楽市楽座を実施した人【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 39 ) }}" title="直江兼続が家康に送った「直江状」とは？痛烈な皮肉がたーぷり【戦国時代の逸話】">直江兼続が家康に送った「直江状」とは？痛烈な皮肉がたーぷり【戦国時代の逸話】</a>を公開しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年7月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 38 ) }}" title="豊臣秀吉の指は6本あった？その真相に迫る【戦国時代の逸話】">豊臣秀吉の指は6本あった？その真相に迫る【戦国時代の逸話】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 37 ) }}" title="櫛橋光（くしはしてる）とは？初代福岡藩主・黒田長政を産んだ官兵衛の正室【戦国時代の女性】">櫛橋光（くしはしてる）とは？初代福岡藩主・黒田長政を産んだ官兵衛の正室【戦国時代の女性】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 36 ) }}" title="戦国時代に活躍した僧侶ランキングTOP5">戦国時代に活躍した僧侶ランキングTOP5</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 35 ) }}" title="織田信長を裏切り窮地に陥れた戦国武将ランキングTOP5">織田信長を裏切り窮地に陥れた戦国武将ランキングTOP5</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 34 ) }}" title="渡辺守綱（わたなべもりつな）は何をした人？徳川十六神将の一人である槍の名手「槍半蔵」【マイナー武将列伝】">渡辺守綱（わたなべもりつな）は何をした人？徳川十六神将の一人である槍の名手「槍半蔵」【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 33 ) }}" title="白鳥長久（しらとりながひさ）は何をした人？最上義光と並ぶ豪傑【マイナー武将列伝】">白鳥長久（しらとりながひさ）は何をした人？最上義光と並ぶ豪傑【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        アマゾンアフィリエイトを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        スマホ版のヘッダーをリッチ化しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.diagnose.index') }}" title="武将診断テスト">武将診断テスト</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        新信長の野望キャラ一覧ページを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.strategy.heirloom') }}" title="家宝システム【新信長の野望】">家宝システム【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年6月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        目次のスクロールをsmoothに変更しました<br>
                                        目次のスクロール量を調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        目次に非表示機能を作成しました<br>
                                        障害対応を行いました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        広告の表示数を調整しました<br>
                                        トップページのメニューを修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        兵科別ページを作成しました<br>
                                        適正別ページを作成しました<br>
                                        軽微なバグ修正を行いました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 32 ) }}" title="上田重安（うえだしげやす）とは？現代にも続く茶道・上田宗箇流の祖【マイナー武将列伝】">上田重安（うえだしげやす）とは？現代にも続く茶道・上田宗箇流の祖【マイナー武将列伝】</a>を公開しました<br>
                                        検索ページを作成しました<br>
                                        スマホ版のデザインを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        サイドバーにランダムな武将の情報を表示する機能を作成しました<br>
                                        広告位置の調整を行いました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.strategy.mukakin') }}" title="無課金キャラランキング【新信長の野望】">無課金キャラランキング【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.strategy.resemara') }}" title="リセマラ当たりランキング【新信長の野望】">リセマラ当たりランキング【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        レアリティ別キャラ一覧ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.strategy.beginner') }}" title="序盤の進め方【新信長の野望】">序盤の進め方【新信長の野望】</a>ページをリライトしました<br>
                                        <a href="{{ route('shinnobunaga.database.samurais.ranking') }}" title="武将最強ランキング【新信長の野望】">武将最強ランキング【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        武将詳細ページでタイトル間を移動できるようにしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.gacha.kokushimusou') }}" title="国士無双ガチャおすすめ">国士無双ガチャおすすめ</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.database.samurais.show', '1') }}" title="織田信長【新信長の野望】">武将詳細【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinnobunaga.database.samurais') }}" title="武将名鑑【新信長の野望】">武将名鑑【新信長の野望】</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        トップページのレイアウトを変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        デザインを調整しました<br>
                                        <a href="{{ route('shinnobunaga.strategy.beginner') }}" title="序盤の進め方【新信長の野望】">序盤の進め方【新信長の野望】</a>ページを公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        <a href="{{ route('capture.military') }}" title="軍事のコツ">軍事のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.march') }}" title="行軍のコツ">行軍のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.decisive_battle') }}" title="決戦のコツ">決戦のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.siege') }}" title="攻城戦のコツ">攻城戦のコツ</a>ページを公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        <a href="{{ route('capture.domestic_affairs') }}" title="内政のコツ">内政のコツ</a>ページを追記しました<br>
                                        <a href="{{ route('capture.agriculture') }}" title="農業のコツ">農業のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.commercial') }}" title="商業のコツ">商業のコツ</a>ページを公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        2枚以上ある武将グラフィックの場合、スライダーで表示するように修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('capture.domestic_affairs') }}" title="内政のコツ">内政のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.diplomacy') }}" title="外交のコツ">外交のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.will') }}" title="志について">志について</a>ページを公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        史実保有家宝を追加しました<br>
                                        <a href="{{ route('capture.plot') }}" title="調略のコツ">調略のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.general_contract') }}" title="普請のコツ">普請のコツ</a>ページを公開しました<br>
                                        <a href="{{ route('capture.human_resources') }}" title="人事のコツ">人事のコツ</a>ページを公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        一部記事をリライトしました<br>
                                        <a href="{{ route('article.single', 31 ) }}" title="【わかりやすく詳しく解説】「清洲会議」日本史上初めて、戦ではなく会議で歴史が動いた瞬間">【わかりやすく詳しく解説】「清洲会議」日本史上初めて、戦ではなく会議で歴史が動いた瞬間</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        一部記事に画像を追加しました<br>
                                        一部記事をリライトしました<br>
                                        ページネーション機能を<a href="{{ route('article.index') }}" title="記事一覧ページ">記事一覧ページ</a>に作成しました<br>
                                        記事に登場する武将一覧を<a href="{{ route('article.index') }}" title="記事一覧ページ">記事一覧ページ</a>に作成しました<br>
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        一部記事をリライトしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        記事ページにmeta descriptionを追加しました<br>
                                        おすすめ記事を各ページ配下に設置しました<br>
                                        一部記事をリライトしました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 28 ) }}" title="正木時茂（まさきときしげ）は何をした人？「槍大膳」の異名を持つ男【マイナー武将列伝】">正木時茂（まさきときしげ）は何をした人？「槍大膳」の異名を持つ男【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 29 ) }}" title="秦泉寺泰惟（じんぜんじやすこれ）は何をした人？長宗我部元親の才能を開花させた人【マイナー武将列伝】">秦泉寺泰惟（じんぜんじやすこれ）は何をした人？長宗我部元親の才能を開花させた人【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 30 ) }}" title="二階堂盛義（にかいどうもりよし）は何をした人？伊達阿南の旦那【マイナー武将列伝】">二階堂盛義（にかいどうもりよし）は何をした人？伊達阿南の旦那【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 27 ) }}" title="戦国武将のかっこいい異名・あだ名ランキングTOP10">戦国武将のかっこいい異名・あだ名ランキングTOP10</a>を公開しました<br>
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました<br>
                                        武将ページにサイト内検索を作成しました<br>
                                        ページトップに戻る機能を作成しました<br>
                                        <a href="{{ route('shinsei.beginner') }}" title="信長の野望 新生 ゲームの始め方">信長の野望 新生 ゲームの始め方</a>ページを公開しました<br>
                                        <a href="{{ route('article.single', 26 ) }}" title="奥重政（おくしげまさ）は何をした人？信長・秀吉を苦しめた根来衆の砲術家【マイナー武将列伝】">奥重政（おくしげまさ）は何をした人？信長・秀吉を苦しめた根来衆の砲術家【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        画像を圧縮しました<br>
                                        URLを調整しました<br>
                                        フォルダ構成を修正しました<br>
                                        <a href="{{ route('release') }}" title="リリース情報">リリース情報</a>ページのレイアウトを修正しました<br>
                                        検索窓を作成しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年5月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">
                                        武将の画像を圧縮しました<br>
                                        URLを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        sitemap.xmlをより正確に記述しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        <a href="{{ route('other.vassals') }}" title="家臣団総称">家臣団総称</a>をリライトしました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        <a href="{{ route('other.vassals') }}" title="家臣団総称">家臣団総称</a>をリライトしました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        <a href="{{ route('other.vassals') }}" title="家臣団総称">家臣団総称</a>をリライトしました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 25 ) }}" title="鳥居忠吉（とりいただよし）は何をした人？息子と共に三河武士の鑑と称された男【マイナー武将列伝】">鳥居忠吉（とりいただよし）は何をした人？息子と共に三河武士の鑑と称された男【マイナー武将列伝】</a>を公開しました<br>
                                        一部武将詳細ページに画像を追加しました<br>
                                        <a href="{{ route('other.vassals') }}" title="家臣団総称">家臣団総称</a>をリライトしました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 24 ) }}" title="大久保忠世（おおくぼただよ）は何をした人？徳川家臣No.5の実力は？【マイナー武将列伝】">大久保忠世（おおくぼただよ）は何をした人？徳川家臣No.5の実力は？【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました<br>
                                        記事ページに目次を作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        <a href="{{ route('shinsei.info') }}" title="信長の野望 新生 最新情報">信長の野望 新生 最新情報</a>ページに「戦の準備」を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        武将列伝で一部リンクに変更しました<br>
                                        <a href="{{ route('other.vassals') }}" title="【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団">【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 23 ) }}" title="遠藤基信（えんどうもとのぶ）は何をした人？僕、政治が得意です！伊達輝宗を支えた文官【マイナー武将列伝】">遠藤基信（えんどうもとのぶ）は何をした人？僕、政治が得意です！伊達輝宗を支えた文官【マイナー武将列伝】</a>を公開しました<br>
                                        css・jsファイルを圧縮しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        武将列伝で一部リンクに変更しました<br>
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>ページに50音でソート機能を作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 22 ) }}" title="那古野勝泰（なごやかつやす）は何をした人？簗田弥次右衛門との禁断の●●な仲（意味深）【マイナー武将列伝】">那古野勝泰（なごやかつやす）は何をした人？簗田弥次右衛門との禁断の●●な仲（意味深）【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('shinsei.tensyou') }}" title="天正猿芝居シナリオ予想">天正猿芝居シナリオ予想</a>を公開しました<br>
                                        記事ページにtwitterのシェア機能を作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        武将列伝で一部リンクに変更しました<br>
                                        <a href="{{ route('article.single', 21 ) }}" title="金上盛備（かながみもりはる）は何をした人？蘆名の執権【マイナー武将列伝】">金上盛備（かながみもりはる）は何をした人？蘆名の執権【マイナー武将列伝】</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました<br>
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに画像を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        武将列伝で一部リンクに変更しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 20 ) }}" title="鹿伏兎定秀（かぶとさだひで）は何をした人？浅井長政との絆【マイナー武将列伝】">鹿伏兎定秀（かぶとさだひで）は何をした人？浅井長政との絆【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに列伝を追加しました<br>
                                        <a href="{{ route('article.single', 19 ) }}" title="大浦三老とは？大浦為信を支えた三人の賢者【大浦三老】">大浦三老とは？大浦為信を支えた三人の賢者【大浦三老】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに列伝を追加しました<br>
                                        <a href="{{ route('article.single', 18 ) }}" title="島津忠長（しまづただなが）は何をした人？島津四兄弟の優秀な従兄弟【マイナー武将列伝】">島津忠長（しまづただなが）は何をした人？島津四兄弟の優秀な従兄弟【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        モバイルフレンドリーテストを行い、問題ありませんでした<br>
                                        一部画像をwebpに対応しました<br>
                                        武将詳細ページでのmeta discriptionを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        Google推奨「JSON-LD」で構造化マークアップを行いました<br>
                                        一部武将詳細ページに列伝を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        一部武将詳細ページに列伝を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 17 ) }}" title="後藤信康（ごとうのぶやす）は何をした人？「黄後藤」の異名を持つ男【伊達家臣二十四将】">後藤信康（ごとうのぶやす）は何をした人？「黄後藤」の異名を持つ男【伊達家臣二十四将】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        記事詳細ページに関連記事を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 16 ) }}" title="斯波長秀（しばながひで）は何をした人？返り咲いた10万石大名！【マイナー武将列伝】">斯波長秀（しばながひで）は何をした人？返り咲いた10万石大名！【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinsei.info') }}" title="信長の野望 新生 最新情報">信長の野望 新生 最新情報</a>ページを更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 13 ) }}" title="青田顕治（あおたあきはる）は何をした人？相馬盛胤から謀反！【マイナー武将列伝】">青田顕治（あおたあきはる）は何をした人？相馬盛胤から謀反！【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 14 ) }}" title="青山忠成（あおやまただなり）は何をした人？東京都港区青山との関係【マイナー武将列伝】">青山忠成（あおやまただなり）は何をした人？東京都港区青山との関係【マイナー武将列伝】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 15 ) }}" title="赤井家清（あかいいえきよ）は何をした人？若くして亡くなった赤井直正の兄【マイナー武将列伝】">赤井家清（あかいいえきよ）は何をした人？若くして亡くなった赤井直正の兄【マイナー武将列伝】</a>を公開しました<br>
                                        記事のカードのデザインを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 12 ) }}" title="青景隆著（あおかげたかあきら）は何をした人？陶隆房をそそのかせた人【マイナー武将列伝】">青景隆著（あおかげたかあきら）は何をした人？陶隆房をそそのかせた人【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年4月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">
                                        <a href="{{ route('scenario.index', 'taishi-pk') }}" title="シナリオ【大志（パワーアップキット）】">シナリオ【大志（パワーアップキット）】</a>を公開しました<br>
                                        サイドバーに項目を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12">
                                        武将に紐づく記事を武将詳細ページに表示しました<br>
                                        <a href="{{ route('article.single', 11 ) }}" title="愛洲移香斎（あいすいこうさい）は何をした人？剣術三大源流の一つ「陰流」の祖【マイナー武将列伝】">愛洲移香斎（あいすいこうさい）は何をした人？剣術三大源流の一つ「陰流」の祖【マイナー武将列伝】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 2 ) }}" title="戦国武将の変な名前ランキング！！">戦国武将の変な名前ランキング！！</a>を加筆修正しました<br>
                                        <a href="{{ route('article.category.index', 'minor') }}" title="マイナー武将列伝">マイナー武将列伝</a>ページを作成しました<br>
                                        <a href="{{ route('article.category.index', 'kuroda-twentyfour') }}" title="黒田二十四騎">黒田二十四騎</a>ページを作成しました<br>
                                        スマホでのデザインを調整しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12">
                                        記事にサムネイルを設定しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">
                                        記事にサムネイルを設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12">
                                        <a href="{{ route('database.ranking', 'taishi-pk') }}" title="能力値ランキング【大志（パワーアップキット）】">能力値ランキング【大志（パワーアップキット）】</a>を公開しました<br>
                                        2カラム（左コンテンツ、右サイドメニュー）から3カラム（左ゲーム関連サイドメニュー、中央コンテンツ、右記事などその他のコンテンツサイドメニュー）に変更しました<br>
                                        サイドバーに新着記事を追加しました<br>
                                        サイドバーにプロフィールを追加しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 10 ) }}" title="益田正親（ますだまさちか）は何をした人？貧農の出ながら優秀！【黒田二十四騎】">益田正親（ますだまさちか）は何をした人？貧農の出ながら優秀！【黒田二十四騎】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">23日</td>
                                    <td class="p-12">
                                        <a href="{{ route('shinsei.info') }}" title="信長の野望 新生 最新情報">信長の野望 新生 最新情報</a>ページを更新しました<br>
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>に能力値のランキングを追加しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">22日</td>
                                    <td class="p-12">
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>の情報を大幅にアップデートしました<br>
                                        ログファイルが日毎に出るように設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">21日</td>
                                    <td class="p-12">
                                        <a href="{{ route('tactics.index', 'taishi-pk') }}" title="戦法【大志（パワーアップキット）】">戦法【大志（パワーアップキット）】</a>ページを作成しました<br>
                                        <a href="{{ route('article.single', 9 ) }}" title="三宅家義（みやけいえよし）は何をした人？黒田家の海上幕僚長！【黒田二十四騎】">三宅家義（みやけいえよし）は何をした人？黒田家の海上幕僚長！【黒田二十四騎】</a>を公開しました<br>
                                        おすすめメニューにエフェクトを追加しました<br>
                                        <a href="{{ route('facility.index', 'taishi-pk') }}" title="設備">設備</a>一覧ページの内容を更新しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">20日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">19日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 8 ) }}" title="野口一成（のぐちかずしげ）は何をした人？左半身を切らせて、敵を倒す！【黒田二十四騎】">野口一成（のぐちかずしげ）は何をした人？左半身を切らせて、敵を倒す！【黒田二十四騎】</a>を公開しました<br>
                                        <a href="{{ route('taishi-pk.strategy.index') }}" title="作戦【大志（パワーアップキット）】">作戦【大志（パワーアップキット）】</a>ページを作成しました<br>
                                        <a href="{{ route('administrator') }}" title="管理人について">管理人について</a>ページに管理人の画像を追加しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">18日</td>
                                    <td class="p-12">
                                        <a href="{{ route('building.index', 'taishi-pk') }}" title="施設">施設</a>一覧ページを作成しました<br>
                                        <a href="{{ route('article.single', 7 ) }}" title="村田吉次（むらたよしつぐ）は何をした人？朱柄が許された猛者！【黒田二十四騎】">村田吉次（むらたよしつぐ）は何をした人？朱柄が許された猛者！【黒田二十四騎】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">17日</td>
                                    <td class="p-12">
                                        <a href="{{ route('scenario.recommend', 'taishi') }}" title="おすすめシナリオ">おすすめシナリオ</a>を作成しました<br>
                                        <a href="{{ route('tensyu.index') }}" title="天守">天守</a>一覧ページを作成しました<br>
                                        <a href="{{ route('facility.index', 'taishi-pk') }}" title="設備">設備</a>一覧ページを作成しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">16日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 5 ) }}" title="毛屋武久（けやたけひさ）は何をした人？家康から餅もらった人！【黒田二十四騎】">毛屋武久（けやたけひさ）は何をした人？家康から餅もらった人！【黒田二十四騎】</a>を公開しました<br>
                                        <a href="{{ route('article.single', 6 ) }}" title="原種良（はらたねよし）は何をした人？もともとは大友家の人質！【黒田二十四騎】">原種良（はらたねよし）は何をした人？もともとは大友家の人質！【黒田二十四騎】</a>を公開しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">15日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 4 ) }}" title="黒田一成（くろだかずしげ）は何をした人？摂津有岡城で官兵衛を助けた人（の息子）！【黒田二十四騎】">黒田一成（くろだかずしげ）は何をした人？摂津有岡城で官兵衛を助けた人（の息子）！【黒田二十四騎】</a>を公開しました<br>
                                        スマホサイズでのデザインを調整しました<br>
                                        記事の更新日時の表記を修正しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">14日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 3 ) }}" title="黒田直之（くろだなおゆき）は何をした人？武将としてキリシタンとして活躍した黒田二十四騎！">黒田直之（くろだなおゆき）は何をした人？武将としてキリシタンとして活躍した黒田二十四騎！</a>を公開しました<br>
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>の情報を更新しました<br>
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>に関連する記事一覧を作成しました<br>
                                        <a href="{{ route('vote_campaign.index') }}" title="投票キャンペーン">投票キャンペーン</a>の投票完了ページに関連する武将の記事一覧を作成しました<br>
                                    </td>
                                </tr>
                                    <td class="p-12">13日</td>
                                    <td class="p-12">
                                        google adsense に合格し、ads.txt・広告を設置しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">12日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war') }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">11日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">10日</td>
                                    <td class="p-12">
                                        <a href="{{ route('article.single', 2 ) }}" title="規模が大きい戦ランキングTOP5">規模が大きい戦ランキングTOP5</a>を公開しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">9日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war') }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>の内容を更新しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">8日</td>
                                    <td class="p-12">
                                        meta情報を更新しました<br>
                                        Laravelのバージョンを8 -> 9（LTS）にアップグレードしました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">7日</td>
                                    <td class="p-12">
                                        meta情報を更新しました<br>
                                        <a href="{{ route('release') }}" title="リリース情報">リリース情報</a>に「信長の野望 新生が発売される7/21までにアップしたい項目」を追加しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">6日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました<br>
                                        <a href="{{ route('other.war') }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>の内容を更新しました<br>
                                        <a href="{{ route('sitemap') }}" title="戦国時代の合戦一覧">sitemap.xml</a>を作成しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">5日</td>
                                    <td class="p-12">
                                        Twitterアカウントを開設しました<br>
                                        アカウントは<a href="https://twitter.com/YD5qsgGyX4OseNf" target="_blank" title="信長の野望 徹底攻略 twitterアカウント">こちら</a><br>
                                        <a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました<br>
                                        エラー時にリダイレクトさせるページを作成しました<br>
                                        Laravelのバージョンを6 -> 8にアップグレードしました<br>
                                        <a href="{{ route('scenario.index', 'shinsei') }}" class="side_bar_section_link hover_orange" title="信長の野望 新生 シナリオ一覧">信長の野望 新生 シナリオ一覧</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">4日</td>
                                    <td class="p-12">
                                        <a href="{{ route('other.war') }}" title="戦国時代の合戦一覧">戦国時代の合戦一覧</a>ページを作成しました<br>
                                        <a href="{{ route('shinsei.info') }}" title="信長の野望 新生 最新情報">信長の野望 新生 最新情報</a>ページを作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">3日</td>
                                    <td class="p-12">トップページのメインビジュアルを変更しました<br>
                                        <a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">2日</td>
                                    <td class="p-12"><a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">1日</td>
                                    <td class="p-12">アイコン（自作）を追加しました<br>
                                        aタグにtitle属性を設定しました
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="mt-24">
                            <div class="ps back_cloud_blue p-12">
                                <p>2022年3月</p>
                            </div>
                            <table class="w-100">
                                <tr class="va-b">
                                    <td class="p-12">31日</td>
                                    <td class="p-12">広告を設置しました<br>
                                        「信長の野望 新生」の情報を更新しました<br>
                                        <a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました<br>
                                        httpでアクセスした場合、httpsにリダイレクトするように設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">30日</td>
                                    <td class="p-12">httpsに対応しました<br>
                                        <a href="{{ route('database.samurais', 'taishi-pk') }}" title="武将名鑑">武将名鑑</a>の情報を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">29日</td>
                                    <td class="p-12"><a href="{{ route('other.timeline') }}" title="戦国年表">戦国年表</a>の内容を更新しました<br>
                                        「旧国名を見る」機能を改修しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">28日</td>
                                    <td class="p-12"><a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました<br>
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">27日</td>
                                    <td class="p-12"><a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました<br>
                                        「旧国名を見る」機能を作成しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">26日</td>
                                    <td class="p-12">アイコン（自作）を追加しました<br>
                                        <a href="{{ route('other.game_title') }}" title="タイトル一覧">タイトル一覧</a>の内容を更新しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">25日</td>
                                    <td class="p-12"><a href="{{ route('vote_campaign.index') }}" title="投票キャンペーン">投票キャンペーン</a>にコメント入力欄を追加しました<br>
                                        <a href="{{ route('vote_campaign.index') }}" title="投票キャンペーン">投票キャンペーン</a>に総投票数を表示しました<br>
                                        <a href="{{ route('vote_campaign.index') }}" title="投票キャンペーン">投票キャンペーン</a>のデザインを修正しました<br>
                                        スマホサイズでのデザインを調整しました<br>
                                        <a href="{{ route('article.index') }}" title="記事ページ">記事ページ</a>に視聴回数を表示しました<br>
                                        ファビコンを設定しました<br>
                                        アイコン（自作）を設定しました
                                    </td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12">24日</td>
                                    <td class="p-12"><a href="{{ route('article.index') }}" title="記事">記事</a>に画像が表示されるようになりました<br>タブレットサイズのデザインを調整しました</td>
                                </tr>
                                <tr class="va-b">
                                    <td class="p-12" style="width: 80px;">23日</td>
                                    <td class="p-12">本番リリースを行いました</td>
                                </tr>
                            </table>
                        </div>
                        {{-- <h2 class="mt-24">信長の野望 新生が発売される7/21までにアップしたい項目</h2>
                        <ul class="mt-12">
                            <li style="text-decoration: line-through;">武将詳細情報の整備</li>
                            <li style="text-decoration: line-through;">30記事作成（30/30）</li>
                            <li>新生用攻略ページの準備</li>
                            <li style="text-decoration: line-through;">サムネの変更</li>
                            <li>勢力図の遷移ページの作成</li>
                            <li style="text-decoration: line-through;">能力値ランキングページの作成</li>
                            <li>創造の武将情報登録</li>
                            <li>戦法一覧ページの作成</li>
                            <li style="text-decoration: line-through;">Google広告の設置</li>
                            <li style="text-decoration: line-through;">ゲームの始め方ページの作成</li>
                            <li style="text-decoration: line-through;">大志の攻略記事の作成</li>
                            <li style="text-decoration: line-through;">meta discriptionの設置</li>
                            <li style="text-decoration: line-through;">Laravelバージョン9にアップデート</li>
                            <li style="text-decoration: line-through;">aタグにタイトル設定</li>
                            <li style="text-decoration: line-through;">合戦一覧ページの作成</li>
                            <li style="text-decoration: line-through;">アフィリエイトの貼り付け</li>
                            <li style="text-decoration: line-through;">httpsにリダイレクト</li>
                            <li style="text-decoration: line-through;">バグ処理 403ページ作成</li>
                            <li style="text-decoration: line-through;">サイトマップ作成</li>
                        </ul>
                        <h2 class="mt-24">戦略</h2>
                        <ul class="mt-12">
                            <li>大河ドラマ「どうする家康」のために、家康家臣の記事を優先で作成する</li>
                            <li>新生発売後すぐPV数を稼ぐために、ページを準備する</li>
                            <li>30記事毎に記事をリライト</li>
                            <li>武将診断ページを作成する</li>
                            <li>Twitterで武将列伝を投稿</li>
                        </ul> --}}
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'release'])
    </div>
@endsection