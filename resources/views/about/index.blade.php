@extends('layouts.layout')

@section('title', 'このサイトについて | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="「信長の野望 徹底攻略」の基本情報を記載しています。サイト名・トップページURL・管理人・サイト開設日・このサイトを開設した経緯について記載しています。">
    <meta property="og:description" content="「信長の野望 徹底攻略」の基本情報を記載しています。サイト名・トップページURL・管理人・サイト開設日・このサイトを開設した経緯について記載しています。" />
    <meta property="og:title" content="このサイトについて | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "このサイトについて | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <time>
                        <i class="far fa-calendar-check"></i><span class="calender">2022年03月23日</span>
                        <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年11月07日</span>
                    </time>
                    <h1>このサイトについて</h1>
        
                    <div class="main_contents">
                        <h2 class="mt-24 rich_h2">基本情報</h2>
                        <p class="mt-12">
                            サイト名
                        </p>
                        <p class="back_blue p-12">信長の野望 徹底攻略（のぶながのやぼう てっていこうりゃく）</p>
                        <p class="mt-12">
                            トップページURL
                        </p>
                        <p class="back_blue p-12"><a href="https://nobunaga-kouryaku.com" title="信長の野望 徹底攻略（のぶながのやぼう てっていこうりゃく）">https://nobunaga-kouryaku.com</a></p>
                        <p class="mt-12">
                            管理人
                        </p>
                        <p class="back_blue p-12">taka</p>
                        <p class="mt-12">
                            サイト開設日
                        </p>
                        <p class="back_blue p-12">2022年03月23日</p>

                        <h2 class="mt-24 rich_h2">管理人について</h2>
                        <p class="mt-12">管理人については<a href="{{ route('administrator') }}" title="管理人情報">管理人情報</a>をご確認ください。</p>
                        <h2 class="mt-24 rich_h2">このサイトを開設した経緯</h2>
                        <p class="mt-12">管理人の私は歴史が好きです。</p>
                        <p>まず「軍師 官兵衛」を見てから戦国時代が好きです。そして山口県の萩市に住んでいたこともあり、明治維新付近も好きです。今は福岡に住んでおり、元寇の防塁跡地が近くにあるので最近は元寇について学んでいます。</p>
                        <p>とにかく歴史が好きです。</p>
                        <p class="mt-12">またゲームが好きで、その中でも「信長の野望」は特に好きです。</p>
                        <p>情報発信したいなと思い「<a href="{{ route('top') }}" title="信長の野望 徹底攻略">信長の野望 徹底攻略</a>」を作成しました。</p>
                        <p>ただ攻略情報を載せるwebサイトを作るのではなく、歴史が学べたり好きな武将が見つかるそんなwebサイトにしたいと思っています。</p>
                        <p>至らぬところもあると思いますが、応援お願いいたします！</p>
                        <h2 class="mt-24 rich_h2">お仕事依頼</h2>
                        <h3 class="mt-12 rich_h3">サイト制作</h3>
                        <p>webサイト制作承ります！</p>
                        <p>この「信長の野望 徹底攻略」のようなサイトや、WordPressで作るブログやコーポレートサイトなど幅広く対応できます。</p>
                        <p>相談・希望の方は<a href="{{ route('contact') }}" title="お問い合わせ">お問い合わせ</a>からご連絡お願いします！</p>
                        <h3 class="mt-12 rich_h3">エンジニア育成</h3>
                        <p>管理人はエンジニアの本職の方で月給100万を越しました。</p>
                        <p>エンジニアに転職したい、副業したいという方の勉強のサポートをしています。</p>
                        <p>MENTAというサイトで、受付していますのでよかったら見てみてください。</p>
                        <p>MENTAでのプロフィールページは<a href="https://menta.work/user/59584" target="_blank">こちら</a></p>

                        <h2 class="mt-24 rich_h2">リリース情報</h2>
                        <p class="mt-12"><a href="{{ route('release') }}" title="リリース情報">リリース情報</a>をご確認ください。</p>
                        <p>まだまだデータが不足していますので、随時アップデートしていきます！</p>
                        <p>間違いなどありましたら、<a href="{{ route('contact') }}" title="お問い合わせ">お問い合わせ</a>からご連絡お願いします！</p>

                        <h2 class="mt-24 rich_h2">プライバシーポリシー</h2>
                        <p class="mt-12"><a href="{{ route('privacy') }}" title="プライバシーポリシー">プライバシーポリシー</a>をご確認ください。</p>

                        <h2 class="mt-24 rich_h2">当サイトはリンクフリーです</h2>
                        <p class="mt-12">
                            当サイトは管理人の許可なくリンクを貼って頂いて問題ありません。<br>
                            内容についての引用、イラスト、スクリーンショットのご利用も、該当記事へ「引用または出典」として（信長の野望 徹底攻略）へリンクを貼って頂くようお願い致します。
                        </p>
                        <h2 class="mt-24 rich_h2">引用</h2>
                        <p>戦国合戦史辞典 存亡を賭けた戦国864の戦い</p>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'about'])
    </div>
@endsection