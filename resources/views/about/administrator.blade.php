@extends('layouts.layout')

@section('title', '管理人について | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望 徹底攻略」の管理人について説明しているページです。普段何をやっているか、「信長の野望」のプレイ履歴、姉妹サイトを記載し、管理人への要望の受付を行っています。">
    <meta property="og:description" content="「信長の野望 徹底攻略」の管理人について説明しているページです。普段何をやっているか、「信長の野望」のプレイ履歴、姉妹サイトを記載し、管理人への要望の受付を行っています。" />
    <meta property="og:title" content="管理人について | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "管理人について | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <h1>管理人について</h1>
                    <div class="main_contents">
                        <h2 class="mt-24 rich_h2">何をやっている人？</h2>
                        <div class="mt-12 d-f sp_block">
                            <div>
                                <p>
                                    現在フリーランスとして活躍しています。<br>
                                    webサイトの開発をやっているので、お仕事依頼の方は<a href="{{ route('contact') }}" title="お問合わせ">お問い合わせ</a>からよろしくお願いいたします！
                                </p>
                                <ul class="mt-24">
                                    <li>生まれ： 1993年生まれ</li>
                                    <li>出身地： 兵庫県</li>
                                    <li>住まい：福岡県</li>
                                    <li>趣味：旅行・歴史</li>
                                    <li>好きな人物：黒田官兵衛</li>
                                </ul>
                                <p class="mt-24">あれ、誰かが兵庫生まれで晩年福岡に住んでいたような...</p>
                            </div>
                            <div>
                                <img src="{{ asset('img/common/taka.png')}}" alt="信長の野望 徹底攻略の管理人 taka" width="100%" height="100%">
                            </div>
                        </div>
                        <h2 class="mt-24 rich_h2">「信長の野望」のプレイ履歴</h2>
                        <p class="mt-24">
                            父が「革新」をやっているを見つけたのが、信長の野望に出会ったきっかけです。<br>
                            自分用のセーブデータを作り、毎日毎日母に怒られるまで全国統一に励みました。<br>
                            その後「天道」をプレイした時は、鉄砲部隊が強すぎたので、鉄砲を利用しない縛りをしたり、足軽・弓部隊のみの縛りをしたりして遊んでいました。
                        </p>
                        <p class="mt-24">「創造」ももちろんプレイしましたが、一番プレイ時間が長かったのが「創造 戦国立志伝」です。今でもやります。</p>
                        <p>大名プレイよりも家臣プレイで始めるのが面白く、能力平均50くらいの武将を操作し、活躍し優秀な部下を揃え、主人を裏切り軍団を作るのはやはり楽しいですね。 </p>
                        <p class="mt-12">このサイトを立ち上げて「大志」も結構プレイするようになりました。</p>
                        <p>兵数上限を少なくしたら、大大名にも勝てる！</p>
                        <p class="mt-12">それにしても、家臣プレイ復活しないかな。</p>
                        <p class="mt-24">コーエーさんいつも楽しくプレイさせていただいています。今後も面白いゲームをお願いします！！</p>
                        <h2 class="mt-24 rich_h2">姉妹サイト</h2>
                        <p class="mt-24">雑記ブログ<a href="https://t-file.blog/" target="_blank" title="t-file">t-file</a>も運営していますので、こちらもよければ見ていってください。</p>
                        <h2 class="mt-24 rich_h2">管理人への要望</h2>
                        <p class="mt-24">こんな機能欲しいですとかこんな記事書いてくださいやサイトの作り方教えてくださいなど意見がありましたら、<a href="{{ route('contact') }}" title="お問い合わせ">お問い合わせ</a>からお願いいたします。</p>
                        <p>この攻略サイトがどうやって作っているか気になる人はいるのかな？需要があればサイトの作り方の記事を姉妹サイトの方で書こうかな。</p>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'administrator'])
    </div>
@endsection