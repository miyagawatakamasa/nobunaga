@extends('layouts.layout')

@section('title', '信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム信長の野望の攻略サイトです。ゲームのデータベース、攻略記事だけでなく、戦国時代についての記事・ランキング・投票機能・年表など各コンテンツがあります。「信長の野望 大志」や「信長の野望 新生」の攻略情報を扱っています。">
    <meta property="og:description" content="ゲーム信長の野望の攻略サイトです。ゲームのデータベース、攻略記事だけでなく、戦国時代についての記事・ランキング・投票機能・年表など各コンテンツがあります。「信長の野望 大志」や「信長の野望 新生」の攻略情報を扱っています。" />
    <meta property="og:title" content="信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/top.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebSite",
            "image": "{{ asset('img/top/nobu.png')}}",  
            "name": "信長の野望 徹底攻略",
            "url": "{{ url()->current() }}",
            "potentialAction": {
                "@type": "SearchAction",
                "target": "{{ url()->current() }}/search?q={search_term_string}",
                "query-input": "required name=search_term_string"
            }
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper back_white p-12">
        <div class="top_contents mt-24">
            <div class="top_left_contents">
                <div class="main_contents">
                    <div class="sp_display">
                        <livewire:search />
                    </div>
                    <h1 class="main_visual">
                        <picture>
                            <source type="image/webp" srcset="{{ asset('img/top/nobu.webp')}}">
                            <img src="{{ asset('img/top/nobu.png')}}" alt="信長の野望 徹底攻略" width="100%" height="100%">
                        </picture>
                        <p class="main_title">信長の野望 徹底攻略</p>
                    </h1>
                      <p class="small_text">※メインビジュアルのイラスト1000円とかで書いてくれる人いないかな？<br><a href="{{ route('contact') }}" class="" title="お問合わせ">お問合わせ</a>から連絡お願いしまーす！</p>
                    <h2 class="rich_h2 mt-12 d-f ai-c">
                        <img src="{{ asset('img/common/kouryaku.png')}}" alt="攻略アイコン" width="30px" height="30px" class="mr-8">
                        <p>攻略【新信長の野望】</p>
                    </h2>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.strategy.beginner') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="序盤の進め方">
                                <img src="{{ asset('img/common/beginner.png')}}" alt="初心者アイコン" width="30px" height="30px" class="mr-8">
                                <p>序盤の進め方</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.ranking') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="武将最強ランキング">
                                <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                                <p>武将最強ランキング</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.strategy.resemara') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="リセマラランキング">
                                <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                                <p>リセマラランキング</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.strategy.mukakin') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="無課金キャラランキング">
                                <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                                <p>無課金キャラランキング</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.gacha.kokushimusou') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="国士無双ガチャおすすめ">
                                <img src="{{ asset('img/common/gacha.png')}}" alt="ガチャアイコン" width="30px" height="30px" class="mr-8">
                                <p>国士無双ガチャおすすめ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.strategy.heirloom') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="家宝システム">
                                <img src="{{ asset('img/common/heirloom.png')}}" alt="家宝アイコン" width="30px" height="30px" class="mr-8">
                                <p>家宝システム</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="武将名鑑">
                                <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                                <p>武将名鑑</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'captain') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="主将キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/captain.png') }}" alt="主将" width="30px" height="100%" class="mr-8">
                                <p>主将キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'staff') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="参謀キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/staff.png') }}" alt="参謀" width="30px" height="100%" class="mr-8">
                                <p>参謀キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'pioneer') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="先鋒キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/pioneer.png') }}" alt="先鋒" width="30px" height="100%" class="mr-8">
                                <p>先鋒キャラ一覧</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'spear-soldier') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="槍兵キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/spear-soldier.png') }}" alt="槍兵" width="30px" height="30px" class="mr-8">
                                <p>槍兵キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'cavalry') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="騎兵キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/cavalry.png') }}" alt="騎兵" width="30px" height="30px" class="mr-8">
                                <p>騎兵キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'ninja') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="忍者キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/ninja.png') }}" alt="忍者" width="30px" height="30px" class="mr-8">
                                <p>忍者キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'gun') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="鉄砲キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/gun.png') }}" alt="鉄砲" width="30px" height="30px" class="mr-8">
                                <p>鉄砲キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'odzu') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="大筒キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/odzu.png') }}" alt="大筒" width="30px" height="30px" class="mr-8">
                                <p>大筒キャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'farmer') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="農兵キャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/farmer.png') }}" alt="農兵" width="30px" height="30px" class="mr-8">
                                <p>農兵キャラ一覧</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'ssr') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="SSRキャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/ssr.png') }}" alt="SSR" width="30px" height="30px" class="mr-8">
                                <p>SSRキャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'sr') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="SRキャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/sr.png') }}" alt="SR" width="30px" height="30px" class="mr-8">
                                <p>SRキャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'r') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="Rキャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/r.png') }}" alt="R" width="30px" height="30px" class="mr-8">
                                <p>Rキャラ一覧</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('shinnobunaga.database.samurais.show.rarity', 'n') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="Nキャラ一覧">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/n.png') }}" alt="N" width="30px" height="30px" class="mr-8">
                                <p>Nキャラ一覧</p>
                            </a>
                        </li>
                    </ul>
                    <h2 class="rich_h2 mt-36 d-f ai-c">
                        <img src="{{ asset('img/common/db.png')}}" alt="データベースアイコン" width="30px" height="30px" class="mr-8">
                        <p>データベース【新生】</p>
                    </h2>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('database.samurais', 'shinsei') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="武将名鑑">
                                <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                                <p>武将名鑑</p>
                            </a>
                        </li>
                    </ul>

                    <h2 class="rich_h2 mt-36 d-f ai-c">
                        <img src="{{ asset('img/common/db.png')}}" alt="データベースアイコン" width="30px" height="30px" class="mr-8">
                        <p>データベース【大志パワーアップキット】</p>
                    </h2>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('database.samurais', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="武将名鑑">
                                <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                                <p>武将名鑑</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('scenario.index', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="シナリオ">
                                <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                                <p>シナリオ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('tactics.index', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="戦法">
                                <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法アイコン" width="30px" height="30px" class="mr-8">
                                <p>戦法</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('taishi-pk.strategy.index') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="作戦">
                                <img src="{{ asset('img/common/strategy.png')}}" alt="作戦アイコン" width="30px" height="30px" class="mr-8">
                                <p>作戦</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('tensyu.index') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="天守">
                                <img src="{{ asset('img/common/tensyu.png')}}" alt="天守アイコン" width="30px" height="30px" class="mr-8">
                                <p>天守</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('facility.index', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="設備">
                                <img src="{{ asset('img/common/facility.png')}}" alt="設備アイコン" width="30px" height="30px" class="mr-8">
                                <p>設備</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('building.index', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="施設">
                                <img src="{{ asset('img/common/building.png')}}" alt="施設アイコン" width="30px" height="30px" class="mr-8">
                                <p>施設</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('resource.index') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="資源">
                                <img src="{{ asset('img/common/resource.png')}}" alt="資源アイコン" width="30px" height="30px" class="mr-8">
                                <p>資源</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('taimei.index') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="大命">
                                <img src="{{ asset('img/common/taimei.png')}}" alt="大命アイコン" width="30px" height="30px" class="mr-8">
                                <p>大命</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('database.will', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="志と特性">
                                <img src="{{ asset('img/common/will.png')}}" alt="志と特性アイコン" width="30px" height="30px" class="mr-8">
                                <p>志と特性</p>
                            </a>
                        </li>
                    </ul>
                    <h2 class="rich_h2 mt-36 d-f ai-c">
                        <img src="{{ asset('img/common/kouryaku.png')}}" alt="攻略情報アイコン" width="30px" height="30px" class="mr-8">
                        <p>攻略情報【大志パワーアップキット】</p>
                    </h2>
                    <ul class="main_contents_ul mt-12">
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('database.ranking', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="能力値ランキング">
                                <img src="{{ asset('img/common/ranking.png')}}" alt="能力値ランキング" width="30px" height="30px" class="mr-8">
                                <p>能力値ランキング</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('scenario.recommend', 'taishi-pk') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="おすすめシナリオ">
                                <img src="{{ asset('img/common/scenario.png')}}" alt="おすすめシナリオ" width="30px" height="30px" class="mr-8">
                                <p>おすすめシナリオ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.military') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="軍事のコツ">
                                <img src="{{ asset('img/common/gunshi.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>軍事のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.march') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="行軍のコツ">
                                <img src="{{ asset('img/common/strategy.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>行軍のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.decisive_battle') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="決戦のコツ">
                                <img src="{{ asset('img/common/strategy.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>決戦のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.siege') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="攻城戦のコツ">
                                <img src="{{ asset('img/common/tensyu.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>攻城戦のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.plot') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="調略のコツ">
                                <img src="{{ asset('img/common/makimono.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>調略のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.domestic_affairs') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="内政のコツ">
                                <img src="{{ asset('img/common/scenario.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>内政のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.commercial') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="商業のコツ">
                                <img src="{{ asset('img/common/commercial.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>商業のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.agriculture') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="農業のコツ">
                                <img src="{{ asset('img/common/agriculture.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>農業のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.diplomacy') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="外交のコツ">
                                <img src="{{ asset('img/common/scenario.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>外交のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.general_contract') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="普請のコツ">
                                <img src="{{ asset('img/common/tensyu.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>普請のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.human_resources') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="人事のコツ">
                                <img src="{{ asset('img/common/people.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>人事のコツ</p>
                            </a>
                        </li>
                        <li class="main_contents_li mt-12">
                            <a href="{{ route('capture.will') }}" class="main_contents_anchor shadow shadow_hover d-f ai-c" title="志について">
                                <img src="{{ asset('img/common/will.png')}}" alt="" width="30px" height="30px" class="mr-8">
                                <p>志について</p>
                            </a>
                        </li>
                    </ul>
    
                    {{-- 武将名鑑 --}}
                    {{-- 武将能力値ランキング --}}
                    {{-- シナリオ --}}
                    {{-- 言行録 --}}
                    {{-- 志と特性 --}}
                    {{-- 戦法一覧 --}}
                    {{-- 作戦一覧 --}}
                    {{-- 開発のポイント --}}
                    {{-- 政略のポイント --}}
                    {{-- 合戦の基本 --}}
                    {{-- 合戦のテクニック --}}
                    {{-- おすすめ大名 --}}
                    {{-- 小技・裏技集 --}}
                    {{-- 家宝一覧 --}}
                </div>
            </div>
            <div class="top_right_contents">
                <div class="pc_display">
                    <livewire:search />
                </div>
                {{-- <div class="mx-auto mt-24 sp-w-60"> --}}
                    {{-- google アドセンス --}}
                    {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                        crossorigin="anonymous"></script> --}}
                    <!-- 信長トップページサイドバー -->
                    {{-- <ins class="adsbygoogle"
                        style="display:block"
                        data-ad-client="ca-pub-2958289145034418"
                        data-ad-slot="1651028759"
                        data-ad-format="auto"
                        data-full-width-responsive="true"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script> --}}
                {{-- </div> --}}
                <div class="info shadow mt-24">
                    <h2>お知らせ</h2>
                    <ul class="info_ul">
                        @foreach ($Infos as $Info)
                            <li class="info_li">
                                <p class="info_time">{{ $Info->date->format('Y/m/d') }}</p>
                                <p>{!! $Info->contents !!}</p>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="mt-24 shadow">
                    <a href="{{ route('other.diagnose.index') }}">
                        <img src="{{ asset('img/diagnose/banner.png')}}" alt="武将診断テスト" width="100%" height="100%" class="d-b">
                    </a>
                </div>
                {{-- ゲームのアフィリエイト --}}
                <div class="mt-24 ta-c">
                    <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=nobunagakou06-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0BZTP474L&linkId=aae5964576c966183bab9689019e92fd"></iframe>
                    {{-- <iframe sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=tfile08-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B09WW1B5G8&linkId=f9ed3cfb5b876253bf4b353320e9ea69"></iframe> --}}
                </div>
            </div>
        </div>
        {{-- <section class="mt-24">
            <h2 class="rich_h2">ソフト別攻略</h2>
            開設中
        </section> --}}

        <section class="mt-36">
            <h2 class="rich_h2 d-f ai-c">
                <img src="{{ asset('img/common/new.png')}}" alt="new" width="30px" height="30px" class="mr-8">
                <p>信長の野望 新生 最新情報</p>
            </h2>
            {{-- <p class="mt-24">「信長の野望 新生」の<span class="color_red">予約が3/30より開始！！</span></p> --}}
            <p>PS4版の予約は<a title="PS4版の予約" href="https://www.amazon.co.jp/%E3%80%90PS4%E3%80%91%E4%BF%A1%E9%95%B7%E3%81%AE%E9%87%8E%E6%9C%9B%E3%83%BB%E6%96%B0%E7%94%9F-PC%E5%A3%81%E7%B4%99-%E9%85%8D%E4%BF%A1-%E6%97%A9%E6%9C%9F%E8%B3%BC%E5%85%A5%E7%89%B9%E5%85%B8%E3%82%B7%E3%83%8A%E3%83%AA%E3%82%AA%E3%80%8C%E9%95%B7%E7%AF%A0%E8%A8%AD%E6%A5%BD%E5%8E%9F%E3%81%AE%E6%88%A6%E3%81%84%E3%80%8D%E3%83%80%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%89%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%AB-%E3%80%90Amazon-co-jp%E9%99%90%E5%AE%9A%E3%80%91/dp/B09WW1B5G8?qid=1648732736&sr=8-3&linkCode=ll1&tag=tfile08-22&linkId=0a023c41dbcccb6fdeacd524be605778&language=ja_JP&ref_=as_li_ss_tl" target="_blank">こちら</a></p>
            <p>Switch版の予約は<a title="Switch版の予約" href="https://www.amazon.co.jp/dp/B09WXQKCK3?qid=1648732736&sr=8-1&linkCode=ll1&tag=tfile08-22&linkId=b6b35c8003c16ce21e0e0cf106117cb0&language=ja_JP&ref_=as_li_ss_tl" target="_blank">こちら</a></p>
            <ul class="d-f mt-12 sub_contents_ul">
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('database.samurais', 'shinsei') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="信長の野望 新生 武将名鑑">
                        <img src="{{ asset('img/common/samurai.png')}}" alt="new" width="30px" height="30px" class="mr-8">
                        <p>武将名鑑</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('shinsei.info') }}" class="normal_block_anchor shadow shadow_hover sub_contents_ancho d-f ai-c" title="信長の野望 新生 最新情報">
                        <img src="{{ asset('img/common/new.png')}}" alt="new" width="30px" height="30px" class="mr-8">
                        <p>最新情報</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('scenario.index', 'shinsei') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="信長の野望 新生 シナリオ一覧">
                        <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                        <p>シナリオ一覧</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('shinsei.policy') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="信長の野望 新生 政策一覧">
                        <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                        <p>政策一覧</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('shinsei.beginner') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="天正猿芝居シナリオ予想">
                        <img src="{{ asset('img/common/beginner.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>ゲームの始め方</p>
                    </a>
                </li>
                {{-- <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('shinsei.tensyou') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="天正猿芝居シナリオ予想">
                        <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                        <p>天正猿芝居シナリオ予想</p>
                    </a>
                </li> --}}
                {{-- <li class="mt-12 sub_contents_li ta-c">
                    <a href="https://www.gamecity.ne.jp/shinsei/fight.html" target="_blank" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="戦闘">
                        <img src="{{ asset('img/common/fight.png')}}" alt="戦闘アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦闘</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="https://www.gamecity.ne.jp/shinsei/strategy.html" target="_blank" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="戦略">
                        <img src="{{ asset('img/common/gunshi.png')}}" alt="戦略アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦略</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="https://www.gamecity.ne.jp/shinsei/battle.html" target="_blank" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="合戦">
                        <img src="{{ asset('img/common/strategy.png')}}" alt="合戦アイコン" width="30px" height="30px" class="mr-8">
                        <p>合戦</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="https://www.gamecity.ne.jp/shinsei/domestic.html" target="_blank" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="内政">
                        <img src="{{ asset('img/common/domestic.png')}}" alt="内政アイコン" width="30px" height="30px" class="mr-8">
                        <p>内政</p>
                    </a>
                </li> --}}
            </ul>
        </section>
        <section class="mt-36">
            <h2 class="rich_h2 d-f ai-c">
                <img src="{{ asset('img/common/scenario.png')}}" alt="記事アイコン" width="30px" height="30px" class="mr-8">
                <p>新着記事</p>
            </h2>
            <ul class="card_ul">
                @foreach ($NewArticles as $NewArticle)
                    <li class="card_list wide_card mt-24">
                        <a href="{{ route('article.single', $NewArticle->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $NewArticle->title }}">
                            <picture>
                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($NewArticle->id) }}" class="card_image">
                                <img src="{{ My_func::return_article_main_img_url($NewArticle->id) }}" width="100%" height="100%" class="card_image" alt="{{ $NewArticle->title }}" loading="lazy">
                            </picture>
                            <div class="card_bottom">
                                <div class="d-f j-sb card_info sp_block">
                                    <time>
                                        @if ($NewArticle->is_updated)
                                            {{ $NewArticle->additioned_at->format('Y年m月d日') }}
                                        @else
                                            {{ $NewArticle->created_at->format('Y年m月d日') }}
                                        @endif
                                    </time>
                                    <p class="card_category">
                                        @foreach ($NewArticle->articleCategorySearches as $mainArticleCategorySearch)
                                            @if ($mainArticleCategorySearch->is_main_category)
                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                            @endif
                                        @endforeach
                                    </p>
                                </div>
                                <p class="card_title">{{ $NewArticle->title }}</p>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="mt-36 ta-c">
                <a href="{{ route('article.index') }}" title="一覧を見る">一覧を見る</a>
            </div>
        </section>

        <section class="mt-36">
            <h2 class="rich_h2 d-f ai-c">
                <img src="{{ asset('img/common/ranking.png')}}" alt="ランキングアイコン" width="30px" height="30px" class="mr-8">
                <p>戦国いろいろランキング</p>
            </h2>
            <ul class="card_ul">
                @foreach ($RankingArticleCategorySearches as $RankingArticleCategorySearch)
                    <li class="card_list wide_card mt-24">
                        <a href="{{ route('article.single', $RankingArticleCategorySearch->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $RankingArticleCategorySearch->article->title }}">
                            <picture>
                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($RankingArticleCategorySearch->article->id) }}" class="card_image">
                                <img src="{{ My_func::return_article_main_img_url($RankingArticleCategorySearch->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $RankingArticleCategorySearch->article->title }}" loading="lazy">
                            </picture>
                            <div class="card_bottom">
                                <div class="d-f j-sb card_info sp_block">
                                    <time>
                                        @if ($RankingArticleCategorySearch->article->is_updated)
                                            {{ $RankingArticleCategorySearch->article->additioned_at->format('Y年m月d日') }}
                                        @else
                                            {{ $RankingArticleCategorySearch->article->created_at->format('Y年m月d日') }}
                                        @endif
                                    </time>
                                    <p class="card_category">
                                        @foreach ($RankingArticleCategorySearch->article->articleCategorySearches as $mainArticleCategorySearch)
                                            @if ($mainArticleCategorySearch->is_main_category)
                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                            @endif
                                        @endforeach
                                    </p>
                                </div>
                                <p class="card_title">{{ $RankingArticleCategorySearch->article->title }}</p>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="mt-36 ta-c">
                <a href="{{ route('article.category.index', 'ranking') }}" title="一覧を見る">一覧を見る</a>
            </div>
        </section>

        {{-- <section class="mt-24">
            <h2 class="rich_h2">シナリオ掲示板</h2>
            開設中
        </section> --}}

        <section class="mt-36">
            <h2 class="rich_h2 d-f ai-c">
                <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
                <p>投票キャンペーン</p>
            </h2>
            <p class="mt-24"><a href="{{ route('vote_campaign.index') }}" title="投票キャンペーン">投票キャンペーン</a>とは、いろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツです。</p>
            <ul class="card_ul">
                @foreach ($VoteCampaigns as $VoteCampaign)
                    <li class="card_list wide_card mt-24">
                        <a href="{{ route('vote_campaign.show', $VoteCampaign->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $VoteCampaign->title }}">
                            <picture>
                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($VoteCampaign->id) }}" class="card_image">
                                <img src="{{ My_func::return_article_main_img_url($VoteCampaign->id) }}" width="100%" height="100%" class="card_image" alt="{{ $VoteCampaign->title }}" loading="lazy">
                            </picture>
                            <div class="card_bottom">
                                <div class="d-f j-sb card_info">
                                    <time>{{ $VoteCampaign->updated_at->format('Y年m月d日') }}</time>
                                </div>
                                <p class="card_title">{{ $VoteCampaign->title }}</p>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </section>

        {{-- <section class="mt-24">
            <h2 class="rich_h2">イラスト・ギャラリー</h2>
            開設中
        </section> --}}

        {{-- <section class="mt-24">
            <h2 class="rich_h2">アンケート</h2>
            開設中
        </section> --}}

        {{-- 合戦 --}}

        {{-- 姫 --}}

        {{-- 家紋一覧 --}}

        {{-- 購入はこちら --}}

        {{-- <section class="mt-36">
            <h2 class="rich_h2">イラスト募集</h2>
            <p class="mt-12">準備中</p>
        </section> --}}

        <section class="mt-36">
            <h2 class="rich_h2 d-f ai-c">
                <img src="{{ asset('img/common/contents.png')}}" alt="コンテンツアイコン" width="30px" height="30px" class="mr-8">
                <p>その他コンテンツ</p>
            </h2>
            <ul class="d-f mt-12 sub_contents_ul">
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('article.index') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="記事一覧">
                        <img src="{{ asset('img/common/scenario.png')}}" alt="記事アイコン" width="30px" height="30px" class="mr-8">
                        <p>記事一覧</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.timeline') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="戦国年表">
                        <img src="{{ asset('img/common/timeline.png')}}" alt="戦国年表アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦国年表</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.game_title') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="ゲームタイトル一覧">
                        <img src="{{ asset('img/common/game.png')}}" alt="ゲームタイトル一覧アイコン" width="30px" height="30px" class="mr-8">
                        <p>ゲームタイトル一覧</p>
                    </a>
                </li>
                {{-- ゲームの始め方・購入方法 --}}
                {{-- <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.tactics') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c">
                        <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦法一覧</p>
                    </a>
                </li> --}}
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.war') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c">
                        <img src="{{ asset('img/common/strategy.png')}}" alt="合戦アイコン" width="30px" height="30px" class="mr-8">
                        <p>合戦一覧</p>
                    </a>
                </li>
                {{-- <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.war') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c">
                        <img src="{{ asset('img/common/gunshi.png')}}" alt="戦法アイコン" width="30px" height="30px" class="mr-8">
                        <p>勢力図の推移</p>
                    </a>
                </li> --}}
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('article.category.index', 'minor') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c">
                        <img src="{{ asset('img/common/samurai.png')}}" alt="マイナー武将列伝アイコン" width="30px" height="30px" class="mr-8">
                        <p>マイナー武将列伝</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.vassals') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="家臣団総称">
                        <img src="{{ asset('img/common/samurai.png')}}" alt="家臣団アイコン" width="30px" height="30px" class="mr-8">
                        <p>家臣団総称</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.diagnose.index') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="武将診断テスト">
                        <img src="{{ asset('img/common/diagnose.png')}}" alt="診断アイコン" width="30px" height="30px" class="mr-8">
                        <p>武将診断テスト</p>
                    </a>
                </li>
                <li class="mt-12 sub_contents_li ta-c">
                    <a href="{{ route('other.word') }}" class="normal_block_anchor shadow shadow_hover sub_contents_anchor d-f ai-c" title="武将診断テスト">
                        <img src="{{ asset('img/common/diagnose.png')}}" alt="用語集アイコン" width="30px" height="30px" class="mr-8">
                        <p>戦国時代用語集</p>
                    </a>
                </li>
            </ul>
        </section>
        
    </div>
@endsection