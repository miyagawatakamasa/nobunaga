@extends('layouts.layout')

@section('title', '' . $ShinseiPkSamuraiParameter->name . '【新生PK】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="このページは「信長の野望 新生PK（パワーアップキット）」の攻略データベース内の特定の武将の詳細ページです。信長の野望 新生PKに登場する「{{ $ShinseiPkSamuraiParameter->name }}」の情報、能力、スキルなどを確認することができます。">
    <meta property="og:description" content="{{ $ShinseiPkSamuraiParameter->samurai->retuden }}" />
    <meta property="og:title" content="「{{ $ShinseiPkSamuraiParameter->name }}」の武将パラメーター({{ $GameTitle->name }}) | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ My_func::return_min_samurai_img_url($ShinseiPkSamuraiParameter->samurai->id, $GameTitle->slug) }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "{{ $ShinseiPkSamuraiParameter->name }}",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/slick/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){    
            $('#slider').slick({
                arrows: false,
                centerMode: true,
            });
        });
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <p>武将名鑑【{{$GameTitle->name}}】</p>
                    <h1 class="mt-24 bb-blue">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                            <p class="h1">{{ $ShinseiPkSamuraiParameter->name }}（{{ $ShinseiPkSamuraiParameter->furigana }}）</p>
                        </div>
                    </h1>
                    <div class="mt-12 d-f j-sb ai-c">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('shinsei-pk.database.show', [$BeforeSamurai->samurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        <livewire:search />
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('shinsei-pk.database.show', [$NextSamurai->samurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <ul class="d-f font_small title_box mt-12">
                        @if (isset($HadouSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('hadou.database.samurais.show', $HadouSamuraiParameter->samurai->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/hadou.png')}}" alt="新信長アイコン" width="84px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        <li class="active title_li p-8">
                            <a href="" class="title_anchor active normal_block_anchor shadow_hover shadow d-b">
                                <img src="{{ asset('img/common/shinsei_pk.png')}}" alt="新生PKアイコン" width="78px" height="40px" class="title_img">
                            </a>
                        </li>
                        @if (isset($ShinseiSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei.database.samurais.show', $ShinseiSamuraiParameter->samurai->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei.png')}}" alt="新生アイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinnobuSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinnobu.png')}}" alt="新信長アイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($TaishiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('database.samurais.show' , ['taishi-pk', $ShinseiPkSamuraiParameter->samurai_id] ) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/taishi_pk.png')}}" alt="大志PKアイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                    </ul>
                    <div class="d-f mt-12 parameter_img_area">
                        <table class="font_small ta-c parameter_table">
                            <tr>
                                <th colspan="6">{{ $ShinseiPkSamuraiParameter->name }} の能力値</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n" style="min-width: 75px;">統率 
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の防御力、城の防御力に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiPkSamuraiParameter->leadership))
                                        <span class="status_bar" style="width: {{ $ShinseiPkSamuraiParameter->leadership }}px;"></span>{{ $ShinseiPkSamuraiParameter->leadership }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $ShinseiPkSamuraiParameter->samuraiid }}&sort=sinsei-leadership">{{ $ShinseiPkSamuraiParameter->leadership_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">武勇
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の攻撃力、強攻時に敵城に与えるダメージ、<br>城の攻撃力に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiPkSamuraiParameter->brave))
                                        <span class="status_bar" style="width: {{ $ShinseiPkSamuraiParameter->brave }}px;"></span>{{ $ShinseiPkSamuraiParameter->brave }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $ShinseiPkSamuraiParameter->samuraiid }}&sort=sinsei-brave">{{ $ShinseiPkSamuraiParameter->brave_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">知略
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">出陣部隊の包囲時のダメージ量、城の包囲時の防御力、<br>また戦法のダメージに影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiPkSamuraiParameter->wisdom))
                                        <span class="status_bar" style="width: {{ $ShinseiPkSamuraiParameter->wisdom }}px;"></span>{{ $ShinseiPkSamuraiParameter->wisdom }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $ShinseiPkSamuraiParameter->samuraiid }}&sort=sinsei-wisdom">{{ $ShinseiPkSamuraiParameter->wisdom_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">政務
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">城の収入に影響する。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l">
                                    @if (isset($ShinseiPkSamuraiParameter->affairs))
                                        <span class="status_bar" style="width: {{ $ShinseiPkSamuraiParameter->affairs }}px;"></span>{{ $ShinseiPkSamuraiParameter->affairs }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $ShinseiPkSamuraiParameter->samuraiid }}&sort=sinsei-affairs">{{ $ShinseiPkSamuraiParameter->affairs_rank }}</a> 位)</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">合計</th>
                                <td colspan="4" class="ta-l of-h">
                                    @if (isset($ShinseiPkSamuraiParameter->all))
                                        <span class="status_bar" style="width: {{ $ShinseiPkSamuraiParameter->all / 4 }}px;"></span>{{ $ShinseiPkSamuraiParameter->all }} <span class="font_min ml-24">(<a href="{{ route('database.ranking', 'shinsei') }}?samurai-id={{ $ShinseiPkSamuraiParameter->samuraiid }}&sort=sinsei-all">{{ $ShinseiPkSamuraiParameter->all_rank }}</a> 位)</span><span class="f-r font_min">2201人中</span>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="samurai_img_area d-f">
                            {{-- @if ($imgCount == 0) --}}
                                <div class="d-f ai-c j-c w-100">
                                    <picture>
                                        <source type="image/webp" srcset="{{ My_func::return_min_samurai_img_webp_url($ShinseiPkSamuraiParameter->samurai->id, "shinsei-pk") }}">
                                        <img src="{{ My_func::return_min_samurai_img_url($ShinseiPkSamuraiParameter->samurai->id, "shinsei-pk") }}" alt="{{ $ShinseiPkSamuraiParameter->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img">
                                    </picture>
                                </div>
                            {{-- @else
                                <div id="slider" class="d-f ai-c">
                                    @for ($i = 0; $i < ($imgCount + 1); $i++)
                                        @if ($i == 0)
                                            <div class="d-f ai-c j-c">
                                                <picture>
                                                    <source type="image/webp" srcset="{{ Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $ShinseiPkSamuraiParameter->samuraiid . '.webp') }}">
                                                    <img src="{{ Storage::disk('s3')->url('img/shinsei/samurai/' . $ShinseiPkSamuraiParameter->samuraiid . '.jpg') }}" alt="{{ $ShinseiPkSamuraiParameter->name }}のグラフィック画像1" width="80%" height="100%" class="m-a samurai_img">
                                                </picture>
                                            </div>
                                        @else
                                            <div class="d-f ai-c j-c add_img">
                                                <picture>
                                                    <source type="image/webp" srcset="{{ Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $ShinseiPkSamuraiParameter->samuraiid . '_' . $i . '.webp') }}">
                                                    <img src="{{ Storage::disk('s3')->url('img/shinsei/samurai/' . $ShinseiPkSamuraiParameter->samuraiid . '_' . $i . '.jpg') }}" alt="{{ $ShinseiPkSamuraiParameter->name }}のグラフィック画像{{ $i + 1 }}" width="80%" height="100%" class="m-a samurai_img">
                                                </picture>
                                            </div>
                                        @endif
                                    @endfor
                                </div>
                            @endif --}}
                        </div>
                    </div>
                    {{-- @include('components.samurai_basic_data', ['Samurai' => $Samurai]) --}}

                    <div class="mt-36 ta-c pc_display">
                        <div id="im-af643d07e0944e0b8a908c2accf2d86a">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1808041,type:"banner",display:"inline",elementid:"im-af643d07e0944e0b8a908c2accf2d86a"})</script>
                        </div>
                    </div>
                    <div class="mt-36 ta-c sp_display">
                        <div id="im-7181044169e0457d9a2a4bb3b1c8337f">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1808042,type:"banner",display:"inline",elementid:"im-7181044169e0457d9a2a4bb3b1c8337f"})</script>
                        </div>
                    </div>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th colspan="6" style="width: 50%;">その他のデータ</th>
                            <th colspan="6" style="width: 50%;">列伝</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">誕生年</th>
                            <td colspan="4">{{ $ShinseiPkSamuraiParameter->samurai->birth_year }}年</td>
                            <td colspan="6" rowspan="3" class="ta-l">{!! $ShinseiPkSamuraiParameter->samurai->retuden_html !!}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">死亡年</th>
                            <td colspan="4">{{ $ShinseiPkSamuraiParameter->samurai->death_year }}年</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">主義
                                <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">革新、中道、保守の3種類ある。忠誠の増減に関係する。<br>また、政策の発令条件になっている。</p>
                                    </div>
                                </span>
                            </th>
                            <td colspan="4">{{ $ShinseiPkSamuraiParameter->policy }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">特性
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">武将の個性。政略、軍事など、様々な場面で効果を得られる。</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->characteristic1 }}</th>
                            <td colspan="10" class="ta-l">{{ $characteristic1->description }}</td>
                        </tr>
                        @if (isset($ShinseiPkSamuraiParameter->characteristic2))
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->characteristic2 }}</th>
                                <td colspan="10" class="ta-l">{{ $characteristic2->description }}</td>
                            </tr>
                        @endif
                        @if (isset($ShinseiPkSamuraiParameter->characteristic3))
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->characteristic3 }}</th>
                                <td colspan="10" class="ta-l">{{ $characteristic3->description }}</td>
                            </tr>
                        @endif
                        <tr>
                            <th colspan="12">戦法
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">合戦で使える特殊な技。<br>部隊を強化したり、ダメージを与えたり、<br class="sp_display">様々な効果を得られる。</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->tactics }}</th>
                            <td colspan="10" class="ta-l">{{ $tactics->description }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">主義
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">革新、中道、保守の3種類ある。忠誠の増減に関係する。<br>また、政策の発令条件になっている。</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->head_of_house }}</th>
                            <td colspan="10" class="ta-l">{{ $headOfHouse->description }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">奉行
                                <span class="info_icon"><img src="{{ asset('img/common/info_white.png')}}" alt="" width="12px" height="12px">
                                    <div class="baloon_text_block p-0">
                                        <p class="baloon_text ta-l top_ballon">準備中</p>
                                    </div>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $ShinseiPkSamuraiParameter->magistrate }}</th>
                            <td colspan="10" class="ta-l">{{ $magistrate->description }}</td>
                        </tr>
                    </table>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th colspan="12">シナリオ</th>
                        </tr>
                        <tr>
                            <th colspan="3" class="back_cloud_blue color_font_black fw-n">年月</th>
                            <th colspan="3" class="back_cloud_blue color_font_black fw-n">シナリオ</th>
                            <th colspan="3" class="back_cloud_blue color_font_black fw-n">年齢</th>
                            <th colspan="3" class="back_cloud_blue color_font_black fw-n">城</th>
                        </tr>
                        <tr>
                            <td colspan="3">1546年1月</td>
                            <td colspan="3">信長元服</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->nobugen_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->nobugen_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1553年4月</td>
                            <td colspan="3">尾張統一</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->owarito_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->owarito_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1560年4月</td>
                            <td colspan="3">桶狭間の戦い</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->oke_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->oke_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1567年8月</td>
                            <td colspan="3">天下布武</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->tenka_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->tenka_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1570年4月</td>
                            <td colspan="3">信長包囲網</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->houi_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->houi_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1572年12月</td>
                            <td colspan="3">三方ヶ原の戦い</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->mikata_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->mikata_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1582年5月</td>
                            <td colspan="3">夢幻の如く</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->yume_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->yume_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1591年10月</td>
                            <td colspan="3">天下無事ならず</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->tenkabuzi_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->tenkabuzi_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1600年7月</td>
                            <td colspan="3">関ヶ原の戦い</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->seki_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->seki_shiro }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">1614年8月</td>
                            <td colspan="3">大坂の陣</td>
                            <td colspan="3">{{ My_func::return_entry_status($ShinseiPkSamuraiParameter->osaka_age) }}</td>
                            <td colspan="3">{{ $ShinseiPkSamuraiParameter->osaka_shiro }}</td>
                        </tr>
                    </table>
                    
                    <div class="mt-24 d-f j-sb">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('shinsei-pk.database.show', [$BeforeSamurai->samurai->id]) }}" class="font_small"><{{ $BeforeSamurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('shinsei-pk.database.show', [$NextSamurai->samurai->id]) }}" class="font_small">{{ $NextSamurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    @if (count($WarSamurais))
                        <div class="mt-24 ta-c">
                            <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=nobunagakou06-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0BZTP474L&linkId=aae5964576c966183bab9689019e92fd"></iframe>
                        </div>
                        <section class="mt-36 war_samurai">
                            <h2 class="rich_h2">{{ $ShinseiPkSamuraiParameter->name }}が登場する合戦</h2>
                            <ul>
                                @foreach ($WarSamurais as $WarSamurai)
                                    <li class="mt-24">
                                        <h3 class="rich_h3"><ruby>{{ $WarSamurai->war->name }}<rt>{{ $WarSamurai->war->kana }}</rt></ruby>　[<a href="{{ route('other.war.single', $WarSamurai->war->disturbance->id) }}">{{ $WarSamurai->war->disturbance->name }}</a>]</h3>
                                        @include('components.war', ['war' => $WarSamurai->war])
                                        <p class="ta-r mt-12">
                                            <a href="{{ route('other.war.single', $WarSamurai->war->disturbance_id) }}">
                                                詳細を見る
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    @if (count($SamuraiRelatedArticles))
                        <div class="mt-24 ta-c">
                            <iframe title="ad" sandbox="allow-popups allow-scripts allow-modals allow-forms allow-same-origin" style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=nobunagakou06-22&language=ja_JP&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0BZTP474L&linkId=aae5964576c966183bab9689019e92fd"></iframe>
                        </div>
                        <section class="mt-36">
                            <h2 class="rich_h2">{{ $ShinseiPkSamuraiParameter->name }}が登場する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24 narrow_card">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($SamuraiRelatedArticle->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($SamuraiRelatedArticle->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $SamuraiRelatedArticle->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>{{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}</time>
                                                    <p class="card_category">
                                                        @foreach ($SamuraiRelatedArticle->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif
                    <div class="mt-24">
                        <ins class="adsbygoogle"
                        style="display:block"
                        data-ad-format="fluid"
                        data-ad-layout-key="-do+95-1f-g0+ye"
                        data-ad-client="ca-pub-2958289145034418"
                        data-ad-slot="5115790106"></ins>
                        <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinsei-pk.database.show', 'argument1' => $GameTitle, 'argument2' => $ShinseiPkSamuraiParameter])
                </article>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection