@extends('layouts.layout')

@section('title', '武将名鑑【新生 PK】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="一覧です。レアリティ・属性を一覧で出しています！新生 PKで武将を探す際の参考にしてください。">
    <meta property="og:description" content="一覧です。レアリティ・能力値を一覧で出しています！新生 PKで武将を探す際の参考にしてください。" />
    <meta property="og:title" content="武将名鑑【新生 PK】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将名鑑【新生 PK】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/samurai_kana.js') }}"></script>
@endsection

@section('content')
<div class="inner inner_wrapper">
    <div class="single_contents">
        @include('components.game_aside')
        <div class="left_contents">
            <article class="article_contents">
                <h1 id="title" class="h1 d-f ai-c">
                    <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                    <p>武将名鑑【新生 PK】</p>
                </h1>
                <div class="mt-12">
                    <livewire:search />
                </div>
                {{-- <div class="mt-12">
                    <livewire:shinsei-pk-samurai />
                </div> --}}
                <div class="search_area back_white">
                    <div class="mt-12">
                        <ul class="d-f kana_ul">
                            <li class="kana_list">
                                <a href="" class="kana_anchor active">あ</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">か</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">さ</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">た</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">な</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">は</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">ま</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">や</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">ら</a>
                            </li>
                            <li class="kana_list">
                                <a href="" class="kana_anchor">わ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="mt-12">
                        <ul class="shiin d-f">
                            <li class="shiin_li main" data-shiin="あ" data-boin-group="あ">
                                <a href="" class="shiin_anchor active">あ</a>
                            </li>
                            <li class="shiin_li main" data-shiin="い" data-boin-group="あ">
                                <a href="" class="shiin_anchor">い</a>
                            </li>
                            <li class="shiin_li main" data-shiin="う" data-boin-group="あ">
                                <a href="" class="shiin_anchor">う</a>
                            </li>
                            <li class="shiin_li main" data-shiin="え" data-boin-group="あ">
                                <a href="" class="shiin_anchor">え</a>
                            </li>
                            <li class="shiin_li main" data-shiin="お" data-boin-group="あ">
                                <a href="" class="shiin_anchor">お</a>
                            </li>
                            <li class="shiin_li" data-shiin="か" data-boin-group="か">
                                <a href="" class="shiin_anchor active">か</a>
                            </li>
                            <li class="shiin_li" data-shiin="き" data-boin-group="か">
                                <a href="" class="shiin_anchor">き</a>
                            </li>
                            <li class="shiin_li" data-shiin="く" data-boin-group="か">
                                <a href="" class="shiin_anchor">く</a>
                            </li>
                            <li class="shiin_li" data-shiin="け" data-boin-group="か">
                                <a href="" class="shiin_anchor">け</a>
                            </li>
                            <li class="shiin_li" data-shiin="こ" data-boin-group="か">
                                <a href="" class="shiin_anchor">こ</a>
                            </li>
                            <li class="shiin_li" data-shiin="さ" data-boin-group="さ">
                                <a href="" class="shiin_anchor active">さ</a>
                            </li>
                            <li class="shiin_li" data-shiin="し" data-boin-group="さ">
                                <a href="" class="shiin_anchor">し</a>
                            </li>
                            <li class="shiin_li" data-shiin="す" data-boin-group="さ">
                                <a href="" class="shiin_anchor">す</a>
                            </li>
                            <li class="shiin_li" data-shiin="せ" data-boin-group="さ">
                                <a href="" class="shiin_anchor">せ</a>
                            </li>
                            <li class="shiin_li" data-shiin="そ" data-boin-group="さ">
                                <a href="" class="shiin_anchor">そ</a>
                            </li>
                            <li class="shiin_li" data-shiin="た" data-boin-group="た">
                                <a href="" class="shiin_anchor active">た</a>
                            </li>
                            <li class="shiin_li" data-shiin="ち" data-boin-group="た">
                                <a href="" class="shiin_anchor">ち</a>
                            </li>
                            <li class="shiin_li" data-shiin="つ" data-boin-group="た">
                                <a href="" class="shiin_anchor">つ</a>
                            </li>
                            <li class="shiin_li" data-shiin="て" data-boin-group="た">
                                <a href="" class="shiin_anchor">て</a>
                            </li>
                            <li class="shiin_li" data-shiin="と" data-boin-group="た">
                                <a href="" class="shiin_anchor">と</a>
                            </li>
                            <li class="shiin_li" data-shiin="な" data-boin-group="な">
                                <a href="" class="shiin_anchor active">な</a>
                            </li>
                            <li class="shiin_li" data-shiin="に" data-boin-group="な">
                                <a href="" class="shiin_anchor">に</a>
                            </li>
                            <li class="shiin_li" data-shiin="ぬ" data-boin-group="な">
                                <a href="" class="shiin_anchor">ぬ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ね" data-boin-group="な">
                                <a href="" class="shiin_anchor">ね</a>
                            </li>
                            <li class="shiin_li" data-shiin="の" data-boin-group="な">
                                <a href="" class="shiin_anchor">の</a>
                            </li>
                            <li class="shiin_li" data-shiin="は" data-boin-group="は">
                                <a href="" class="shiin_anchor active">は</a>
                            </li>
                            <li class="shiin_li" data-shiin="ひ" data-boin-group="は">
                                <a href="" class="shiin_anchor">ひ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ふ" data-boin-group="は">
                                <a href="" class="shiin_anchor">ふ</a>
                            </li>
                            <li class="shiin_li" data-shiin="へ" data-boin-group="は">
                                <a href="" class="shiin_anchor">へ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ほ" data-boin-group="は">
                                <a href="" class="shiin_anchor">ほ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ま" data-boin-group="ま">
                                <a href="" class="shiin_anchor active">ま</a>
                            </li>
                            <li class="shiin_li" data-shiin="み" data-boin-group="ま">
                                <a href="" class="shiin_anchor">み</a>
                            </li>
                            <li class="shiin_li" data-shiin="む" data-boin-group="ま">
                                <a href="" class="shiin_anchor">む</a>
                            </li>
                            <li class="shiin_li" data-shiin="め" data-boin-group="ま">
                                <a href="" class="shiin_anchor">め</a>
                            </li>
                            <li class="shiin_li" data-shiin="も" data-boin-group="ま">
                                <a href="" class="shiin_anchor">も</a>
                            </li>
                            <li class="shiin_li" data-shiin="や" data-boin-group="や">
                                <a href="" class="shiin_anchor active">や</a>
                            </li>
                            <li class="shiin_li" data-shiin="ゆ" data-boin-group="や">
                                <a href="" class="shiin_anchor">ゆ</a>
                            </li>
                            <li class="shiin_li" data-shiin="よ" data-boin-group="や">
                                <a href="" class="shiin_anchor">よ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ら" data-boin-group="ら">
                                <a href="" class="shiin_anchor active">ら</a>
                            </li>
                            <li class="shiin_li" data-shiin="り" data-boin-group="ら">
                                <a href="" class="shiin_anchor">り</a>
                            </li>
                            <li class="shiin_li" data-shiin="る" data-boin-group="ら">
                                <a href="" class="shiin_anchor">る</a>
                            </li>
                            <li class="shiin_li" data-shiin="れ" data-boin-group="ら">
                                <a href="" class="shiin_anchor">れ</a>
                            </li>
                            <li class="shiin_li" data-shiin="ろ" data-boin-group="ら">
                                <a href="" class="shiin_anchor">ろ</a>
                            </li>
                            <li class="shiin_li" data-shiin="わ" data-boin-group="わ">
                                <a href="" class="shiin_anchor active">わ</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <ul class="samurai_ul mt-24">
                    @foreach ($ShinseiPkSamuraiParameters as $ShinseiPkSamuraiParameter)
                        <li class="samurai_li @if(mb_substr($ShinseiPkSamuraiParameter->furigana, 0, 1) == 'あ') active @endif" data-kana="{{ mb_substr($ShinseiPkSamuraiParameter->furigana, 0, 1) }}">
                            <a href="{{ route('shinsei-pk.database.show', [$ShinseiPkSamuraiParameter->id]) }}" title="{{ $ShinseiPkSamuraiParameter->name }}" class="samurai_anchor">
                                <div class="img_area">
                                    <picture>
                                        <source type="image/webp" srcset="{{ My_func::return_min_samurai_img_webp_url($ShinseiPkSamuraiParameter->samurai->id, "shinsei-pk") }}">
                                        <img src="{{ My_func::return_min_samurai_img_url($ShinseiPkSamuraiParameter->samurai->id, "shinsei-pk") }}" alt="{{ $ShinseiPkSamuraiParameter->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img">
                                    </picture>
                                </div>
                                <div class="text_area">
                                    <p class="hover_white">{{ $ShinseiPkSamuraiParameter->name }} <span class="font_small hover_white">（{{ $ShinseiPkSamuraiParameter->furigana }}）</span></p>
                                    <p class="mt-8 font_small hover_white">{!! $ShinseiPkSamuraiParameter->samurai->retuden !!}</p>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </article>
            @include('components.recommend_articles')
            @include('components.breadcrumbs', ['slug' => 'database.samurais', 'argument1' => $GameTitle])
        </div>
        @include('components.aside')
    </div>
</div>
@endsection