<section class="mt-36">
    <h2 class="rich_h2 d-f ai-c">
        <img src="{{ asset('img/common/scenario.png')}}" alt="記事アイコン" width="30px" height="30px" class="mr-8">
        <p>新着記事</p>
    </h2>
    <ul class="card_ul">
        @foreach ($NewArticles as $NewArticle)
            <li class="card_list wide_card mt-24">
                <a href="{{ route('article.single', $NewArticle->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $NewArticle->title }}">
                    <picture>
                        <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($NewArticle->id) }}" class="card_image">
                        <img src="{{ My_func::return_article_main_img_url($NewArticle->id) }}" width="100%" height="100%" class="card_image" alt="{{ $NewArticle->title }}">
                    </picture>
                    <div class="card_bottom">
                        <div class="d-f j-sb card_info sp_block">
                            <time>
                                @if ($NewArticle->is_updated)
                                    {{ $NewArticle->additioned_at->format('Y年m月d日') }}
                                @else
                                    {{ $NewArticle->created_at->format('Y年m月d日') }}
                                @endif
                            </time>
                            <p class="card_category">
                                @foreach ($NewArticle->articleCategorySearches as $mainArticleCategorySearch)
                                    @if ($mainArticleCategorySearch->is_main_category)
                                        {{ $mainArticleCategorySearch->articleCategory->name }}
                                    @endif
                                @endforeach
                            </p>
                        </div>
                        <p class="card_title">{{ $NewArticle->title }}</p>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="mt-36 ta-c">
        <a href="{{ route('article.index') }}" title="一覧を見る">一覧を見る</a>
    </div>
</section>


