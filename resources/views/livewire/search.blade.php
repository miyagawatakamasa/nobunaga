<div class="livewire_search_area">
    {{-- <script async src="https://cse.google.com/cse.js?cx=e32df88667fb979c0"></script>
    <div class="gcse-search"></div> --}}
    <form action="{{ route('search') }}" method="GET" class="search_form j-sb">
        <input wire:model="search" type="text" placeholder="サイト内検索" class="shadow search" name="q" />
        <button type="submit" class="search_button">検索</button>
    </form>
    <ul class="search_ul back_white">
        @foreach($Samurais as $Samurai)
            <li class="search_li">
                @if ($Samurai->shinsei)
                    <a href="{{ route('database.samurais.show', ['shinsei', $Samurai->id]) }}" class="search_anchor">{{ $Samurai->name }}
                        <img src="{{ asset('img/common/arrow.png')}}" alt="" width="15px" height="15px">
                    </a>
                @else
                    <a href="{{ route('database.samurais.show', ['taishi-pk', $Samurai->id]) }}" class="search_anchor">{{ $Samurai->name }}
                        <img src="{{ asset('img/common/arrow.png')}}" alt="" width="15px" height="15px">
                    </a>
                @endif
            </li>
        @endforeach

        @foreach($SamuraiNames as $SamuraiName)
        <li class="search_li">
            @if ($SamuraiName->samurai->shinsei)
                <a href="{{ route('database.samurais.show', ['shinsei', $SamuraiName->samurai->id]) }}" class="search_anchor">{{ $SamuraiName->search_word }}
                    <img src="{{ asset('img/common/arrow.png')}}" alt="" width="15px" height="15px">
                </a>
            @else
                <a href="{{ route('database.samurais.show', ['taishi-pk', $SamuraiName->samurai->id]) }}" class="search_anchor">{{ $SamuraiName->search_word }}
                    <img src="{{ asset('img/common/arrow.png')}}" alt="" width="15px" height="15px">
                </a>
            @endif
        </li>
    @endforeach
    </ul>
</div>
