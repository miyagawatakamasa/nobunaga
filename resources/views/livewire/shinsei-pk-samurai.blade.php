<div>
    <form action="">
        <div class="mt-12">
            <label for="q">名前</label>
            <input wire:model="search" type="text" placeholder="織田信長" class="shadow search" name="q" />
        </div>

        <div class="samurai_form_wrapper">
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="leadership_min" class="mr-8">統率</label>
                <div class="d-f ai-c">
                    <input wire:model="leadership_min" type="number" placeholder="60" class="shadow narrow_input" name="leadership_min" />
                     ~ 
                    <input wire:model="leadership_max" type="number" placeholder="60" class="shadow narrow_input" name="leadership_max" />
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="brave_min" class="mr-8">武勇</label>
                <div class="d-f ai-c">
                    <input wire:model="brave_min" type="number" placeholder="60" class="shadow narrow_input" name="brave_min" />
                     ~ 
                    <input wire:model="brave_max" type="number" placeholder="60" class="shadow narrow_input" name="brave_max" />
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="wisdom_min" class="mr-8">知略</label>
                <div class="d-f ai-c">
                    <input wire:model="wisdom_min" type="number" placeholder="60" class="shadow narrow_input" name="wisdom_min" />
                     ~ 
                    <input wire:model="wisdom_max" type="number" placeholder="60" class="shadow narrow_input" name="wisdom_max" />
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="affairs_min" class="mr-8">政務</label>
                <div class="d-f ai-c">
                    <input wire:model="affairs_min" type="number" placeholder="60" class="shadow narrow_input" name="affairs_min" />
                     ~ 
                    <input wire:model="affairs_max" type="number" placeholder="60" class="shadow narrow_input" name="affairs_max" />
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="all_min" class="mr-8">全体</label>
                <div class="d-f ai-c">
                    <input wire:model="all_min" type="number" placeholder="240" class="shadow narrow_input" name="all_min" />
                     ~ 
                    <input wire:model="all_max" type="number" placeholder="240" class="shadow narrow_input" name="all_max" />
                </div>
            </div>
        </div>
        <div class="samurai_form_wrapper">
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="characteristic" class="mr-8">特性</label>
                <div class="d-f ai-c">
                    <select name="" id="" class="shadow" wire:model="characteristic">
                        <option value=""></option>
                        @foreach ($ShinseiPkSkills as $ShinseiPkSkill)
                            @if ($ShinseiPkSkill->type == 'characteristic')
                                <option value="{{ $ShinseiPkSkill->name }}">{{ $ShinseiPkSkill->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="tactics" class="mr-8">戦法</label>
                <div class="d-f ai-c">
                    <select name="" id="" class="shadow" wire:model="tactics">
                        <option value=""></option>
                        @foreach ($ShinseiPkSkills as $ShinseiPkSkill)
                            @if ($ShinseiPkSkill->type == 'tactics')
                                <option value="{{ $ShinseiPkSkill->name }}">{{ $ShinseiPkSkill->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="head_of_house" class="mr-8">主義</label>
                <div class="d-f ai-c">
                    <select name="" id="" class="shadow" wire:model="head_of_house">
                        <option value=""></option>
                        @foreach ($ShinseiPkSkills as $ShinseiPkSkill)
                            @if ($ShinseiPkSkill->type == 'head_of_house')
                                <option value="{{ $ShinseiPkSkill->name }}">{{ $ShinseiPkSkill->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mt-12 d-f ai-c samurai_form_content">
                <label for="magistrate" class="mr-8">奉行</label>
                <div class="d-f ai-c">
                    <select name="" id="" class="shadow" wire:model="magistrate">
                        <option value=""></option>
                        @foreach ($ShinseiPkSkills as $ShinseiPkSkill)
                            @if ($ShinseiPkSkill->type == 'magistrate')
                                <option value="{{ $ShinseiPkSkill->name }}">{{ $ShinseiPkSkill->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
    
    <h2 class="mt-24">検索結果</h2>
    <ul class="samurai_ul mt-12">
        @foreach ($Samurais as $Samurai)
            <li class="samurai_li active">
                <a href="{{ route('shinsei-pk.database.show', $Samurai->samurai->id) }}" class="samurai_anchor">
                    <div class="img_area">
                        <picture>
                            {{-- <source type="image/webp" srcset="https://nobunaga-pro.s3.ap-northeast-1.amazonaws.com/img/shinsei/samurai_webp/1.webp"> --}}
                            <img src="{{ asset('img/common/ready.png') }}" alt="{{ $Samurai->name }}のグラフィック画像" width="120px" height="100%" class="m-a samurai_img">
                        </picture>
                    </div>
                    <div class="text_area">
                        <p class="hover_white">{{ $Samurai->name }} <span class="font_small hover_white">（{{ $Samurai->furigana }}）</span></p>
                        <table>
                            <tr>
                                <th>統率</th>
                                <td>{{ $Samurai->leadership }}</td>
                                <th>武勇</th>
                                <td>{{ $Samurai->brave }}</td>
                                <th>知略</th>
                                <td>{{ $Samurai->wisdom }}</td>
                                <th>政務</th>
                                <td>{{ $Samurai->affairs }}</td>
                                <th>全体</th>
                                <td>{{ $Samurai->all }}</td>
                            </tr>
                            <tr>
                                <th>特性1</th>
                                <td>{{ $Samurai->characteristic1 }}</td>
                                <th>特性2</th>
                                <td>{{ $Samurai->characteristic2 }}</td>
                                <th>特性3</th>
                                <td>{{ $Samurai->characteristic3 }}</td>
                                <th>戦法</th>
                                <td>{{ $Samurai->tactics }}</td>
                                <th>主義</th>
                                <td>{{ $Samurai->head_of_house }}</td>
                                <th>奉行</th>
                                <td>{{ $Samurai->magistrate }}</td>
                            </tr>
                        </table>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
</div>
