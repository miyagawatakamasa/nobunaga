<div id="sp-slide-header" class="back_white inner_wrapper p-12 @if($isActive)active @endif">
    <div class="d-f j-sb ai-c">
      <div class="openbtn @if($isActive)active @endif" wire:click="isActive"><span class="border"></span><span class="border"></span><span class="border"></span></div>
      <livewire:search />
    </div>
    @if($isActive)
        <div id="sp-humberger-overlay" class="@if($isActive)active @endif">
            <div class="menu single_contents">
                @include('components.aside')
            </div>
        </div>
    @endif
</div>