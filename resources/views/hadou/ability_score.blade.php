@extends('layouts.layout')

@section('title', '武将名鑑【覇道】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="一覧です。レアリティ・属性を一覧で出しています！覇道で武将を探す際の参考にしてください。">
    <meta property="og:description" content="一覧です。レアリティ・能力値を一覧で出しています！覇道で武将を探す際の参考にしてください。" />
    <meta property="og:title" content="武将名鑑【覇道】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将名鑑【覇道】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/ranking.png')}}" alt="能力値ランキングアイコン" width="30px" height="30px" class="mr-8">
                        <p>能力値ランキング【覇道】</p>
                    </h1>
                    <table class="font_small ta-c mt-12 w-100" id="sort-table">
                        <thead>
                            <tr>
                                <th class="c-p sort_th">名前</th>
                                <th class="c-p sort_th">統率</th>
                                <th class="c-p sort_th">武勇</th>
                                <th class="c-p sort_th">知略</th>
                                <th class="c-p sort_th">政治</th>
                                <th class="c-p sort_th">戦力</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($HadouSamuraiParameters as $SamuraiParameter)
                            <tr>
                                <td data-sort="{{ $SamuraiParameter->samurai->kana }}"><a href="{{ route('database.samurais.show', ['hadou', $SamuraiParameter->id]) }}" title="{{ $SamuraiParameter->samurai->name }}">{{ $SamuraiParameter->samurai->name }}</a></td>
                                <td class="@if($SamuraiParameter->leadership >= 600) back_orange @elseif($SamuraiParameter->leadership >= 400) back_yellow @endif" data-sort="{{ $SamuraiParameter->leadership }}">{{ $SamuraiParameter->leadership }}</td>
                                <td class="@if($SamuraiParameter->brave >= 600) back_orange @elseif($SamuraiParameter->brave >= 400) back_yellow @endif" data-sort="{{ $SamuraiParameter->brave }}">{{ $SamuraiParameter->brave }}</td>
                                <td class="@if($SamuraiParameter->wisdom >= 600) back_orange @elseif($SamuraiParameter->wisdom >= 400) back_yellow @endif" data-sort="{{ $SamuraiParameter->wisdom }}">{{ $SamuraiParameter->wisdom }}</td>
                                <td class="@if($SamuraiParameter->affairs >= 600) back_orange @elseif($SamuraiParameter->affairs >= 400) back_yellow @endif" data-sort="{{ $SamuraiParameter->affairs }}">{{ $SamuraiParameter->affairs }}</td>
                                <td class="@if($SamuraiParameter->strength >= 4000) back_orange @elseif($SamuraiParameter->strength >= 3000) back_yellow @endif" data-sort="{{ $SamuraiParameter->strength }}">{{ $SamuraiParameter->strength }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'hadou.database.samurais'])
                </article>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection