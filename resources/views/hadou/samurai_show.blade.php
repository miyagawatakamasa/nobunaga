@extends('layouts.layout')

@section('title', $HadouSamuraiParameter->samurai->name . '【覇道】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「信長の野望 覇道」での「{{ $HadouSamuraiParameter->samurai->name }}」の情報を記載しています。">
    <meta property="og:description" content="ゲーム「信長の野望 覇道」での「{{ $HadouSamuraiParameter->samurai->name }}」の情報を記載しています。" />
    <meta property="og:title" content="{{ $HadouSamuraiParameter->samurai->name }}【覇道】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/hadou/samurai/' . $HadouSamuraiParameter->id . '.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将名鑑【覇道】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <p>武将名鑑【覇道】</p>
                    <h1 class="mt-24 bb-blue">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                            <p class="h1">{{ $HadouSamuraiParameter->samurai->name }}（{{ $HadouSamuraiParameter->samurai->furigana }}）</p>
                        </div>
                    </h1>
                    <div class="mt-12 d-f j-sb ai-c">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('hadou.database.samurais.show', $BeforeSamurai->samurai->id) }}" class="font_small"><{{ $BeforeSamurai->samurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        <livewire:search />
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('hadou.database.samurais.show', $NextSamurai->samurai->id) }}" class="font_small">{{ $NextSamurai->samurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <ul class="d-f font_small title_box mt-12">
                        <li class="active title_li p-8">
                            <a href="" class="title_anchor normal_block_anchor shadow_hover shadow active d-b">
                                <img src="{{ asset('img/common/hadou.png')}}" alt="新信長アイコン" width="84px" height="40px" class="title_img">
                            </a>
                        </li>
                        @if (isset($ShinseiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei-pk.database.show', [$Samurai->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei_pk.png')}}" alt="新生PKアイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei.database.samurais.show', $ShinseiSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei.png')}}" alt="新生アイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinnobuSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinnobu.png')}}" alt="新信長アイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($TaishiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $TaishiPkSamuraiParameter->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/taishi_pk.png')}}" alt="大志PKアイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                    </ul>
                    {{-- <p class="mt-12">{{ $ShinnobuSamuraiParameter->heading }}</p> --}}
                    <div class="d-f mt-12 parameter_img_area">
                        <table class="font_small ta-c parameter_table">
                            <tr>
                                <th colspan="6">{{ $HadouSamuraiParameter->samurai->name }} の能力値</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n" style="width: 60px;">統率
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">部隊の攻撃や防御を定め、通常攻撃のダメージに影響<br>
                                            兵舎、道場での任命効果に影響</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($HadouSamuraiParameter->leadership >= 600) back_orange @elseif ($HadouSamuraiParameter->leadership >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $HadouSamuraiParameter->leadership / 6 }}px;"></span>{{ $HadouSamuraiParameter->leadership }} <span class="font_min ml-24">({{ $HadouSamuraiParameter->leadership_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">武勇
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">部隊の武勇を定め、武勇戦法ダメージ、状態変化率に影響<br>
                                            矢倉での任命効果に影響</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($HadouSamuraiParameter->brave >= 600) back_orange @elseif ($HadouSamuraiParameter->brave >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $HadouSamuraiParameter->brave / 6 }}px;"></span>{{ $HadouSamuraiParameter->brave }} <span class="font_min ml-24">({{ $HadouSamuraiParameter->brave_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">知略
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">部隊の知略を定め、知略戦法のダメージ、状態変化率に影響<br>
                                            民家、市場、石垣での任命効果に影響</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($HadouSamuraiParameter->wisdom >= 600) back_orange @elseif ($HadouSamuraiParameter->wisdom >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $HadouSamuraiParameter->wisdom / 6 }}px;"></span>{{ $HadouSamuraiParameter->wisdom }} <span class="font_min ml-24">({{ $HadouSamuraiParameter->wisdom_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">政治
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">部隊の破壊を定め、要所や城門などへのダメージに影響<br>
                                            屋敷、水田、材木所、石切場、製鉄所での任命効果に影響</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($HadouSamuraiParameter->affairs >= 600) back_orange @elseif ($HadouSamuraiParameter->affairs >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $HadouSamuraiParameter->affairs / 6 }}px;"></span>{{ $HadouSamuraiParameter->affairs }} <span class="font_min ml-24">({{ $HadouSamuraiParameter->affairs_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">合計</th>
                                <td colspan="4" class="ta-l of-h">
                                    <span class="status_bar" style="width: {{ $HadouSamuraiParameter->all / 24 }}px;"></span>{{ $HadouSamuraiParameter->all }} <span class="font_min ml-24">({{ $HadouSamuraiParameter->all_rank }} 位)</span><span class="f-r font_min">105人中</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">戦闘力</th>
                                <td colspan="4" class="of-h @if($HadouSamuraiParameter->strength >= 4000) back_orange @elseif($HadouSamuraiParameter->strength >= 3000) back_yellow @endif">
                                    {{ $HadouSamuraiParameter->strength }}
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">天賦</th>
                                <td colspan="4" class="of-h">
                                    {{ $HadouSamuraiParameter->talent }}
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n"><img src="{{ Storage::disk('s3')->url('img/hadou/spear.png') }}" alt="" width="16px" height="16px"></th>
                                <td colspan="4" class="of-h">
                                    {{ $HadouSamuraiParameter->spear }}
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n"><img src="{{ Storage::disk('s3')->url('img/hadou/horsemanship.png') }}" alt="" width="16px" height="16px"></th>
                                <td colspan="4" class="of-h">
                                    {{ $HadouSamuraiParameter->horsemanship }}
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n"><img src="{{ Storage::disk('s3')->url('img/hadou/bow.png') }}" alt="" width="16px" height="16px"></th>
                                <td colspan="4" class="of-h">
                                    {{ $HadouSamuraiParameter->bow }}
                                </td>
                            </tr>

                        </table>
                        <div class="samurai_img_area d-f">
                            <div class="d-f ai-c j-c">
                                <img src="{{ Storage::disk('s3')->url('img/hadou/samurai/' . $HadouSamuraiParameter->id . '.png') }}" alt="{{ $HadouSamuraiParameter->samurai->name }}のグラフィック画像" width="80%" class="m-a samurai_img hadou_img">
                            </div>
                        </div>
                    </div>
                    <table class="font_small ta-c mt-12 w-100">
                        <tr>
                            <th colspan="6" style="width: 50%;">その他のデータ</th>
                            <th colspan="6" style="width: 50%;">歴史</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">レアリティ</th>
                            <td colspan="4">
                                <a href="{{ route('hadou.database.samurais.show.rarity', $HadouSamuraiParameter->rarity) }}" title="{{ $HadouSamuraiParameter->rarity }}キャラ一覧">
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $HadouSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%" class="d-b">
                                </a>
                            </td>
                            <td colspan="6" rowspan="2" class="ta-l">{!! $HadouSamuraiParameter->samurai->retuden_html !!}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">声優（CV）</th>
                            <td colspan="4">{{ $HadouSamuraiParameter->voice_actor }}</td>
                        </tr>
                        <tr>
                            <th colspan="12">戦法</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">{{ $HadouSamuraiParameter->tactics }}</th>
                            <td colspan="10" class="ta-l">{!! nl2br(e($HadouSamuraiParameter->tactics_body)) !!}</td>
                        </tr>
                        @if(isset($HadouSamuraiParameter->tactics_reason))
                            <tr>
                                <td colspan="12" class="ta-l">{!! nl2br(e($HadouSamuraiParameter->tactics_reason)) !!}</td>
                            </tr>
                        @endif
                    </table>
                    <section class="mt-36">
                        <h3 class="rich_h3">技能</h3>
                        @foreach ($HadouSkillSearches as $HadouSkillSearch)
                            <h4 class="mt-24 rich_h4">{{ $HadouSkillSearch->hadouSkill->name }}</h4>
                            <table class="font_small mt-12 w-100">
                                <tr>
                                    <th style="width: 15%;">効果レベル</th>
                                    <th>効果</th>
                                </tr>
                                <tr>
                                    <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅰ</td>
                                    <td>{!! nl2br(e($HadouSkillSearch->hadouSkill->effect1)) !!}</td>
                                </tr>
                                @if (isset($HadouSkillSearch->hadouSkill->effect2))
                                    <tr>
                                        <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅱ</td>
                                        <td>{!! nl2br(e($HadouSkillSearch->hadouSkill->effect2)) !!}</td>
                                    </tr>
                                @endif
                                @if (isset($HadouSkillSearch->hadouSkill->effect3))
                                    <tr>
                                        <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅲ</td>
                                        <td>{!! nl2br(e($HadouSkillSearch->hadouSkill->effect3)) !!}</td>
                                    </tr>
                                @endif
                                @if (isset($HadouSkillSearch->hadouSkill->effect4))
                                    <tr>
                                        <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅳ</td>
                                        <td>{!! nl2br(e($HadouSkillSearch->hadouSkill->effect4)) !!}</td>
                                    </tr>
                                @endif
                                @if (isset($HadouSkillSearch->hadouSkill->effect5))
                                    <tr>
                                        <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅴ</td>
                                        <td>{!! nl2br(e($HadouSkillSearch->hadouSkill->effect5)) !!}</td>
                                    </tr>
                                @endif
                            </table>
                        @endforeach
                    </section>

                    <section class="mt-36">
                        <h3 class="rich_h3">秘伝</h3>
                        <h4 class="mt-24 rich_h4">
                            <ruby>
                                {{ $HadouSecretSearch->hadouSecret->name }}
                                <rt>{{ $HadouSecretSearch->hadouSecret->name_kana }}</rt>
                            </ruby>
                        </h4>
                        <p class="mt-12">条件 : {{ $HadouSecretSearch->hadouSecret->conditions }}</p>
                        <p class="mt-12">系統 : {{ $HadouSecretSearch->hadouSecret->system }}</p>
                        <table class="font_small mt-12 w-100">
                            <tr>
                                <th style="width: 15%;">効果レベル</th>
                                <th>効果</th>
                            </tr>
                            <tr>
                                <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅰ</td>
                                <td>{!! nl2br(e($HadouSecretSearch->hadouSecret->effect1)) !!}</td>
                            </tr>
                            @if (isset($HadouSecretSearch->hadouSecret->effect2))
                                <tr>
                                    <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅱ</td>
                                    <td>{!! nl2br(e($HadouSecretSearch->hadouSecret->effect2)) !!}</td>
                                </tr>
                            @endif
                            @if (isset($HadouSecretSearch->hadouSecret->effect3))
                                <tr>
                                    <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅲ</td>
                                    <td>{!! nl2br(e($HadouSecretSearch->hadouSecret->effect3)) !!}</td>
                                </tr>
                            @endif
                            @if (isset($HadouSecretSearch->hadouSecret->effect4))
                                <tr>
                                    <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅳ</td>
                                    <td>{!! nl2br(e($HadouSecretSearch->hadouSecret->effect4)) !!}</td>
                                </tr>
                            @endif
                            @if (isset($HadouSecretSearch->hadouSecret->effect5))
                                <tr>
                                    <td class="ta-c back_cloud_blue color_font_black fw-n">Ⅴ</td>
                                    <td>{!! nl2br(e($HadouSecretSearch->hadouSecret->effect5)) !!}</td>
                                </tr>
                            @endif
                        </table>
                    </section>

                    <div class="mt-24 d-f j-sb ai-c">
                        @if (isset($BeforeSamurai))
                            <div>
                                <a href="{{ route('hadou.database.samurais.show', $BeforeSamurai->samurai->id) }}" class="font_small"><{{ $BeforeSamurai->samurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        @if (isset($NextSamurai))
                            <div>
                                <a href="{{ route('hadou.database.samurais.show', $NextSamurai->samurai->id) }}" class="font_small">{{ $NextSamurai->samurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <div class="mt-36 ta-c pc_display">
                        <div id="im-af643d07e0944e0b8a908c2accf2d86a">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1808041,type:"banner",display:"inline",elementid:"im-af643d07e0944e0b8a908c2accf2d86a"})</script>
                        </div>
                    </div>
                    <div class="mt-36 ta-c sp_display">
                        <div id="im-7181044169e0457d9a2a4bb3b1c8337f">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1808042,type:"banner",display:"inline",elementid:"im-7181044169e0457d9a2a4bb3b1c8337f"})</script>
                        </div>
                    </div>
                    @if (count($WarSamurais))
                        <section class="mt-36 war_samurai">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する合戦</h2>
                            <ul>
                                @foreach ($WarSamurais as $WarSamurai)
                                    <li class="mt-24">
                                        <h3 class="rich_h3">{{ $WarSamurai->war->name }}（{{ $WarSamurai->war->kana }}）　[<a href="{{ route('other.war.single', $WarSamurai->war->disturbance->id) }}">{{ $WarSamurai->war->disturbance->name }}</a>]</h3>
                                        @include('components.war', ['war' => $WarSamurai->war])
                                        <p class="ta-r mt-12">
                                            <a href="{{ route('other.war.single', $WarSamurai->war->disturbance_id) }}">
                                                詳細を見る
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif
                    @if (count($SamuraiRelatedArticles))
                        <section class="mt-36">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24 narrow_card">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($SamuraiRelatedArticle->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($SamuraiRelatedArticle->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $SamuraiRelatedArticle->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>{{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}</time>
                                                    <p class="card_category">
                                                        @foreach ($SamuraiRelatedArticle->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'hadou.database.samurais.show', 'argument1' => $HadouSamuraiParameter])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection