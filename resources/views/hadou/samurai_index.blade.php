@extends('layouts.layout')

@section('title', '武将名鑑【覇道】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="一覧です。レアリティ・属性を一覧で出しています！覇道で武将を探す際の参考にしてください。">
    <meta property="og:description" content="一覧です。レアリティ・能力値を一覧で出しています！覇道で武将を探す際の参考にしてください。" />
    <meta property="og:title" content="武将名鑑【覇道】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将名鑑【覇道】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/samurai.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>武将名鑑【覇道】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <p>【覇道】のキャラ(武将)一覧です。レアリティ・能力値を一覧で出しています！覇道で武将を探す際の参考にしてください。</p>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th>武将名</th>
                                <th>レアリティ</th>
                                <th>戦力</th>
                                <th>統率</th>
                                <th>武勇</th>
                                <th>智略</th>
                                <th>政治</th>
                            </tr>
                            @foreach ($HadouSamuraiParameters as $HadouSamuraiParameter)
                                <tr>
                                    <td>
                                        <a href="{{ route('hadou.database.samurais.show', $HadouSamuraiParameter->samurai->id) }}">
                                            <div>
                                                <img src="{{ Storage::disk('s3')->url('img/hadou/samurai/' . $HadouSamuraiParameter->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                            </div>
                                            @if (isset($HadouSamuraiParameter->samurai))
                                                {{ $HadouSamuraiParameter->samurai->name }}
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('hadou.database.samurais.show.rarity', $HadouSamuraiParameter->rarity) }}"  title="{{ $HadouSamuraiParameter->rarity }}キャラ一覧">
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $HadouSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                        </a>
                                    </td>
                                    <td class="@if($HadouSamuraiParameter->strength >= 4000) back_orange @elseif($HadouSamuraiParameter->strength >= 3000) back_yellow @endif">{{ $HadouSamuraiParameter->strength }}</td>

                                    <td class="@if ($HadouSamuraiParameter->leadership >= 600) back_orange @elseif ($HadouSamuraiParameter->leadership >= 400) back_yellow @endif">{{ $HadouSamuraiParameter->leadership }}</td>
                                    <td class="@if ($HadouSamuraiParameter->brave >= 600) back_orange @elseif ($HadouSamuraiParameter->brave >= 400) back_yellow @endif">{{ $HadouSamuraiParameter->brave }}</td>
                                    <td class="@if ($HadouSamuraiParameter->wisdom >= 600) back_orange @elseif ($HadouSamuraiParameter->wisdom >= 400) back_yellow @endif">{{ $HadouSamuraiParameter->wisdom }}</td>
                                    <td class="@if ($HadouSamuraiParameter->affairs >= 600) back_orange @elseif ($HadouSamuraiParameter->affairs >= 400) back_yellow @endif">{{ $HadouSamuraiParameter->affairs }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'hadou.database.samurais'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection