@extends('layouts.layout')

@section('title', '国士無双ガチャおすすめ【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」での国士無双ガチャおすすめについての攻略記事です。の国士無双ガチャについてまとめています。どの武将がおすすめかを紹介しています。おすすめは前田利家・前田慶次・島津歳久・鍋島直茂です。先に配布されたガチャ券を使って英雄集結ガチャを引き、そこで出たSSRレアの武将と同じ兵科を選択するのが良いでしょう。">
    <meta property="og:description" content="ゲーム「新信長の野望」での国士無双ガチャおすすめについての攻略記事です。の国士無双ガチャについてまとめています。どの武将がおすすめかを紹介しています。おすすめは前田利家・前田慶次・島津歳久・鍋島直茂です。先に配布されたガチャ券を使って英雄集結ガチャを引き、そこで出たSSRレアの武将と同じ兵科を選択するのが良いでしょう。" />
    <meta property="og:title" content="国士無双ガチャおすすめ【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/gacha/kokushimusou.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "国士無双ガチャおすすめ【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月19日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">{{ $Article->additioned_at->format('Y年m月d日') }}</span> --}}
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/gacha.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>国士無双ガチャおすすめ【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="main_visual mt-24" style="background-image: url('{{ Storage::disk('s3')->url('img/shinnobunaga/gacha/kokushimusou.png') }}');"></div>
                        <p class="mt-24 font_small">新信長の野望(シンノブ)の国士無双ガチャについてまとめています。どの武将を引くべきかを紹介しています。</p>
                        <h2>国士無双ガチャとは？</h2>
                        <p><span class="color_red">国士無双ガチャ</span>とは、<span class="fw-b">何度でも引き直しが可能な無料10連ガチャ</span>です。</p>
                        <p>SSRキャラが1体、SRキャラが2体確定排出となります。</p>
                        <h2>9章まで進めるとガチャを引ける</h2>
                        <p>9章で竹中半兵衛を倒すと、[登用]アイコンから国士無双ガチャを引けるようになります。</p>
                        <p>9章の条件は以下になります。</p>
                        <ul>
                            <li>部隊1と2を出陣させる</li>
                            <li>Lv3以上の兵士を出陣（大筒以外）</li>
                            <li>出陣総兵力が3600に到達</li>
                        </ul>
                        <p>天守レベルを10まで上がると学問所が解放されるので、学問所を建設し「部隊拡張・壱」を研究しましょう。</p>
                        <h2>国士無双ガチャ内容</h2>
                        <p>国士無双ガチャで排出されるSSRレアのキャラ一覧です。</p>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th>武将名</th>
                                <th>兵科</th>
                                <th>おすすめ度</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '12') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/12.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        前田利家
                                    </a>
                                </td>
                                <td>槍兵</td>
                                <td>9.5</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '19') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/19.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        前田慶次
                                    </a>
                                </td>
                                <td>騎兵</td>
                                <td>9.5</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '27') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/27.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        島津歳久
                                    </a>
                                </td>
                                <td>鉄砲</td>
                                <td>9.0</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '20') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/20.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        鍋島直茂
                                    </a>
                                </td>
                                <td>大筒</td>
                                <td>9.0</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '16') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/16.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        最上義光
                                    </a>
                                </td>
                                <td>大筒</td>
                                <td>8.5</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '31') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/31.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        織田信秀
                                    </a>
                                </td>
                                <td>大筒</td>
                                <td>8.5</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '29') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/29.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        浅井初
                                    </a>
                                </td>
                                <td>忍者</td>
                                <td>8.0</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', '30') }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/30.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        六角定頼
                                    </a>
                                </td>
                                <td>農兵</td>
                                <td>7.5</td>
                            </tr>
                        </table>
                        <p class="mt-12">おすすめは前田利家・前田慶次・島津歳久・鍋島直茂です。先に配布されたガチャ券を使って英雄集結ガチャを引き、そこで<span class="fw-b yellow_marker">出たSSRレアの武将と同じ兵科を選択するのが良いでしょう</span>。</p>

                        <h3>前田利家</h3>
                        <p><span class="fw-b yellow_marker">騎兵攻撃力UPのバフが強力</span>。武田信玄と組んで攻撃力が突出した騎兵部隊を作ることが可能。</p>
                        <p>奥義「槍の又左」では、ランダムで味方部隊の防御力を上げ、攻撃力が高い敵部隊にダメージを与え強力。</p>
                        <h3>前田慶次</h3>
                        <p><span class="fw-b yellow_marker">槍兵攻撃力UPのバフが強力</span>。徳川家康と組んで攻撃力が突出した槍兵部隊を作ることが可能。</p>
                        <p>奥義「傾奇者」は、味方部隊の攻撃力を上げ、敵に毎ターンダメージを与え強力。</p>
                        <h3>島津歳久</h3>
                        <p>島津義弘と組むことで、強力な鉄砲隊を組むことが可能。</p>
                        <p>島津義弘と島津歳久の奥義で<span class="fw-b">偶数ターンに大ダメージを与えることが強力</span>。</p>
                        <h3>鍋島直茂</h3>
                        <p>軍団の大筒包囲値を上げられる。主将として出陣すると、<span class="fw-b">部隊兵数を増加できる</span>。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.gacha.kokushimusou'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection