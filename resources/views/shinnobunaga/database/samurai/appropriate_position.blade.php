@extends('layouts.layout')

@section('title', $appropriate_position_hiragana . 'キャラ一覧【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。">
    <meta property="og:description" content="一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。" />
    <meta property="og:title" content="{{ $appropriate_position_hiragana }}キャラ一覧【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "{{ $appropriate_position_hiragana }}キャラ一覧【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $appropriate_position . '.png') }}" alt="" width="40px" height="100%" class="mr-8">
                            <p>{{ $appropriate_position_hiragana }}キャラ一覧【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <p>【新信長の野望】の{{ $appropriate_position_hiragana }}のキャラ(武将)一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。</p>
                        @if ($appropriate_position_hiragana == 'captain')
                            <p>武将の統率の高さによって部隊の兵数が増減するため、統率が高い武将を選択すると良い。</p>
                        @elseif ($appropriate_position_hiragana == 'pioneer')
                            <p>武将の武勇の高さによって部隊の攻撃力・防御力が増減するため、武勇が高い武将を選択すると良い。</p>
                        @elseif ($appropriate_position_hiragana == 'staff')
                            <p>武将の智略の高さによって部隊の攻撃力・防御力が増減するため、智略が高い武将を選択すると良い。</p>
                        @endif
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 100px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th>統率</th>
                                <th>武勇</th>
                                <th>智略</th>
                                <th>政治</th>
                            </tr>

                            @foreach ($ShinnobuSamuraiParameters as $ShinnobuSamuraiParameter)
                                <tr>
                                    <td>
                                        <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}">
                                            <div>
                                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                            </div>
                                            @if (isset($ShinnobuSamuraiParameter->samurai))
                                                {{ $ShinnobuSamuraiParameter->samurai->name }}
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%" class="d-b">
                                    </td>
                                    <td>{{ $ShinnobuSamuraiParameter->appropriate_army }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->leadership >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->leadership >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->leadership }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->brave >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->brave >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->brave }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->wisdom >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->wisdom >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->wisdom }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->affairs >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->affairs >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->affairs }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.database.samurais.show.rarity', 'argument1' => $appropriate_position_hiragana ])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection