@extends('layouts.layout')

@section('title', $appropriate_army_hiragana . 'キャラ一覧【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。">
    <meta property="og:description" content="一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。" />
    <meta property="og:title" content="{{ $appropriate_army_hiragana }}キャラ一覧【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "{{ $appropriate_army_hiragana }}キャラ一覧【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $appropriate_army . '.png') }}" alt="" width="30px" height="30px" class="mr-8">
                            <p>{{ $appropriate_army_hiragana }}キャラ一覧【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <p>【新信長の野望】の{{ $appropriate_army_hiragana }}のキャラ(武将)一覧です。レアリティ・属性を一覧で出しています！新信長の野望で武将を探す際の参考にしてください。</p>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 100px;">武将名</th>
                                <th>レアリティ</th>
                                <th>適正</th>
                                <th>統率</th>
                                <th>武勇</th>
                                <th>智略</th>
                                <th>政治</th>
                            </tr>

                            @foreach ($ShinnobuSamuraiParameters as $ShinnobuSamuraiParameter)
                                <tr>
                                    <td>
                                        <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}">
                                            <div>
                                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                            </div>
                                            @if (isset($ShinnobuSamuraiParameter->samurai))
                                                {{ $ShinnobuSamuraiParameter->samurai->name }}
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%" class="d-b">
                                    </td>
                                    <td>{{ $ShinnobuSamuraiParameter->appropriate_position }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->leadership >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->leadership >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->leadership }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->brave >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->brave >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->brave }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->wisdom >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->wisdom >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->wisdom }}</td>
                                    <td class="@if ($ShinnobuSamuraiParameter->affairs >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->affairs >= 400) back_yellow @endif">{{ $ShinnobuSamuraiParameter->affairs }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.database.samurais.show.rarity', 'argument1' => $appropriate_army_hiragana ])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection