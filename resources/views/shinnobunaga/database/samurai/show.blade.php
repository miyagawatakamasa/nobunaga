@extends('layouts.layout')

@section('title', $ShinnobuSamuraiParameter->samurai->name . '【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望（シンノブ）」での「{{ $ShinnobuSamuraiParameter->samurai->name }}」の情報を記載しています。{{ $ShinnobuSamuraiParameter->history }}">
    <meta property="og:description" content="ゲーム「新信長の野望（シンノブ）」での「{{ $ShinnobuSamuraiParameter->samurai->name }}」の情報を記載しています。{{ $ShinnobuSamuraiParameter->history }}" />
    <meta property="og:title" content="{{ $ShinnobuSamuraiParameter->samurai->name }}【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将名鑑【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <p>武将名鑑【新信長の野望】</p>
                    <h1 class="mt-24 bb-blue">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/samurai.png')}}" alt="武将名鑑アイコン" width="30px" height="30px" class="mr-8">
                            <p class="h1">{{ $ShinnobuSamuraiParameter->samurai->name }}（{{ $ShinnobuSamuraiParameter->samurai->furigana }}）</p>
                        </div>
                    </h1>
                    <div class="mt-12 d-f j-sb ai-c">
                        @if (isset($BeforeShinnobuSamuraiParameter))
                            <div>
                                <a href="{{ route('shinnobunaga.database.samurais.show', $BeforeShinnobuSamuraiParameter->id) }}" class="font_small"><{{ $BeforeShinnobuSamuraiParameter->samurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        <livewire:search />
                        @if (isset($NextShinnobuSamuraiParameter))
                            <div>
                                <a href="{{ route('shinnobunaga.database.samurais.show', $NextShinnobuSamuraiParameter->id) }}" class="font_small">{{ $NextShinnobuSamuraiParameter->samurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    <ul class="d-f font_small title_box mt-12">
                        @if (isset($HadouSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('hadou.database.samurais.show', $HadouSamuraiParameter->samurai->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/hadou.png')}}" alt="新信長アイコン" width="84px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei-pk.database.show', [$Samurai->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/shinsei_pk.png')}}" alt="新生PKアイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        @if (isset($ShinseiSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('shinsei.database.samurais.show', $ShinseiSamuraiParameter->id) }}" class="title_anchor normal_block_anchor shadow_hover shadow ta-c d-b">
                                    <img src="{{ asset('img/common/shinsei.png')}}" alt="新生アイコン" width="78px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                        <li class="active title_li p-8">
                            <a href="" class="title_anchor normal_block_anchor shadow_hover shadow active d-b">
                                <img src="{{ asset('img/common/shinnobu.png')}}" alt="新信長アイコン" width="40px" height="40px" class="title_img">
                            </a>
                        </li>
                        @if (isset($TaishiPkSamuraiParameter))
                            <li class="active title_li p-8">
                                <a href="{{ route('database.samurais.show', ['taishi-pk', $TaishiPkSamuraiParameter->id]) }}" class="title_anchor normal_block_anchor shadow_hover shadow d-b">
                                    <img src="{{ asset('img/common/taishi_pk.png')}}" alt="大志PKアイコン" width="40px" height="40px" class="title_img">
                                </a>
                            </li>
                        @endif
                    </ul>
                    <p class="mt-12">{{ $ShinnobuSamuraiParameter->heading }}</p>
                    <div class="d-f mt-12 parameter_img_area">
                        <table class="font_small ta-c parameter_table">
                            <tr>
                                <th colspan="6">{{ $ShinnobuSamuraiParameter->samurai->name }} の能力値</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">統率
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">武将が出撃部隊の主将を務めた際、部隊兵数に影響。<br>
                                                先鋒や参謀を追加しても部隊兵数上限は変化しない。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($ShinnobuSamuraiParameter->leadership >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->leadership >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $ShinnobuSamuraiParameter->leadership / 6 }}px;"></span>{{ $ShinnobuSamuraiParameter->leadership }} <span class="font_min ml-24">({{ $ShinnobuSamuraiParameter->leadership_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">武勇
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">武将が出撃部隊の先鋒を務めた場合、部隊の攻防バフに影響。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($ShinnobuSamuraiParameter->brave >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->brave >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $ShinnobuSamuraiParameter->brave / 6 }}px;"></span>{{ $ShinnobuSamuraiParameter->brave }} <span class="font_min ml-24">({{ $ShinnobuSamuraiParameter->brave_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">智略
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">武将が出撃部隊の参謀を務めた場合、部隊の攻防バフに影響。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($ShinnobuSamuraiParameter->wisdom >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->wisdom >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $ShinnobuSamuraiParameter->wisdom / 6 }}px;"></span>{{ $ShinnobuSamuraiParameter->wisdom }} <span class="font_min ml-24">({{ $ShinnobuSamuraiParameter->wisdom_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">内政
                                    <span class="info_icon"><img src="{{ asset('img/common/info.png')}}" alt="" width="12px" height="12px">
                                        <div class="baloon_text_block p-0">
                                            <p class="baloon_text ta-l top_ballon">武将委任時の修復効果と所属部隊の採集効率に影響。</p>
                                        </div>
                                    </span>
                                </th>
                                <td colspan="4" class="ta-l @if ($ShinnobuSamuraiParameter->affairs >= 600) back_orange @elseif ($ShinnobuSamuraiParameter->affairs >= 400) back_yellow @endif">
                                    <span class="status_bar" style="width: {{ $ShinnobuSamuraiParameter->affairs / 6 }}px;"></span>{{ $ShinnobuSamuraiParameter->affairs }} <span class="font_min ml-24">({{ $ShinnobuSamuraiParameter->affairs_rank }} 位)</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">合計</th>
                                <td colspan="4" class="ta-l of-h">
                                    <span class="status_bar" style="width: {{ $ShinnobuSamuraiParameter->all / 24 }}px;"></span>{{ $ShinnobuSamuraiParameter->all }} <span class="font_min ml-24">({{ $ShinnobuSamuraiParameter->all_rank }} 位)</span><span class="f-r font_min">84人中</span>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" class="back_cloud_blue color_font_black fw-n">戦闘力</th>
                                <td colspan="4" class="ta-l of-h">
                                    {{ $ShinnobuSamuraiParameter->combat_power }}
                                </td>
                            </tr>
                        </table>
                        <div class="samurai_img_area d-f ai-c j-c">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}" alt="{{ $ShinnobuSamuraiParameter->samurai->name }}のグラフィック画像" width="80%" height="100%" class="m-a samurai_img">
                        </div>
                    </div>
                    <table class="font_small ta-c mt-12 w-100">
                        <tr>
                            <th colspan="6" style="width: 50%;">その他のデータ</th>
                            <th colspan="6" style="width: 50%;">歴史</th>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">レアリティ</th>
                            <td colspan="4">
                                <a href="{{ route('shinnobunaga.database.samurais.show.rarity', $ShinnobuSamuraiParameter->rarity) }}" title="{{ $ShinnobuSamuraiParameter->rarity }}キャラ一覧">
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%" class="d-b">
                                </a>
                            </td>
                            <td colspan="6" rowspan="4" class="ta-l">{{ $ShinnobuSamuraiParameter->history }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">タイプ</th>
                            <td colspan="4">{{ $ShinnobuSamuraiParameter->type }}</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">適正</th>
                            <td colspan="4">
                                @if ($ShinnobuSamuraiParameter->appropriate_position == '主将')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'captain') }}" title="主将キャラ一覧">主将</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_position == '参謀')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'staff') }}" title="参謀キャラ一覧">参謀</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_position == '先鋒')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_position', 'pioneer') }}" title="先鋒キャラ一覧">先鋒</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2" class="back_cloud_blue color_font_black fw-n">兵科</th>
                            <td colspan="4">
                                @if ($ShinnobuSamuraiParameter->appropriate_army == '槍兵')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'spear-soldier') }}" title="槍兵キャラ一覧">槍兵</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_army == '騎兵')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'cavalry') }}" title="騎兵キャラ一覧">騎兵</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_army == '忍者')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'ninja') }}" title="忍者キャラ一覧">忍者</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_army == '鉄砲')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'gun') }}" title="鉄砲キャラ一覧">鉄砲</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_army == '大筒')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'odzu') }}" title="大筒キャラ一覧">大筒</a>
                                @elseif ($ShinnobuSamuraiParameter->appropriate_army == '農兵')
                                    <a href="{{ route('shinnobunaga.database.samurais.show.appropriate_army', 'farmer') }}" title="農兵キャラ一覧">農兵</a>
                                @endif
                            </td>
                        </tr>
                        @if ($ShinnobuSamuraiParameter->anecdote)
                            <tr>
                                <td colspan="12" class="back_cloud_blue color_font_black fw-n">逸話</td>
                            </tr>
                            <tr>
                                <td colspan="12">{{ $ShinnobuSamuraiParameter->anecdote }}</td>
                            </tr>
                        @endif
                        @if ($ShinnobuSamuraiParameter->comment)
                            <tr>
                                <td colspan="12" class="back_cloud_blue color_font_black fw-n">コメント</td>
                            </tr>
                            <tr>
                                <td colspan="12" class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameter->comment)) !!}</td>
                            </tr>
                        @endif
                    </table>
                    <div class="mt-24 d-f j-sb">
                        @if (isset($BeforeShinnobuSamuraiParameter))
                            <div>
                                <a href="{{ route('shinnobunaga.database.samurais.show', $BeforeShinnobuSamuraiParameter->id) }}" class="font_small"><{{ $BeforeShinnobuSamuraiParameter->samurai->name }}</a>
                            </div>
                        @else
                            <div></div>
                        @endif
                        @if (isset($NextShinnobuSamuraiParameter))
                            <div>
                                <a href="{{ route('shinnobunaga.database.samurais.show', $NextShinnobuSamuraiParameter->id) }}" class="font_small">{{ $NextShinnobuSamuraiParameter->samurai->name }}></a>
                            </div>
                        @else
                            <div></div>
                        @endif
                    </div>
                    @if (count($WarSamurais))
                        <section class="mt-36 war_samurai">
                            <h2 class="rich_h2">{{ $Samurai->name }}が登場する合戦</h2>
                            <ul>
                                @foreach ($WarSamurais as $WarSamurai)
                                    <li class="mt-24">
                                        <h3 class="rich_h3">{{ $WarSamurai->war->name }}（{{ $WarSamurai->war->kana }}）　[<a href="{{ route('other.war.single', $WarSamurai->war->disturbance->id) }}">{{ $WarSamurai->war->disturbance->name }}</a>]</h3>
                                        @include('components.war', ['war' => $WarSamurai->war])
                                        <p class="ta-r mt-12">
                                            <a href="{{ route('other.war.single', $WarSamurai->war->disturbance_id) }}">
                                                詳細を見る
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif
                    @if (count($SamuraiRelatedArticles))
                        <section class="mt-36">
                            <h2>{{ $Samurai->name }}が登場する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24 narrow_card">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <picture>
                                                <source type="image/webp" srcset="{{ My_func::return_article_main_img_webp_url($SamuraiRelatedArticle->article->id) }}" class="card_image">
                                                <img src="{{ My_func::return_article_main_img_url($SamuraiRelatedArticle->article->id) }}" width="100%" height="100%" class="card_image" alt="{{ $SamuraiRelatedArticle->article->title }}">
                                            </picture>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>{{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}</time>
                                                    <p class="card_category">
                                                        @foreach ($SamuraiRelatedArticle->article->articleCategorySearches as $mainArticleCategorySearch)
                                                            @if ($mainArticleCategorySearch->is_main_category)
                                                                {{ $mainArticleCategorySearch->articleCategory->name }}
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif

                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.database.samurais.show', 'argument1' => $ShinnobuSamuraiParameter])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection