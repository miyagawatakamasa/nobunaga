@extends('layouts.layout')

@section('title', '家宝システム【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」での家宝システムについての攻略記事です。名声は豪商から家宝を買うことができます。豪商の滞在時間は8時間であり、4時間離れると新しい豪商が来訪します。家宝は展示することでステータスを有効にすることができます。展示は領主の貴重値を消費し、領主の貴重値の上限が足りない場合、さらに家宝を展示することができません。">
    <meta property="og:description" content="ゲーム「新信長の野望」での家宝システムについての攻略記事です。信名声は豪商から家宝を買うことができます。豪商の滞在時間は8時間であり、4時間離れると新しい豪商が来訪します。家宝は展示することでステータスを有効にすることができます。展示は領主の貴重値を消費し、領主の貴重値の上限が足りない場合、さらに家宝を展示することができません。" />
    <meta property="og:title" content="家宝システム【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "家宝システム【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月29日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年6月21日</span> --}}
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/heirloom.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>家宝システム【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        {{-- <div class="main_visual mt-24" style="background-image: url('{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}');"></div> --}}
                        <p class="font_small mt-12">新信長の野望(シンノブ)の家宝システムについてまとめています。ゲームを攻略する際の参考にしてください。</p>

                        <h2>家宝システムについて</h2>
                        <p>野党砦を討伐すると名声が得られます。名声は豪商から家宝を買うことができます。</p>
                        <h3>豪商の来訪</h3>
                        <p>豪商の滞在時間は8時間であり、4時間離れると新しい豪商が来訪します。</p>
                        <p>丁銀を消費して豪商を早く招待したり、早めに帰らせたりすることができますが、丁銀がもったいないのでやらないようにしましょう。</p>
                        <h3>家宝ステータス</h3>
                        <p>家宝ステータスは<span class="color_red">固定ステータス・付与効果・一式ステータス・貴重値・種類</span>の五要素から成り立ちます。</p>
                        <h3>ランダム属性</h3>
                        <p>N以上の家宝のみ付与効果が出現します。品質の高い家宝ほど複数の付与効果を獲得できる確率が高くなります。</p>
                        <h3>貴重値</h3>
                        <p>各家宝の展示には領主の貴重値を消費する必要があります。この貴重値上限は領主レベルの上昇と共にアップします。</p>
                        <h3>種類</h3>
                        <p>全ての家宝には4種類に分けられ、それぞれ茶器・書籍・武器・武備になります。家宝の種類と継承に関係しています。</p>
                        <h3>家宝強化</h3>
                        <p>各家宝を豪商から取得した直後、その固有ステータスは初期ステータスになります。</p>
                        <p>名声を使用して家宝を強化し固定ステータスを上げることができます。</p>
                        <p>品質が高い家宝ほど強化の上限も高くなります。</p>
                        <h3>家宝継承</h3>
                        <p>同種の家宝間はステータスの継承が可能です。</p>
                        <p>名声・金砂を消費して付与効果を同種の家宝に移すことができます。</p>
                        <p>ステータスが継承された家宝は消費されるので注意。</p>
                        <h3>家宝展示</h3>
                        <p>家宝は展示することでステータスを有効にすることができます。</p>
                        <p>展示は領主の貴重値を消費し、領主の貴重値の上限が足りない場合、さらに家宝を展示することができません。</p>
                        <p>同じ家宝は一つしか展示できないので注意。</p>
                        <div class="mt-48">
                            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                                 crossorigin="anonymous"></script> --}}
                            <ins class="adsbygoogle"
                                 style="display:block; text-align:center;"
                                 data-ad-layout="in-article"
                                 data-ad-format="fluid"
                                 data-ad-client="ca-pub-2958289145034418"
                                 data-ad-slot="4794895012"></ins>
                            <script>
                                 (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <h3>一式ステータス</h3>
                        <p>一部の下方は特殊な組合を構成することができます。</p>
                        <p>これらを一緒に展示すると、一式ステータスを発揮することができます。</p>
                        <h3>家宝の回収</h3>
                        <p>家宝の回収で購入と強化に消費した60%の名声を返却することができます。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.strategy.heirloom'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection