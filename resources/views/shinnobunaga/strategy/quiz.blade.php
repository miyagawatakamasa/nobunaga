@extends('layouts.layout')

@section('title', 'クイズの答え【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」での序盤の進め方についての攻略記事です。信長の妹であるお市の方が、夫の浅井長政が織田との同盟を破棄するのを聞き、架空武将である主人公に助けを求めるという話となっています。天下統一を目前にして、明智光秀の謀叛に遭い本能寺に散った。居城を攻められ、市と娘たちを信長に託し自害した。織田信長に敗れ、信長の次男・信雄を養子とするが、のちに殺された。">
    <meta property="og:description" content="ゲーム「新信長の野望」での序盤の進め方についての攻略記事です。信長の妹であるお市の方が、夫の浅井長政が織田との同盟を破棄するのを聞き、架空武将である主人公に助けを求めるという話となっています。天下統一を目前にして、明智光秀の謀叛に遭い本能寺に散った。居城を攻められ、市と娘たちを信長に託し自害した。織田信長に敗れ、信長の次男・信雄を養子とするが、のちに殺された。" />
    <meta property="og:title" content="クイズの答え【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "クイズの答え【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月15日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">{{ $Article->additioned_at->format('Y年m月d日') }}</span> --}}
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/beginner.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>クイズの答え【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="main_visual mt-24" style="background-image: url('{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}');"></div>
                        <p class="mt-24">こんにちは、信長の野望シリーズを革新（2005年）の頃からプレイしているtakaです。</p>
                        <p>「新信長の野望」をプレイしているとちょくちょくクイズを出されますね。</p>
                        <p>歴史を知らないと答えられないものや、ゲームの仕様などの出題があるので、たまに間違うことがあります。なのでまとめてみました。</p>
                        <h2>クイズ一覧</h2>
                        <h3></h3>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.strategy.beginner'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection