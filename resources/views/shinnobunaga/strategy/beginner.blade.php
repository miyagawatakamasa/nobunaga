@extends('layouts.layout')

@section('title', '序盤の進め方【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」での序盤の進め方についての攻略記事です。信長の妹であるお市の方が、夫の浅井長政が織田との同盟を破棄するのを聞き、架空武将である主人公に助けを求めるという話となっています。天下統一を目前にして、明智光秀の謀叛に遭い本能寺に散った。居城を攻められ、市と娘たちを信長に託し自害した。織田信長に敗れ、信長の次男・信雄を養子とするが、のちに殺された。">
    <meta property="og:description" content="ゲーム「新信長の野望」での序盤の進め方についての攻略記事です。信長の妹であるお市の方が、夫の浅井長政が織田との同盟を破棄するのを聞き、架空武将である主人公に助けを求めるという話となっています。天下統一を目前にして、明智光秀の謀叛に遭い本能寺に散った。居城を攻められ、市と娘たちを信長に託し自害した。織田信長に敗れ、信長の次男・信雄を養子とするが、のちに殺された。" />
    <meta property="og:title" content="序盤の進め方【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "序盤の進め方【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月15日</span>
                            <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年6月21日</span>
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/beginner.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>序盤の進め方【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="main_visual mt-24" style="background-image: url('{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}');"></div>
                        <p class="mt-24">こんにちは、信長の野望シリーズを革新（2005年）の頃からプレイしているtakaです。</p>
                        <p><span class="fw-b">「新信長の野望」がリリースされまし</span>た。おめでとうございます。</p>
                        <p>歴代の信長の野望シリーズは、パソコンやPlayStation版がほとんどでしたが、今作「新信長の野望」は<span class="fw-b">スマホ版</span>となります。</p>
                        <p>信長の野望シリーズが隙間時間にスマホできるのでうれしいですね。</p>
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2>「新信長の野望」はいつリリースされたのか？</h2>
                        <p>「新信長の野望」は<span class="fw-b">2022年6月14日</span>11:00からサービス開始です。</p>

                        <h2>【新信長の野望】のアプリ情報</h2>
                        <p>信長の野望シリーズの開発といえば©コーエーテクモゲームスさんだが、この【新信長の野望】は<span class="color_red">BBGame株式会社</span>という会社が作成しています。<br>
                            この会社は、「香港台湾、韓国、欧米に続いて日本での事業展開を目指して起業したゲーム会社」です。</p>
                        <p>2013年に発売された「信長の野望・創造」の正式ライセンスを受けて作られているので、一部グラフィックなどは創造のものを使っていますね。</p>
                        <table class="font_small mt-24 w-100">
                            <tr>
                                <th>ジャンル</th>
                                <td>MMO戦略シミュレーションゲーム</td>
                            </tr>
                            <tr>
                                <th>プレイ料金</th>
                                <td>基本プレイ無料（アイテム課金制）</td>
                            </tr>          
                            <tr>
                                <th>対応機種</th>
                                <td>iOS 11.0以上 / Android 5.0以上</td>
                            </tr>
                            <tr>
                                <th>推奨年齢</th>
                                <td>12歳以上</td>
                            </tr>
                            <tr>
                                <th>会社</th>
                                <td>Hong Kong Black Beard Game Limited</td>
                            </tr>
                            <tr>
                                <th>公式サイト</th>
                                <td><a href="https://shinnobunaga.bbgame.jp/" target="_blank">『新信長の野望』公式サイト</a></td>
                            </tr>
                            <tr>
                                <th>タイトル名</th>
                                <td>新信長の野望</td>
                            </tr>
                            <tr>
                                <th>公式Twitter</th>
                                <td><a href="https://shinnobunaga.bbgame.jp/" target="_blank">『新信長の野望』公式Twitter</a></td>
                            </tr>
                            <tr>
                                <th>権利表記</th>
                                <td>©BBGAME All rights reserved.<br>©コーエーテクモゲームス<br>All rights reserved.</td>
                            </tr>
                        </table>
                        <div class="mt-24">
                            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                                crossorigin="anonymous"></script> --}}
                            <ins class="adsbygoogle"
                                style="display:block; text-align:center;"
                                data-ad-layout="in-article"
                                data-ad-format="fluid"
                                data-ad-client="ca-pub-2958289145034418"
                                data-ad-slot="4794895012"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <h2>序盤の進み方</h2>
                        <h3>オープニングムービー</h3>
                        <img src="{{ asset('img/shinnobunaga/beginner/1.png')}}" alt="新信長の野望 オープニングムービー1" width="100%" height="100%">
                        <p>アプリのダウンロードを行い、ゲームをスタートをするとまずオープニングムービーが流れます。</p>
                        <p>世は戦国時代、一人の武将（<a href="{{ route('shinnobunaga.database.samurais.show', '1') }}">織田信長</a>）が日本を導いていく。信長の妹で浅井長政の妻であるお市の方が、斎藤龍興から攻められ、架空武将である主人公に助けを求めるという話となっています。</p>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '1') }}">織田信長</a><br>信秀の嫡男。今川義元を桶狭間で破る。以後、天下布武を標榜して敵対勢力を次々と滅ぼした。天下統一を目前にして、明智光秀の謀叛に遭い本能寺に散った。</p>
                        </div>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '7') }}">お市の方</a><br>織田信長の妹。浅井長政に嫁ぐが、長政の自害後、実家に戻る。本能寺の変後は柴田勝家に嫁ぎ、勝家とともに越前北ノ庄城で自害した。絶世の美女と伝わる。</p>
                        </div>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '43') }}">浅井長政</a><br>近江の戦国大名。小谷城主。久政の子。織田信長の妹・市を娶るが、朝倉家との友誼を重んじ信長と敵対。居城を攻められ、市と娘たちを信長に託し自害した。</p>
                        </div>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '62') }}">斎藤龍興</a><br>美濃の戦国大名。義龍の嫡男。父の死後家督を継ぐ。美濃三人衆に背かれ、織田信長に敗れて追放された。その後は朝倉家に属し、越前刀禰坂合戦で戦死した。</p>
                        </div>
                        <img src="{{ asset('img/shinnobunaga/beginner/2.png')}}" alt="新信長の野望 オープニングムービー2" width="100%" height="100%">

                        <p>オープニングムービーが終わると、サーバーの選択になります。</p>
                        <p>友達と一緒に遊びたい場合は、ここで<span class="fw-b yellow_marker">同じサーバーを選ばないといけないので注意</span>。</p>
                        <h3>キャラ見た目選択</h3>
                        <img src="{{ asset('img/shinnobunaga/beginner/3.png')}}" alt="新信長の野望 キャラ見た目選択" width="100%" height="100%">
                        <p>次にキャラの見た目選択があります。</p>
                        <p>森蘭丸・北畠具教や蘆名盛氏のグラフィックや今作で追加された独自のグラフィックがありますね。</p>
                        <div class="retuden">
                            <p>森蘭丸<br>織田家臣。可成の三男。主君・信長の小姓を務める。利発で容姿美しく、信長に寵愛された。将来を嘱望されるが、本能寺の変で信長に従って奮戦し戦死した。</p>
                        </div>
                        <div class="retuden">
                            <p>北畠具教<br>伊勢国司・北畠家８代当主。晴具の子。織田信長に敗れ、信長の次男・信雄を養子とするが、のちに殺された。塚原卜伝より秘伝「一の太刀」を授けられた。</p>
                        </div>
                        <div class="retuden">
                            <p>蘆名盛氏<br>蘆名家１６代当主。蘆名家を伊達家と並ぶ奥州屈指の大名に育て上げた、蘆名家中興の祖。同盟をうまく利用し、会津一円から北越後に及ぶ所領を獲得した。</p>
                        </div>
                        <h3>名前入力</h3>
                        <p>文字制限厳しい、ユーザー間で一意である必要があります。</p>
                        <h3>チュートリアル</h3>
                        <p>その後チュートリアルを行います。</p>
                        <p>バトルのチュートリアル後には、チュートリアル単発ガチャ（中身固定 井伊直政）を引くことができます。</p>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '37') }}">井伊直政</a><br>徳川家臣。徳川四天王の１人。軍装を赤で統一した軍兵は「赤鬼」と恐れられ、常に先鋒を争った。関ヶ原合戦では島津軍を追撃し、島津豊久を討ち取った。</p>
                        </div>
                        <p>進めていくと「天守をLv4にあげる」ミッションが出ます。</p>
                        <p>天守をLv4にあげるには、鉄砲道場をLv2に、城壁をLv1にレベルアップすると、天守をLv4にアップできます。</p>
                        <p>天守のレベルアップには、10分必要です。</p>
                        <p>短縮するには、丁銀が必要ですが、これはパズドラの魔法石のように、入手機会が少ない貴重なアイテムなため、無課金前提の方は温存しておきましょう。</p>
                        <h3>ミッションを進める</h3>
                        <img src="{{ asset('img/shinnobunaga/beginner/4.png')}}" alt="新信長の野望 ミッション" width="100%" height="100%">
                        <p>天守をLv4にあげると、比較的自由に行動することができます。</p>
                        <p>まずは、画面右下のミッションを進めていきましょう！</p>
                        <img src="{{ asset('img/shinnobunaga/beginner/5.png')}}" alt="新信長の野望 森可成と松浦隆信が仲間に加わる" width="100%" height="100%">
                        <p>ミッションを進めると、森可成と松浦隆信が仲間に加わります。</p>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '70') }}">森可成</a><br>織田家臣。尾張統一戦や桶狭間合戦などで活躍した。宇佐山城主を務め、琵琶湖の南岸を固める。のちに浅井・朝倉連合軍の攻撃を受け、衆寡敵せず戦死した。</p>
                        </div>
                        <div class="retuden">
                            <p><a href="{{ route('shinnobunaga.database.samurais.show', '76') }}">松浦隆信</a><br>肥前の豪族。平戸城主。興信の子。平戸港を拠点として南蛮貿易を行う。豊富な資金力を背景に勢力を拡大するが、のちに龍造寺隆信に敗れ、隆信に従属した。</p>
                        </div>
                        <p>森可成は織田家臣でわかるが、松浦隆信は龍造寺なので、肥前の人ですね。どういうチョイスでしょうか？</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.strategy.beginner'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection