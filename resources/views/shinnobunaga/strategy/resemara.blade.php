@extends('layouts.layout')

@section('title', 'リセマラ当たりランキング【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」でのリセマラ当たりランキングについての記事です。新信長の野望(シンノブ)のリセマラ当たりキャラランキングです。リセマラ終了タイミングやランキングの基準、各キャラの詳細を掲載。シンノブでリセマラをする際の参考にして下さい。">
    <meta property="og:description" content="ゲーム「新信長の野望」でのリセマラ当たりランキングについての記事です。新信長の野望(シンノブ)のリセマラ当たりキャラランキングです。リセマラ終了タイミングやランキングの基準、各キャラの詳細を掲載。シンノブでリセマラをする際の参考にして下さい。" />
    <meta property="og:title" content="リセマラ当たりランキング【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "リセマラ当たりランキング【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月23日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年6月21日</span> --}}
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/ranking.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>リセマラ当たりランキング【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="main_visual mt-24" style="background-image: url('{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}');"></div>
                        <p class="font_small mt-12">新信長の野望(シンノブ)のリセマラ当たりキャラランキングです。リセマラ終了タイミングやランキングの基準、各キャラの詳細を掲載。シンノブでリセマラをする際の参考にして下さい。</p>

                        <h2>リセマラは必要？</h2>
                        <p>新信長の野望では、デイリーミッションで得られる「丁銀」を南蛮商人に使うことで、「登用券・金」という 最高レアリティーのSSRの武将を取得できるガチャを購入できます。</p>
                        <p>ゲーム中盤・終盤では全ての武将を持っているが、覚醒が済んでいないという状況になると予想できるので、<span class="fw-b">リセマラの必要はなく、ゲームを進める方を優先した方が良いでしょう</span>。</p>
                        <p>しかし、絶対欲しい武将がいる場合などはリセマラをする意味があるでしょう。</p>
                        <h2>リセマラ当たりランキング</h2>
                        <h3>SSランク</h3>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            @foreach ($ShinnobuSamuraiParameters as $ShinnobuSamuraiParameter)
                                @if ($ShinnobuSamuraiParameter->rank == 'SS')
                                    <tr>
                                        <td>
                                            <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}">
                                                <div>
                                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                                </div>
                                                @if (isset($ShinnobuSamuraiParameter->samurai))
                                                    {{ $ShinnobuSamuraiParameter->samurai->name }}
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                        </td>
                                        <td>{{ $ShinnobuSamuraiParameter->appropriate_army }}</td>
                                        <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameter->comment)) !!}</td>
                                    </tr> 
                                @endif
                            @endforeach
                        </table>
                        <h3>Sランク</h3>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            @foreach ($ShinnobuSamuraiParameters as $ShinnobuSamuraiParameter)
                                @if ($ShinnobuSamuraiParameter->rank == 'S')
                                    <tr>
                                        <td>
                                            <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id) }}">
                                                <div>
                                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                                </div>
                                                @if (isset($ShinnobuSamuraiParameter->samurai))
                                                    {{ $ShinnobuSamuraiParameter->samurai->name }}
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameter->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                        </td>
                                        <td>{{ $ShinnobuSamuraiParameter->appropriate_army }}</td>
                                        <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameter->comment)) !!}</td>
                                    </tr> 
                                @endif
                            @endforeach
                        </table>
                        <div class="mt-24">
                            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                                crossorigin="anonymous"></script> --}}
                            <ins class="adsbygoogle"
                                style="display:block; text-align:center;"
                                data-ad-layout="in-article"
                                data-ad-format="fluid"
                                data-ad-client="ca-pub-2958289145034418"
                                data-ad-slot="4794895012"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <h2>後々取得できる武将</h2>
                        <p>お市の方（SSR）は7日目のログインボーナスで取得でき、第７章完了時に引ける国士無双ガチャ（引き直しガチャ）で手に入る以下の武将を取得した際は再度リセマラするのも良いでしょう。</p>
                        <p>国士無双ガチャで取得出来る武将</p>
                        <ul>
                            <li>前田利家</li>
                            <li>前田慶次</li>
                            <li>島津歳久</li>
                            <li>鍋島直茂</li>
                            <li>最上義光</li>
                            <li>織田信秀</li>
                            <li>浅井初</li>
                            <li>六角定頼</li>
                        </ul>
                        <a href="{{ route('shinnobunaga.gacha.kokushimusou') }}" class="d-f relate_article mt-24 mb-24 shadow_hover">
                            <div class="relate_article_img_block">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/gacha/kokushimusou.png') }}" alt="" width="100%" height="100%" class="relate_article_img">
                            </div>
                            <div class="relate_article_description_block">
                                <p class="fw-b relate_article_title">国士無双ガチャおすすめ</p>
                                <p class="relate_article_description">国士無双ガチャで取得できる武将のおすすめ度合いを点数で表示しまとめています。</p>
                            </div>
                        </a>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.strategy.resemara'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection