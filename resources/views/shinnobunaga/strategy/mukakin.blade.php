@extends('layouts.layout')

@section('title', '無課金向けキャラランキング【新信長の野望】 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="ゲーム「新信長の野望」での無課金向けキャラランキングについての記事です。についてまとめています。無課金でゲームを攻略する際の参考にしてください。">
    <meta property="og:description" content="ゲーム「新信長の野望」でのリセマラ当たりランキングについての記事です。新信長の野望(シンノブ)ゲーム「新信長の野望」での無課金向けキャラランキングについての記事です。についてまとめています。無課金でゲームを攻略する際の参考にしてください。" />
    <meta property="og:title" content="無課金向けキャラランキング【新信長の野望】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/shinnobunaga/beginner/shinnobunaga.jpg') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "無課金向けキャラランキング【新信長の野望】| 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年6月24日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年6月21日</span> --}}
                        </time>
                    </div>
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/ranking.png')}}" alt="" width="30px" height="30px" class="mr-8">
                            <p>無課金向けキャラランキング【新信長の野望】</p>
                        </div>
                    </h1>
                    <div class="mt-24 article_body">
                        <p class="font_small mt-12">新信長の野望(シンノブ)の無課金におすすめのキャラ(Tier)についてまとめています。無課金でゲームを攻略する際の参考にしてください。</p>

                        <h2>役割別ランキング</h2>
                        <h3>主将適正</h3>
                        <p>武将の統率の高さによって部隊の兵数が増減するため、統率が高い武将を選択すると良い。</p>

                        <h4>Sランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[19]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[19]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[19]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[19]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[19]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[19]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[30]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[30]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[30]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[30]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[30]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[30]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[32]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[32]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[32]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[32]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[32]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[32]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[36]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[36]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[36]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[36]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[36]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[36]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Aランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[41]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[41]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[41]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[41]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[41]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[41]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[57]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[57]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[57]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[57]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[57]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[57]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[40]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[40]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[40]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[40]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[40]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[40]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[47]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[47]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[47]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[47]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[47]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[47]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[54]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[54]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[54]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[54]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[54]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[54]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Bランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[49]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[49]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[49]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[49]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[49]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[49]->comment)) !!}</td>
                            </tr>
                        </table>
                        <div class="mt-24">
                            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                                crossorigin="anonymous"></script> --}}
                            <ins class="adsbygoogle"
                                style="display:block; text-align:center;"
                                data-ad-layout="in-article"
                                data-ad-format="fluid"
                                data-ad-client="ca-pub-2958289145034418"
                                data-ad-slot="4794895012"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <h3>先鋒適正</h3>
                        <p>武将の武勇の高さによって部隊の攻撃力・防御力が増減するため、武勇が高い武将を選択すると良い。</p>
                        <h4>Sランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[11]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[11]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[11]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[11]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[11]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[11]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[18]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[18]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[18]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[18]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[18]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[18]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Aランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[58]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[58]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[58]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[58]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[58]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[58]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[44]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[44]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[44]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[44]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[44]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[44]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[42]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[42]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[42]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[42]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[42]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[42]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[48]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[48]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[48]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[48]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[48]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[48]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[53]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[53]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[53]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[53]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[53]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[53]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Bランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[38]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[38]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[38]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[38]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[38]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[38]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[51]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[51]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[51]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[51]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[51]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[51]->comment)) !!}</td>
                            </tr>
                        </table>
                        <div class="mt-24">
                            {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                                crossorigin="anonymous"></script> --}}
                            <ins class="adsbygoogle"
                                style="display:block; text-align:center;"
                                data-ad-layout="in-article"
                                data-ad-format="fluid"
                                data-ad-client="ca-pub-2958289145034418"
                                data-ad-slot="4794895012"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <h3>参謀適正</h3>
                        <p>武将の智略の高さによって部隊の攻撃力・防御力が増減するため、智略が高い武将を選択すると良い。</p>
                        <h4>SSランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[6]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[6]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[6]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[6]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[6]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[6]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Sランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[26]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[26]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[26]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[26]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[26]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[26]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[28]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[28]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[28]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[28]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[28]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[28]->comment)) !!}</td>
                            </tr>
                        </table>
                        <h4>Aランク</h4>
                        <table class="font_small ta-c mt-24 w-100">
                            <tr>
                                <th style="width: 120px;">武将名</th>
                                <th>レアリティ</th>
                                <th>属性</th>
                                <th style="width: 50%;">コメント</th>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[52]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[52]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[52]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[52]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[52]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[52]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[34]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[34]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[34]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[34]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[34]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[34]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[45]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[45]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[45]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[45]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[45]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[45]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[46]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[46]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[46]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[46]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[46]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[46]->comment)) !!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameters[50]->id) }}">
                                        <div>
                                            <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[50]->id . '.png') }}" style="margin: auto;" alt="" width="60px" height="100%">
                                        </div>
                                        {{ $ShinnobuSamuraiParameters[50]->samurai->name }}
                                    </a>
                                </td>
                                <td>
                                    <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/samurai/' . $ShinnobuSamuraiParameters[50]->rarity . '.png') }}" style="margin: auto;" alt="" width="30px" height="100%">
                                </td>
                                <td>{{ $ShinnobuSamuraiParameters[50]->appropriate_army }}</td>
                                <td class="ta-l">{!! nl2br(e($ShinnobuSamuraiParameters[50]->comment)) !!}</td>
                            </tr>
                        </table>
                        <a href="{{ route('shinnobunaga.gacha.kokushimusou') }}" class="d-f relate_article mt-24 mb-24 shadow_hover">
                            <div class="relate_article_img_block">
                                <img src="{{ Storage::disk('s3')->url('img/shinnobunaga/gacha/kokushimusou.png') }}" alt="" width="100%" height="100%" class="relate_article_img">
                            </div>
                            <div class="relate_article_description_block">
                                <p class="fw-b relate_article_title">国士無双ガチャおすすめ</p>
                                <p class="relate_article_description">国士無双ガチャで取得できる武将のおすすめ度合いを点数で表示しまとめています。</p>
                            </div>
                        </a>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'shinnobunaga.strategy.mukakin'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection