@extends('layouts.layout')

@section('title', 'お問い合わせ 完了 | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="「信長の野望 徹底攻略」のお問い合わせ完了ページです。返信にお時間かかる場合がございます。ご了承くださいませ。">
    <meta property="og:description" content="「信長の野望 徹底攻略」のお問い合わせ完了ページです。返信にお時間かかる場合がございます。ご了承くださいませ。" />
    <meta property="og:title" content="お問い合わせ 完了 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <h1>お問い合わせ完了</h1>
                    <div class="main_contents mt-36">
                        <p>お問い合わせを受け付けました。<br>返信にお時間かかる場合がございます。ご了承くださいませ。</p>
                    </div>
                    <div class="mt-36">
                        <a href="{{ route('top') }}" title="トップページに戻る">トップページに戻る</a>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'contact'])
    </div>
@endsection