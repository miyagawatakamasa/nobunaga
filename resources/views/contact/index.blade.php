@extends('layouts.layout')

@section('title', 'お問い合わせ | 信長の野望 徹底攻略')

@section('meta')
    <meta name="description" content="「信長の野望 徹底攻略」のお問い合わせページです。必須項目を記載し、送信をお願いします。">
    <meta property="og:description" content="「信長の野望 徹底攻略」のお問い合わせページです。必須項目を記載し、送信をお願いします。" />
    <meta property="og:title" content="お問い合わせ | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "お問い合わせ | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <article class="article_contents p-12">
                    <h1>お問い合わせ</h1>
                    <div class="main_contents">
                        <p>下記の必要項目を入力して送信してください。</p>
                        <form id="contact-form" method="POST" action="">
                            @csrf

                            <div class="mt-24 d-f">
                                <div class="left_label">
                                    <label for="name">名前</label>
                                </div>
                                <div class="right_input">
                                    <input id="name" type="text"
                                    class="form-control mt-1 form-control-lg validate[required]" name="name"
                                    value="{{ old('name') }}" autocomplete="name">
                                </div>
                            </div>
                            {{-- @if ($errors->has('name'))
                                <div class="text-danger mt-2">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif --}}

                            <div class="mt-24 d-f">
                                <div class="left_label">
                                    <label for="email">メールアドレス</label>
                                </div>
                                <div class="right_input">
                                    <input id="email" type="text"
                                    class="form-control mt-1 form-control-lg validate[required]" name="email"
                                    value="{{ old('email') }}" autocomplete="email">
                                </div>
                            </div>
                            {{-- @if ($errors->has('email'))
                                <div class="text-danger mt-2">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif --}}

                            <div class="mt-24 d-f">
                                <div class="left_label">
                                    <label for="content">お問い合わせ内容</label>
                                </div>
                                <div class="right_input">
                                    <textarea name="content" id="content" cols="30" rows="10" class="form-control validate[required]" placeholder="お問い合わせ内容をこちらにご記入下さい。"></textarea>
                                </div>
                            </div>
                            {{-- @if ($errors->has('content'))
                                <div class="text-danger">
                                    {{ $errors->first('content') }}
                                </div>
                            @endif --}}

                            <div class="mt-24">
                                <button type="submit" class="mt-12 submit_button">送信する</button>
                                <a href="{{ route('top') }}" class="mt-12 cancel_button" title="キャンセル">キャンセル</a>
                            </div>
                        </form>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="inner inner_wrapper">
        @include('components.breadcrumbs', ['slug' => 'contact'])
    </div>
@endsection