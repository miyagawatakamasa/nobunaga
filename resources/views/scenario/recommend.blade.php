@extends('layouts.layout')

@section('title', 'おすすめシナリオ【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望 {{$GameTitle->name}}」での著者厳選のおすすめシナリオ一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での著者厳選のおすすめシナリオ一覧です。" />
    <meta property="og:title" content="おすすめシナリオ【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "おすすめシナリオ【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                            <p>おすすめシナリオ【{{$GameTitle->name}}】</p>
                        </div>
                    </h1>
                    <div>
                        <p>各シナリオでのおすすめ大名です。</p>
                        <h2>川中島の合戦</h2>
                        <h3>長宗我部家　難易度 ★★★☆☆</h3>
                        <p>国力が50位だが、周りの大名が弱いため、中規模になるには苦労しない。<br />
                            農兵が強く、戦闘で優位に立ちやすい。<br />
                            長宗我部国親が大名だが（数年で亡くなる）長宗我部元親が登場しているのですぐに家督を譲ろう。<br />
                            志が所領拡大から一領具足になる。<br />
                            一領具足の評定は、農業・軍事の方策をそれぞれ４個以上実行すると発動でき、効果が「方策実行に必要な施策力が減る」と優秀なため一直線で発動しにいく。<br />
                            施策では、「長宗我部掟書」が軍事力提案力上昇・中があるため優秀。<br />
                            鉄砲・騎馬の攻撃力上昇・小はおまけ程度。<br />
                            軍事は、農兵メインになるので農兵を選択しよう。</p>
                        <h4><span id="i">安芸家を攻略</span></h4>
                        <p>岡豊城・香宗城それぞれ2000ほど兵を貯め、4000で攻めると勝ちが揺るぎないと出て楽です。<br />
                        序盤は金銭が足りなくなるので、計略などせず無駄遣いしないように注意。</p>
                        <h4><span id="i-2">本山家を攻略</span></h4>
                        <p>強い武将がいないので、問題なく戦で勝てる。</p>
                        <h4><span id="i-3">一条家を攻略</span></h4>
                        <p>こちらも強い武将がおらず大友・西園寺・河野と同盟しているが援軍が余りこないため、問題なく戦で勝てる。</p>
                        <h4><span id="i-4">西園寺家を攻略</span></h4>
                        <p>強い武将がいないので、問題なく戦で勝てる。</p>
                        <h4><span id="i-5">宇都宮家を攻略</span></h4>
                        <p>グズグズしていると河野家や大友家が攻め滅ぼし、家臣を盗られてしまうので注意</p>
                        <h4><span id="i-6">河野家を攻略</span></h4>
                        <p>大除城は戦場上限が3700なため鷺ノ森城を先に攻める。<br />
                        毛利と同盟しているが</p>
                        <h4><span id="i-7">能登村上家を攻略</span></h4>
                        <p>城はひとつだが、戦場上限に差があり毛利の援軍も来るし村上武吉が強いため手強い。<br />
                        自軍は3000程度で、敵軍が5000程度なので、狼煙場（晴れか曇の時、味方全部隊に待機部隊が徐々に合流する）を作成し、持久戦を目指そう。<br />
                        これで四国の西半分を制覇したことだろう。</p>
                        <h4><span id="i-8">三好家を攻略</span></h4>
                        <p>東に三好家、北に毛利家、西に大友家と大大名が立ち塞がる。<br />
                        海を超えての戦は兵糧の消費が激しいので、まず東の三好家の攻略を目指そう。<br />
                        少し時を経たせ、軍事・農業の施策を伸ばす。<br />
                        二方面作戦で三好家に襲いかかる。<br />
                        四国を制圧すると、雑賀衆や浦上、赤松、別所を滅ぼし、武将の補給を行おう。<br />
                        三好を滅ぼす頃には、日本で1位か2位の勢力になっていると思うので、あとは全国統一まで作業ゲームです！</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'scenario.recommend', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection