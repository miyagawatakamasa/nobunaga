@extends('layouts.layout')

@section('title', 'シナリオ【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望 {{$GameTitle->name}}」でのシナリオ一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」でのシナリオ一覧です。" />
    <meta property="og:title" content="シナリオ【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "シナリオ【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1>
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/scenario.png')}}" alt="シナリオアイコン" width="30px" height="30px" class="mr-8">
                            <p>シナリオ【{{$GameTitle->name}}】</p>
                        </div>
                    </h1>
                    <ul class="mt-24">
                        @foreach ($Scenarios as $Scenario)
                            <li>
                                @if ($Scenario->year == 0)
                                    <p>{!! nl2br(e($Scenario->title)) !!}</p>
                                @else
                                    <p>{{ $Scenario->year }}年{{ $Scenario->month }}月　{!! nl2br(e($Scenario->title)) !!}</p>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'scenario.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection