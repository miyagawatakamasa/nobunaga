@extends('layouts.layout')

@section('title', '攻城戦のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。" />
    <meta property="og:title" content="攻城戦のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "攻城戦のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/tensyu.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>攻城戦のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i-23">攻城戦とは</span></h2>
                        <p>攻城戦とは、攻城側（攻め側）と籠城側（守り側）による城をめぐる戦いです。<br />
                        攻城側が勝てば城を奪う事ができます。</p>
                        <p>以下のいずれかの条件を達成すると攻城側の勝利となります。</p>
                        <ul>
                        <li>城の本丸の耐久を0にする</li>
                        <li>籠城側の部隊をすべて壊滅させる</li>
                        </ul>
                        <p>攻城側と籠城側が部隊を配置し、攻城戦を開始すると、あとは自動で進行します。</p>
                        <p>攻城側は一度に一軍勢のみが参加可能です。<br />
                        複数の軍勢が一つの拠点を同時攻略することはできません。</p>
                        <p>籠城側は拠点に武将が一人もいないと籠城戦を行えずに即敗北となります。<br />
                        敵付近の拠点には武将を数人は配属するようにしましょう。</p>
                        <p>攻城戦中にズームアウトすることで全国マップに戻る事ができます。</p>
                        <p>逆に全国マップから攻城戦中の城にズームインすることで攻城戦を再開する事ができます。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection