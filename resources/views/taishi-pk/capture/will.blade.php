@extends('layouts.layout')

@section('title', '志について【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での志についての攻略記事です。特に大名の志は、志を実現するための特性という形で、特殊な効果をもたらします。大名の後継者を選ぶ際に、先代の大名の志を引き継ぐか、後継の大名の志にするか選べます。後継大名の志を選んだ場合、志特性効果の開放状況は、新しい志によって決まります。一部の武将はゲームの進行によって志が変化することがあります。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での志についての攻略記事です。特に大名の志は、志を実現するための特性という形で、特殊な効果をもたらします。大名の後継者を選ぶ際に、先代の大名の志を引き継ぐか、後継の大名の志にするか選べます。後継大名の志を選んだ場合、志特性効果の開放状況は、新しい志によって決まります。一部の武将はゲームの進行によって志が変化することがあります。" />
    <meta property="og:title" content="志について【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "志について【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/will.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>志について【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">志とは</span></h2>
                        <p>志は、武将それぞれが理想とする生き方です。</p>
                        <p>特に大名の志は、志を実現するための特性という形で、特殊な効果をもたらします。</p>
                        <p>他大名も志を果たすために動いています。</p>
                        <h2><span id="i-2">志特性効果の解放について</span></h2>
                        <p>志特性の2番目と3番目の効果は、それぞれ異なる条件を満たすと開放されます。</p>
                        <p>特性には悪い効果もあり、無効にできないので注意しましょう。</p>
                        <h2><span id="i-3">志の引き継ぎと変化について</span></h2>
                        <p>大名の後継者を選ぶ際に、先代の大名の志を引き継ぐか、後継の大名の志にするか選べます。<br />
                        志を引き継いだ場合、開放した志特性効果や実行した方策も引き継がれます。</p>
                        <p>後継大名の志を選んだ場合、志特性効果の開放状況は、新しい志によって決まります。<br />
                        また新しい志で実行できない方策はすべて未実行隣、その分の思索力を獲得できます。</p>
                        <p>一部の武将はゲームの進行によって志が変化することがあります。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection