@extends('layouts.layout')

@section('title', '商業のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。" />
    <meta property="og:title" content="商業のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "商業のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/commercial.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>商業のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">商業とは</span></h2>
                        <p>「<span class="color_red">商業</span>」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。</p>
                        <p>全国には多数の商圏があり、<span class="fw-b">勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます</span>。</p>
                        <p>多くの商圏へ進出し、勢力の財源を整えましょう。</p>
                        <h3><span id="i-2">まずは進出</span></h3>
                        <p>ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。<br />
                        まずは、進出していない商圏へ進出し金銭収入を高めましょう。</p>
                        <p>商業コマンドは毎月、「<span class="color_red">行動数</span>」の回数だけ商圏へ命令を行えます。<br />
                            <span class="fw-b">行動数は使い切るように無駄なく商圏へ命令を行いましょう</span>。</p>
                        <p>また、行動数は方策で最大数を、大命で今月の回数を増やすことができます。<br />
                        しかし軍事面の大命の方が優先度が高いので行動数を大命で増やすのはおすすめしません。</p>
                        <h3><span id="i-3">進出ができなくなったら</span></h3>
                        <p>商圏の進出は領内の商圏にしかできません。<br />
                        ただし、外交で「<span class="color_red">通商</span>」を結んだ勢力には、進出した商圏を繋ぐ形で進出を行えます。</p>
                        <p>領内の商圏への進出が完了したら、近隣の勢力と「<span class="color_red">親善</span>」を重ね、「<span class="color_red">交渉</span>」から通称関係を結びましょう。</p>
                        <p>通商を結んだ勢力を増やすことで、全国の商圏に進出することもできます。<br />
                        進出済み商圏を増やし、財政を安定させましょう。</p>
                        <h2><span id="i">商圏への進出</span></h2>
                        <p>進出できる商圏は以下の通りです。</p>
                        <ul>
                        <li>自勢力の領内にある商圏</li>
                        <li>外交により通商協定を結んだ勢力の領内にある商圏（進出済み商圏との隣接が必要）</li>
                        </ul>
                        <p>最初に進出した勢力は「影響力」を多く持つことができます。<br />
                        どの勢力も進出していない商圏には、積極的に進出しましょう。</p>
                        <p>商圏に進出すると、勢力名と影響力が商圏情報に表示されます。</p>
                        <h2><span id="i-2">通常商圏と特殊商圏</span></h2>
                        <p>商圏には「<span class="color_red">通常商圏</span>」と「<span class="color_red">特殊商圏</span>」の2種類があり、以下のような特徴があります。</p>
                        <h3><span id="i-3">通常商圏</span></h3>
                        <ul>
                        <li>「進出」した勢力に毎月金銭収入を与える。</li>
                        <li>「商圏収益」「収入補正」「影響力」の割合に応じて収入が決まる。</li>
                        <li>「投資」すると他勢力の影響力を奪い、自勢力の影響力が増す。</li>
                        <li>進出している勢力が多いほど発展が早い。</li>
                        <li>「独占」すると他勢力は追い出され、進出できなくなる。</li>
                        </ul>
                        <h3><span id="i-4">特殊商圏</span></h3>
                        <ul>
                        <li>自勢力内にあるだけで収入を得られる。</li>
                        <li>「進出」と「投資」ができない。</li>
                        <li>独占すると特別な恩恵が発生し、その地方での投資すると当家収入が増加する。</li>
                        <li>最大まで発展しており、収入は増えない。</li>
                        </ul>
                        <h2><span id="i-5">楽市楽座とは</span></h2>
                        <p>志特性や方策で「<span class="color_red">自由交易</span>」を持つ勢力が商圏に進出すると「<span class="color_red">楽市楽座商圏</span>」に変化します。</p>
                        <p>楽市楽座商圏は成長そくだが通常の商圏より早くなり、商圏収入を上げやすくなります。<br />
                        ただし、「<span class="color_red">独占</span>」が実行されると楽市楽座は解除され、通常の商圏に戻ります。</p>
                        <p>独占で楽市楽座が解除された商圏は、「<span class="color_red">独占解除</span>」や拠点への進行で独占が解かれ、再び「<span class="color_red">自由交易</span>」を持つ勢力が商圏へ進出していれば、楽市楽座商圏に変化します。</p>
                        <h2><span id="i-6">商圏の発展</span></h2>
                        <h3><span id="i-7">通常商圏</span></h3>
                        <p>時間とともに「商圏収益」が自然に増加し、それに応じて収入も増えます。<br />
                        複数勢力が進出した商圏では商人たちの競合により、さらに発展の速度が増します。</p>
                        <h3><span id="i-8">特殊商圏</span></h3>
                        <p>既に最大まで発展しています。</p>
                        <h2><span id="i-9">特殊商圏とは</span></h2>
                        <p>特殊商圏は発展しないため、より多くの収入を得るには「<span class="color_red">独占</span>」する必要があります。<br />
                            <span class="fw-b">独占には高額な費用が必要ですが、特殊商圏ごとの特別な恩恵を得られます</span>。</p>
                        <p>恩恵の効果は商圏の「独占効果」にて確認できます。</p>
                        <p>商圏の独占時勢力の商圏には「独占」ができます。<br />
                        独占すると、他勢力の影響力を排除し、自勢力への収入を100%にできます。<br />
                        また、通商協定のある他勢力からの商圏進出を防ぎます。</p>
                        <p>ただし、複数勢力の競合による商圏収益の発展速度の上昇効果がなくなり、独占で追い出した勢力との外交関係が悪化するため、注意が必要です。<br />
                        独占状態は「独占解除」でいつでも解除できます。</p>
                        <h2><span id="i-10">大商圏とは</span></h2>
                        <p>大商圏とは、<span class="fw-b">一国内にある全ての通常商圏が特定の条件を満たした場合に成立する商圏です</span>。</p>
                        <p>一回の投資で大商圏傘下にある全ての商圏に投資でき、発展速度や収入補正も上がり、さらに他勢力が進出できなくなります。</p>
                        <p>国内の通常商圏が多いほど強力ですが、他勢力の侵攻などにより成立条件を満たさなくなると、解体されてしまいます。</p>
                        <p>同じ国（尾張や美濃など）の商圏が以下の条件を全て満たすと、国内にある通常商圏が連携し、大商圏が成立します。</p>
                        <ul>
                        <li>商圏のある群を当家の勢力圏にした</li>
                        <li>当家の影響力を100%にした（独占でなくても可）</li>
                        <li>商圏収益が450以上になった</li>
                        </ul>
                        <p>商圏が大商圏の条件を満たしているかは、商業コマンドで、通常商圏を選択した後、「国内商圏」のリストで確認できます。</p>
                        <h2><span id="i-11">資源とは</span></h2>
                        <p>一部の商圏には「<span class="color_red">資源</span>」が存在します。<br />
                        資源がある商圏へ進出することで資源の効果を得られます。</p>
                        <p>資源がある商圏へ進出することで提案される方策や、資源を多く持つほど効果が上昇する方策もあります。</p>
                        <p>商圏へ進出する際には商圏収益だけでなく、資源にも注目してみると良いでしょう。</p>
                        <p>各資源の効果は以下の通りです。</p>
                        <ul>
                        <li>馬産地<br />
                        馬産地のある商圏に多く進出すると取引で軍馬の相場が下がる。<br />
                        施設「牧場」がある場合、牧場の軍馬生産を3倍にする。</li>
                        <li>金山<br />
                        金山のある商圏の収入が増える。</li>
                        <li>銀山<br />
                        銀山のある商圏の収入が増える。<br />
                        金山より効果が小さい。</li>
                        <li>鉱山<br />
                        鉱山のある商圏に多く進出すると取引で鉄砲の相場が下がる。</li>
                        <li>鉄砲鍛冶<br />
                        毎月鉄砲が納品される。<br />
                        施設「鍛治村」がある場合、鍛治村の鉄砲生産を5倍にする。</li>
                        <li>貿易港<br />
                        貿易港がある商圏の収入が増え、毎月鉄砲が納品される。<br />
                        鉄砲と軍馬の購入量が増える。</li>
                        <li>商業港<br />
                        鉄砲と軍馬の購入量が増える。</li>
                        <li>漁港<br />
                        漁港のある商圏に多く進出すると農業で肥料を増やす方策が提案される。</li>
                        </ul>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection