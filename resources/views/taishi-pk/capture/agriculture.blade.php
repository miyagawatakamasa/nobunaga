@extends('layouts.layout')

@section('title', '農業のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。" />
    <meta property="og:title" content="農業のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "農業のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/agriculture.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>農業のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i-12">農業とは</span></h2>
                        <p>「<span class="color_red">農業</span>」とは拠点から「<span class="color_red">兵糧</span>」を得るため、民の農作業を支援するコマンドです。</p>
                        <h3><span id="i-13">重要なデータ</span></h3>
                        <p>農業には農地・肥沃度・生産性・治水という4つの重要な要素があります。<br />
                        それらを上昇させて拠点から得られる兵糧を増やしていきます。</p>
                        <h3><span id="i-14">実行タイミング</span></h3>
                        <p>「農業」は、以下の各季節の変わり目の月に実行できます。</p>
                        <ul>
                        <li>春（3月）</li>
                        <li>夏（6月）</li>
                        <li>秋（9月）</li>
                        <li>冬（12月）</li>
                        </ul>
                        <p>月ごとに2種類の命令から実行するものを選び、労力・肥料・良種のいずれかを消費して拠点の農作業を支援します。</p>
                        <h2><span id="i-15">兵糧収入を増やすには</span></h2>
                        <p><span class="w-b">毎月9月</span>に拠点の農民・農兵・肥沃度・生産性・治水が大きいほど兵糧収入は増えます。</p>
                        <p>農民と農兵の数は兵糧収入に大きく関わるので、まずは開墾を積極的に行い、農地を増やしましょう。</p>
                        <h3><span id="i-16">労力・肥料・良種</span></h3>
                        <p>農業の命令に必要な労力・肥料・良種は毎年9月の収穫後に最大値まで回復します。<br />
                        1年間の消費の配分には気を付けましょう。</p>
                        <h2><span id="i-17">農業コマンドについて</span></h2>
                        <p>農業で実行できるコマンドは以下です。</p>
                        <p>基本的には推奨拠点のみで施策を行うと良いでしょう。</p>
                        <h3><span id="3">春（3月）</span></h3>
                        <p>種蒔きは効果的です。必ず行いましょう。</p>
                        <ul>
                        <li>種蒔き<br />
                        生産性が大きく上昇。良種を消費。</li>
                        <li>土作り<br />
                        肥沃度が上昇。労力を消費。</li>
                        </ul>
                        <h3><span id="6">夏（6月）</span></h3>
                        <p>治水の低い拠点があれば、灌漑をしましょう。</p>
                        <ul>
                        <li>草刈り<br />
                        生産性が上昇。労力を消費。</li>
                        <li>灌漑<br />
                        治水が上昇。労力を消費。</li>
                        </ul>
                        <h3><span id="9">秋（9月）</span></h3>
                        <p>民忠の低い拠点があれば労役免除も有効です。</p>
                        <ul>
                        <li>開墾<br />
                        農地が上昇。治水が減少。労力を消費。</li>
                        <li>労役免除<br />
                        民忠と生産性が上昇。<br />
                        代わりに、1年間労力を消費する農業コマンドが実行不可に。</li>
                        </ul>
                        <h3><span id="12">冬（12月）</span></h3>
                        <ul>
                        <li>開墾</li>
                        <li>肥撤き<br />
                        肥沃度が大きく上昇。肥料を消費。</li>
                        </ul>
                        <h2><span id="i-18">労役免除について</span></h2>
                        <p>「労役免除」とは、民忠を上昇させる特殊なコマンドです。9月にのみ実行できます。</p>
                        <h3><span id="i-19">メリット</span></h3>
                        <ul>
                        <li><span class="fw-b">民忠が大きく上昇</span>。</li>
                        <li>民忠上昇により生産性が上昇。</li>
                        </ul>
                        <h3><span id="i-20">デメリット</span></h3>
                        <ul>
                        <li>労力を1消費。</li>
                        <li>その拠点では労力を消費する農業コマンドが1年間選べなくなる。</li>
                        </ul>
                        <h2><span id="i-21">農業コマンドの使用回数</span></h2>
                        <p>農業コマンドでは、<span class="fw-b">労力・肥料・良種の数によって実行できる回数が制限されます</span>。</p>
                        <p>どの季節に、どの拠点で、何に使うか、この選択が農業の要点です。<br />
                        基本的にはおすすめ通りに実行すれば良いです。</p>
                        <h3><span id="i-22">勢力管理</span></h3>
                        <p>3種類とも勢力全体で管理します。<br />
                        各拠点満遍なく実行したり、拠点を絞って実行したりもできます。</p>
                        <h3><span id="i-23">回数リセット</span></h3>
                        <p><span class="fw-b">9月の収穫時に上限値に合わせてリセットされます</span>。<br />
                        大命で増加した未使用の労力はこの時消えてしまいます。</p>
                        <p>8月までには全ての回数を使い切ることをお勧めします。</p>
                        <h2><span id="i-24">コマンドの使用回数</span></h2>
                        <h3><span id="i-25">回数の増やし方</span></h3>
                        <ul>
                        <li>労力<br />
                        勢力の農民の数によって決まる。<br />
                        開墾で農地を広げると農民が増える。<br />
                        労力は方策や大命でも増やせる。</li>
                        <li>良種<br />
                        方策「良種種別」で増やせる。
                        </li>
                        <li>肥料<br />
                        方策「肥料増産」で増やせる。
                        </li>
                        </ul>
                        <h2><span id="i-26">各種パラメータについて</span></h2>
                        <h3><span id="i-27">土地の広さ</span></h3>
                        <p>農地は拠点に所在できる、農民と農兵の合計人数を表します。<br />
                        「<span class="color_red">開墾</span>」で最大農地まで拡張できます。</p>
                        <p>最大農地は拠点ごとに定まっていますが、志特性や方策で上昇させることができます。<br />
                        また、開墾による拡張が進むと、民忠が上昇します。</p>
                        <p>農地に空きがある場合、月末に流民の一部が農民となります。<br />
                        兵糧収入は農民の人数に大きく依存します。<br />
                        農地の大きさと人口には注意しましょう。</p>
                        <h3><span id="i-28">土地の豊かさ（肥沃度）</span></h3>
                        <p>「<span class="color_red">土作り</span>」と「<span class="color_red">肥捌き</span>」で上昇します。</p>
                        <h3><span id="i-29">民の生産効率（生産性）</span></h3>
                        <p>毎年9月に拠点の民忠と、農民・農兵の比率で大きさが決まります。</p>
                        <p>「種蒔き」と「草刈り」を実行すると、次の9月を迎えるまで一時的に上昇します。<br />
                        「募兵」で農兵を減らしたり、民忠を高く保つことで高い値を維持できます。</p>
                        <h3><span id="i">水の統制度合（治水）</span></h3>
                        <p>「<span class="color_red">灌漑</span>」で上昇、「<span class="color_red">開墾</span>」で減少します。<br />
                        治水が高ければ洪水から田畑を守ることもあります。</p>
                        <p>農民と農兵の人口・肥沃度・生産性・治水の各パラメータが高いほど、9月の兵糧収入は増加します。</p>
                        <p>特に農地は兵糧収入に大きく関わり、農兵の人数にも影響するため、積極的に拡張しましょう。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection