@extends('layouts.layout')

@section('title', '人事のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での人事についての攻略記事です。城主となった武将は忠誠が上がります。城主を選ぶ際は忠誠が低い武将を優先しましょう。忠誠は、武将の大名に対する忠誠の度合いです。忠誠は最大で20、野心は最大で15です。家臣の忠誠があがる家宝を大名が所有している 他にも士官が長い武将は忠誠が挙がります。優秀な武将は忠誠をすぐに上げて、他勢力からの調略を阻止しましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での人事についての攻略記事です。城主となった武将は忠誠が上がります。城主を選ぶ際は忠誠が低い武将を優先しましょう。忠誠は、武将の大名に対する忠誠の度合いです。忠誠は最大で20、野心は最大で15です。家臣の忠誠があがる家宝を大名が所有している 他にも士官が長い武将は忠誠が挙がります。優秀な武将は忠誠をすぐに上げて、他勢力からの調略を阻止しましょう。" />
    <meta property="og:title" content="人事のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "人事のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/people.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>人事のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">任命とは</span></h2>
                        <p>城主を任命することができます。<br />
                        <span class="fw-b">城主となった武将は忠誠が上がります</span>。<br />
                        城主を選ぶ際は忠誠が低い武将を優先しましょう</p>
                        <p>デメリットがないので積極的に利用しましょう。</p>
                        <h2><span id="i-2">登用とは</span></h2>
                        <p>浪人が新たに領内に来た場合、月初に自動で登用画面になります。<br />
                        <span class="fw-b">人はいればいるほどいいので、能力が低い武将も必ず登用しましょう</span>。</p>
                        <h2><span id="i-3">追放とは</span></h2>
                        <p>追放で家臣を勢力から追い出すことができます。<br />
                        追放された武将は浪人となり、他勢力の領内に移動します。<br />
                        登用し直すことができないので、デメリットでしかないです。<br />
                        <span class="fw-b">縛りプレイをしたいドMの方のみ利用しましょう</span>。</p>
                        <h3><span id="i-4">おすすめ縛りプレイ</span></h3>
                        <ul>
                        <li>ハーレム縛り<br />
                        難易度 : ★★★★★<br />
                        男武将を全て追放する</li>
                        <li>一族縛り<br />
                        難易度 : ★★★★<br />
                        親族以外の武将を追放する</li>
                        <li>坊主縛り<br />
                        難易度 : ★★★<br />
                        坊主以外の武将を追放する</li>
                        </ul>
                        <h2><span id="i-5">忠誠とは</span></h2>
                        <p>忠誠は、武将の大名に対する忠誠の度合いです。<br />
                        武将ごとに野心が設定されています。</p>
                        <p><span class="fw-b">忠誠が低く野心が高い武将ほど、裏切ったり出奔したりする可能性が高くなります</span>。<br />
                        忠誠は最大で20、野心は最大で15です。<br />
                        忠誠が野心より低いと<span class="color_red">赤色のアイコン</span>で表示され、出奔や裏切りやすく危険です。<br />
                        野心に対して、忠誠があまり高くないと、<span class="color_red">黄色のアイコン</span>で表示され、可能性は低いものの裏切られる場合があります。<br />
                        野心に対して忠誠が十分に高いと、<span class="color_red">青色アイコン</span>で表示されます。<br />
                        <span class="fw-b yellow_marker">忠誠が高いと全能力に補正がかかり、統率・武勇・知略・内政・外政パラメーターが+5されます</span>。<br />
                        新たに他勢力から調略を受けることができませんが、忠誠が青になる前に密約を結んでいると裏切られる可能性があります。<br />
                        忠誠は様々な要因で変動します。<br />
                        例えば以下のような原因があります。</p>
                        <ul>
                        <li>大名の行いが志にかなっている</li>
                        <li>大名の一門である</li>
                        <li>大名と相性が良い</li>
                        <li>志特性の効果を受けている</li>
                        </ul>
                        <p>意図的に忠誠を上げる手段もいくつかあります。</p>
                        <ul>
                        <li>家臣の忠誠があがる家宝を大名が所有している</li>
                        <li>家臣に家宝を渡す</li>
                        <li>家臣に感状を渡す</li>
                        <li>城主に任命する</li>
                        <li>大名を除くもの同士で縁組する</li>
                        <li>忠誠を上げる大命を使う</li>
                        </ul>
                        <p>他にも士官が長い武将は忠誠が挙がります。<br />
                        優秀な武将は忠誠をすぐに上げて、他勢力からの調略を阻止しましょう。</p>
                        <h2><span id="i-6">成長とは</span></h2>
                        <p>すべての武将は命令を実行したり、特定の行動を撮ったりすると経験値を獲得します。<br />
                        獲得する経験値の量は、行動や成果などによって変わります。</p>
                        <p>経験値が一定までたまると能力が上昇します。<br />
                        特定の段階まで能力が上昇すると、個性を習得することがあります。<br />
                        経験値の増加以外でも、特定の条件を満たすと武将が成長します。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.human_resources'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection