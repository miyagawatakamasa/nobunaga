@extends('layouts.layout')

@section('title', '普請のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での普請についての攻略記事です。「改修」によって、拠点の天守を強化したり、曲輪に設備したりすることができます。天守は前線の拠点以外でも改修すると良いでしょう。巨城には「巨城天守」が設置されており、天守の中で特に大きな効果が得られます。小田原城と大阪城には「総構え」が設置されており毎月兵糧収入を得ることができます。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での普請についての攻略記事です。「改修」によって、拠点の天守を強化したり、曲輪に設備したりすることができます。天守は前線の拠点以外でも改修すると良いでしょう。巨城には「巨城天守」が設置されており、天守の中で特に大きな効果が得られます。小田原城と大阪城には「総構え」が設置されており毎月兵糧収入を得ることができます。" />
    <meta property="og:title" content="普請のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "普請のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/tensyu.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>普請のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">改修について</span></h2>
                        <p>「<span class="color_red">改修</span>」によって、拠点の天守を強化したり、曲輪に設備したりすることができます。<br />
                        敵の侵攻に対する備えとなるのはもちろんのこと、<span class="fw-b">その拠点から出陣した部隊が強化される</span>こともあります。</p>
                        <p>前線の拠点や戦術的に狙われやすい拠点は、積極的に改修しておくと良いでしょう。</p>
                        <h3><span id="i-2">拠点の防御力</span></h3>
                        <p>防御力とは<span class="fw-b">拠点の攻め口の中で最も弱いルートの評価</span>です。<br />
                        改修を行う事で評価を上げることができます。<br />
                        拠点を強化する際の指針にすると良いでしょう。</p>
                        <h3><span id="i-3">天守の改修</span></h3>
                        <p>以下の効果があります。<br />
                        天守は前線の拠点以外でも改修すると良いでしょう。</p>
                        <ul>
                        <li>天守のある曲輪の耐久が上昇</li>
                        <li>その拠点から出陣した部隊の能力を上昇</li>
                        <li>一部の天守では民忠が上昇</li>
                        </ul>
                        <a href="{{ route('tensyu.index') }}" class="d-f relate_article mt-24 mb-24 shadow_hover">
                            <div class="relate_article_img_block">
                                <img src="/img/sozai/shiro.jpg" alt="" width="100%" height="100%" class="relate_article_img">
                            </div>
                            <div class="relate_article_description_block">
                                <p class="fw-b relate_article_title">天守一覧</p>
                                <p class="relate_article_description">信長の野望 大志の「天守」一覧をまとめています。</p>
                            </div>
                        </a>
                        <h2><span id="i-4">設備の設置</span></h2>
                        <p>「設備」を曲輪に設置することで、籠城戦の際に、様々効果が得られます。<br />
                        設備には効果範囲があるので注意。</p>
                        <a href="{{ route('facility.index') }}" class="d-f relate_article mt-24 mb-24 shadow_hover">
                            <div class="relate_article_img_block">
                                <img src="/img/sozai/mon2.jpg" alt="" width="100%" height="100%" class="relate_article_img">
                            </div>
                            <div class="relate_article_description_block">
                                <p class="fw-b relate_article_title">設備一覧</p>
                                <p class="relate_article_description">信長の野望 大志の「設備」一覧をまとめています。</p>
                            </div>
                        </a>
                        <h2><span id="i-5">曲輪の増設</span></h2>
                        <p>拠点の「空地」に曲輪を「増設」することで城の守りの弱い場所を補強することができます。<br />
                        多額の金銭を必要とするため、どの改修を優先するか慎重に判断しましょう。</p>
                        <h2><span id="i-6">巨城天守</span></h2>
                        <p>巨城には「<span class="color_red">巨城天守</span>」が設置されており、天守の中で特に大きな効果が得られます。<br />
                        小田原城と大阪城には「総構え」が設置されており<span class="fw-b">毎月兵糧収入を得ることができます</span>。</p>
                        <h2><span id="i-7">築城とは</span></h2>
                        <p>「<span class="color_red">築城</span>」とは、新たな拠点を建てるコマンドです。<br />
                        建てる郡によって、平城か山城のどちらかになります。さらに曲輪の数や能力も違うため、どこに築城するかが重要です。</p>
                        <p>築城した郡に商圏がない場合には、新たに商圏ができます。<br />
                        内政を充実させたい場合にも築城すると良いでしょう。<br />
                        以下の条件を満たす群にのみ、築城が可能です。</p>
                        <ul>
                        <li>別の拠点がない</li>
                        <li>隣接する群に拠点が建っていない</li>
                        <li>海や湖などの水上ではない</li>
                        </ul>
                        <h2><span id="i-8">廃城とは</span></h2>
                        <p>「<span class="color_red">廃城</span>」とは、「築城」で建てた拠点を破壊するコマンドです。<br />
                        最初から立っている城は廃城にできません。<br />
                        また、廃城にした拠点の商圏が撤去あれ、群などの所属も変更されます。<br />
                        <span class="fw-b">メリットがないので基本は行いません</span>。</p>
                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.general_contract'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection