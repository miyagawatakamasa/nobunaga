@extends('layouts.layout')

@section('title', '調略のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での人事についての攻略記事です。城主となった武将は忠誠が上がります。城主を選ぶ際は忠誠が低い武将を優先しましょう。忠誠は、武将の大名に対する忠誠の度合いです。忠誠は最大で20、野心は最大で15です。家臣の忠誠があがる家宝を大名が所有している 他にも士官が長い武将は忠誠が挙がります。優秀な武将は忠誠をすぐに上げて、他勢力からの調略を阻止しましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での人事についての攻略記事です。城主となった武将は忠誠が上がります。城主を選ぶ際は忠誠が低い武将を優先しましょう。忠誠は、武将の大名に対する忠誠の度合いです。忠誠は最大で20、野心は最大で15です。家臣の忠誠があがる家宝を大名が所有している 他にも士官が長い武将は忠誠が挙がります。優秀な武将は忠誠をすぐに上げて、他勢力からの調略を阻止しましょう。" />
    <meta property="og:title" content="調略のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "調略のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/makimono.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>調略のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">工作とは</span></h2>
                        <p>「<span class="color_red">工作</span>」は他勢力に家臣を密使として送り込み、忠誠の低い武将との接触を試みるコマンドです。<br />
                        接触に成功すると、その武将との密約交渉が可能になります。</p>
                        <p>忠誠アイコンが赤色の武将は密使の能力に関わらずに接触できる可能性がありますが、忠誠アイコンが黄色の武将は「適正」が非常に高い武将か、「大命」の効果がないと接触できません。</p>
                        <p>工作で接触に成功した武将とは「<span class="color_red">密約</span>」を行うことができます。<br />
                        ただし、以下の理由により密約できなくなる場合もあるため、有力な武将と接触できた際は、積極的に試してみましょう。</p>
                        <ul>
                        <li>忠誠が青アイコンになるまで上昇する</li>
                        <li>他家と密約を結んでしまう</li>
                        <li>他家に所属が変わってしまう</li>
                        <li>大命「花押入感状」の対象になる</li>
                        </ul>
                        <h2><span id="i-2">密約とは</span></h2>
                        <p>「<span class="color_red">密約</span>」は工作で接触に成功した武将と交渉し、密約を結ぶkコマンドです。<br />
                        密約が成立すると「<span class="color_red">内応</span>」や「<span class="color_red">寝返</span>」が指示できます。</p>
                        <p><span class="fw-b">密約の交渉役を選ぶ際は「<span class="color_red">共通話題</span>」が多い武将がおすすめです</span>。<br />
                        密約を始める際に世間話から始められますが共通話題が多いと効果的な選択肢が出やすくなります。<br />
                        世間話を終えると密約の交渉に入ります。</p>
                        <p>密約で提示できる条件は以下の4種類です。<br />
                        「金銭」「家宝」「城主確約」「お家再興」</p>
                        <h3><span id="i-3">城主確約</span></h3>
                        <p>・相手が選んだ場合<br />
                        敵の城ごと寝返ることが期待できる<br />
                        ・提示した場合<br />
                        自勢力の城主に任命。数年間は解任不可。</p>
                        <h3><span id="i-4">お家再興</span></h3>
                        <p>本人が望む場合のみ選べます。<br />
                        <span class="fw-b">内応を促すと、大名として独立し、自勢力に従属します</span>。</p>
                        <p>密約を結んだ武将は以下の指示ができます。</p>
                        <ul>
                        <li>内応コマンドで裏切り行為を実行に移す</li>
                        <li>決戦や攻城戦にて「寝返」させる<br />
                        ※「金銭」や「家宝」で結んだ密約のみ</li>
                        </ul>
                        <p>また密約中の武将を武昌を敵中に残し、暗躍させることで、毎月以下の効果が期待できます。</p>
                        <ul>
                        <li>同じ勢力にいる忠誠が低い武将の紹介</li>
                        <li>施策力の増加</li>
                        </ul>
                        <p>ただし、半年以上暗躍されると感づかれることがあり、その際は密約が破棄されます。<br />
                        密約を一度結んだ武将は、忠誠が上昇しても内応などの行為を指示することができます。</p>
                        <p>逆に自分の家臣が密約を受けてしまった場合は、忠誠を上げるだけでは密約を解除できません。<br />
                        時間経過によって露見するか、大命の「<span class="color_red">花押入感状</span>」を実行する必要があります。</p>
                        <p>忠誠が低く優秀な武将は早い段階で感状や家宝を与えたり、城主の任命、姫との縁組などを行い、敵の調略を未然に防ぎましょう。</p>
                        <h2><span id="i-5">内応とは</span></h2>
                        <p>「<span class="color_red">内応</span>」とは、密約に応じた武将の約定に従って寝返らせるコマンドです。</p>
                        <p>金銭や家宝の約定であれば、引き抜くか戦場で寝返らせて、自勢力に迎え入れます。<br />
                        城主確約の約定では城主としての地位を数年間保証します。</p>
                        <p>なお、密約に応じる武将が自ら城主確約を希望した場合、所属勢力の拠点ごと寝返ることもあります。<br />
                        お家再興は、その武将が敵拠点で旗揚げし、配下を伴って自勢力に従属します。</p>
                        <h2><span id="i-6">寝返とは</span></h2>
                        <p>密約に応じた武将が決戦や攻城戦に出陣していると、交戦中に「<span class="color_red">寝返</span>」を指示できます。<br />
                        ただし、戦況が著しく悪いと応じません。</p>
                        <p>決戦では「寝返」する武将が部隊長であれば部隊ごと味方になり、副将であれば所属する部隊を潰走させます。<br />
                        「寝返」の対象となる部隊を視界に入れた状態で、画面下部の部隊リストを敵陣営に切り替え、「寝返」を押すことで寝返らせることができます。</p>                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection