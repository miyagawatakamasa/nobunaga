@extends('layouts.layout')

@section('title', '軍事のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。">
    <meta property="og:description" content="ゲーム「信長の野望 大志（パワーアップキット）」での内政についての攻略記事です。「商業」とは、商圏を管理し、毎月の金銭収入を増やすコマンドです。全国には多数の商圏があり、勢力が進出して影響力を持つ商圏からは、毎月金銭収入を得られます。ゲームを始めた直後は、影響力を持つ商圏が少なく、勢力の金銭収入が心許ないです。領内の商圏への進出が完了したら、近隣の勢力と「親善」を重ね、「交渉」から通称関係を結びましょう。" />
    <meta property="og:title" content="軍事のコツ【大志（パワーアップキット）】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "軍事のコツ【大志（パワーアップキット）】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/gunshi.png')}}" alt="" width="30px" height="30px" class="mr-8">
                        <p>軍事のコツ【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24 article_body">
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        <h2><span id="i">募兵とは</span></h2>
                        <p>「募兵」とは、拠点の兵数を調整するコマンドです。</p>
                        <p>拠点の塀が多いほど、行軍や決戦に多くの兵を参加させられます。</p>
                        <p>拠点ごとに農兵と足軽、それぞれの兵数を調整できます。</p>
                        <h3><span id="i-2">農兵は農民から募兵</span></h3>
                        <p>スライダーで兵数を指定すると、その兵数になるように農兵の数が毎月変動していきます。</p>
                        <p>農兵を募兵する際には、以下を覚えておきましょう。</p>
                        <ul>
                        <li>金銭は不要。</li>
                        <li>農民が減るため、募兵しすぎに注意。</li>
                        <li>農兵を減らすことで、農民に戻せる。</li>
                        </ul>
                        <p>ただし、農民に戻った農兵は疲労のため、9月まで農業に参加できない。</p>
                        <h3><span id="i-3">足軽は流民から募兵</span></h3>
                        <p>スライダーで兵数を指定すると、その兵数になるよう足軽の数が毎月変動していきます。</p>
                        <p>足軽を募兵する際には、以下を覚えておきましょう。</p>
                        <ul>
                        <li>毎月、兵数に応じた金銭が必要。<br />
                        金銭収支が赤字でも雇えるが、月末に俸給が払えないと拠点から去っていく。</li>
                        <li>兵数を減らすことで、足軽を解雇できる。<br />
                        ただし、解雇された足軽は拠点を去るため拠点の人口が減る。</li>
                        </ul>
                        <h2><span id="LvLv">農兵Lv・足軽Lvとは</span></h2>
                        <p>農兵Lvと足軽Lvは、勢力の農兵と足軽の練度を表しています。<br />
                        Lvが高いほど同じ兵数でも決戦を有利に進めることができるでしょう。</p>
                        <p>Lvは以下の方法で上げることができます。</p>
                        <ul>
                        <li>方策や志特性で永続的に上昇させる</li>
                        <li>大命の効果で一時的に上場させる</li>
                        </ul>
                        <p>大名の志によってこれらの有無は変化します。<br />
                        自勢力がこれらの方策や大命を持っていたら決戦の前に是非実行しましょう。</p>
                        <p>農兵Lvが上昇すると、以下の効果があります。</p>
                        <ul>
                        <li>農兵を含む部隊の「攻撃力」と「防御力」と「士気」が高くなる。</li>
                        <li>決戦が長時間になったとき、農兵の疲労による士気低下を防ぐ。</li>
                        </ul>
                        <p>足軽Lvが上昇すると、以下の効果があります。</p>
                        <ul>
                        <li>足軽を含む部隊の「攻撃力」と「防御力」と「士気」が高くなる。</li>
                        <li>戦況劣勢が続いても、農兵の疲労による士気低下が起きなくなる。</li>
                        </ul>
                        <h2><span id="i-4">宣戦とは</span></h2>
                        <p>「宣戦」とは、他勢力に大して宣戦布告を行い、合戦を始めるコマンドです。</p>
                        <p>宣戦を布告すると、侵攻側と防御側それぞれの陣営に加わる勢力が助力を表明します。</p>
                        <p>味方の陣営に加わった勢力は、金銭・兵糧や援軍を送ってくれる事があります。</p>
                        <p>敵陣営にいる勢力であっても、助力した勢力には侵攻できません。<br />
                        あくまで宣戦布告した勢力同士だけが侵攻できます。</p>
                        <p>合戦は「交渉」による「講和」が承諾されるか、どちらかの勢力が滅亡すると終結します。</p>                    </div>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'capture.plot'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection