@extends('layouts.layout')

@section('title', '天守【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での天守一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での天守一覧です。" />
    <meta property="og:title" content="天守【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "天守【{{ $GameTitle->name }}】信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/tensyu.png')}}" alt="天守アイコン" width="30px" height="30px" class="mr-8">
                        <p>天守【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24">
                        <p>信長の野望 大志PKでは、天守を増築することができます。</p>
                        <p class="mt-12">しかし、今作では攻城戦が単調なので防御のための天守の増築は優先度が低めです。</p>
                        <p class="mt-12">敵の大勢力と接した城の防御を高める時、または、お金が余ったゲーム後半で、出陣部隊の戦闘力を上げたり、民忠をあげる際に増築を行います。</p>
                    </div>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th style="width: 70px">名前</th>
                            <th>レベル</th>
                            <th>費用</th>
                            <th>開発期間</th>
                            <th>効果</th>
                        </tr>
                        @foreach ($TaishiTensyus as $TaishiTensyu)
                            <tr>
                                <td class="ta-l">{{ $TaishiTensyu->name }}</td>
                                <td>
                                    @if ($TaishiTensyu->level)
                                        {{ $TaishiTensyu->level }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if ($TaishiTensyu->cost)
                                        {{ $TaishiTensyu->cost }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if ($TaishiTensyu->period)
                                        {{ $TaishiTensyu->period }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="ta-l">
                                    @if ($TaishiTensyu->effect)
                                        {{ $TaishiTensyu->effect }}
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>                
                        @endforeach
                        <tr>
                            <th>名前</th>
                            <th>レベル</th>
                            <th>費用</th>
                            <th>開発期間</th>
                            <th>効果</th>
                        </tr>
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'tensyu.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection