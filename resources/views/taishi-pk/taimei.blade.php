@extends('layouts.layout')

@section('title', '大命【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での大命一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での大命一覧です。" />
    <meta property="og:title" content="大命【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "大命【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h2 class="d-f ai-c">
                        <img src="{{ asset('img/common/taimei.png')}}" alt="大命アイコン" width="30px" height="30px" class="mr-8">
                        <p>大命【{{$GameTitle->name}}】</p>
                    </h2>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th>No</th>
                            <th>大命</th>
                            <th>説明</th>
                            <th>期間</th>
                            <th>農業</th>
                            <th>商業</th>
                            <th>軍事</th>
                            <th>論議</th>
                            <th>大命</th>
                        </tr>
                        @foreach ($Taimeis as $Taimei)
                            <tr>
                                <td class="back_cloud_blue">{{ $Taimei->id }}</td>
                                <td class="ta-l">{{ $Taimei->name }}</td>
                                <td class="ta-l">{{ $Taimei->description }}</td>
                                <td>{{ $Taimei->period }}</td>
                                <td>{{ $Taimei->agriculture }}</td>
                                <td>{{ $Taimei->commercial }}</td>
                                <td>{{ $Taimei->military }}</td>
                                <td>{{ $Taimei->discussion }}</td>
                                <td>{{ $Taimei->taimei }}</td>
                            </tr>                
                        @endforeach
                        <tr>
                            <th>No</th>
                            <th>大命</th>
                            <th>説明</th>
                            <th>期間</th>
                            <th>農業</th>
                            <th>商業</th>
                            <th>軍事</th>
                            <th>論議</th>
                            <th>大命</th>
                        </tr>
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'taimei.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection