@extends('layouts.layout')

@section('title', '施設【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での施設一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での施設一覧です。" />
    <meta property="og:title" content="施設【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "施設【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/building.png')}}" alt="施設アイコン" width="30px" height="30px" class="mr-8">
                        <p>施設【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24">
                        <p>序盤は金銭・兵糧が足りなので、商人町・漁村・貯水池を作る。</p>
                        <p class="mt-12">大名がいる居城は、勢力を勢力を吸収したときに武将が大量に入ってくる。<br>
                        こまめに各城に人材を配置するのが面倒なので、居城に武将が溜まりがちになる。</p>
                        <p>したがって、居城周りに用兵塾・武芸塾・術策塾を建てると勢力下の武将が強く育つ。</p>
                        <p class="mt-12">敵勢力との隣接地では、防塁・巡見所を建設することで防御力を高める。</p>
                        <p>ゲーム中盤、金銭に余裕ができれば、牧場・鍛冶村を建設し戦闘を優位に進める。</p>
                    </div>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th rowspan="2" style="width: 60px">施設</th>
                            <th>系統</th>
                            <th>能力</th>
                            <th>個性1</th>
                            <th>個性2</th>
                            <th>新設費用</th>
                            <th>レベル上限</th>
                            <th>合算レベル上限</th>
                            <th>条件</th>
                        </tr>
                        <tr>
                            <th colspan="8">効果</th>
                        </tr>
                        @foreach ($TaishiPkBuildings as $TaishiPkBuilding)
                            <tr>
                                <td rowspan="2" class="back_cloud_blue">{{ $TaishiPkBuilding->name }}</td>
                                <td>{{ $TaishiPkBuilding->system }}</td>
                                <td>{{ $TaishiPkBuilding->ability }}</td>
                                <td>{{ $TaishiPkBuilding->individuality_first }}</td>
                                <td>{{ $TaishiPkBuilding->individuality_secound }}</td>
                                <td>{{ $TaishiPkBuilding->cost }}</td>
                                <td>{{ $TaishiPkBuilding->level }}</td>
                                <td>
                                    @if($TaishiPkBuilding->level_limit)
                                        {{ $TaishiPkBuilding->level_limit }}
                                    @else
                                        -
                                    @endisset
                                </td>
                                <td class="ta-l">{{ $TaishiPkBuilding->conditions }}</td>
                            </tr>
                            <tr>
                                <td colspan="8" class="ta-l">{{ $TaishiPkBuilding->effect }}</td>
                            </tr>             
                        @endforeach
                        <tr>
                            <th rowspan="2">施設</th>
                            <th>系統</th>
                            <th>能力</th>
                            <th>個性1</th>
                            <th>個性2</th>
                            <th>新設費用</th>
                            <th>レベル上限</th>
                            <th>合算レベル上限</th>
                            <th>条件</th>
                        </tr>
                        <tr>
                            <th colspan="8">効果</th>
                        </tr>
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'building.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection