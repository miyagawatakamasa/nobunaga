@extends('layouts.layout')

@section('title', '設備【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での設備一覧です。信長の野望 大志PKは攻城戦が単調になりがち。本丸に近い方に「砲台」や「丸太罠」を設置し、二の丸や三の丸には「櫓」や「落穴」を設置すればOK。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での設備一覧です。信長の野望 大志PKは攻城戦が単調になりがち。本丸に近い方に「砲台」や「丸太罠」を設置し、二の丸や三の丸には「櫓」や「落穴」を設置すればOK。" />
    <meta property="og:title" content="設備【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "設備【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/facility.png')}}" alt="設備アイコン" width="30px" height="30px" class="mr-8">
                        <p>設備【{{$GameTitle->name}}】</p>
                    </h1>
                    <div class="mt-24">
                        <p>信長の野望 大志PKは攻城戦が単調になりがち。<br>
                            城に作る設備は、何を作ろうとも1枠使ってしまうので、費用の高めのものを設置する方が防御力が高くなる。</p>
                        <p class="mt-12">本丸に近い方に「砲台」や「丸太罠」を設置し、二の丸や三の丸には「櫓」や「落穴」を設置すればOK。</p>
                        <p class="mt-12">大事な武将がいる城であれば「抜道」を作っておくと保険になる。</p>
                        <p>※「抜道」を破壊された場合、効果がなくなるので攻城戦時注意</p>
                    </div>
                    <table class="font_small ta-c mt-36 w-100">
                        <tr>
                            <th>設備名</th>
                            <th>費用</th>
                            <th>期間</th>
                            <th>効果</th>
                        </tr>
                        @foreach ($TaishiPkFacilities as $TaishiPkFacility)
                            <tr>
                                <td class="ta-l">{{ $TaishiPkFacility->name }}</td>
                                <td class="ta-l">{{ $TaishiPkFacility->cost }}</td>
                                <td class="ta-l">{{ $TaishiPkFacility->period }}</td>
                                <td class="ta-l">{{ $TaishiPkFacility->effect }}</td>
                            </tr>          
                        @endforeach
                        <tr>
                            <th>設備名</th>
                            <th>費用</th>
                            <th>期間</th>
                            <th>効果</th>
                        </tr>
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'facility.index', 'argument1' => $GameTitle])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection