@extends('layouts.layout')

@section('title', '作戦【' . $GameTitle->name . '】 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での作戦一覧です。">
    <meta property="og:description" content="ゲーム「信長の野望 {{$GameTitle->name}}」での作戦一覧です。" />
    <meta property="og:title" content="作戦【{{$GameTitle->name}}】 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "作戦【{{ $GameTitle->name }}】 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/strategy.png')}}" alt="作戦アイコン" width="30px" height="30px" class="mr-8">
                        <p>作戦【{{$GameTitle->name}}】</p>
                    </h1>
                    <table class="font_small ta-c mt-24 w-100">
                        @if ($GameTitle->slug == 'taishi')
                            <tr>
                                <th>No</th>
                                <th>作戦名</th>
                                <th>地形</th>
                                <th>効果</th>
                            </tr>
                            @foreach ($TaishiStrategies as $TaishiStrategy)
                                <tr>
                                    <td class="back_cloud_blue">{{ $TaishiStrategy->id }}</td>
                                    <td class="ta-l">{{ $TaishiStrategy->name }}</td>
                                    <td class="ta-l">{{ $TaishiStrategy->terrain }}</td>
                                    <td class="ta-l">{{ $TaishiStrategy->description }}</td>
                                </tr>                
                            @endforeach
                            <tr>
                                <th>No</th>
                                <th>作戦名</th>
                                <th>地形</th>
                                <th>効果</th>
                            </tr>
                        @elseif ($GameTitle->slug == 'taishi-pk')
                            <tr>
                                <th rowspan="2">名前</th>
                                <th>読み</th>
                                <th>必要軍議</th>
                                <th>条件</th>
                                <th>必要部隊数</th>
                            </tr>
                            <tr>
                                <th colspan="4">効果</th>
                            </tr>
                            @foreach ($TaishiPkStrategies as $TaishiPkStrategy)
                                <tr>
                                    <td rowspan="2" class="back_cloud_blue">{{ $TaishiPkStrategy->name }}</td>
                                    <td class="ta-l">{{ $TaishiPkStrategy->kana }}</td>
                                    <td>{{ $TaishiPkStrategy->cost }}</td>
                                    <td class="ta-l">{{ $TaishiPkStrategy->terrain }}</td>
                                    <td>{{ $TaishiPkStrategy->required_units }}</td>
                                </tr>   
                                <tr>
                                    <td colspan="4" class="ta-l">{{ $TaishiPkStrategy->description }}</td>
                                </tr>             
                            @endforeach
                            <tr>
                                <th rowspan="2">名前</th>
                                <th>読み</th>
                                <th>必要軍議</th>
                                <th>条件</th>
                                <th>必要部隊数</th>
                            </tr>
                            <tr>
                                <th colspan="4">効果</th>
                            </tr>
                        @endif
                    </table>
                    <div class="mt-36 pc_display">
                        <div id="im-f50b6c9400ee4f73ab4f9c133b3dfd74">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546545,asid:1783181,type:"banner",display:"inline",elementid:"im-f50b6c9400ee4f73ab4f9c133b3dfd74"})</script>
                        </div>
                    </div>
                    <div class="mt-36 sp_display">
                        <div id="im-3c7e0a0de6cf4f969594b146872e3b60">
                            <script async src="https://imp-adedge.i-mobile.co.jp/script/v1/spot.js?20220104"></script>
                            <script>(window.adsbyimobile=window.adsbyimobile||[]).push({pid:76890,mid:546546,asid:1783183,type:"banner",display:"inline",elementid:"im-3c7e0a0de6cf4f969594b146872e3b60"})</script>
                        </div>
                    </div>
                    @include('components.recommend_articles')
                    @include('components.breadcrumbs', ['slug' => 'taishi-pk.strategy.index'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection