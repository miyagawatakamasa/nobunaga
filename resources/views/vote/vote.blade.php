@extends('layouts.layout')

@section('title', '「' . $VoteCampaign->title . '」投票 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="投票キャンペーン「{{ $VoteCampaign->title }}」の投票ページです。選択した武将に投票をお願いします。">
    <meta property="og:description" content="投票キャンペーン「{{ $VoteCampaign->title }}」の投票ページです。選択した武将に投票をお願いします。" />
    <meta property="og:title" content="「{{ $VoteCampaign->title }}」投票 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "「{{ $VoteCampaign->title }}」投票 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
                        <p>[{{ $VoteCampaign->title }}]の投票</p>
                    </h1>
                        <form id="contact-form" method="POST" action="{{ route('vote_campaign.post', $VoteCampaign->id) }}">
                            @csrf
                            <div class="mt-36 d-f">
                                <div class="left_label">
                                    <label for="samurai_id">投票する武将[必須]</label>
                                </div>
                                <input type="hidden" name="vote_campanign_id" value="{{ $VoteCampaign->id }}">
                                <div class="right_input">
                                    <select name="samurai_id" id="samurai_id">
                                        <option>武将を選択する</option>
                                        @foreach ($Samurais as $Samurai)
                                            <option value="{{ $Samurai->id }}" @if(request('samurai_id') == $Samurai->id ) selected @endif>{{ $Samurai->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if ($errors->has('samurai_id'))
                                <div class="text-danger">
                                    {{ $errors->first('samurai_id') }}
                                </div>
                            @endif
                            @if (null !== session('errorMessage'))
                                <div class="text-danger">
                                    {{ session('errorMessage') }}
                                </div>
                            @endif
                            <p class="mt-12">※検索機能作るので待っててね</p>
                            <div class="mt-24 d-f">
                                <div class="left_label">
                                    <label for="samurai_id">投票理由[任意]</label>
                                </div>
                                <div class="right_input">
                                    <textarea name="reason" id="reason" cols="30" rows="6" maxlength="180"></textarea>
                                </div>
                            </div>
                            <div class="mt-24 ta-c">
                                <button type="submit" class="mt-12 submit_button">送信する</button>
                            </div>
                        </form>
                        @include('components.breadcrumbs', ['slug' => 'vote_campaign.vote', 'argument1' => $VoteCampaign])
                    </div>
                </div>
            @include('components.aside')
        </div>
    </div>
@endsection