@extends('layouts.layout')

@section('title', $VoteCampaign->title . ' | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="投票キャンペーンとは、戦国時代のいろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツです。投票キャンペーン「{{ $VoteCampaign->title }}」の詳細ページです。集計結果が確認できます。">
    <meta property="og:description" content="投票キャンペーンとは、戦国時代のいろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツです。投票キャンペーン「{{ $VoteCampaign->title }}」の詳細ページです。集計結果が確認できます。" />
    <meta property="og:title" content="{{ $VoteCampaign->title }} | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vote.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "image": "{{ My_func::return_vote_main_img_url($VoteCampaign->id) }}",  
            "name": "{{ $VoteCampaign->title }} | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f j-sb sp_block">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">{{ $VoteCampaign->created_at->format('Y年m月d日') }}</span>
                            @if ($VoteCampaign->created_at != $VoteCampaign->updated_at)
                                <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">{{ $VoteCampaign->updated_at->format('Y年m月d日') }}</span>
                            @endif
                        </time>
                        <div>
                            <p>投票数 : {{ $totalVotes }}</p>
                        </div>
                    </div>

                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
                            <p>{{ $VoteCampaign->title }}</p>
                        </div>
                    </h1>
                    <div class="main_visual mt-24" style="background-image: url('{{ My_func::return_vote_main_img_url($VoteCampaign->id) }}');"></div>

                    <div class="main_contents mt-48">
                        <div class="article_body">
                            {!! nl2br(e($VoteCampaign->body)) !!}
                        </div>
                    </div>
                    <div class="mt-24 red_border_box">
                        <p>ひとりに決めらんないっていう方は、毎日投票できるので何回も投票しよう！</p>
                    </div>
                    <div class="mt-36 ta-c">
                        <a href="{{ route('vote_campaign.vote', $VoteCampaign->id) }}" class="next_button" title="投票する">投票する</a>
                    </div>
                    <div class="mt-48" id="total">
                        <h2 class="rich_h2">集計（{{ $VoteCampaign->title }}）</h2>
                        @if ($VoteResults)
                            <p class="mt-24">現時点の「{{ $VoteCampaign->title }}」の投票結果です。</p>
                            <div class="mt-36">
                                <table class="font_small ta-c w-100">
                                    <tr>
                                        <th>順位</th>
                                        <th>名前</th>
                                        <th>得票数</th>
                                        <th></th>
                                    </tr>
                                    @foreach ($VoteResults as $index => $VoteResult)
                                        <?php
                                            if(isset($beforeCount)) {
                                                if ($VoteResult['count'] < $beforeCount) {
                                                    $rank = $rank + $plusRank;
                                                    $plusRank = 1;
                                                } elseif ($VoteResult['count'] == $beforeCount) {
                                                    $plusRank++;
                                                }
                                            } else {
                                                $rank = 1;
                                                $plusRank = 1;
                                            }
                                        ?>
                                        <tr>
                                            <td class="back_cloud_blue">{{ $rank }}位</td>
                                            <td><a href="{{ route('database.samurais.show', [$GameTitle->slug, $VoteResult['samurai']['id']]) }}" title="{{ $VoteResult['samurai']['name'] }}">{{ $VoteResult['samurai']['name'] }}</a></td>
                                            <td>{{ $VoteResult['count'] }} 票</td>
                                            <td><a href="{{ route('vote_campaign.vote', $VoteCampaign->id) }}?samurai_id={{ $VoteResult['samurai']['id'] }}" title="この武将に投票する">この武将に投票する</a></td>
                                        </tr>
                                        <?php
                                            $beforeCount = $VoteResult['count'];
                                        ?>
                                    @endforeach
                                    <tr>
                                        <th>順位</th>
                                        <th>名前</th>
                                        <th>得票数</th>
                                        <th></th>
                                    </tr>
                                </table>
                            </div>
                        @else
                            <p class="mt-24">現在投票はありません。</p>
                        @endif
                        <div class="mt-36 ta-c">
                            <a href="{{ route('vote_campaign.vote', $VoteCampaign->id) }}" class="next_button" title="投票する">投票する</a>
                        </div>
                    </div>
                    <div class="mt-48" id="total">
                        <h2 class="rich_h2">投票理由</h2>
                        <div>
                            @if (count($VoteReasons))
                                <ul>
                                    @foreach ($VoteReasons as $VoteReason)
                                        <li class="mt-24">
                                            <div class="d-f j-sb">
                                                <p>投票武将 <a href="{{ route('database.samurais.show', [$GameTitle->slug, $VoteReason['samurai']['id']]) }}" title="{{ $VoteReason['samurai']['name'] }}">{{ $VoteReason['samurai']['name'] }}</a></p>
                                                <p>{{ $VoteReason->updated_at->format('Y年m月d日') }}</p>
                                            </div>
                                            <div class="back_blue mt-8 reason">
                                                <p>{!! nl2br(e($VoteReason->reason)) !!}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>      
                            @else
                                <p class="mt-24">投票理由はありません。</p>   
                            @endif
                        </div>
                        <div class="mt-36 ta-c">
                            <a href="{{ route('vote_campaign.vote', $VoteCampaign->id) }}" class="next_button" title="投票する">投票する</a>
                        </div>
                        <div class="mt-24">
                            <a href="{{ route('vote_campaign.index') }}" title="キャンペーン一覧に戻る">キャンペーン一覧に戻る</a>
                        </div>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'vote_campaign.show', 'argument1' => $VoteCampaign])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection