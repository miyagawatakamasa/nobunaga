@extends('layouts.layout')

@section('title', '投票キャンペーン | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="投票キャンペーンとは、戦国時代のいろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツです。やって欲しいお題があればお問い合わせからお願いします!おひとり様1日1お題に対して1投票までです。">
    <meta property="og:description" content="投票キャンペーンとは、戦国時代のいろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツです。やって欲しいお題があればお問い合わせからお願いします!おひとり様1日1お題に対して1投票までです。" />
    <meta property="og:title" content="投票キャンペーン | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "投票キャンペーン | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
                        <p>投票キャンペーン</p>
                    </h1>
                    <p class="mt-12">総投票数 : {{ $totalVotes }}</p>
                    <h2 class="mt-24 rich_h2">投票キャンペーンとは？</h2>
                    <p class="mt-36">投票キャンペーンとは、<span class="color_red">戦国時代のいろいろなお題に対してユーザーが投票し結果を見よう!というゆるーいコンテンツ</span>です。</p>
                    <p>やって欲しいお題があれば<a href="{{ route('contact') }}" title="お問い合わせ">お問い合わせ</a>からお願いします!</p>
                    <h2 class="mt-24 rich_h2">投票条件</h2>
                    <p class="mt-24">おひとり様<span class="color_red">1日1お題に対して1投票まで</span>です。</p>
                    <p>1日経てばまた投票できるようになるので、お気に入りの武将をランクインさせよう!</p>
                    <p>ブックマークしてくれると毎日投票が楽になるよ！</p>
                    <p>※どの武将がランクインしても特に何も起こらないので不正はやめてくださいね!</p>
                    <h2 class="mt-24 rich_h2">キャンペーン一覧</h2>
                    <ul class="card_ul">
                        @foreach ($VoteCampaigns as $VoteCampaign)
                        <li class="card_list mt-24 narrow_card">
                            <a href="{{ route('vote_campaign.show', $VoteCampaign->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $VoteCampaign->title }}">
                                <div class="card_image" style="background-image: url('@if (file_exists(asset('img/vote/{{ $VoteCampaign->id }}/main.png'))){{ asset('img/vote/' . $VoteCampaign->id . '/main.png') }} @else {{ asset('img/top/nobu.png') }} @endif ');"></div>
                                <div class="card_bottom">
                                    <div class="d-f j-sb card_info">
                                        <time>{{ $VoteCampaign->created_at->format('Y年m月d日') }}</time>
                                    </div>
                                    <p class="card_title">{{ $VoteCampaign->title }}</p>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>


                    @include('components.breadcrumbs', ['slug' => 'vote_campaign.index'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection