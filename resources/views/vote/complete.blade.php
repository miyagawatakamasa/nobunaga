@extends('layouts.layout')

@section('title', '「' . $VoteCampaign->title . '」投票完了 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="投票キャンペーン「{{ $VoteCampaign->title }}」の投票完了ページです。投票本当にありがとうございました。1日経てばまた投票できるようになるのでまた明日も投票お願いします！">
    <meta property="og:description" content="投票キャンペーン「{{ $VoteCampaign->title }}」の投票完了ページです。投票本当にありがとうございました。1日経てばまた投票できるようになるのでまた明日も投票お願いします！" />
    <meta property="og:title" content="「{{ $VoteCampaign->title }}」投票完了 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="d-f ai-c">
                        <img src="{{ asset('img/common/vote.png')}}" alt="投票キャンペーンアイコン" width="30px" height="30px" class="mr-8">
                        <p>[{{ $VoteCampaign->title }}]の投票 完了</p>
                    </h1>
                    <p class="mt-36">投票を受け付けました。</p>
                    <p class="mt-12">いつもいつもありがとうございます！！</p>
                    <p>この攻略サイトは趣味でやっているので、ユーザーさんに使っていただくと<span class="color_red">やる気が出ます！</span></p>

                    @if (rand() % 2 == 0)
                        <p class="mt-24">投票ページを<span class="color_red">ブックマーク</span>しておくとすぐ投票できるよ！</p>
                    @else
                        <p class="mt-24">本当に投票してくれてうれしいです！</p>
                    @endif
                    <p>1日経てばまた投票できるようになるのでまた明日も投票お願いします！</p>
                    @if (count($SamuraiRelatedArticles))
                        <section class="mt-36">
                            <h2>関連する記事一覧</h2>
                            <ul class="card_ul">
                                @foreach ($SamuraiRelatedArticles as $SamuraiRelatedArticle)
                                    <li class="card_list mt-24">
                                        <a href="{{ route('article.single', $SamuraiRelatedArticle->article->id ) }}" class="card_anchor shadow shadow_hover" title="{{ $SamuraiRelatedArticle->article->title }}">
                                            <div class="card_image" style="background-image: url('@if (file_exists(asset('img/article/{{ $SamuraiRelatedArticle->article->id }}/main.png'))){{ asset('img/article/' . $SamuraiRelatedArticle->article->id . '/main.png') }} @else {{ asset('img/top/nobu.png') }} @endif ');"></div>
                                            <div class="card_bottom">
                                                <div class="d-f j-sb card_info sp_block">
                                                    <time>
                                                        @if ($SamuraiRelatedArticle->article->is_updated)
                                                            {{ $SamuraiRelatedArticle->article->additioned_at->format('Y年m月d日') }}
                                                        @else
                                                            {{ $SamuraiRelatedArticle->article->created_at->format('Y年m月d日') }}
                                                        @endif
                                                    </time>
                                                    <p class="card_category">{{ $SamuraiRelatedArticle->article->articleCategorySearches['0']->articleCategory->name }}</p>
                                                </div>
                                                <p class="card_title">{{ $SamuraiRelatedArticle->article->title }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul> 
                        </section>
                    @endif
                    <div class="mt-24">
                        <a href="{{ route('vote_campaign.show', $VoteCampaign->id ) }}#total" class="mr-8" title="集計を確認する">集計を確認する</a>
                        <a href="{{ route('vote_campaign.index') }}" title="投票キャンペーンに戻る">投票キャンペーンに戻る</a>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'vote_campaign.complete', 'argument1' => $VoteCampaign])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection