<!doctype html>
<html lang="ja" prefix="og: http://ogp.me/ns#">
<head>
    <!-- Laravelで作ってまーす。コーディングはコピーしていいよ。 -->
    <!-- MENTAでプログラミング学習のサポートやっています！ -->
    <!-- こちらからチェック！！ https://menta.work/user/59584 -->
    <meta charset="utf-8">
    <meta name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:locale" content="ja">
    <meta property="og:type" content="WebPage" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:site_name" content="信長の野望 徹底攻略" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@nobutettei" />
    <meta name="twitter:creator" content="@nobutettei" />
    <meta name="twitter:domain" content="{{ url()->current() }}" />
    @yield('meta')
    <title>@yield('title')</title>
    @livewireStyles
    <link rel="stylesheet" href="{{ asset('css/all.css') }}?date={{ \Carbon\Carbon::now() }}">
    <link rel="stylesheet" rel="preconnect" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    @yield('css')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    @yield('js')
</head>
<body>
  @include('components.header')
  <main class="position-relative">
    @yield('content')
  </main>
  @include('components.footer')
  <script type="text/javascript" src="{{ asset('js/jquery.rwdImageMaps.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/16.1.0/smooth-scroll.min.js" defer></script>

  <script>
    $(function() {
      $('img[usemap]').rwdImageMaps();
      var $oldCountryImgUrl = "{{ asset('img/common/old_country_color.png')}}";
      var $colorMapUrl = '{{ url("/") }}' + '/img/common/color_map/';

      @foreach ($OldContries as $OldContry)
        var $colorMap{{ $OldContry['id'] }} = "{{ asset('img/common/color_map/color_map' . $OldContry['id'] . '.png') }}";
      @endforeach

      $('.old_country_name_list').hover(function() {
        var $id = $(this).data('id');
        $(".old_country_tr_" + $id).show();
        $("#old-country-color").attr('src', $colorMapUrl + 'color_map_' + $id + '.png');
      },function() {
        var $id = $(this).data('id');
        $(".old_country_tr_" + $id).hide();
        $("#old-country-color").attr('src', $oldCountryImgUrl);
      });

      // スムーススクロール
      var scroll = new SmoothScroll('a[href*="#"]', {
        speed: 500,
        speedAsDuration: true
      });

      $(document).on({
        'mouseenter' : function() {
            var $id = $(this).data('id');
            $("#old-country-color").attr('src', $colorMapUrl + 'color_map_' + $id + '.png');
            $(".old_country_tr_" + $id).show();
            $(".old_country_anchor_" + $id).toggleClass('hover');
        },
        'mouseleave' : function(){
            var $id = $(this).data('id');
            $("#old-country-color").attr('src', $oldCountryImgUrl);
            $(".old_country_tr_" + $id).hide();
            $(".old_country_anchor_" + $id).toggleClass('hover');
        }
      }, '.old_country_area');
    });
  </script>
  <script type="text/javascript" src="{{ asset('js/all.js') }}" defer></script>

  @yield('footer_js')
  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
     crossorigin="anonymous"></script>
  @livewireScripts
  @production
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YKEKW8H5Q4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-YKEKW8H5Q4');
    </script>

    {{-- googleadsense --}}
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
    crossorigin="anonymous"></script>
  @endproduction
</body>
</html>
