@extends('layouts.layout')

@section('title', 'ゲームタイトル一覧 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="「信長の野望」のゲームタイトル一覧です。シリーズ1作目の「信長の野望」から、シリーズ16作目となる最新作「信長の野望 新生」までの発売日・概要を記載しています。">
    <meta property="og:description" content="「信長の野望」のゲームタイトル一覧です。シリーズ1作目の「信長の野望」から、シリーズ16作目となる最新作「信長の野望 新生」までの発売日・概要を記載しています。" />
    <meta property="og:title" content="ゲームタイトル一覧 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "ゲームタイトル一覧 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年03月24日</span>
                            <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2023年08月28日</span>
                        </time>
                    </div>
                    <h1 class="mt-36 d-f ai-c">
                        <img src="{{ asset('img/common/game.png')}}" alt="ゲームタイトル一覧アイコン" width="30px" height="30px" class="mr-8">
                        <p>ゲームタイトル一覧</p>
                    </h1>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            <div class="table_of_contents_area mt-24">
                                <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                                <div id="table-of-content" class="mt-12 active toc_contents"></div>
                            </div>
                            @foreach ($GameTitles as $GameTitle)
                                <h2 class="mt-36">{{ $GameTitle->name }}</h2>
                                <div class="mx-auto mt-24" style="width: 80%;">
                                    <img src="{{ asset('img/game_title/' . $GameTitle->id . '.jpg')}}" alt="{{ $GameTitle->name }}" width="100%" height="100%">
                                </div>
                                <p>発売日 : {{ $GameTitle->year }}年
                                    @if ($GameTitle->month == 0)
                                        予定
                                    @else
                                        {{ $GameTitle->month }}月
                                    @endif</p>
                                <p class="mt-24">{!! nl2br(e($GameTitle->description)) !!}</p>
                            @endforeach
                            @include('components.breadcrumbs', ['slug' => 'other.game_title'])
                        </div>
                    </div>
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection