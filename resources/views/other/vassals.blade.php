@extends('layouts.layout')

@section('title', '【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団')

@section('meta')
	<meta name="description" content="戦国大名の家臣団一覧です。">
    <meta property="og:description" content="戦国大名の家臣団一覧です。" />
    <meta property="og:title" content="【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        {{-- 時間 --}}
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年05月20日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">{{ $Article->additioned_at->format('Y年m月d日') }}</span> --}}
                        </time>
                    </div>
                    <h1>【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団</h1>
                    <div class="main_contents mt-48">
                        <div class="article_body">
                            <p>
                                戦国時代は大名を支える優秀な家臣達がいました。<br>特に精鋭中の精鋭には「徳川四天王」のようなかっこいい呼称が付けられ、それは各大名家に存在します。<br>
                                どのような家臣団の呼称があるのかまとめたので、早速見ていきましょう！
                            </p>
                            <div class="table_of_contents_area mt-24">
                                <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                                <div id="table-of-content" class="mt-12 active toc_contents"></div>
                            </div>
                                <h2>織田家</h2>
                                <p>やっぱり織田家から紹介しないとですね。</p>
                                <p><a href="/database/samurai/554">織田信長</a>を当主として、多くの家臣が活躍しました。</p>
                                <h3>織田四天王</h3>
                                <p>織田四天王とは、織田家で宿老級の4人の武将の総称です。</p>
                                <p>各方面に軍団長として派遣されていました。</p>
                                <ul>
                                    <li><a href="/database/samurai/1039">柴田勝家</a>
                                        <p>織田家臣。「かかれ柴田」の異名をとった猛将。北陸方面軍の総大将を務めた。本能寺の変後、<a href="/database/samurai/1420">羽柴秀吉</a>と争い賤ヶ岳合戦で敗れ、居城・北庄城で自害した。</p>
                                    </li>
                                    <li class="mt-8"><a href="/database/samurai/1571">丹羽長秀</a>
                                        <p>織田家臣。「米五郎左」の異名をとる。安土城の普請奉行を務めるなど、行政面で活躍した。本能寺の変後は<a href="/database/samurai/1420">羽柴秀吉</a>に属し、越前北庄１２０万石を領した。</p>
                                    </li>
                                    <li class="mt-8"><a href="/database/samurai/1211">滝川一益</a>
                                        <p>織田家臣。各地の合戦で活躍し「進むも退くも滝川」と称された。甲斐平定後、関東管領となる。本能寺の変後、北条軍と戦って惨敗し、以後は勢威を失った。</p>
                                    </li>
                                    <li class="mt-8"><a href="/database/samurai/49">明智光秀</a>
                                        <p>織田家臣。優れた才知と教養により重用されるが、突如謀叛を起こし<a href="/database/samurai/554">信長</a>を本能寺に討つ。しかし事後調略に失敗し、山崎合戦で敗れ逃亡中に殺された。</p>
                                    </li>
                                </ul>
                                <h3>織田五大将</h3>
                                <p>織田四天王に<a href="/database/samurai/1420">羽柴秀吉</a>（豊臣秀吉）を加え、織田五大将と呼びます。</p>
                                <ul>
                                    <li><a href="/database/samurai/1039">羽柴秀吉</a>
                                        <p>戦国一の出世頭。<a href="/database/samurai/554">織田信長</a>に仕え、傑出した人望と知略を武器に活躍し、頭角を現す。本能寺の変後、<a href="/database/samurai/49">明智光秀</a>、<a href="/database/samurai/1039">柴田勝家</a>らを次々と倒し、天下に覇を唱えた。</p>
                                    </li>
                                </ul>
                                <h3>三十六功臣</h3>
                                <p>永禄11(1568)年に上洛を果たし、朝廷より官位を勧められたとき、<a href="/database/samurai/554">信長</a>は生死をともにした功臣の賞を第一に願い出ました。<br>
                                    そこでこの<a href="/database/samurai/554">信長</a>の意思に基づき、神社創建の折、真先に功臣36名を選び、「三十六功臣」と称します。</p>
                                <ul>
                                    <li><a href="/database/samurai/188">池田恒興</a>
                                    <p>織田家臣。<a href="/database/samurai/554">信長</a>の乳兄弟。姉川合戦などで活躍した。本能寺の変後は、織田家四宿老の１人となる。<a href="/database/samurai/1420">羽柴秀吉</a>に味方して小牧長久手の合戦に出陣し、戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/284">稲葉一鉄</a>
                                    <p>斎藤家臣。美濃三人衆の１人。主家滅亡後、織田家に仕える。姉川合戦では浅井軍に横槍を入れ、味方を勝利に導いた。頑固な性格から「一徹」の語源になる。</p></li>
                                    <li class="mt-8">猪子兵助</li>
                                    <li class="mt-8"><a href="/database/samurai/356">氏家卜全</a>
                                    <p>斎藤家臣。美濃三人衆の１人。主家滅亡後、織田家に仕える。伊勢平定戦で功を立てた。長島一向一揆との戦いで織田軍が敗れた際、殿軍を務め、戦死した。</p></li>
                                    <li class="mt-8">織田宏良</li>
                                    <li class="mt-8"><a href="/database/samurai/557">織田信光</a>
                                    <p><a href="/database/samurai/549">信定</a>の子。武勇に優れ、今川家との小豆坂合戦では殿軍を務めた。甥・<a href="/database/samurai/554">信長</a>の清洲城攻略戦で活躍し、那古野城主となるが、その数カ月後に家臣に謀殺された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/700">蒲生氏郷</a>
                                    <p>織田家臣。<a href="/database/samurai/701">賢秀</a>の子。主君・<a href="/database/samurai/554">信長</a>の娘を娶る。本能寺の変後は<a href="/database/samurai/1420">豊臣秀吉</a>に仕え活躍、陸奥会津９２万石を領した。文武に秀でたその器量を<a href="/database/samurai/1420">秀吉</a>は恐れたという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/688">河尻秀隆</a>
                                    <p>織田家臣。黒母衣衆筆頭。<a href="/database/samurai/554">信長</a>の嫡男・<a href="/database/samurai/552">信忠</a>の補佐役となる。甲斐平定戦で活躍し、戦後、甲斐一国を与えられた。本能寺の変後、甲斐の国人一揆で殺された。</p></li>
                                    <li class="mt-8">斎藤新五</li>
                                    <li class="mt-8">坂井久蔵</li>
                                    <li class="mt-8">坂井政尚</li>
                                    <li class="mt-8"><a href="/database/samurai/948">佐久間信盛</a>
                                    <p>織田家臣。各地の合戦で活躍し「のき佐久間」の異名をとる。石山本願寺攻めの総大将を務めるが、本願寺の退去後、怠慢不手際の叱責を受け、追放された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/950">佐久間盛政</a>
                                    <p>柴田家臣。盛次の嫡男。叔父・<a href="/database/samurai/1039">勝家</a>に仕える。「鬼玄蕃」の異名をとった猛将。賤ヶ岳合戦で深入りし、柴田軍の敗因を作った。戦後、捕らえられ斬首された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/974">佐々成政</a>
                                    <p>織田家臣。<a href="/database/samurai/1039">柴田勝家</a>に属して北陸の攻略に貢献する。本能寺の変後、<a href="/database/samurai/1420">羽柴秀吉</a>と対立するが、敗れて降伏。転封先・肥後の領国経営に失敗し自害を命じられた。</p></li>
                                    <li class="mt-8">菅谷長頼</li>
                                    <li class="mt-8"><a href="/database/samurai/1211">滝川一益</a>
                                    <p>織田家臣。各地の合戦で活躍し「進むも退くも滝川」と称された。甲斐平定後、関東管領となる。本能寺の変後、北条軍と戦って惨敗し、以後は勢威を失った。</p></li>
                                    <li class="mt-8">武井夕庵</li>
                                    <li class="mt-8">道家尾張守</li>
                                    <li class="mt-8"><a href="/database/samurai/1571">丹羽長秀</a>
                                    <p>織田家臣。「米五郎左」の異名をとる。安土城の普請奉行を務めるなど、行政面で活躍した。本能寺の変後は<a href="/database/samurai/1420">羽柴秀吉</a>に属し、越前北庄１２０万石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1420">羽柴秀吉</a>
                                    <p>戦国一の出世頭。<a href="/database/samurai/554">織田信長</a>に仕え、傑出した人望と知略を武器に活躍し、頭角を現す。本能寺の変後、<a href="/database/samurai/49">明智光秀</a>、<a href="/database/samurai/1039">柴田勝家</a>らを次々と倒し、天下に覇を唱えた。</p></li>
                                    <li class="mt-8">原田直政</li>
                                    <li class="mt-8">平手汎秀</li>
                                    <li class="mt-8"><a href="/database/samurai/1693">平手政秀</a>
                                    <p>織田家臣。次席家老。<a href="/database/samurai/554">信長</a>の傅役を務めた。<a href="/database/samurai/554">信長</a>の妻に<a href="/database/samurai/921">斎藤道三</a>の娘・<a href="/database/samurai/741">帰蝶</a>を迎えるよう尽力するなど、外交面で活躍。<a href="/database/samurai/554">信長</a>の奇行を改めさせるため諫死した。</p></li>
                                    <li class="mt-8">福富秀勝</li>
                                    <li class="mt-8"><a href="/database/samurai/1722">不破光治</a>
                                    <p>斎藤家臣。主家滅亡後は織田家に属す。主君・<a href="/database/samurai/554">信長</a>の馬廻を務め、各地で活躍した。のちに<a href="/database/samurai/1039">柴田勝家</a>の目付として越前に赴く。本能寺の変後は<a href="/database/samurai/1039">勝家</a>に属した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1766">細川藤孝</a>
                                    <p>足利家臣。主君・<a href="/database/samurai/82">義輝</a>の横死後は<a href="/database/samurai/82">義輝</a>の弟・<a href="/database/samurai/79">義昭</a>の擁立に貢献した。その後は的確な情勢判断で細川家の命脈を保った。古今伝授を受けた文化人としても著名。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1781">堀秀政</a>
                                    <p>織田家臣。各地で戦功を立てる一方、<a href="/database/samurai/1381">徳川家康</a>の饗応役も務めるなど、文武の両面に才能を発揮した。本能寺の変後は<a href="/database/samurai/1420">豊臣秀吉</a>に属し、一門格の待遇を受けた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1829">前田利家</a>
                                    <p>織田家臣。数々の合戦で活躍し「槍の又左」の異名をとった。のちに<a href="/database/samurai/1039">柴田勝家</a>に従って北陸平定に貢献。<a href="/database/samurai/554">信長</a>の死後は<a href="/database/samurai/1420">豊臣秀吉</a>に仕え、五大老の１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1990">村井貞勝</a>
                                    <p>織田家臣。家中随一の吏僚。京都所司代を務め、京の治安維持、禁裏の修築などに従事し、主君・<a href="/database/samurai/554">信長</a>の内政を助けた。本能寺の変の際、二条御所で戦死した。</p></li>
                                    <li class="mt-8">毛利新介</li>
                                    <li class="mt-8"><a href="/database/samurai/2038">森蘭丸</a>
                                    <p>織田家臣。<a href="/database/samurai/2035">可成</a>の三男。主君・<a href="/database/samurai/554">信長</a>の小姓を務める。利発で容姿美しく、<a href="/database/samurai/554">信長</a>に寵愛された。将来を嘱望されるが、本能寺の変で<a href="/database/samurai/554">信長</a>に従って奮戦し戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2035">森可成</a>
                                    <p>織田家臣。尾張統一戦や桶狭間合戦などで活躍した。宇佐山城主を務め、琵琶湖の南岸を固める。のちに浅井・朝倉連合軍の攻撃を受け、衆寡敵せず戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2079">簗田政綱</a>
                                    <p>織田家臣。桶狭間合戦では斥候として奇襲成功の立役者となり、戦後尾張沓掛３千貫を与えられた。のちに大聖寺城主となるが、一揆の再起により改易された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2081">山内一豊</a>
                                    <p>織田家臣。妻・<a href="/database/samurai/2083">千代</a>の内助の功が著名。本能寺の変後は<a href="/database/samurai/1420">豊臣秀吉</a>に属す。関ヶ原合戦の際は居城・掛川城を<a href="/database/samurai/1381">徳川家康</a>に献上し、戦後、土佐高知２４万石を得た。</p></li>
                                    <li class="mt-8">湯浅甚助</li>
                                </ul>
                                <h3>摂津三守護</h3>
                                <p>摂津攻略後、「摂津守護」に任じられ摂津を統治した3人の総称。</p>
                                <ul>
                                    <li><a href="/database/samurai/184">池田勝正</a>
                                    <p>摂津の豪族。池田城主。<a href="/database/samurai/193">長正</a>の子。<a href="/database/samurai/554">織田信長</a>の畿内平定軍に降り、伊丹家・和田家とともに「摂津三守護」と称されるが三好家に通じた一族により追放された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2195">和田惟政</a>
                                    <p>足利家臣。主君・<a href="/database/samurai/82">義輝</a>の横死後は<a href="/database/samurai/82">義輝</a>の弟・<a href="/database/samurai/79">義昭</a>に仕え、将軍職就任を陰から支えた。キリスト教を保護し、宣教師・ルイス＝フロイスを<a href="/database/samurai/554">織田信長</a>に紹介した。</p></li>
                                    <li class="mt-8">伊丹親興</li>
                                </ul>
                                {{-- <h3>清洲三奉行</h3> --}}
                                <h2>豊臣家</h2>
                                <p>次は豊臣家の紹介です。羽柴秀吉時代・<a href="/database/samurai/1420">豊臣秀吉</a>時代・<a href="/database/samurai/1421">豊臣秀頼</a>時代とありますよ。</p>
                                <h3>羽柴四天王</h3>
                                <p><a href="/database/samurai/1420">秀吉</a>古参の家臣で功績を挙げた四人の総称だが、意外にもマイナー揃いです。</p>
                                <ul>
                                    <li>宮田光次</li>
                                    <li class="mt-8">神子田正治</li>
                                    <li class="mt-8">尾藤知宣</li>
                                    <li class="mt-8">戸田勝隆</li>
                                </ul>
                                <h3>両兵衛（りょうべえ）</h3>
                                <p>羽柴四天王と打って変わってこちらは有名！</p>
                                <p><a href="/database/samurai/1420">秀吉</a>に仕えた2人の軍師・<a href="/database/samurai/1235">竹中半兵衛</a>と<a href="/database/samurai/820">黒田官兵衛</a>の総称。二兵衛（にへえ）とも呼ばれます。</p>
                                <p>播磨侵攻までは<a href="/database/samurai/1235">竹中半兵衛</a>が、<a href="/database/samurai/1235">半兵衛</a>が三木城の兵糧攻めの際に病死してからは<a href="/database/samurai/820">官兵衛</a>が<a href="/database/samurai/1420">秀吉</a>を支えます！</p>
                                <ul>
                                    <li><a href="/database/samurai/1235">竹中半兵衛</a>
                                    <p>斎藤家臣。わずか１６人で主家の居城・稲葉山城を乗っ取る。その卓抜した知略を<a href="/database/samurai/1420">羽柴秀吉</a>に見込まれ、軍師となった。<a href="/database/samurai/1420">秀吉</a>の中国攻めに従軍し、陣中で病没。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/820">黒田官兵衛</a>
                                    <p>豊臣家臣。主君・<a href="/database/samurai/1420">秀吉</a>の参謀を務め、<a href="/database/samurai/1420">秀吉</a>の天下統一に大きく貢献した。しかしその卓抜した戦略的手腕を恐れられ、禄高は豊前中津１２万石におさえられた。</p></li>
                                </ul>
                                <h3>賎ヶ岳七本槍（しずがたけしちほんやり）</h3>
                                <p><a href="/database/samurai/1039">柴田勝家</a>との賎ヶ岳の戦いで武功を上げた、<a href="/database/samurai/1420">秀吉</a>子飼いの武将七人の総称。</p>
                                <ul>
                                    <li><a href="/database/samurai/630">糟屋武則</a>
                                    <p>豊臣家臣。播磨の出身。賤ヶ岳七本槍の１人。関ヶ原合戦の際は西軍に属し、伏見城攻撃などに参加した。戦後、改易されるが、のちに幕臣として登用された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/631">片桐且元</a>
                                    <p>豊臣家臣。賤ヶ岳七本槍の１人。主君・<a href="/database/samurai/1421">秀頼</a>の傅役として主家存続のため奔走するが、<a href="/database/samurai/1381">徳川家康</a>への内通疑惑により大坂城を退去した。以後は徳川家に属した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/640">加藤清正</a>
                                    <p>豊臣家臣。賤ヶ岳七本槍の１人。朝鮮派兵で活躍し「虎加藤」の逸話を残す。<a href="/database/samurai/1420">秀吉</a>死後は<a href="/database/samurai/224">石田三成</a>と対立、関ヶ原合戦で東軍に属し、肥後熊本５２万石を得た。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/647">加藤嘉明</a>
                                    <p>豊臣家臣。賤ヶ岳七本槍の１人。「沈勇の士」と評された。豊臣水軍の主力として各地の合戦で活躍。関ヶ原合戦では東軍に属し、伊予松山２０万石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1694">平野長泰</a>
                                    <p>豊臣家臣。賎ヶ岳七本槍の一人。関ヶ原合戦では東軍に属す。大坂の陣直前、<a href="/database/samurai/1381">徳川家康</a>に大坂城入りを告げたが、危惧した<a href="/database/samurai/1381">家康</a>に江戸城に留め置かれたという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1701">福島正則</a>
                                    <p>豊臣家臣。賤ヶ岳七本槍の筆頭。関ヶ原合戦では東軍の主力として奮戦し、安芸広島４９万石を得る。しかし、のちに居城・広島城無断修築の罪で改易された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2181">脇坂安治</a>
                                    <p>豊臣家臣。賤ヶ岳七本槍の１人。主君・<a href="/database/samurai/1420">秀吉</a>の中国征伐の際に「丹波の赤鬼」<a href="/database/samurai/12">赤井直正</a>を討ち取る功を立てた。関ヶ原合戦では東軍に寝返り所領を安堵された。</p></li>
                                </ul>
                                <h3>五奉行</h3>
                                <p>豊臣政権下で財務を担当した5人の総称。みんなバリバリ仕事できる官僚ですね。</p>
                                <p><a href="/database/samurai/224">石田三成</a>もいます！</p>
                                <ul>
                                    <li><a href="/database/samurai/63">浅野長政</a>
                                    <p>豊臣家臣。<a href="/database/samurai/1420">秀吉</a>の正室・<a href="/database/samurai/1580">寧子</a>の義弟。五奉行筆頭として主家の執政に参画した。<a href="/database/samurai/1420">秀吉</a>の死後、<a href="/database/samurai/224">石田三成</a>と対立して<a href="/database/samurai/1381">徳川家康</a>に接近し、以後は徳川家に仕えた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/224">石田三成</a>
                                    <p>豊臣家臣。五奉行の１人として国政に参画。主君・<a href="/database/samurai/1420">秀吉</a>の死後、西軍を指揮して<a href="/database/samurai/1381">徳川家康</a>と関ヶ原で戦うが、諸将の統制をとれずに敗れ、京都で斬首された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1497">長束正家</a>
                                    <p>丹羽家臣。主家没落後は豊臣家に仕え、五奉行の１人となる。算術に通じ、主家の賦課収納の財政処理を担当した。関ヶ原合戦では西軍に属し、戦後自害した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1851">増田長盛</a>
                                    <p>豊臣家臣。五奉行の１人として検地などを行う。<a href="/database/samurai/1418">豊臣秀次</a>の死後、大和郡山２０万石を領した。関ヶ原合戦の際は大坂城で<a href="/database/samurai/1421">豊臣秀頼</a>を守る。戦後、改易された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1828">前田玄以</a>
                                    <p>豊臣家臣。はじめ延暦寺の僧であったが還俗し、<a href="/database/samurai/552">織田信忠</a>に仕える。本能寺の変の際は<a href="/database/samurai/552">信忠</a>の嫡男・<a href="/database/samurai/560">秀信</a>を守って脱出した。のちに豊臣家五奉行の１人となる。</p></li>
                                </ul>
                                <h3>五大老</h3>
                                <p>豊臣政権を支えた5人の大大名。</p>
                                <p>石高が豊臣家に次いででかい家達ですね。</p>
                                <p><a href="/database/samurai/1420">秀吉</a>存命の頃は、<a href="/database/samurai/1381">家康</a>も五大老のうちの一人、つまり家来でした。</p>
                                <ul>
                                    <li><a href="/database/samurai/1381">徳川家康</a>
                                    <p>江戸幕府の創始者。<a href="/database/samurai/1382">広忠</a>の子。桶狭間の合戦後に自立。織田家との同盟、豊臣家への従属を経て勢力を拡大する。関ヶ原合戦で勝利を収め征夷大将軍となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1829">前田利家</a>
                                    <p>織田家臣。数々の合戦で活躍し「槍の又左」の異名をとった。のちに<a href="/database/samurai/1039">柴田勝家</a>に従って北陸平定に貢献。<a href="/database/samurai/554">信長</a>の死後は<a href="/database/samurai/1420">豊臣秀吉</a>に仕え、五大老の１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2010">毛利輝元</a>
                                    <p><a href="/database/samurai/2009">毛利隆元</a>の嫡男。祖父・<a href="/database/samurai/2015">元就</a>の死後、毛利家を継ぎ、秀吉の下では五大老の１人となる。関ヶ原合戦では西軍総大将の座に就くが、戦場に出ることはなかった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/326">上杉景勝</a>
                                    <p>出羽米沢藩主。<a href="/database/samurai/1471">長尾政景</a>の子。<a href="/database/samurai/329">上杉謙信</a>の養子となった。<a href="/database/samurai/329">謙信</a>の死後、御館の乱に勝利して家督を継いだ。関ヶ原合戦では西軍に属し、最上・伊達軍と戦った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/350">宇喜多秀家</a>
                                    <p>豊臣家臣。<a href="/database/samurai/349">直家</a>の嫡男。主君・<a href="/database/samurai/1420">秀吉</a>に寵愛され、五大老の１人となるが、内乱により重臣の大半を失う。関ヶ原合戦では西軍に属し、戦後八丈島へ配流された。</p></li>
                                </ul>
                                <h3>三中老</h3>
                                <p>政事に参与し、五大老と五奉行との意見が合わないときの仲裁役です。</p>
                                <ul>
                                    <li><a href="/database/samurai/201">生駒親正</a>
                                    <p>豊臣家臣。讃岐高松１７万石を領した。主君・<a href="/database/samurai/1420">秀吉</a>の死後は、三中老の１人となる。関ヶ原合戦で西軍に属すが、子・<a href="/database/samurai/200">一正</a>が東軍に属したため、改易を免れた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1787">堀尾吉晴</a>
                                    <p>豊臣家臣。情に厚く「仏の茂助」の異名をとる。主君・<a href="/database/samurai/1420">秀吉</a>の死後、三中老の１人となる。関ヶ原合戦では子・<a href="/database/samurai/1785">忠氏</a>が東軍に属し、出雲松江２４万石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1454">中村一氏</a>
                                    <p>豊臣家臣。岸和田城主を務める。主君・<a href="/database/samurai/1420">秀吉</a>の死後、三中老の１人となり、駿河府中１４万石を領した。関ヶ原合戦では東軍に属すが、決戦の直前に病死した。</p></li>
                                </ul>
                                <h3>豊臣方四天王</h3>
                                <p>大坂の陣における<a href="/database/samurai/1421">秀頼</a>を支えた4人の総称。<a href="/database/samurai/19">明石全登</a>は入り損ねました。</p>
                                <ul>
                                    <li><a href="/database/samurai/994">真田幸村</a>
                                    <p><a href="/database/samurai/992">昌幸</a>の次男。蟄居先の紀伊九度山から大坂城に入り、大坂の陣で寡兵ながらも徳川の大軍を相手に奮戦した。その戦いぶりは「真田日本一の兵」と称賛された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/906">後藤又兵衛</a>
                                    <p>黒田家臣。侍大将を務めるが、謀叛の嫌疑により浪人。のち<a href="/database/samurai/1421">豊臣秀頼</a>に招かれ、大坂城に入る。人望を集め、徳川軍相手に奮戦するが、大坂夏の陣で戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/760">木村重成</a>
                                    <p>豊臣家臣。<a href="/database/samurai/759">定光</a>の子という。主君・<a href="/database/samurai/1421">秀頼</a>の乳兄弟で、小姓を務めた。徳川家との和睦の際は豊臣家の使者を務めた。大坂夏の陣で<a href="/database/samurai/171">井伊直孝</a>軍と戦い、戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1311">長宗我部盛親</a>
                                    <p><a href="/database/samurai/1310">元親</a>の四男。関ヶ原合戦で西軍に属して改易され、京で寺子屋を開く。のち大坂城に入り、<a href="/database/samurai/1355">藤堂高虎</a>軍を壊滅させるなど活躍したが、大坂落城後、斬首された。</p></li>
                                </ul>
                                <h3>若江八人衆</h3>
                                <p><a href="/database/samurai/1418">豊臣秀次</a>の家臣団の通称。</p>
                                <ul>
                                    <li>大場土佐</li>
                                    <li class="mt-8">大山伯耆</li>
                                    <li class="mt-8">高野越中</li>
                                    <li class="mt-8">藤堂良政</li>
                                    <li class="mt-8">舞兵庫</li>
                                    <li class="mt-8">牧野成里</li>
                                    <li class="mt-8">森九兵衛</li>
                                    <li class="mt-8">安井喜内</li>
                                </ul>
                                <h2>徳川家</h2>
                                <h3>徳川四天王</h3>
                                <p>徳川家康の側近として仕え、徳川幕府樹立の功績を立てた四人の総称。</p>
                                <ul>
                                    <li><a href="/database/samurai/928">酒井忠次</a>
                                    <p>徳川家臣。徳川四天王筆頭。主君・<a href="/database/samurai/1381">家康</a>の養育係を務めた。<a href="/database/samurai/1381">家康</a>成人後は東三河衆を率いて各地を転戦し活躍。その才覚は<a href="/database/samurai/554">織田信長</a>や<a href="/database/samurai/1420">豊臣秀吉</a>にも称賛された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1804">本多忠勝</a>
                                    <p>徳川家臣。徳川四天王の１人。「<a href="/database/samurai/1381">家康</a>に過ぎたるもの」と評された家中随一の猛将。名槍・蜻蛉切を手に５７度の合戦に参陣し、傷一つ負わなかったという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/938">榊原康政</a>
                                    <p>徳川家臣。徳川四天王の１人。「無」の旗を掲げて戦場を疾駆し、各地で抜群の功を立てた。晩年、「老臣権を争うは亡国の兆し」と老中への就任を辞退した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/174">井伊直政</a>
                                    <p>徳川家臣。徳川四天王の１人。軍装を赤で統一した軍兵は「赤鬼」と恐れられ、常に先鋒を争った。関ヶ原合戦では島津軍を追撃し、<a href="/database/samurai/1061">島津豊久</a>を討ち取った。</p></li>
                                </ul>
                                <p>本多忠勝・榊原康政・井伊直政の三人で三傑と言います。酒井さん残念。</p>
                                <h3>徳川十六神将</h3>
                                <p>徳川四天王にさらに十二人を加えました。</p>
                                <ul>
                                    <li>米津常春</li>
                                    <li class="mt-8">高木清秀</li>
                                    <li class="mt-8">内藤正成</li>
                                    <li class="mt-8"><a href="/database/samurai/438">大久保忠世</a>
                                    <p>徳川家臣。<a href="/database/samurai/433">忠員</a>の長男。三方ヶ原合戦、長篠合戦など多くの合戦に従軍し、その豪胆な性格で抜群の功を立て、<a href="/database/samurai/554">織田信長</a>や<a href="/database/samurai/1420">豊臣秀吉</a>にも器量を高く評価された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/434">大久保忠佐</a>
                                    <p>徳川家臣。<a href="/database/samurai/433">忠員</a>の次男。兄・<a href="/database/samurai/438">忠世</a>とともに各地で戦功を立てる。その剛勇ぶりは<a href="/database/samurai/554">織田信長</a>をも賞嘆させるほどであった。関ヶ原合戦後、駿河沼津２万石を領す。</p></li>
                                    <li class="mt-8">蜂屋貞次</li>
                                    <li class="mt-8"><a href="/database/samurai/1424">鳥居元忠</a>
                                    <p>徳川家臣。関ヶ原合戦の際に主君・<a href="/database/samurai/1381">家康</a>の命で伏見城に籠城する。１３日間の攻防戦の末、城兵とともに玉砕した。その忠節は「三河武士の鑑」と称賛された。</p></li>
                                    <li class="mt-8">鳥居忠広</li>
                                    <li class="mt-8"><a href="/database/samurai/2190">渡辺守綱</a>
                                    <p>徳川家臣。各地を転戦して戦功を立て、「槍の半蔵」の異名をとる。関ヶ原合戦には旗本として参陣。晩年は尾張徳川家の家老となり、当主・<a href="/database/samurai/1384">義直</a>を補佐した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1681">平岩親吉</a>
                                    <p>徳川家臣。温厚篤実な性格から、主君・<a href="/database/samurai/1381">家康</a>の絶大な信頼を受け、<a href="/database/samurai/1381">家康</a>の長男・<a href="/database/samurai/1881">松平信康</a>の傅役を務める。信康の自害後は家康の九男・<a href="/database/samurai/1384">義直</a>の傅役となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1628">服部正成</a>
                                    <p>徳川家臣。半三保長の子。父の跡を継ぎ隠密頭を務める。主君・<a href="/database/samurai/1381">家康</a>の伊賀越えの際には、警護を担当し、無事に帰国させた。「鬼の半蔵」の異名をとった。</p></li>
                                    <li class="mt-8">松平康忠</li>
                                </ul>
                                <h3>徳川二十八神将</h3>
                                <p>徳川十六神将にさらに十四人を加え、鳥居忠広と内藤正成の二人を除外しました。</p>
                                <ul>
                                    <li><a href="/database/samurai/164">安藤直次</a></li>
                                    <li class="mt-8">本多忠俊</li>
                                    <li class="mt-8"><a href="/database/samurai/278">伊奈忠政</a>
                                    <p>徳川家臣。後方支援で活躍。父・<a href="/database/samurai/277">忠次</a>とともに新田開発、治水に務め、父の死後関東郡代職を継ぐ。大坂の陣では普請奉行として堀の埋め立てなどを行った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/501">岡部長盛</a>
                                    <p>徳川家臣。<a href="/database/samurai/502">正綱</a>の子。長久手合戦などで戦功を立て、主君・<a href="/database/samurai/1381">家康</a>が関東に入部した際に上総・下総で１万２千石を与えられた。のちに丹波亀山２万石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/435">大久保忠教</a>
                                    <p>徳川家臣。<a href="/database/samurai/433">忠員</a>の子。通称・彦左衛門。高天神城攻めで、<a href="/database/samurai/503">岡部元信</a>を討ち取る。多くの浪人を保護し、義侠の士と慕われた。晩年には「三河物語」を著した。</p></li>
                                    <li class="mt-8">大須賀康高</li>
                                    <li class="mt-8"><a href="/database/samurai/533">奥平信昌</a>
                                    <p>徳川家臣。<a href="/database/samurai/530">貞能</a>の子。一時<a href="/database/samurai/1219">武田信玄</a>に属すが、<a href="/database/samurai/1219">信玄</a>の死後、帰参。長篠合戦では長篠城を死守し勝利に大きく貢献した。その功により、<a href="/database/samurai/1381">家康</a>の娘・亀を娶った。</p></li>
                                    <li class="mt-8">酒井正親</li>
                                    <li class="mt-8"><a href="/database/samurai/1118">菅沼定盈</a>
                                    <p>今川家臣。野田城主。桶狭間合戦後、徳川家に仕える。<a href="/database/samurai/1219">武田信玄</a>の三河侵攻では一族の多くが<a href="/database/samurai/1219">信玄</a>に寝返る中、一貫して徳川家に属し、居城を攻め落とされた。</p></li>
                                    <li class="mt-8">内藤家長</li>
                                    <li class="mt-8"><a href="/database/samurai/1439">内藤信成</a>
                                    <p>徳川家臣。幼少より<a href="/database/samurai/1381">家康</a>に仕える。三方ヶ原の戦いでは殿軍を務めた。関ヶ原合戦後に駿府、次いで近江長浜を治める。一説には<a href="/database/samurai/1381">家康</a>の異父弟だという。</p></li>
                                    <li class="mt-8">本多康高</li>
                                    <li class="mt-8">松平伊忠</li>
                                    <li class="mt-8"><a href="/database/samurai/1921">水野勝成</a>
                                    <p>徳川家臣。遠江高天神城攻めなど多くの合戦に従軍した。一時は<a href="/database/samurai/1420">豊臣秀吉</a>などに仕えたが、のちに帰参し、備中福山１０万石を領す。島原の乱にも参陣した。</p></li>
                                </ul>
                                <h3>徳川御三家</h3>
                                <p>徳川幕府の将軍を輩出できる、徳川性を認められた家の総称。</p>
                                <p>徳川頼房の末裔が最後の将軍徳川慶喜ですね。</p>
                                <ul>
                                    <li><a href="/database/samurai/1384">徳川義直</a>
                                    <p><a href="/database/samurai/1381">家康</a>の九男。徳川御三家の一・尾張藩の藩祖で「剛」と評された。学問を好んで儒教を奨励。産業振興、税制整備、治水などを行い、藩政の基礎を固めた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1385">徳川頼宣</a>
                                    <p><a href="/database/samurai/554">家康</a>の十男。徳川御三家の一・紀州藩の藩祖。血気盛んで「勇」と評された。大坂の陣で後方の陣に置かれたことを、涙を流して悔しがったという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1386">徳川頼房</a>
                                    <p><a href="/database/samurai/1381">家康</a>の十一男。徳川御三家の一・水戸藩の藩祖で「才」と評された。幼少より才気溢れ、<a href="/database/samurai/1381">家康</a>の寵愛を受けた。家康に希望を尋ねられ「天下」と答えたという。</p></li>
                                </ul>
                                <h3>井伊谷三人衆</h3>
                                <p><a href="/database/samurai/1381">徳川家康</a>が遠州攻めを行った際に<a href="/database/samurai/302">今川氏真</a>から徳川方へ離反した三人の総称。</p>
                                <ul>
                                    <li><a href="/database/samurai/897">近藤康用</a>
                                    <p>徳川家臣。井伊谷三人衆の一人。<a href="/database/samurai/1381">家康</a>の遠江攻略に際し、懐柔され今川家から離反。戦傷のため歩行さえ困難であったが武田家との戦いにも功があったという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1120">菅沼忠久</a></li>
                                    {{-- 列伝入れる --}}
                                    <li class="mt-8"><a href="/database/samurai/1136">鈴木重時</a></li>
                                    {{-- 列伝入れる --}}
                                </ul>
                                <h2>武田家</h2>
                                <h3>甲陽五名臣</h3>
                                <p><a href="/database/samurai/1226">武田信虎</a>、晴信父子に仕えた武田家の直臣。</p>
                                <ul>
                                    <li><a href="/database/samurai/1645">原虎胤</a>
                                    <p>武田家臣。甲陽五名臣の１人。下総千葉家臣・原家の一族。生涯で３８度の合戦に参加。城攻めに長じ、また情けに厚い豪傑で「夜叉美濃」の異名をとった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/583">小幡虎盛</a>
                                    <p>武田家臣。甲陽五名臣の１人。「鬼虎」の異名をとり、生涯で３６枚の感状を受けた。晩年は<a href="/database/samurai/845">高坂昌信</a>の副将を務めた。「よくみのほどをしれ」の遺言が有名。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2136">横田高松</a>
                                    <p>武田家臣。甲陽五名臣の１人。近江の出身。敵の先手を打つ戦法を得意とした。「戸石崩れ」と呼ばれる<a href="/database/samurai/1999">村上義清</a>との戦いで殿軍を務め、奮戦するが戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1243">多田満頼</a>
                                    <p>武田家臣。甲陽五名臣の１人。美濃の出身。信濃虚空蔵山の砦において、地獄の妖婆・火車鬼を退治したという伝説を持つ。夜襲の采配は家中随一と評された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2111">山本勘助</a>
                                    <p>武田家臣。文武百般に通じ、主君・<a href="/database/samurai/1219">信玄</a>の軍師を務めた。第四次川中島の合戦で「きつつきの戦法」を<a href="/database/samurai/329">上杉謙信</a>に見破られた責を負い、乱軍に突入し戦死した。</p></li>
                                </ul>
                                <h3>戦国三弾正</h3>
                                <p><a href="/database/samurai/1219">武田信玄</a>に仕え、弾正忠を名乗った武将の総称。</p>
                                <ul>
                                    <li>逃げ弾正・<a href="/database/samurai/845">高坂昌信</a>
                                    <p>武田家臣。武田四名臣の１人。主君・<a href="/database/samurai/1219">信玄</a>の小姓から侍大将となる。武略・用兵は家中随一といわれ「逃げ弾正」と呼ばれた。「甲陽軍鑑」の原著者と伝わる。</p></li>
                                    <li class="mt-8">槍弾正・<a href="/database/samurai/1751">保科正俊</a>
                                    <p>武田家臣。高遠城主。信濃先方衆として各地の合戦に従軍し、活躍した。「槍弾正」の異名をとり「甲陽軍鑑」に戦国三弾正の１人として名を挙げられている。</p></li>
                                    <li class="mt-8">攻め弾正・<a href="/database/samurai/993">真田幸隆</a>
                                    <p>武田家臣。信州先方衆。主君・<a href="/database/samurai/1219">信玄</a>が攻略出来なかった信濃砥石城を謀略で落城させ、知略は<a href="/database/samurai/1219">信玄</a>に勝ると賞された。以後、<a href="/database/samurai/1219">信玄</a>の参謀の１人として活躍する。</p></li>
                                </ul>
                                <h3>武田四名臣</h3>
                                <p><a href="/database/samurai/1219">武田信玄</a>・<a href="/database/samurai/1218">勝頼</a>期の重臣四人の総称。</p>
                                <p>震え上がりそうな猛者揃いです。</p>
                                <ul>
                                    <li><a href="/database/samurai/1659">馬場信春</a>
                                    <p>武田家臣。武田四名臣の１人。多くの合戦に参加し一度も負傷せず「不死身の鬼美濃」と呼ばれた。長篠合戦の際に殿軍として主君・<a href="/database/samurai/1218">勝頼</a>の逃亡を助け、戦死。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1441">内藤昌豊</a>
                                    <p>武田家臣。武田四名臣の１人。<a href="/database/samurai/1222">武田信繁</a>の死後、主君・<a href="/database/samurai/1219">信玄</a>の副将格となる。武略に優れ、箕輪城主として西上野方面の治政を担当した。長篠合戦で戦死した。</p></li>
                                    <li class="mt-8">山県昌景</li>
                                    <li class="mt-8"><a href="/database/samurai/845">高坂昌信</a>
                                    <p>武田家臣。武田四名臣の１人。主君・<a href="/database/samurai/1219">信玄</a>の小姓から侍大将となる。武略・用兵は家中随一といわれ「逃げ弾正」と呼ばれた。「甲陽軍鑑」の原著者と伝わる。</p></li>
                                </ul>
                                <h3>武田二十四将</h3>
                                <p>武田二十四将とは、<a href="/database/samurai/1219">武田信玄</a>に仕えた武将のうち、後世に講談や軍記などで一般的な評価が特に高い24人をさして呼ばれるようになった武田家家臣団の呼称です。</p>
                                <ul>
                                    <li><a href="/database/samurai/46">秋山信友</a>
                                    <p>武田家臣。伊那衆を統率する。<a href="/database/samurai/1381">徳川家康</a>に「武田軍の猛牛」と評されたほどの猛将。岩村城に籠城して<a href="/database/samurai/554">織田信長</a>軍と戦うが、敗れて捕虜となり、磔刑にされた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/114">穴山信君</a>
                                    <p>武田家臣。一門衆の筆頭格。合戦ではおもに本陣を守った。武田家滅亡後は、<a href="/database/samurai/1381">徳川家康</a>に降る。本能寺の変を知り、堺から本国へ帰る途中で何者かに討たれた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/139">甘利虎泰</a>
                                    <p>武田家臣。主君・<a href="/database/samurai/1226">信虎</a>の追放に関わる。以後は<a href="/database/samurai/1226">信虎</a>の子・<a href="/database/samurai/1219">信玄</a>に仕え、<a href="/database/samurai/239">板垣信方</a>とともに宿老を務めた。上田原合戦で<a href="/database/samurai/1999">村上義清</a>軍と激闘を展開し、戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/239">板垣信方</a>
                                    <p>武田家臣。主君・<a href="/database/samurai/1226">信虎</a>の追放に関わる。以後は<a href="/database/samurai/1226">信虎</a>の子・<a href="/database/samurai/1219">信玄</a>に仕え、各地の合戦で活躍した。上田原合戦で先鋒を務め<a href="/database/samurai/1999">村上義清</a>軍と戦い、激闘の末戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/250">一条信龍</a>
                                    <p><a href="/database/samurai/1219">武田信玄</a>の異母弟。甲斐上野城主。甲斐源氏の流れを汲む一条家の名跡を継ぐ。<a href="/database/samurai/1219">信玄</a>の遺言により<a href="/database/samurai/1218">勝頼</a>の後見人を務め、衰退する武田家に最期まで尽くした。</p></li>
                                    <li class="mt-8">小畠虎盛</li>
                                    <li class="mt-8"><a href="/database/samurai/586">小幡昌盛</a>
                                    <p>武田家臣。<a href="/database/samurai/583">虎盛</a>の子。豊後守と称した。はじめ海津城の城番を務めるが、父の死後、城番を辞して旗本足軽大将衆となり騎馬３騎、足軽１０人を率いた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/588">飯富虎昌</a>
                                    <p>武田家臣。軍装を赤で統一した部隊を率いて活躍、「甲山の猛虎」の異名をとった。<a href="/database/samurai/1219">信玄</a>の長男・<a href="/database/samurai/1231">義信</a>の傅役を務めたが<a href="/database/samurai/1231">義信</a>の謀叛未遂事件の責任をとり自害。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/593">小山田信茂</a>
                                    <p>武田家臣。<a href="/database/samurai/592">出羽守信有</a>の子。投石を得意とする部隊を率いて各地の合戦で活躍。<a href="/database/samurai/554">織田信長</a>の甲斐侵攻軍に降るが、主君・<a href="/database/samurai/1218">勝頼</a>の死後、裏切り者として斬られた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/845">高坂昌信</a>
                                    <p>武田家臣。武田四名臣の１人。主君・<a href="/database/samurai/1219">信玄</a>の小姓から侍大将となる。武略・用兵は家中随一といわれ「逃げ弾正」と呼ばれた。「甲陽軍鑑」の原著者と伝わる。</p></li>
                                    <li class="mt-8">三枝守友</li>
                                    <li class="mt-8"><a href="/database/samurai/993">真田幸隆</a>
                                    <p>武田家臣。信州先方衆。主君・<a href="/database/samurai/1219">信玄</a>が攻略出来なかった信濃砥石城を謀略で落城させ、知略は<a href="/database/samurai/1219">信玄</a>に勝ると賞された。以後、<a href="/database/samurai/1219">信玄</a>の参謀の１人として活躍する。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/987">真田信綱</a>
                                    <p>武田家臣。<a href="/database/samurai/993">幸隆</a>の長男。父の隠居後、信州先方衆の１人として各地を転戦、勇名を馳せる。長篠合戦では３尺３寸の大刀を振るって奮戦するが、銃弾に倒れた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1222">武田信繁</a>
                                    <p><a href="/database/samurai/1226">信虎</a>の次男。文武に優れて人望も高く、兄・<a href="/database/samurai/1219">信玄</a>の副将として活躍した。川中島合戦で本陣を守って奮戦、戦死した。後年「まことの武将」と高く評価される。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1220">武田信廉</a>
                                    <p><a href="/database/samurai/1226">信虎</a>の三男。次兄・<a href="/database/samurai/1222">信繁</a>の死後、親族衆の筆頭として長兄・<a href="/database/samurai/1219">信玄</a>を補佐。容貌が信玄に似ていたため、影武者も務めた。画才があり、人物画などの作品を残す。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1243">多田満頼</a>
                                    <p>武田家臣。甲陽五名臣の１人。美濃の出身。信濃虚空蔵山の砦において、地獄の妖婆・火車鬼を退治したという伝説を持つ。夜襲の采配は家中随一と評された。</p></li>
                                    <li class="mt-8">土屋昌次</li>
                                    <li class="mt-8"><a href="/database/samurai/1441">内藤昌豊</a>
                                    <p>武田家臣。武田四名臣の１人。<a href="/database/samurai/1222">武田信繁</a>の死後、主君・<a href="/database/samurai/1219">信玄</a>の副将格となる。武略に優れ、箕輪城主として西上野方面の治政を担当した。長篠合戦で戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1659">馬場信春</a>
                                    <p>武田家臣。武田四名臣の１人。多くの合戦に参加し一度も負傷せず「不死身の鬼美濃」と呼ばれた。長篠合戦の際に殿軍として主君・<a href="/database/samurai/1218">勝頼</a>の逃亡を助け、戦死。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1645">原虎胤</a>
                                    <p>武田家臣。甲陽五名臣の１人。下総千葉家臣・原家の一族。生涯で３８度の合戦に参加。城攻めに長じ、また情けに厚い豪傑で「夜叉美濃」の異名をとった。</p></li>
                                    <li class="mt-8">原昌胤</li>
                                    {{-- 漏れ --}}
                                    <li>山県昌景</li>
                                    <li class="mt-8"><a href="/database/samurai/2111">山本勘助</a>
                                    <p>武田家臣。文武百般に通じ、主君・<a href="/database/samurai/1219">信玄</a>の軍師を務めた。第四次川中島の合戦で「きつつきの戦法」を<a href="/database/samurai/329">上杉謙信</a>に見破られた責を負い、乱軍に突入し戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2136">横田高松</a>
                                    <p>武田家臣。甲陽五名臣の１人。近江の出身。敵の先手を打つ戦法を得意とした。「戸石崩れ」と呼ばれる<a href="/database/samurai/1999">村上義清</a>との戦いで殿軍を務め、奮戦するが戦死した。</p></li>
                                </ul>
                                <h2>上杉家</h2>
                                <h3>上杉四天王</h3>
                                <ul>
                                    <li><a href="/database/samurai/351">宇佐美定満</a>
                                    <p>上杉家臣。越後流軍学の祖という。<a href="/database/samurai/1104">上条定憲</a>の乱の際は上条方に属すが、<a href="/database/samurai/1104">定憲</a>の死後、帰参。国政に参画するなど活躍したが、<a href="/database/samurai/1471">長尾政景</a>と舟遊び中に溺死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/603">柿崎景家</a>
                                    <p>上杉家臣。主君・<a href="/database/samurai/329">謙信</a>に「越後七郡で彼にかなう者はなし」と評された家中随一の猛将。上杉軍の主力として活躍したが<a href="/database/samurai/554">織田信長</a>への内通疑惑により殺された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1443">直江景綱</a>
                                    <p>上杉家臣。与板城主。主君・<a href="/database/samurai/329">謙信</a>の信頼厚く、側近として内政・外交に辣腕を振るう。のちに<a href="/database/samurai/329">謙信</a>の旧名・景虎から一字を拝領し、実綱から景綱へと改名した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/125">甘粕景持</a>
                                    <p>上杉家臣。三条城主。初名は長重。第四次川中島合戦では殿軍を務め、妻女山を襲撃した武田軍別働隊と戦う。<a href="/database/samurai/329">謙信</a>の死後は<a href="/database/samurai/326">景勝</a>に仕えて数々の戦功を立てた。</p></li>
                                </ul>
                                <h2>毛利家</h2>
                                <h3>毛利両川</h3>
                                <p>安芸国に勢力を伸ばしている吉川氏に毛利元就の次男を、小早川氏に三男をそれぞれ養子に送り、毛利家の分家に変え勢力を吸収することで中国地方の制服に大きな役割を果たしました。</p>
                                <p>毛利元就、嫡男・隆元亡き後の毛利輝元時代でも、この二人は叔父として毛利家を支えました。</p>
                                <ul>
                                    <li><a href="/database/samurai/884">小早川隆景</a>
                                    <p><a href="/database/samurai/2015">毛利元就</a>の三男。安芸の豪族・小早川家を継ぎ、山陽地方の攻略にあたる。本能寺の変後は毛利家の存続をはかって<a href="/database/samurai/1420">豊臣秀吉</a>に接近し、五大老の１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/748">吉川元春</a>
                                    <p><a href="/database/samurai/2015">毛利元就</a>の次男。安芸の豪族・吉川家を継ぎ、山陰地方の攻略にあたる。不敗を誇った家中随一の猛将である一方、陣中で「太平記」４０巻を写本したという。</p></li>
                                </ul>
                                <h3>毛利十八将</h3>
                                <ul>
                                    <li><a href="/database/samurai/884">小早川隆景</a>
                                    <p><a href="/database/samurai/2015">毛利元就</a>の三男。安芸の豪族・小早川家を継ぎ、山陽地方の攻略にあたる。本能寺の変後は毛利家の存続をはかって<a href="/database/samurai/1420">豊臣秀吉</a>に接近し、五大老の１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/748">吉川元春</a>
                                    <p><a href="/database/samurai/2015">毛利元就</a>の次男。安芸の豪族・吉川家を継ぎ、山陰地方の攻略にあたる。不敗を誇った家中随一の猛将である一方、陣中で「太平記」４０巻を写本したという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1020">宍戸隆家</a>
                                    <p>安芸の豪族。<a href="/database/samurai/2015">毛利元就</a>と争うが、のちに<a href="/database/samurai/2015">元就</a>の娘・五龍を娶って和睦し、毛利家の一門衆となる。<a href="/database/samurai/748">吉川元春</a>と軍事行動をともにし、各地の合戦で活躍した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/137">天野隆重</a>
                                    <p>大内家臣。主家滅亡後は毛利家に属す。尼子家滅亡後、出雲月山富田城代となり<a href="/database/samurai/2106">山中幸盛</a>の軍を撃退した。のちに<a href="/database/samurai/2015">元就</a>の五男・元秋を補佐して出雲を支配した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2158">吉見正頼</a>
                                    <p>大内家臣。主君・<a href="/database/samurai/425">義隆</a>の姉を娶る。<a href="/database/samurai/425">義隆</a>の敵・<a href="/database/samurai/1117">陶晴賢</a>打倒を目指して挙兵し、<a href="/database/samurai/2015">毛利元就</a>と結んで<a href="/database/samurai/1117">陶晴賢</a>と戦う。<a href="/database/samurai/1117">陶晴賢</a>が厳島合戦で敗死したあとは毛利家に属した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/869">児玉就忠</a>
                                    <p>毛利家臣。主君・<a href="/database/samurai/2015">元就</a>の家督相続直後より側近として活躍し、のちに五奉行の一員となった。「視野も広く人ざわりもよく、事務練達の者」と元就に評された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/637">桂元澄</a>
                                    <p>毛利家臣。安芸桜尾城主を務め、厳島神社を含む神領の管理・支配を担当した。厳島合戦の際は<a href="/database/samurai/1117">陶晴賢</a>に偽の書簡を送り<a href="/database/samurai/1117">晴賢</a>を厳島へ誘き出すことに成功した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1705">福原貞俊</a>
                                    <p>毛利家臣。筆頭家老を務めた。<a href="/database/samurai/884">小早川隆景</a>を補佐して山陽方面の経略を行う。主君・<a href="/database/samurai/2015">元就</a>の死後、四人衆の１人となり当主・<a href="/database/samurai/2010">輝元</a>を補佐して主家の国政に参画。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/789">口羽通良</a>
                                    <p>毛利家臣。<a href="/database/samurai/1022">志道広良</a>の次男。<a href="/database/samurai/748">吉川元春</a>を補佐して山陰方面の経略を行う。主君・<a href="/database/samurai/2015">元就</a>の死後、四人衆の１人となり当主・<a href="/database/samurai/2010">輝元</a>を補佐して主家の国政に参画した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1022">志道広良</a>
                                    <p>毛利家臣。主君・<a href="/database/samurai/2015">元就</a>の毛利家相続に尽力。のちに<a href="/database/samurai/2015">元就</a>の要請で<a href="/database/samurai/2015">元就</a>の子・<a href="/database/samurai/2009">隆元</a>の後見役を務めた。<a href="/database/samurai/2009">隆元</a>に「君は船、臣は水にて候」と当主のあり方を説いた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/16">赤川元保</a>
                                    <p>毛利家臣。主君・<a href="/database/samurai/2009">隆元</a>から信任され、五奉行制の成立後、その筆頭奉行に就任した。<a href="/database/samurai/2009">隆元</a>が出雲遠征に赴く途中で急死したため、その責任を問われて殺された。</p></li>
                                    <li class="mt-8">粟屋元秀</li>
                                    <li class="mt-8">渡辺長</li>
                                    <li class="mt-8"><a href="/database/samurai/803">熊谷信直</a>
                                    <p>安芸武田家臣。のちに主家と対立し、毛利家に属す。娘が<a href="/database/samurai/2015">元就</a>の次男・<a href="/database/samurai/748">吉川元春</a>に嫁いでからは一門衆として重用され、吉川軍の先鋒を務めて各地で奮戦した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/795">国司元相</a>
                                    <p>毛利家臣。郡山合戦で敵将３４人を倒した勇将。のち五奉行の一員となる。主君の<a href="/database/samurai/2009">隆元</a>の名代として上洛した際、将軍・<a href="/database/samurai/82">足利義輝</a>から「槍の鈴」を免許された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/155">粟屋元親</a>
                                    <p>毛利家臣。<a href="/database/samurai/2015">元就</a>に仕えて歴戦し、多くの武功を挙げている。<a href="/database/samurai/2009">隆元</a>の信任を得て側近となり、五奉行の一人として内政面を補佐した。毛利十八将にも数えられる。</p></li>
                                    <li class="mt-8">飯田元親</li>
                                    <li class="mt-8"><a href="/database/samurai/296">井上元兼</a>
                                    <p>毛利家臣。主君・<a href="/database/samurai/2015">元就</a>の毛利家相続に尽力した。のちに軍役や普請などの諸役を怠るなどの横柄な態度をとったため、<a href="/database/samurai/2015">元就</a>によって井上一族３０名が殺された。</p></li>
                                </ul>
                                <h2>伊達家</h2>
                                <h3>伊達三傑</h3>
                                <ul>
                                    <li><a href="/database/samurai/634">片倉景綱</a>
                                    <p>伊達家臣。１９歳で主君・<a href="/database/samurai/1290">政宗</a>の傅役となり「智」の面で<a href="/database/samurai/1290">政宗</a>を補佐した智将。<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に参陣するよう<a href="/database/samurai/1290">政宗</a>を説得し、伊達家の存続に貢献した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1284">伊達成実</a>
                                    <p>伊達家臣。<a href="/database/samurai/1283">実元</a>の子。「武」の面で主君の<a href="/database/samurai/1290">政宗</a>を補佐した伊達家中随一の猛将。「英毅大略あり」と評された。晩年には<a href="/database/samurai/1380">徳川家光</a>に奥州の軍談を語っている。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/570">鬼庭綱元</a>
                                    <p>伊達家臣。主君・<a href="/database/samurai/1290">政宗</a>の腹心となる。朝鮮派兵の際に、<a href="/database/samurai/1420">豊臣秀吉</a>の愛妾・香の前を賜り、<a href="/database/samurai/1290">政宗</a>の怒りを買って出奔。のちに帰参し、国老として国政に携わった。</p></li>
                                </ul>
                                <h2>明智家</h2>
                                <h3>明智三羽烏</h3>
                                <ul>
                                    <li><a href="/database/samurai/2065">安田国継</a>
                                    <p>明智家臣。名は国継。明智三羽烏の一。本能寺の変で<a href="/database/samurai/2038">森蘭丸</a>を討ち、<a href="/database/samurai/554">織田信長</a>に一番槍をつける活躍をした。のち変名して<a href="/database/samurai/700">蒲生氏郷</a>、<a href="/database/samurai/1346">寺沢広高</a>らに仕えた。</p></li>
                                    <li class="mt-8">古川九兵衛</li>
                                    <li class="mt-8">箕浦大内藏</li>
                                </ul>
                                <h3>明智五宿老</h3>
                                <ul>
                                    <li><a href="/database/samurai/47">明智秀満</a>
                                    <p>明智家臣。岳父・<a href="/database/samurai/49">光秀</a>のもとで本能寺の変に加担した。山崎合戦では安土城を守るが、本軍の敗報を受けて坂本城に退却し、財宝を包囲軍に譲渡して自害した。</p></li>
                                    <li class="mt-8">明智光忠</li>
                                    <li class="mt-8"><a href="/database/samurai/918">斎藤利三</a>
                                    <p>斎藤家臣。主家滅亡後は<a href="/database/samurai/49">明智光秀</a>の家老となる。本能寺の変や山崎合戦に従軍し敗戦後に捕らえられ、斬首された。娘は将軍・<a href="/database/samurai/1380">徳川家光</a>の乳母を務めた春日局。</p></li>
                                    <li class="mt-8">藤田行政</li>
                                    <li class="mt-8"><a href="/database/samurai/1931">溝尾茂朝</a>
                                    <p>明智家臣。通称は庄兵衛。主君・<a href="/database/samurai/49">光秀</a>の側近を務め、厚い信頼を受けた。山崎合戦の敗北後、<a href="/database/samurai/49">光秀</a>の命で介錯を行い、<a href="/database/samurai/49">光秀</a>の首を藪中へ隠したのち、自害した。</p></li>
                                </ul>
                                <h2>北条家</h2>
                                <h3>北条五色備</h3>
                                <ul>
                                    <li>黄備・<a href="/database/samurai/1747">北条綱成</a>
                                    <p>北条家臣。<a href="/database/samurai/787">福島正成</a>の子。父の死後、<a href="/database/samurai/1737">北条氏綱</a>を頼り、<a href="/database/samurai/1737">氏綱</a>の娘を娶って一門となる。河越合戦などで活躍し、その旗印より「地黄八幡」と呼ばれ畏怖された。</p></li>
                                    <li class="mt-8">赤備・<a href="/database/samurai/1748">北条綱高</a>
                                    <p>北条家臣。北条五色備えの赤備えを率いた勇将。初名を種政といったが、<a href="/database/samurai/1737">北条氏綱</a>が<a href="/database/samurai/333">上杉朝定</a>を攻めた際に大功を立て、<a href="/database/samurai/1737">氏綱</a>より「綱」の字を拝領した。</p></li>
                                    <li class="mt-8">青備・<a href="/database/samurai/1414">富永直勝</a>
                                    <p>北条家臣。北条五色備の「青備」を率いる。一門以外では最も高禄を受けたといわれる。第二次国府台合戦で先陣を務めるが、<a href="/database/samurai/981">里見義弘</a>の反撃で戦死した。</p></li>
                                    <li class="mt-8">白備・<a href="/database/samurai/622">笠原康勝</a>、後に清水上野介
                                    <p>北条家臣。能登守と称す。笠原家は父・<a href="/database/samurai/620">信為</a>の時に北条家に属したという。１５５４年、今川家との加島合戦において、<a href="/database/samurai/1734">北条氏繁</a>らとともに先陣を務めた。</p></li>
                                    <li class="mt-8">黒備・<a href="/database/samurai/1267">多目元忠</a>(御由緒六家)
                                    <p>北条家臣。<a href="/database/samurai/1743">北条氏康</a>の軍師。北条五色備の「黒備」を率いる。河越夜戦で深追いした<a href="/database/samurai/1743">氏康</a>を呼び戻すため、独断で全軍に退却の号令を発し、<a href="/database/samurai/1743">氏康</a>を救った。</p></li>
                                </ul>
                                <h2>黒田家</h2>
                                <h3>黒田八虎</h3>
                                <p>黒田長政を支えた精鋭八人の総称です。</p>
                                <ul>
                                    <li>黒田兵庫助利高 : <a href="/database/samurai/820">黒田孝高</a>の実弟・<a href="/database/samurai/825">黒田職隆</a>の次男</li>
                                    <li class="mt-8">黒田修理亮利則 : <a href="/database/samurai/820">黒田孝高</a>の異母弟・<a href="/database/samurai/825">黒田職隆</a>の三男</li>
                                    <li class="mt-8">黒田図書助直之 : <a href="/database/samurai/820">黒田孝高</a>の異母弟・<a href="/database/samurai/825">黒田職隆</a>の四男</li>
                                    <li class="mt-8"><a href="/database/samurai/819">黒田三左衛門一成</a> : <a href="/database/samurai/820">孝高</a>の養子
                                    <p>黒田八虎の一人。<a href="/database/samurai/142">荒木村重</a>に捕らわれた<a href="/database/samurai/820">黒田官兵衛</a>を助ける。関ヶ原合戦では<a href="/database/samurai/224">石田三成</a>の重臣を討ち取り、のち<a href="/database/samurai/823">長政</a>の命で『大坂夏の陣図屏風』も制作した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/810">栗山善助利安</a>
                                    <p>黒田家臣。黒田八虎の一人。名は利安。<a href="/database/samurai/820">官兵衛</a>に仕えた股肱の臣。関ヶ原に際しては、義弟・<a href="/database/samurai/2033">母里太兵衛</a>とともに<a href="/database/samurai/820">官兵衛</a>に従い、豊後の大友軍と戦った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/297">井上九郎右衛門之房</a>
                                    <p>黒田家臣。黒田八虎の一人。<a href="/database/samurai/825">職隆</a>の頃より四代に仕えた忠臣。石垣原の戦いでは<a href="/database/samurai/820">孝高</a>に従って武功を挙げた。<a href="/database/samurai/823">長政</a>没後の「黒田騒動」では藩の存続に貢献した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2033">母里太兵衛友信</a>
                                    <p>黒田家臣。<a href="/database/samurai/906">後藤基次</a>と双璧をなした家中屈指の猛将。<a href="/database/samurai/1418">福島正則</a>が大杯に満たした酒を呑み干し、名槍「日本号」を拝領した。その姿は今も「黒田節」に残る。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/906">後藤又兵衛基次</a>
                                    <p>黒田家臣。侍大将を務めるが、謀叛の嫌疑により浪人。のち<a href="/database/samurai/1421">豊臣秀頼</a>に招かれ、大坂城に入る。人望を集め、徳川軍相手に奮戦するが、大坂夏の陣で戦死した。</p></li>
                                    </ul>
                                <h3>黒田二十四騎</h3>
                                <p>黒田二十四騎とは、黒田八虎に以下の十六人を追加した総称。</p>
                                <ul>
                                    <li><a href="/database/samurai/836">毛屋主水武久</a>
                                    <p>黒田家臣。近江の出身。<a href="/database/samurai/1039">柴田勝家</a>など数家を経て、黒田家に仕官。物見に優れ、関ヶ原合戦では<a href="/database/samurai/1381">徳川家康</a>に「敵は寡勢」と報告し、饅頭を与えられたという。</p></li>
                                    <li class="mt-8">吉田六郎太夫長利 : 黒田孝高の乳兄弟</li>
                                    <li class="mt-8"><a href="/database/samurai/2001">村田兵助吉次</a>
                                    <p>黒田家臣。黒田二十四騎の一。宝蔵院流槍術の免許皆伝で武勇に優れ、朱槍を許可された。一方で気性が激しく、気に食わないからと領民を虐殺したりしている。</p></li>
                                    <li class="mt-8">三宅山太夫家義</li>
                                    <li class="mt-8">益田与助正親</li>
                                    <li class="mt-8">堀平右衛門定則</li>
                                    <li class="mt-8">久野四兵衛重勝</li>
                                    <li class="mt-8"><a href="/database/samurai/1644">原弥左衛門種良</a>
                                    <p>黒田家臣。豊前の豪族・宝珠山氏の出。九州征伐で黒田家に仕え、案内役を務める。この時、姓が長いとして改姓した。関ヶ原合戦は<a href="/database/samurai/820">黒田官兵衛</a>に属し戦った。</p></li>
                                    <li class="mt-8">林太郎右衛門直利</li>
                                    <li class="mt-8">野村太郎兵衛祐勝 : 母里友信の異母弟</li>
                                    <li class="mt-8">野口藤九郎一成</li>
                                    <li class="mt-8">竹森新右衛門次貞</li>
                                    <li class="mt-8">桐山孫兵衛信行</li>
                                    <li class="mt-8">衣笠久左衛門景延</li>
                                    <li class="mt-8"><a href="/database/samurai/698">菅六之助正利</a>
                                    <p>黒田家臣。黒田二十四騎の一。賤ヶ岳合戦で初陣。九州征伐や城井長房との戦いで活躍、関ヶ原合戦で島左近を討ち取るなど武功抜群で朱具足を許されていた。</p></li>
                                    <li class="mt-8">小河伝右衛門信章</li>
                                </ul>
                                <h2>加藤家</h2>
                                <h3>加藤家三傑</h3>
                                <ul>
                                    <li><a href="/database/samurai/1084">庄林一心</a>
                                    <p><a href="/database/samurai/640">加藤清正</a>家臣。通称隼人。<a href="/database/samurai/180">飯田覚兵衛</a>、森本義太夫と加藤家三傑と呼ばれた。はじめ<a href="/database/samurai/142">荒木村重</a>、<a href="/database/samurai/1166">仙石秀久</a>に仕える。天草一揆討伐に功があり、朱槍を許された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/180">飯田覚兵衛</a></li>
                                    <li class="mt-8">森本義太夫</li>
                                </ul>
                                <h3>加藤十六将</h3>
                                <p>加藤十六将とは、加藤家三傑に以下の十三人を追加した総称。</p>
                                <ul>
                                    <li>加藤重次</li>
                                    <li class="mt-8">加藤可重</li>
                                    <li class="mt-8"><a href="/database/samurai/761">斑鳩平次</a>
                                    <p><a href="/database/samurai/640">加藤清正</a>家臣。はじめ<a href="/database/samurai/329">上杉謙信</a>に仕え、その後、<a href="/database/samurai/1084">庄林一心</a>に武勇を認められて<a href="/database/samurai/640">清正</a>に仕える。朝鮮の役で一番槍の功を７度まで立て、３千石を与えられた。</p></li>
                                    <li class="mt-8">加藤安政</li>
                                    <li class="mt-8">龍造寺又八</li>
                                    <li class="mt-8">貴田統治</li>
                                    <li class="mt-8">吉村氏吉</li>
                                    <li class="mt-8">山内甚三郎</li>
                                    <li class="mt-8">九鬼広隆</li>
                                    <li class="mt-8">天野助左衛門</li>
                                    <li class="mt-8"><a href="/database/samurai/761">木村又蔵</a>
                                    <p><a href="/database/samurai/640">加藤清正</a>家臣。加藤十六将の一。はじめ<a href="/database/samurai/2175">六角義賢</a>に属し臆病又蔵の異名を取ったが八幡神の加護で無敵の勇士となる。<a href="/database/samurai/640">清正</a>の命で、大坂の陣で豊臣に味方した。</p></li>
                                    <li class="mt-8">森本一久</li>
                                    <li class="mt-8">斎藤利宗</li>
                                    <li class="mt-8"><a href="/database/samurai/26">赤星親武</a>
                                    <p><a href="/database/samurai/640">加藤清正</a>家臣。<a href="/database/samurai/25">統家</a>の子。娘は<a href="/database/samurai/640">加藤清正</a>の側室。加藤十六将の一。のち<a href="/database/samurai/640">清正</a>を介して<a href="/database/samurai/1421">豊臣秀頼</a>に仕え、大坂の陣で奮戦。天王寺口の戦いで討死を遂げた。</p></li>
                                </ul>
                                <h2>浅井家</h2>
                                <h3>海赤雨三将</h3>
                                <ul>
                                    <li><a href="/database/samurai/598">海北綱親</a>
                                    <p>浅井家臣。画家・海北友松の父。<a href="/database/samurai/1420">豊臣秀吉</a>が「我が軍法の師」と讃えた勇将。武者奉行を務め各地で活躍した。主家滅亡時に戦死した。「海赤雨三将」の１人。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/13">赤尾清綱</a>
                                    <p>浅井家臣。<a href="/database/samurai/598">海北綱親</a>・<a href="/database/samurai/140">雨森弥兵衛</a>とともに「海赤雨三将」と称された。主家の居城・小谷城に赤尾曲輪を設け在番した。主家滅亡時に捕虜となり、斬首された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/140">雨森弥兵衛</a>
                                    <p>浅井家臣。雨森城主。名は清貞。<a href="/database/samurai/598">海北綱親</a>・<a href="/database/samurai/13">赤尾清綱</a>と共に「海赤雨三将」と称された。浅井家滅亡後、雨森氏は各地に離散した。</p></li>
                                </ul>
                                <h2>今川家</h2>
                                <h3>蟹江七本槍</h3>
                                <ul>
                                    <li><a href="/database/samurai/433">大久保忠員</a>
                                    <p>徳川家臣。兄・<a href="/database/samurai/437">忠俊</a>とともに主君・広忠の岡崎帰城に尽力。蟹江城攻めでも活躍した。三河一向一揆では一族の者とともに上和田砦に籠城し、一揆勢と戦った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/437">大久保忠俊</a>(または大久保忠勝)
                                    <p>徳川家臣。主君・<a href="/database/samurai/1382">広忠</a>の岡崎帰城に尽力し、また三河一向一揆が勃発した際には主君・<a href="/database/samurai/1381">家康</a>を助けて一揆勢を撃破するなど、主家の苦難時代を支え続けた忠臣。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/438">大久保忠世</a>
                                    <p>徳川家臣。<a href="/database/samurai/433">忠員</a>の長男。三方ヶ原合戦、長篠合戦など多くの合戦に従軍し、その豪胆な性格で抜群の功を立て、<a href="/database/samurai/554">織田信長</a>や<a href="/database/samurai/1420">豊臣秀吉</a>にも器量を高く評価された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/434">大久保忠佐</a>
                                    <p>徳川家臣。<a href="/database/samurai/433">忠員</a>の次男。兄・<a href="/database/samurai/438">忠世</a>とともに各地で戦功を立てる。その剛勇ぶりは<a href="/database/samurai/554">織田信長</a>をも賞嘆させるほどであった。関ヶ原合戦後、駿河沼津２万石を領す。</p></li>
                                    <li class="mt-8">阿倍忠政</li>
                                    <li class="mt-8">杉浦鎮貞</li>
                                    <li class="mt-8">杉浦鎮栄</li>
                                </ul>
                                <h2>結城家</h2>
                                <h3>結城四天王</h3>
                                <ul>
                                    <li><a href="/database/samurai/1209">多賀谷重経</a>
                                    <p>結城家臣。下妻城主。政経の子。<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に参陣し、所領を安堵された。関ヶ原合戦で西軍に属したため改易され、放浪の末近江彦根で死去した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1929">水谷正村</a>
                                    <p>結城家臣。下館城主。結城四天王の一。おもに下野方面に出陣し、宇都宮家と抗争を繰り広げた。のちに<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に参陣し、所領を安堵された。</p></li>
                                    <li class="mt-8">山川朝信</li>
                                    <li class="mt-8"><a href="/database/samurai/312">岩上朝堅</a>
                                    <p>結城家臣。旧姓は三浦。主君・<a href="/database/samurai/2117">晴朝</a>から結城四天王の一・岩上姓の名乗りを許された。<a href="/database/samurai/966">佐竹義重</a>や<a href="/database/samurai/1262">田村清顕</a>と文書のやりとりを行うなど、外交面で活躍した。</p></li>
                                </ul>
                                <h2>三好家</h2>
                                <h3>三好三人衆</h3>
                                <ul>
                                    <li><a href="/database/samurai/1972">三好長逸</a>
                                    <p>三好家臣。三好三人衆の筆頭。主君・義継や<a href="/database/samurai/1890">松永久秀</a>らと離合集散を繰り返し、家中に混乱を招く。のち<a href="/database/samurai/554">織田信長</a>の畿内平定軍に敗れ逃亡、行方不明となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1979">三好政康</a>
                                    <p>三好家臣。三好三人衆の１人。<a href="/database/samurai/1890">松永久秀</a>とともに将軍・<a href="/database/samurai/82">足利義輝</a>を殺害した。<a href="/database/samurai/554">織田信長</a>の畿内平定軍に敗れ、逃亡。のち豊臣家に仕え、大坂夏の陣で戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/321">岩成友通</a>
                                    <p>三好家臣。三好三人衆の１人。三好家一族同様の扱いを受けた。将軍・<a href="/database/samurai/79">足利義昭</a>の挙兵に応じ、山城淀城に籠城するが、<a href="/database/samurai/1766">細川藤孝</a>らの軍に攻められ、敗死した。</p></li>
                                </ul>
                                <h2>西園寺家</h2>
                                <h3>西園寺十五将</h3>
                                <ul>
                                    <li><a href="/database/samurai/908">西園寺公広</a>
                                    <p>伊予の豪族。黒瀬城主。叔父・<a href="/database/samurai/909">実充</a>の養子となり家督を継ぐ。のちに<a href="/database/samurai/1420">豊臣秀吉</a>の四国征伐軍に降るが、西園寺旧臣の叛乱を警戒した戸田勝隆により謀殺された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/909">西園寺宣久</a>
                                    <p>西園寺家臣。<a href="/database/samurai/908">公広</a>の弟。板島城主。西園寺十五将の１人に数えられ、板島殿と呼ばれた。伊勢神宮に参拝した際、紀行日記である『伊勢参宮海陸記』を著した。</p></li>
                                    <li class="mt-8">観修寺基栓 </li>
                                    <li class="mt-8">津島通顕</li>
                                    <li class="mt-8"><a href="/database/samurai/1750">法華津前延</a>
                                    <p>西園寺家臣。伊予法華津城主。西園寺十五将の１人。大友家の侵攻軍を何度も撃退した。<a href="/database/samurai/1420">豊臣秀吉</a>の四国平定後、宇和郡を領した戸田勝隆の命により下城した。</p></li>
                                    <li class="mt-8">今城能定</li>
                                    <li class="mt-8"><a href="/database/samurai/1427">土居清良</a>
                                    <p>西園寺家臣。伊予大森城主。西園寺十五将の１人。一時土佐へ逃れるが、土佐一条家の援助で復帰し、各地で活躍した。知略勇武で知られ「清良記」を著した。</p></li>
                                    <li class="mt-8">河野通賢</li>
                                    <li>竹林院実親</li>
                                    <li class="mt-8">渡辺教忠</li>
                                    <li class="mt-8"><a href="/database/samurai/732">北之川経安</a></li>
                                    <li class="mt-8">魚成親能</li>
                                    <li class="mt-8">宇都宮乗綱</li>
                                    <li class="mt-8"><a href="/database/samurai/370">宇都宮房綱</a>
                                    <p>西園寺家臣。萩森城主。伊予宇都宮家の一族という。近隣の元城主・摂津親宣と領地問題で何度も争った。西園寺十五将の１人に数えられ、萩森殿と呼ばれた。</p></li>
                                    <li class="mt-8">南方親安</li>
                                </ul>
                                <a href="/article/124" class="d-f relate_article mt-24 mb-24 shadow_hover">
                                    <div class="relate_article_img_block">
                                        <img src="/img/article/124/main.png" alt="" width="100%" height="100%" class="relate_article_img">
                                    </div>
                                    <div class="relate_article_description_block">
                                        <p class="fw-b relate_article_title">西園寺十五将の一人で謀反の巻き添えのため戦死した北之川親安（きたのがわちかやす）とは？【マイナー武将列伝】</p>
                                    </div>
                                </a>
                                <h2>大浦家</h2>
                                <h3>大浦三老</h3>
                                <ul>
                                    <li><a href="/database/samurai/518">小笠原信浄</a>
                                    <p>津軽家臣。主君・<a href="/database/samurai/1314">為信</a>の創業期を支えた大浦三老の１人。信濃国の出身で津軽に移住し、大浦家に仕える。大光寺城、石川城攻めに従軍するなど、活躍した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/657">兼平綱則</a>
                                    <p>津軽家臣。主君・<a href="/database/samurai/1314">為信</a>の創業期を支えた大浦三老の１人。兼平家は大浦一族で、種里盛純（<a href="/database/samurai/430">大浦盛信</a>の弟）が兼平村を領した際に、地名を姓としたという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2039">森岡信元</a>
                                    <p>津軽家臣。主君・<a href="/database/samurai/1314">為信</a>の創業期を支えた大浦三老の１人。<a href="/database/samurai/211">石川高信</a>との戦いに参加して和徳城を攻略、城主となる。のちに<a href="/database/samurai/1314">為信</a>から叛意を疑われ謀殺された。</p></li>
                                </ul>
                                <h2>美濃斉藤家</h2>
                                <h3>美濃三人衆</h3>
                                <ul>
                                    <li><a href="/database/samurai/284">稲葉一鉄</a>
                                        <p>斎藤家臣。美濃三人衆の１人。主家滅亡後、織田家に仕える。姉川合戦では浅井軍に横槍を入れ、味方を勝利に導いた。頑固な性格から「一徹」の語源になる。</p>
                                    </li>
                                    <li class="mt-8"><a href="/database/samurai/165">安藤守就</a>
                                    <p>斎藤家臣。美濃三人衆の１人。主家滅亡後は<a href="/database/samurai/554">織田信長</a>に属すが、のちに追放された。本能寺の変に乗じて旧領回復の兵を挙げるが、<a href="/database/samurai/284">稲葉一鉄</a>と戦って敗死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/356">氏家卜全</a>
                                    <p>斎藤家臣。美濃三人衆の１人。主家滅亡後、織田家に仕える。伊勢平定戦で功を立てた。長島一向一揆との戦いで織田軍が敗れた際、殿軍を務め、戦死した。</p></li>
                                </ul>
                                <h2>朝倉家</h2>
                                <h3>一乗谷四奉行</h3>
                                <p>朝倉家の政務を担った一乗谷の重臣。</p>
                                <ul>
                                    <li><a href="/database/samurai/56">朝倉景連</a>
                                    <p>朝倉家臣。<a href="/database/samurai/58">朝倉宗滴</a>の加賀一向一揆攻めで戦功を挙げた。四奉行の一人として内政に手腕を発揮。<a href="/database/samurai/60">義景</a>が犬追物を開催した際、豪奢な出で立ちで皆を驚かせた。</p></li>
                                    <li class="mt-8">前波景定</li>
                                    <li class="mt-8">小泉長利</li>
                                    <li class="mt-8"><a href="/database/samurai/681">河合吉統</a>
                                    <p>朝倉家臣。一乗谷四奉行の１人として国政に参画したほか、浅井家救援のために近江に出陣するなど活躍した。刀禰坂合戦において、織田軍に討たれたという。</p></li>
                                </ul>
                                <h2>筒井家</h2>
                                <h3>筒井家三老臣</h3>
                                <ul>
                                    <li><a href="/database/samurai/1863">松倉右近</a>
                                    <p>筒井家臣。<a href="/database/samurai/1046">島清興</a>・<a href="/database/samurai/2037">森好之</a>とともに、筒井家三老臣の１人に数えられる。主君・<a href="/database/samurai/1328">定次</a>が伊賀上野に転封となった際は、伊賀名張城を築き、８千３百石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1046">島左近</a>
                                    <p>筒井家臣。のち浪人し、<a href="/database/samurai/224">石田三成</a>に高禄で召し抱えられる。「<a href="/database/samurai/224">三成</a>に過ぎたるもの」と謳われた名将。関ヶ原合戦で縦横無尽の活躍をし、壮絶な戦死を遂げた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2037">森好之</a>
                                    <p>筒井家臣。<a href="/database/samurai/1046">島清興</a>・<a href="/database/samurai/1863">松倉重信</a>とともに、筒井家三老臣の１人に数えられる。家中で重要な地位を占めたが、主君・<a href="/database/samurai/1328">定次</a>と対立して致仕、大和に戻って帰農した。</p></li>
                                </ul>
                                <h2>畠山家</h2>
                                <h3>第1次畠山七人衆</h3>
                                <ul>
                                    <li>伊丹総堅</li>
                                    <li class="mt-8">平総知</li>
                                    <li class="mt-8"><a href="/database/samurai/1303">長続連</a>
                                    <p>畠山家臣。畠山七人衆に名を連ねた。温井・三宅一党の叛乱鎮圧に活躍し、重臣筆頭として畠山家の実権を握る。のちに<a href="/database/samurai/554">織田信長</a>に通じ、<a href="/database/samurai/329">上杉謙信</a>に討たれた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1573">温井総貞</a>
                                    <p>畠山家臣。和歌に造詣深く、主君・<a href="/database/samurai/1615">義総</a>の寵愛を得る。遊佐家を倒して主家の実権を握り、領内の動揺を招く。のちに権力奪回を目指す主君・<a href="/database/samurai/1613">義綱</a>に殺された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1960">三宅総広</a>
                                    <p>畠山家臣。畠山七人衆の一人。<a href="/database/samurai/1573">温井総貞</a>と結んで遊佐家を破り、権勢を振るう。のち加賀の一向一揆を扇動して<a href="/database/samurai/1613">畠山義綱</a>に反乱するが敗死したという。</p></li>
                                    <li class="mt-8">遊佐宗円</li>
                                    <li class="mt-8"><a href="/database/samurai/2123">遊佐続光</a>
                                        <p>畠山家臣。温井家との政争に敗れて出奔するが、和睦して帰参する。のちに上杉家に通じて能登を治めるが、織田家の台頭により逐電し、捕縛され斬首された。</p>
                                    </li>
                                </ul>
                                <h3>第2次畠山七人衆</h3>
                                <ul>
                                    <li><a href="/database/samurai/183">飯川光誠</a>
                                    <p>畠山家臣。年寄衆を務めた。主君・<a href="/database/samurai/1613">義綱</a>が能登を追放された際、これに従う。<a href="/database/samurai/1613">義綱</a>の能登入国作戦の中枢を担い、軍勢を指揮して善戦したが、失敗に終わった。</p></li>
                                    <li class="mt-8">神保総誠</li>
                                    <li class="mt-8"><a href="/database/samurai/1303">長続連</a>
                                    <p>畠山家臣。畠山七人衆に名を連ねた。温井・三宅一党の叛乱鎮圧に活躍し、重臣筆頭として畠山家の実権を握る。のちに<a href="/database/samurai/554">織田信長</a>に通じ、<a href="/database/samurai/329">上杉謙信</a>に討たれた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1573">温井続宗</a>（総貞の子）
                                    <p>畠山家臣。<a href="/database/samurai/1574">総貞</a>の子。父を軍事面で補佐し、温井家を家中最大の勢力に発展させた。父が主君・<a href="/database/samurai/1613">義綱</a>に殺されたあと、一族とともに謀叛を起こすが、敗死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1960">三宅総広</a>
                                        <p>畠山家臣。畠山七人衆の一人。<a href="/database/samurai/1573">温井総貞</a>と結んで遊佐家を破り、権勢を振るう。のち加賀の一向一揆を扇動して<a href="/database/samurai/1613">畠山義綱</a>に反乱するが敗死したという。</p></li>
                                    <li class="mt-8">三宅綱賢</li>
                                    <li class="mt-8">遊佐宗円</li>
                                </ul>
                                <h2>山名家</h2>
                                <h3>山名四天王</h3>
                                <ul>
                                    <li><a href="/database/samurai/610">垣屋続成</a>
                                    <p>山名家臣。楽々前城主。主君・致豊の但馬守護就任に伴って守護代となり、領国経営の実権を握った。のちに<a href="/database/samurai/1182">田結庄是義</a>と対立し、<a href="/database/samurai/1182">是義</a>に奇襲されて自害した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1182">田結庄是義</a>
                                    <p>山名家臣。鶴城主。田結庄家は山名四天王の一。織田家への従属を唱え、毛利家への接近をはかる<a href="/database/samurai/610">垣屋続成</a>と対立。<a href="/database/samurai/610">続成</a>を討つが、<a href="/database/samurai/610">続成</a>の子・<a href="/database/samurai/612">光成</a>に討たれた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2049">八木豊信</a>
                                    <p>山名家臣。八木城主。八木家は山名四天王の一。<a href="/database/samurai/1420">羽柴秀吉</a>の但馬侵攻軍に敗れて降伏した。以後は<a href="/database/samurai/1420">秀吉</a>に属して因幡攻略に従事し、若桜鬼ヶ城を守ったという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/460">太田垣輝延</a>
                                    <p>山名家臣。竹田城主。太田垣家は山名四天王の一。織田家から毛利家に寝返ったため、<a href="/database/samurai/1420">羽柴秀吉</a>に攻められる。一時は居城を奪還するがのちに敗れ、逃亡した。</p></li>
                                </ul>
                                <h2>六角家</h2>
                                <h3>六角家の両藤</h3>
                                <ul>
                                    <li><a href="/database/samurai/1098">進藤貞治</a>・<a href="/database/samurai/1097">進藤賢盛</a>
                                    <p>進藤貞治 : 六角家臣。木浜城主。<a href="/database/samurai/899">後藤賢豊</a>とともに「両藤」と称された。幕府や細川家、本願寺との折衝や、延暦寺と法華宗の紛争の調停など、おもに外交面で活躍した。</p>
                                    <p>進藤賢盛 : 六角家臣。<a href="/database/samurai/554">織田信長</a>の上洛軍に降る。以後は<a href="/database/samurai/948">佐久間信盛</a>の与力として、各地に従軍した。<a href="/database/samurai/948">信盛</a>の改易後は旗本となる。本能寺の変後は<a href="/database/samurai/700">蒲生氏郷</a>の与力となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/899">後藤賢豊</a>
                                    <p>六角家臣。進藤家とともに「六角家の両藤」と称された。主君・<a href="/database/samurai/2175">義賢</a>親子が上洛した際は警護役を務めた。いわゆる観音寺騒動において主君・<a href="/database/samurai/2176">義治</a>に殺された。</p></li>
                                </ul>
                                <h2>蘆名家</h2>
                                <h3>蘆名四天</h3>
                                <ul>
                                    <li><a href="/database/samurai/649">金上盛備</a>
                                    <p>蘆名家臣。主君・<a href="/database/samurai/93">盛隆</a>の死後、<a href="/database/samurai/1290">伊達政宗</a>の弟・<a href="/database/samurai/1282">小次郎</a>を跡継ぎに推す一派を抑え<a href="/database/samurai/966">佐竹義重</a>の次男・<a href="/database/samurai/94">義広</a>を当主に迎えることに成功した。摺上原合戦で戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1895">松本氏輔</a>
                                    <p>蘆名家臣。舜輔の子。松本家は蘆名四天の宿老の一。図書助と称し、船岡城主を務めた。１５７５年、<a href="/database/samurai/1262">田村清顕</a>の軍と安積郡福原で戦った際に戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/957">佐瀬種常</a>
                                    <p>蘆名家臣。佐瀬家は、蘆名四天の宿老の一。耶麻郡中田付で六斎市を開いていたが、中田付では不便だとして、小田付村に六斎市を移した。摺上原合戦で戦死。</p></li>
                                    <li class="mt-8">佐瀬常雄</li>
                                </ul>
                                <h2>宇喜多家</h2>
                                <h3>宇喜多三老</h3>
                                <ul>
                                    <li><a href="/database/samurai/537">長船貞親</a>
                                    <p>宇喜多家臣。宇喜多三老の１人。明禅寺合戦など各地の合戦に従軍し、主君・<a href="/database/samurai/349">直家</a>の創業を助けた。<a href="/database/samurai/1368">戸川秀安</a>の隠退後は国政を担当する。のち妹婿に殺された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/495">岡家利</a>
                                    <p>宇喜多家臣。<a href="/database/samurai/498">利勝</a>の子。主家の内乱により出奔、<a href="/database/samurai/1381">徳川家康</a>に仕える。しかし、大坂の陣において子・平内が大坂方に属したため、<a href="/database/samurai/1381">家康</a>の勘気を蒙り、自害した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1368">戸川秀安</a>
                                    <p>宇喜多家臣。宇喜多三老の１人。明禅寺合戦や児島八浜合戦など多くの合戦に従軍し、主君・<a href="/database/samurai/349">直家</a>の創業を助けた。のちに筆頭家老を務め、国政にも参画した。</p></li>
                                </ul>
                                <h2>島津家</h2>
                                <h3>島津看経所四名臣</h3>
                                <ul>
                                    <li><a href="/database/samurai/668">鎌田政年</a>
                                    <p>島津家臣。肥後矢崎城攻めなどで活躍した。薩摩馬越城攻めで大活躍し、主君・忠良から功を賞され、島津家の看経所に名を残した４人のうちの１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1545">新納忠元</a>
                                    <p>島津家臣。薩摩馬越城攻めや肥後経略など、各地の合戦で活躍し、島津家の看経所に名を残した４人のうちの１人。「二才咄格式定目」を著して子弟を戒めた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/768">肝付兼盛</a>
                                    <p>島津家臣。肝付家の庶流。父・<a href="/database/samurai/766">兼演</a>から加治木城主を継いだ。<a href="/database/samurai/1051">島津貴久</a>・<a href="/database/samurai/1066">義久</a>の家老を務めた。蒲生家攻めや伊東家攻めで特に軍功があった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/684">川上久朗</a>
                                    <p>島津家臣。<a href="/database/samurai/682">忠克</a>の子。島津家の看経所に名を残した４人のうちの１人。１８歳の時には主君・<a href="/database/samurai/1066">義久</a>から守護代に推されたという。薩摩大口城攻撃戦で戦死した。</p></li>
                                </ul>
                                <a href="/article/81" class="d-f relate_article mt-24 mb-24 shadow_hover"><div class="relate_article_img_block">
                                    <img src="/img/article/81/main.png" alt="" width="100%" height="100%" class="relate_article_img"></div>
                                    <div class="relate_article_description_block"><p class="fw-b relate_article_title">「永久に島津家はその功を忘れぬ」島津看経所四名臣の一人・鎌田政年（かまたまさとし）とは？【マイナー武将列伝】</p>
                                </div></a>
                                <h2>龍造寺家</h2>
                                <h3>龍造寺四天王</h3>
                                <p>江里口信常もしくは円城寺信胤と、文献によって異なる</p>
                                <ul>
                                    <li><a href="/database/samurai/1522">成松信勝</a>（遠江守）
                                    <p>龍造寺家臣。龍造寺四天王の１人。今山合戦で大友軍総大将を討つ功を立てた。沖田畷合戦に軍奉行として従軍、主君・<a href="/database/samurai/2164">隆信</a>の戦死を聞くと敵陣に突入し戦死。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1679">百武賢兼</a>（志摩守）
                                    <p>龍造寺家臣。龍造寺四天王の１人。今山合戦などで活躍し、武勇百人にまさると主君・隆信から百武姓を賜る。沖田畷合戦の際は<a href="/database/samurai/2164">隆信</a>の身辺を守り、戦死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/758">木下昌直（四郎兵衛尉）</a>
                                    <p>龍造寺家臣。龍造寺四天王の１人といわれる。沖田畷合戦で主君・<a href="/database/samurai/2164">隆信</a>戦死の報を聞くと、<a href="/database/samurai/2164">鍋島直茂</a>を離脱させたのち、敵陣に突入した。生死は諸説あり不明。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/403">江里口信常（藤七兵衛尉）</a>
                                    <p>龍造寺家臣。龍造寺四天王の１人。沖田畷合戦で主君・<a href="/database/samurai/2164">隆信</a>戦死の報を聞くと、単身で<a href="/database/samurai/1047">島津家久</a>軍の本陣に突入し戦死。「無双の剛の者」と家久に賞賛された。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/404">円城寺信胤</a>
                                    <p>龍造寺家臣。龍造寺四天王の１人といわれる。沖田畷合戦で主君・<a href="/database/samurai/2164">隆信</a>戦死の報告を受けると、<a href="/database/samurai/2164">隆信</a>に似せた出で立ちをしたのち、敵陣に斬り込んで戦死した。</p></li>
                                </ul>
                                <h3>龍造寺三法師</h3>
                                <ul>
                                    <li>高木泰栄</li>
                                    <li class="mt-8">馬渡賢斎</li>
                                    <li class="mt-8">成富源意</li>
                                </ul>
                                <h3>東肥前十九将</h3>
                                <ul>
                                    <li>朝日宗贇</li>
                                    <li class="mt-8">姉川惟安</li>
                                    <li class="mt-8">綾部鎮幸</li>
                                    <li class="mt-8">出雲氏忠</li>
                                    <li class="mt-8">犬塚家重</li>
                                    <li class="mt-8">犬塚鎮直</li>
                                    <li class="mt-8">犬塚鎮家</li>
                                    <li class="mt-8"><a href="/database/samurai/389">江上武種</a>
                                    <p>少弐家臣。肥前勢福寺城主。<a href="/database/samurai/2164">龍造寺隆信</a>の家督相続に反対した東肥前十九将の１人。龍造寺家臣・<a href="/database/samurai/563">小田政光</a>を討つなど活躍したが、のちに龍造寺家臣となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/563">小田政光</a>
                                    <p>少弐家臣。肥前蓮池城主。<a href="/database/samurai/2164">龍造寺隆信</a>の家督相続に反対した東肥前十九将の１人で、<a href="/database/samurai/2164">隆信</a>と争う。<a href="/database/samurai/2164">隆信</a>の肥前復帰後は<a href="/database/samurai/2164">隆信</a>に仕え、<a href="/database/samurai/389">江上武種</a>討伐戦で戦死した。</p></li>
                                    <li class="mt-8">神代勝利</li>
                                    <li class="mt-8">宗尚夏</li>
                                    <li class="mt-8">高木胤秀</li>
                                    <li class="mt-8"><a href="/database/samurai/1184">高木鑑房</a>
                                    <p>龍造寺家臣。<a href="/database/samurai/2164">龍造寺隆信</a>の家督相続に反対した、東肥前十九将の１人。<a href="/database/samurai/2164">隆信</a>の肥前復帰の際に討たれた。斬られた際、頭が無い状態で従者の首を斬ったという。</p></li>
                                    <li class="mt-8">筑紫惟門</li>
                                    <li class="mt-8">馬場鑑周</li>
                                    <li class="mt-8">藤崎盛義</li>
                                    <li class="mt-8">本告信景</li>
                                    <li class="mt-8">横岳資誠</li>
                                    <li class="mt-8">八戸宗暘</li>
                                </ul>
                                <h2>大友家</h2>
                                <h3>豊後三老</h3>
                                <ul>
                                    <li><a href="/database/samurai/361">臼杵鑑速</a>
                                    <p>大友家臣。豊後三老の１人。外交事務を担当する一方で、肥前方分を務めて主家の国政に参画する。また筑前平定軍の総大将を務めるなど、各方面で活躍した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2144">吉岡長増</a>
                                    <p>大友家臣。豊後三老の１人。知略に優れた。大友家と毛利家が筑前で対峙した際は、<a href="/database/samurai/425">大内義隆</a>の従兄弟・輝弘を扇動して周防に侵攻させ、毛利軍を撤退させた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2151">吉弘鑑理</a>
                                    <p>大友家臣。豊後三老の１人。主君・義鎮の執政を補佐した。勢場ヶ原合戦で大内軍を撃退し、また多々良浜合戦で毛利軍を破るなど、多くの合戦で功を立てた。</p></li>
                                </ul>
                                <h2>南部家</h2>
                                <h3>南部四天王</h3>
                                <ul>
                                    <li>福士氏</li>
                                    <li class="mt-8">三上氏</li>
                                    <li class="mt-8">安芸氏</li>
                                    <li class="mt-8">桜庭氏、<a href="/database/samurai/1706">桜庭直綱</a>など
                                    <p>桜庭直綱 : 南部家臣。南部四天王の一人。岩崎一揆鎮圧などに活躍。親族の北信景が豊臣家に加担して大坂城に入ると、家系図を偽造して敗戦後の処分を免れたとされる。</p></li>
                                </ul>
                                <h2>那須家</h2>
                                <h3>那須七党</h3>
                                <ul>
                                    <li>那須氏</li>
                                    <li class="mt-8">蘆野氏</li>
                                    <li class="mt-8">伊王野氏</li>
                                    <li class="mt-8">千本氏</li>
                                    <li class="mt-8">福原氏、<a href="/database/samurai/1706">福原資孝</a>・<a href="/database/samurai/1707">福原資保</a>など
                                    <p>福原資孝 : 那須七党の一。<a href="/database/samurai/465">大田原資清</a>の子。父が強制的に隠居させた福原資安の跡を継ぎ、兄・<a href="/database/samurai/446">大関高増</a>を補佐した。<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に遅参し、所領を削られた。</p>
                                    <p>福原資保 : 那須七党の一。<a href="/database/samurai/1706">資孝</a>の次男。兄・資広の養子となり家督を継ぐ。関ヶ原合戦では東軍に属す。大坂の陣では<a href="/database/samurai/1815">本多正信</a>に属して戦った。のちに大坂城番を務めた。</p></li>
                                    <li class="mt-8">大関氏、<a href="/database/samurai/446">大関高増</a>など
                                    <p>大関高増 : 那須七党の一。<a href="/database/samurai/465">大田原資清</a>の子。大関・大田原・福原家を率い、伊王野・蘆野家と結び、大伯父として主君・<a href="/database/samurai/1493">資晴</a>を後見するなど家中最大の勢力を築き上げた。</p></li>
                                    <li class="mt-8">大田原氏、<a href="/database/samurai/465">大田原資清</a>・<a href="/database/samurai/466">大田原綱清</a>・<a href="/database/samurai/467">大田原晴清</a>など
                                    <p>大田原資清 : 那須七党の一。大田原城主。黒羽城主・大関増次を討って子・<a href="/database/samurai/446">高増</a>に大関家を継がせたり、娘を主君・<a href="/database/samurai/1496">政資</a>の側室とするなど、那須家中で最大の勢力を築いた。</p>
                                    <p>大田原綱清 : 那須七党の一。<a href="/database/samurai/465">資清</a>の三男。兄の高増・資孝が別家を立てたため、大田原家を継ぐ。<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に際しては、子・<a href="/database/samurai/467">晴清</a>を<a href="/database/samurai/1420">秀吉</a>の出迎えに遣わした。</p>
                                    <p>大田原晴清 : 那須七党の一。<a href="/database/samurai/466">綱清</a>の子。<a href="/database/samurai/1420">豊臣秀吉</a>の小田原征伐に参陣し、所領を安堵された。関ヶ原合戦では東軍に属し、上杉家の様子を探るなど活躍し、戦後加増された。</p>
                                    </li>
                                </ul>
                                <h2>佐野家</h2>
                                <h3>佐野四天王</h3>
                                <ul>
                                    <li><a href="/database/samurai/2086">山上道及</a>
                                    <p>佐野家臣。名は照久、氏秀とも。佐野四天王の１人。首供養を３度行ったという家中随一の猛将。のち武者修業のため致仕した。関ヶ原合戦では上杉家に属す。</p></li>
                                    <li class="mt-8">大貫定行</li>
                                    <li class="mt-8">竹沢定冬</li>
                                    <li class="mt-8">津布久士郎</li>
                                </ul>
                                <h2>遠山家</h2>
                                <h3>遠山七家</h3>
                                <ul>
                                    <li>岩村遠山氏</li>
                                    <li class="mt-8">明知遠山氏</li>
                                    <li class="mt-8">苗木遠山氏</li>
                                    <li class="mt-8">飯羽間遠山氏</li>
                                    <li class="mt-8">明照遠山氏</li>
                                    <li class="mt-8">串原遠山氏</li>
                                    <li class="mt-8">安木遠山氏</li>
                                </ul>
                                <h2>豪族や大名家を超えての総称</h2>
                                <h3>三英傑</h3>
                                <ul>
                                    <li><a href="/database/samurai/554">織田信長</a>
                                    <p><a href="/database/samurai/555">信秀</a>の嫡男。<a href="/database/samurai/305">今川義元</a>を桶狭間で破る。以後、天下布武を標榜して敵対勢力を次々と滅ぼした。天下統一を目前にして、<a href="/database/samurai/49">明智光秀</a>の謀叛に遭い本能寺に散った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1420">豊臣秀吉</a>
                                    <p>戦国一の出世頭。<a href="/database/samurai/554">織田信長</a>に仕え、傑出した人望と知略を武器に活躍し、頭角を現す。本能寺の変後、<a href="/database/samurai/49">明智光秀</a>、<a href="/database/samurai/1039">柴田勝家</a>らを次々と倒し、天下に覇を唱えた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1381">徳川家康</a>
                                    <p>江戸幕府の創始者。<a href="/database/samurai/1382">広忠</a>の子。桶狭間の合戦後に自立。織田家との同盟、豊臣家への従属を経て勢力を拡大する。関ヶ原合戦で勝利を収め征夷大将軍となった。</p></li>
                                </ul>
                                <h3>戦国三梟雄</h3>
                                <ul>
                                    <li><a href="/database/samurai/921">斎藤道三</a>
                                    <p>「蝮」の異名をとった美濃の戦国大名。僧から油商人に転身、次いで美濃守護・<a href="/database/samurai/1373">土岐頼芸</a>に仕官、<a href="/database/samurai/1373">頼芸</a>を追放して国主となった。のちに子・<a href="/database/samurai/923">義龍</a>と戦い、敗死。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1890">松永久秀</a>
                                    <p>三好家臣。主家を簒奪し、将軍・<a href="/database/samurai/82">足利義輝</a>を殺し、東大寺大仏殿を焼いた稀代の梟雄。のち<a href="/database/samurai/554">織田信長</a>に属し、謀叛を起こすが敗れ「平蜘蛛」とともに爆死した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/349">宇喜多直家</a>
                                    <p>浦上家臣。乙子城主。権謀術数の限りを尽くして敵を葬り去り、家中最大の勢力を築き上げる。最後は主君・<a href="/database/samurai/383">宗景</a>を追放して備前国を掌握した稀代の謀将。</p></li>
                                </ul>
                                <h3>天下の三陪臣</h3>
                                <ul>
                                    <li><a href="/database/samurai/1444">直江兼続</a>
                                    <p>上杉家臣。筆頭家老を務めた。<a href="/database/samurai/1420">豊臣秀吉</a>の評価は高く、陪臣ながら出羽米沢３０万石を領した。関ヶ原合戦の際は西軍に属し、<a href="/database/samurai/1381">徳川家康</a>に「直江状」を送った。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/884">小早川隆景</a>
                                    <p><a href="/database/samurai/2015">毛利元就</a>の三男。安芸の豪族・小早川家を継ぎ、山陽地方の攻略にあたる。本能寺の変後は毛利家の存続をはかって<a href="/database/samurai/1420">豊臣秀吉</a>に接近し、五大老の１人となった。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1778">堀直政</a>
                                    <p>織田家臣。姓は奥田とも。親族である<a href="/database/samurai/1781">堀秀政</a>に付き従い、各地で戦功を立てる。<a href="/database/samurai/1781">秀政</a>死後は息子の<a href="/database/samurai/1780">堀秀治</a>を補佐。その実績から天下の三陪臣の一人とも称される。</p></li>
                                </ul>
                                <h3>武蔵七党</h3>
                                <ul>
                                    <li>横山党</li>
                                    <li class="mt-8">猪俣党、<a href="/database/samurai/1714">藤田康邦</a>など
                                    <p>藤田康邦 : 山内上杉家臣。武蔵七党の１つ、猪俣党に属す。河越夜戦で<a href="/database/samurai/1743">北条氏康</a>に敗れて降服した。<a href="/database/samurai/1743">氏康</a>の三男・<a href="/database/samurai/1733">氏邦</a>を娘婿として家督を譲り、隠居後は用土康邦と称す。</p></li>
                                    <li class="mt-8">野与党</li>
                                    <li class="mt-8">村山党</li>
                                    <li class="mt-8">西党（西野党）</li>
                                    <li class="mt-8">児玉党</li>
                                    <li class="mt-8">丹党（丹治党）</li>
                                </ul>
                                <h3>信濃の四大将</h3>
                                <ul>
                                    <li><a href="/database/samurai/1999">村上義清</a>
                                    <p>信濃の豪族。葛尾城主。<a href="/database/samurai/1219">武田信玄</a>軍の攻撃を２度も退け、近隣に勇名を轟かす。しかし、<a href="/database/samurai/993">真田幸隆</a>の計略に敗れて居城を失い、越後の長尾景虎の庇護を受けた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/515">小笠原長時</a>
                                    <p>信濃守護。<a href="/database/samurai/516">長棟</a>の長男。<a href="/database/samurai/1219">武田信玄</a>に信濃を追われ、越後・摂津・会津など諸国を流浪する。子・<a href="/database/samurai/512">貞慶</a>が<a href="/database/samurai/554">信長</a>の下で旧領に復帰するが、戻ることなく死亡した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1151">諏訪頼重</a>
                                    <p>信濃の戦国大名。祖父・<a href="/database/samurai/1154">頼満</a>の死後、諏訪大社大祝となる。義兄・<a href="/database/samurai/1219">武田信玄</a>と戦うが敗れ、幽閉の後に自害させられた。娘は<a href="/database/samurai/1219">信玄</a>の側室となり、<a href="/database/samurai/1218">勝頼</a>を産んだ。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/725">木曾義康</a>
                                    <p>信濃木曾谷の豪族。<a href="/database/samurai/722">義在</a>の嫡男。<a href="/database/samurai/1999">村上義清</a>・<a href="/database/samurai/515">小笠原長時</a>・<a href="/database/samurai/1151">諏訪頼重</a>とともに「信濃の四大将」と呼ばれた。<a href="/database/samurai/1219">武田信玄</a>の攻撃に頑強に抵抗したが敗れ、降伏した。</p></li>    
                                </ul>
                                <a href="/article/119" class="d-f relate_article mt-24 mb-24 shadow_hover">
                                    <div class="relate_article_img_block">
                                        <img src="/img/article/119/main.png" alt="" width="100%" height="100%" class="relate_article_img">
                                    </div>
                                    <div class="relate_article_description_block">
                                        <p class="fw-b relate_article_title">「信濃の四大将」と呼ばれるも信玄を前に散った木曾義康（きそよしやす）とは？【マイナー武将列伝】</p>
                                    </div>
                                </a>
                                <h3>利休七哲</h3>
                                <ul>
                                    <li><a href="/database/samurai/1831">前田利長</a>→抹消
                                    <p><a href="/database/samurai/1829">利家</a>の嫡男。家督を相続後、謀叛の兆しありとの噂が流れるが、母・芳春院を人質に出して討伐を免れた。関ヶ原合戦で東軍に属し、加賀１００万石を領した。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/700">蒲生氏郷</a>
                                    <p>織田家臣。<a href="/database/samurai/701">賢秀</a>の子。主君・<a href="/database/samurai/554">信長</a>の娘を娶る。本能寺の変後は<a href="/database/samurai/1420">豊臣秀吉</a>に仕え活躍、陸奥会津９２万石を領した。文武に秀でたその器量を<a href="/database/samurai/1420">秀吉</a>は恐れたという。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1761">細川忠興</a>
                                    <p>織田家臣。<a href="/database/samurai/1766">藤孝</a>の子。<a href="/database/samurai/49">明智光秀</a>の娘を娶るが、本能寺の変後は豊臣家に属した。関ヶ原合戦では東軍に属し、豊前中津３９万６千石を領した。利休七哲の１人。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/1719">古田織部重然</a>
                                    <p>豊臣家臣。名は重然。織部正に任じられたことから「織部」と呼ばれ、陶器「織部焼」を創始する。利休七哲の１人でもあり、諸大名の茶の師匠を務めた。</p></li>
                                    <li class="mt-8">牧村利貞</li>
                                    <li class="mt-8"><a href="/database/samurai/1205">高山右近</a>
                                    <p>織田家臣。高槻城主。<a href="/database/samurai/1206">友照</a>の子。父と同じくキリスト教に入信する。各地の合戦で活躍するが、後に改宗を拒否したため改易され、幕府の命で呂宋へ追放された。</p></li>
                                    <li class="mt-8">芝山宗綱</li>
                                    <li class="mt-8">瀬田正忠←追加</li>
                                </ul>
                                <h3>利休門三人衆</h3>
                                <ul>
                                    <li><a href="/database/samurai/700">蒲生氏郷</a>
                                        <p>織田家臣。<a href="/database/samurai/701">賢秀</a>の子。主君・<a href="/database/samurai/554">信長</a>の娘を娶る。本能寺の変後は<a href="/database/samurai/1420">豊臣秀吉</a>に仕え活躍、陸奥会津９２万石を領した。文武に秀でたその器量を<a href="/database/samurai/1420">秀吉</a>は恐れたという。</p></li>
                                    <li class="mt-8">芝山宗綱</li>
                                    <li class="mt-8"><a href="/database/samurai/1761">細川忠興</a>
                                        <p>織田家臣。<a href="/database/samurai/1766">藤孝</a>の子。<a href="/database/samurai/49">明智光秀</a>の娘を娶るが、本能寺の変後は豊臣家に属した。関ヶ原合戦では東軍に属し、豊前中津３９万６千石を領した。利休七哲の１人。</p></li>
                                </ul>
                                <h3>会津三奉行</h3>
                                <ul>
                                    <li><a href="/database/samurai/421">大石綱元</a>
                                    <p>山内上杉家臣。主家滅亡後は<a href="/database/samurai/326">上杉景勝</a>に仕える。<a href="/database/samurai/326">景勝</a>の会津転封に従い、保原城代となった。<a href="/database/samurai/2067">安田能元</a>、<a href="/database/samurai/311">岩井信能</a>とともに会津三奉行の一人に数えられた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/2067">安田能元</a>
                                    <p>上杉家臣。安田城主。恩賞問題のこじれにより自害した兄・<a href="/database/samurai/2063">顕元</a>の跡を継ぐ。主家の会津移封に従い、二本松城代となった。会津三奉行の１人に数えられた。</p></li>
                                    <li class="mt-8"><a href="/database/samurai/311">岩井信能</a>
                                    <p>上杉家臣。御館の乱では<a href="/database/samurai/326">上杉景勝</a>に属し飯山城主となる。城下町を整備して近世飯山町の基礎を築いた。主家の会津転封後は、会津三奉行の１人に数えられた。</p></li>
                                </ul>
                                <h3>天草十七人衆</h3>
                                <ul>
                                    <li>蘆塚忠右衛門</li>
                                    <li class="mt-8">千々石五郎左衛門</li>
                                    <li class="mt-8">大矢野松右衛門</li>
                                    <li class="mt-8"><a href="/database/samurai/27">赤星道重</a>
                                    <p>島原の乱指導者・天草十七人衆の一人。<a href="/database/samurai/26">親武</a>の子。大坂の陣で父とともに大坂城に籠城し脱出。島原の乱では原城本丸付近を守り、一騎打ちの末討死した。</p></li>
                                    <li class="mt-8">益田好次</li>
                                    <li class="mt-8">森宗意軒</li>
                                    <li class="mt-8">駒木根友房</li>
                                    <li class="mt-8">山田右衛門作</li>
                                    <li class="mt-8">天草玄察</li>
                                    <li class="mt-8">千束善右衛門</li>
                                    <li class="mt-8">栖本左京進</li>
                                    <li class="mt-8">鹿子木右馬助</li>
                                    <li class="mt-8">田崎刑部</li>
                                    <li class="mt-8">蘆塚左内</li>
                                    <li class="mt-8">蘆塚忠太夫</li>
                                    <li class="mt-8">戸塚宗右衛門</li>
                                    <li class="mt-8">有馬休意</li>
                                </ul>
                                <h3>土佐七雄</h3>
                                <ul>
                                    <li>本山氏、<a href="/database/samurai/2027">本山茂宗</a>など
                                    <p>本山茂宗 : 土佐の豪族。本山城主。嶺北地方一帯に加え南方の平野部に支配圏を広げ、本山家最大の版図を築いた。のちに子・<a href="/database/samurai/2026">茂辰</a>に本山城を譲り、土佐朝倉城に移った。</p></li>
                                    <li class="mt-8">吉良氏、<a href="/database/samurai/777">吉良親貞</a>・<a href="/database/samurai/778">吉良親実</a>など
                                    <p>吉良親貞 : <a href="/database/samurai/1306">長宗我部国親</a>の次男。剛毅かつ知略に富んだ武将で、兄・<a href="/database/samurai/1310">元親</a>の片腕として土佐統一に貢献した。土佐一条家滅亡後は土佐中村城代となるが間もなく病死した。</p>
                                    <p>吉良親実 : 長宗我部家臣。<a href="/database/samurai/778">親貞</a>の嫡男。宗家の家督相続に際し、四男・<a href="/database/samurai/1311">盛親</a>の擁立を望む伯父・<a href="/database/samurai/1310">元親</a>を諫めるが、対立勢力の<a href="/database/samurai/1670">久武親直</a>に讒言され、<a href="/database/samurai/1310">元親</a>に自害させられた。</p></li>
                                    <li class="mt-8">安芸氏、<a href="/database/samurai/35">安芸国虎</a>など
                                    <p>安芸国虎 : 土佐の豪族。安芸城主。<a href="/database/samurai/1310">長宗我部元親</a>の居城・土佐岡豊城への招請を断り、<a href="/database/samurai/1310">元親</a>と戦う。しかし<a href="/database/samurai/1310">元親</a>の謀略により家臣団が内部崩壊を起こして敗北、自害した。</p></li>
                                    <li class="mt-8">津野氏、<a href="/database/samurai/1336">津野定勝</a>・<a href="/database/samurai/1337">津野基高</a>など
                                    <p>津野定勝 : 一条家臣。土佐七雄の一・津野家当主。<a href="/database/samurai/1337">基高</a>の子。鳥坂峠合戦に従軍。<a href="/database/samurai/1310">長宗我部元親</a>が一条家からの離反を要求するも拒絶。長宗我部派の家臣に追放された。</p>
                                    <p>津野基高 : 土佐七雄の一・津野家当主。幼くして家督を相続したため、一条基房の攻撃を受ける。太平家や本山家と結んで抵抗したが、ついには屈服。その傘下に入った。</p></li>
                                    <li class="mt-8">香宗我部氏、<a href="/database/samurai/850">香宗我部親泰</a>など
                                    <p>香宗我部親泰 : <a href="/database/samurai/1306">長宗我部国親</a>の三男。阿波中富川合戦で<a href="/database/samurai/1177">十河存保</a>軍を破るなど、兄・<a href="/database/samurai/1310">元親</a>の片腕として四国統一に貢献した。織田家に使者として赴くなど、外交でも活躍した。</p></li>
                                    <li class="mt-8">大平氏、大平元国など</li>
                                    <li class="mt-8">長宗我部氏、<a href="/database/samurai/1306">長宗我部国親</a>・<a href="/database/samurai/1310">長宗我部元親</a>など
                                    <p>長宗我部国親 : 土佐の戦国大名。岡豊城主。父・兼序の死後、<a href="/database/samurai/251">一条房家</a>を頼り、房家の援助により居城に復帰。婚姻外交と積極的な富国策で長宗我部家の再興に生涯を捧げた。</p>
                                    <p>長宗我部元親 : 土佐の戦国大名。岡豊城主。国親の子。剽悍の一領具足衆を率い、瞬く間に周辺諸国を制圧。１０数年で四国統一を成し遂げ「土佐の出来人」の異名をとった。</p></li>
                                </ul>
                                <h3>由利十二頭</h3>
                                <ul>
                                    <li>矢島氏</li>
                                    <li class="mt-8">仁賀保氏、<a href="/database/samurai/1551">仁賀保挙誠</a>・<a href="/database/samurai/1552">仁賀保挙晴</a>など
                                    <p>仁賀保挙誠 : 由利十二頭の筆頭。文禄の役では肥前名護屋城に在陣した。会津征伐で功を立て<a href="/database/samurai/1381">徳川家康</a>から感状を授かった。関ヶ原合戦では東軍に属し、所領を安堵された。</p>
                                    <p>仁賀保挙晴 : 由利十二頭の一。<a href="/database/samurai/2019">最上義光</a>と争う。欧州仕置で出羽由利郡南部の領有を認められた。関ヶ原合戦後、移封。しかし奮戦がのちに評価され、旧領に復した。</p></li>
                                    <li class="mt-8">赤尾津氏、<a href="/database/samurai/14">赤尾津家保</a>・<a href="/database/samurai/15">赤尾津光政</a>など
                                    <p>赤尾津家保 : 由利十二頭の一。仙北大曲城主・前田道信が侵攻してくると、これを返り討ちにした。しかし後年、道信の次男・利宗の仇討ち合戦に敗れて戦死したという。</p>
                                    <p>赤尾津光政 : 由利十二頭の一。由利郡に侵攻した<a href="/database/samurai/1278">大宝寺義氏</a>が赤尾津領に迫ると、<a href="/database/samurai/161">安東愛季</a>に救助を要請。安東家の援軍を得た<a href="/database/samurai/15">光政</a>はの荒沢戦いで大宝寺軍を壊滅させた。</p></li>
                                    <li class="mt-8">潟保氏</li>
                                    <li class="mt-8">打越氏</li>
                                    <li class="mt-8">子吉氏</li>
                                    <li class="mt-8">下村氏</li>
                                    <li class="mt-8">玉米氏</li>
                                    <li class="mt-8">鮎川氏</li>
                                    <li class="mt-8">石沢氏</li>
                                    <li class="mt-8">滝沢氏</li>
                                    <li class="mt-8">岩屋氏</li>
                                    <li class="mt-8">羽川氏</li>
                                    <li class="mt-8">芹田氏</li>
                                    <li class="mt-8">沓沢氏</li>
                                </ul>
                                <h3>伊賀十二人衆</h3>
                                <ul>
                                    <li>百田籐兵衛(<a href="/database/samurai/2030">百地三太夫</a>)
                                    <p>伊賀の豪族。服部・藤林と並ぶ伊賀上忍家の一つ。忍術を駆使して<a href="/database/samurai/545">織田信雄</a>の伊賀侵攻軍を撃退するが、のちに<a href="/database/samurai/554">織田信長</a>の大軍の攻撃を受け、戦死したという。</p></li>
                                    <li class="mt-8">福喜多将監</li>
                                    <li class="mt-8">町井左馬充貞信</li>
                                    <li class="mt-8">田屋掃部介</li>
                                    <li class="mt-8">音羽半六</li>
                                    <li class="mt-8">富岡忠兵衛</li>
                                    <li class="mt-8">小泉左京</li>
                                    <li class="mt-8">中林助左衛門(中村助左衛門)</li>
                                    <li class="mt-8">布生大善</li>
                                    <li class="mt-8">滝野十郎吉政</li>
                                    <li class="mt-8"><a href="/database/samurai/339">植田豊前光信</a>
                                    <p>伊賀の豪族。伊賀十二人衆の中心人物。<a href="/database/samurai/545">織田信雄</a>の伊賀侵攻の際は、<a href="/database/samurai/545">信雄</a>の臣・柘植三郎左衛門を討ち、織田軍を撃退した。のち織田軍に敗れ、三河に逃れた。</p></li>
                                    <li class="mt-8">家喜下総</li>
                                </ul>
                                <h3>申次七人衆</h3>
                                <ul>
                                    <li>摂津元造</li>
                                    <li class="mt-8">大舘常興</li>
                                    <li class="mt-8">大舘晴光</li>
                                    <li class="mt-8"><a href="/database/samurai/790">朽木稙綱</a>
                                    <p>近江の豪族。三好元長に京を追われた将軍・<a href="/database/samurai/83">足利義晴</a>を居館にかくまう。その功により申次七人衆の１人に加えられた。以後も何度か足利将軍家をかくまった。</p></li>
                                    <li class="mt-8">細川高久</li>
                                    <li class="mt-8">海老名高助</li>
                                    <li class="mt-8">本郷光泰</li>
                                    <li class="mt-8">荒川氏隆</li>
                                </ul>
                                <h2>まとめ</h2>
                                <p>いかがでしたか？</p>
                                <p>結構ありましたね。</p>
                                <p>賤ヶ岳七本槍や黒田二十四騎など聞いたことあるものがあれば、全く知らないというも多かったですね。</p>
                                <p>まとめるのしんどかったですが、誰かのためになると幸いです。</p>
                                <p>よくまとめた！って思ってくれた方は、Twitterでシェアしたり、ブログやってる人であれば当サイトのリンク貼ってくださーい！やる気が出ます！</p>
                                <p>他にも面白い記事がありますので、見ていって下さい。</p>
                            </section>
                            <div class="mt-24">
                                <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="【完全版】四天王・○人衆・○神将などと呼ばれた戦国時代の家臣団" data-url="{{ url()->current() }}" data-hashtags="信長の野望徹底攻略" data-size="large" data-show-count="false">ツイート</a>
                                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                        </div>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'other.vassals'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection