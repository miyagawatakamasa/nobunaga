@extends('layouts.layout')

@section('title', '戦国年表 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="戦国時代に絞った年表です。随時更新中です。">
    <meta property="og:description" content="戦国時代に絞った年表です。随時更新中です。" />
    <meta property="og:title" content="戦国年表 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "戦国年表 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script>
        $(function(){
            $('.priority_radio_button').on('click', function() {
                $value = $(this).attr('value');
                if ($value == 1) {
                    $('.timeline_list').css('display','table-row');
                } else {
                    $('.priority_0').css('display','none');
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年03月24日</span>
                            <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年11月13日</span>
                        </time>
                    </div>
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/timeline.png')}}" alt="戦国年表アイコン" width="30px" height="30px" class="mr-8">
                            <p>戦国年表</p>
                        </div>
                    </h1>
                    <p class="mt-24">戦国時代に絞った年表です。</p>
                    <div class="mt-12">
                        <label style="cursor: pointer">
                            <input type="radio" name="priority" value="1" checked class="priority_radio_button">
                            <span>全て表示</span>
                        </label>
                        <label style="cursor: pointer">
                            <input type="radio" name="priority" value="0" class="priority_radio_button">
                            <span>優先度の高い事象のみ表示</span>
                        </label>
                    </div>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th>年</th>
                            <th>月</th>
                            <th>日</th>
                            <th>出来事</th>
                        </tr>
                        @foreach ($Timelines as $Timeline)
                            <tr class="priority_{{$Timeline->priority}} timeline_list">
                                <td class="back_cloud_blue">{{ $Timeline->year }}</td>
                                <td>
                                    @if ($Timeline->month == '0')

                                    @else
                                        {{ $Timeline->month }}
                                    @endif
                                </td>
                                <td>
                                    @if ($Timeline->day == '0')

                                    @else
                                        {{ $Timeline->day }}
                                    @endif
                                </td>
                                <td class="ta-l">{!! nl2br(e($Timeline->body)) !!}</td>
                            </tr>                
                        @endforeach
                        <tr>
                            <th>年</th>
                            <th>月</th>
                            <th>日</th>
                            <th>出来事</th>
                        </tr>
                    </table>
                @include('components.breadcrumbs', ['slug' => 'other.timeline'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection