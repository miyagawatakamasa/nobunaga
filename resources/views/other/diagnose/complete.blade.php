@extends('layouts.layout')

@section('title', '武将診断結果 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="武将診断結果。あなたは「{{ $DiagnoseSamurai->samurai->name }}」タイプです。">
    <meta property="og:description" content="武将診断結果。あなたは「{{ $DiagnoseSamurai->samurai->name }}」タイプ" />
    <meta property="og:title" content="武将診断結果 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/diagnose/main.png') }}">
    <meta property="og:type" content="website" />
    <meta property="og:url" contetnt="{{ url()->current() }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/diagnose.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将診断結果 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/diagnose.png')}}" alt="診断アイコン" width="30px" height="30px" class="mr-8">
                            <p>武将診断結果</p>
                        </div>
                    </h1>
                    <div class="article_body">
                        <h2>あなたは「{{ $DiagnoseSamurai->samurai->name }}」タイプ</h2>
                        <div class="d-f sp_block">
                            <div class="mr-8">
                                <picture>
                                    <source type="image/webp" srcset="{{ Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $DiagnoseSamurai->samurai->id . '.webp') }}">
                                    <img src="{{ Storage::disk('s3')->url('img/shinsei/samurai/' . $DiagnoseSamurai->samurai->id . '.jpg') }}" alt="{{ $DiagnoseSamurai->samurai->name }}のグラフィック画像1" width="200px" height="auto" class="m-a samurai_img">
                                </picture>
                            </div>
                            <div class="retuden">
                                <p>{!! $DiagnoseSamurai->samurai->retuden_html !!}</p>
                            </div>
                        </div>
                        <p>{!! nl2br(e($DiagnoseSamurai->description)) !!}</p>

                        @if ($isPsychopath)
                            <p>綿密な計画で人の気持ちを操るあなたは<span class="color_red">サイコパス</span>です。</p>
                            <p>今すぐ結果をシェアして、あなたがサイコパスであることを共有し、友人に認識してもらいましょう。</p>
                        @else
                            <p>安心してください。あなたはサイコパスではありません。</p>
                        @endif

                        {{-- <h3>特徴</h3>
                        <ul>
                            <li>aaaaa</li>
                            <li>aaaaa</li>
                        </ul>
                        <h3>向いている仕事</h3>
                        <ul>
                            <li>aaaaa</li>
                            <li>aaaaa</li>
                        </ul>
                        <div class="retuden">
                            <p>ああああ</p>
                        </div> --}}

                        <div class="mt-24 d-f">
                            <a class="btn_tw btn mr-8" href="https://twitter.com/intent/tweet?url={{ route('other.diagnose.complete', $DiagnoseSamurai->id) }}&amp;text=あなたは「{{ $DiagnoseSamurai->samurai->name }}」タイプです。武将診断テスト&amp;hashtags=戦国,武将,診断テスト,信長の野望" target="_blank_" rel="nofollow"><i class="fab fa-twitter fa-fw"></i>結果をシェアする</a>
                            <a class="btn_fb btn" href="http://www.facebook.com/share.php?u={{ route('other.diagnose.complete', $DiagnoseSamurai->id) }}" target="_blank_" rel="nofollow"><i class="fab fa-facebook fa-fw"></i>結果をシェアする</a>
                        </div>
                        <h2>みんなの診断結果</h2>
                        <table class="font_small ta-c w-100">
                            <tr>
                                <th>武将名</th>
                                <th>診断数</th>
                                <th>割合</th>
                            </tr>
                            @foreach ($DiagnoseSamurais as $index => $DiagnoseSamurai)
                                <tr>
                                    <td class="back_cloud_blue"><a href="{{ route('other.diagnose.complete', $DiagnoseSamurai->id) }}">{{ $DiagnoseSamurai->samurai->name }}</a></td>
                                    <td>{{ $countData[$DiagnoseSamurai->id] }} 人</td>
                                    <td>{{ round($countData[$DiagnoseSamurai->id] / count($DiagnoseSamuraiUsers) * 100, 1) }} %</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>武将名</th>
                                <th>診断数</th>
                                <th>割合</th>
                            </tr>
                        </table>
                        <h3>裏設定</h3>
                        <p>サイコパスの人数 : {{ $psychopathDiagnoseSamuraiUsersCount }} / {{ count($DiagnoseSamuraiUsers) }} 人</p>
                    </div>
                    <div class="m-a mt-24">
                        <a href="{{ route('other.diagnose.index') }}" class="diagnose_button mt-48" title="もう一度診断を始める">もう一度診断を始める</a>
                    </div>
                    <div class="mt-48 d-f">
                        <a class="btn_tw btn mr-8" href="https://twitter.com/intent/tweet?url={{ route('other.diagnose.index') }}&amp;text=あなたは「{{ $DiagnoseSamurai->samurai->name }}」タイプです。武将診断テスト&amp;hashtags=戦国,武将,診断テスト,信長の野望" target="_blank_" rel="nofollow"><i class="fab fa-twitter fa-fw"></i>結果をシェアする</a>
                        <a class="btn_fb btn" href="http://www.facebook.com/share.php?u={{ route('other.diagnose.index') }}" target="_blank_" rel="nofollow"><i class="fab fa-facebook fa-fw"></i>結果をシェアする</a>
                    </div>

                    <div class="mt-48">
                        {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                             crossorigin="anonymous"></script> --}}
                        <ins class="adsbygoogle"
                             style="display:block; text-align:center;"
                             data-ad-layout="in-article"
                             data-ad-format="fluid"
                             data-ad-client="ca-pub-2958289145034418"
                             data-ad-slot="4794895012"></ins>
                        <script>
                             (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'other.diagnose.complete', 'argument1' => $DiagnoseSamurai])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection

@section('footer_js')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v14.0" nonce="SHn2PKKo"></script>
@endsection