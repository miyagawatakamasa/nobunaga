@extends('layouts.layout')

@section('title', '武将診断テスト | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="武将診断メーカーのテストページです。全16問約2分で終わります。">
    <meta property="og:description" content="武将診断メーカーのテストページです。全16問約2分で終わります。" />
    <meta property="og:title" content="武将診断テスト | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/diagnose/main.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/diagnose.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将診断テスト | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            <div class="wide_contents">
                <div class="article_contents p-12">
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/diagnose.png')}}" alt="診断アイコン" width="30px" height="30px" class="mr-8">
                            <p>診断スタート</p>
                        </div>
                    </h1>
                    <div class="article_body">
                        <div class="retuden">
                            <p>以下の質問に対してあなたが直感的に近いと思うものを選んでください。</p>
                            <p>全16問 2分</p>
                        </div>
                        <form id="diagnose-form" method="POST" action="">
                            @csrf
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q1. どんな困難があろうとも叶えたい夢がある</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q1" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q1" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q1" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q1" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q1" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q2. 人の気持ちを操ることは楽しい</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q2" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q2" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q2" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q2" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q2" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-24">
                                <div>
                                    <label for="name">Q3. この世は弱肉強食だから負けた人間は淘汰されるべきだ</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q3" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q3" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q3" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q3" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q3" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-24">
                                <div>
                                    <label for="name">Q4. 賑やかなことが大好き</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q4" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q4" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q4" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q4" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q4" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-24">
                                <div>
                                    <label for="name">Q5. 曲がったことは正す必要がある</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q5" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q5" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q5" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q5" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q5" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q6. 気配りが得意</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q6" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q6" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q6" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q6" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q6" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q7. すぐカッとなる</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q7" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q7" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q7" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q7" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q7" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q8. 体を動かすより頭を使う方が得意だ</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q8" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q8" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q8" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q8" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q8" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q9. 本能で動くタイプだ</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q9" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q9" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q9" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q9" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q9" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q10. 争い事が好きだ</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q10" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q10" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q10" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q10" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q10" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q11. 仕事は計画的に実行する</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q11" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q11" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q11" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q11" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q11" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q12. 人生は成功するために生きている</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q12" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q12" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q12" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q12" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q12" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q13. 事務作業が得意</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q13" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q13" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q13" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q13" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q13" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q14. リーダーシップを発揮できる</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q14" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q14" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q14" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q14" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q14" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q15. 新しいもの好き</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q15" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q15" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q15" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q15" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q15" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-24">
                                <div>
                                    <label for="name">Q16. 人を裏切ってでも成功したい</label>
                                </div>
                                <table class="w-100 diagnose_table mt-24 ta-c">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q16" value="1" required>
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q16" value="2">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q16" value="3">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q16" value="4">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                             </td>
                                            <td>
                                                <label>
                                                    <input type="radio" name="q16" value="5">
                                                    <span class="outside"><span class="inside"></span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>思う</td>
                                            <td colspan="3">
                                                <img src="{{ asset('img/common/double_arrow.svg')}}" alt="診断アイコン" width="100%" height="100%">
                                            </td>
                                            <td>思わない</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="mt-24">
                                <button type="submit" class="m-a submit_button">送信する</button>
                            </div>
                        </form>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'other.game_title'])
                </div>
            </div>
        </div>
    </div>
@endsection