@extends('layouts.layout')

@section('title', '武将診断テスト | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="あなたの性格を12人の戦国武将を元に診断します。全16問（約2分）なので簡単に診断できます。サイコパスかどうかの裏診断もあるので是非結果をSNSでシェアしましょう。">
    <meta property="og:description" content="あなたの性格を12人の戦国武将を元に診断します。全16問（約2分）なので簡単に診断できます。サイコパスかどうかの裏診断もあるので是非結果をSNSでシェアしましょう。" />
    <meta property="og:title" content="武将診断テスト | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/diagnose/main.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/diagnose.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "武将診断テスト | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/diagnose.png')}}" alt="診断アイコン" width="30px" height="30px" class="mr-8">
                            <p>武将診断テスト</p>
                        </div>
                    </h1>
                    <p class="mt-24">全16問（約2分）</p>
                    <div class="mt-12">
                        <img src="{{ asset('img/diagnose/main.png') }}" alt="武将診断テスト" width="100%" height="100%">
                    </div>
                    <p class="mt-24"><span class="fw-b yellow_marker">あなたの性格</span>を<span class="fw-b">{{ count($DiagnoseSamurais) }}人</span>の戦国武将を元に診断します。</p>
                    <p class="mt-12">結果をツイッターなどのSNSでシェアしてみよう！</p>
                    <p>診断結果後、<span class="yellow_marker">他の人の診断分布</span>を見れます。</p>
                    <p class="font_small">※<span class="color_red">サイコパス</span>の人専用メッセージもあるよ。</p>

                    <div class="m-a mt-48">
                        <a href="{{ route('other.diagnose.new') }}" class="diagnose_button mt-48" title="診断を始める">診断を始める</a>
                    </div>

                    <div class="m-a mt-48 article_body">
                        <h2>みんなの診断結果</h2>
                        <table class="font_small ta-c w-100">
                            <tr>
                                <th>武将名</th>
                                <th>診断数</th>
                                <th>割合</th>
                            </tr>
                            @foreach ($DiagnoseSamurais as $index => $DiagnoseSamurai)
                                <tr>
                                    <td class="back_cloud_blue"><a href="{{ route('other.diagnose.complete', $DiagnoseSamurai->id) }}">{{ $DiagnoseSamurai->samurai->name }}</a></td>
                                    <td>{{ $countData[$DiagnoseSamurai->id] }} 人</td>
                                    <td>{{ round($countData[$DiagnoseSamurai->id] / count($DiagnoseSamuraiUsers) * 100, 1) }} %</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>武将名</th>
                                <th>診断数</th>
                                <th>割合</th>
                            </tr>
                        </table>
                        <h3>裏設定</h3>
                        <p>サイコパスの人数 : {{ $psychopathDiagnoseSamuraiUsersCount }} / {{ count($DiagnoseSamuraiUsers) }} 人</p>
                    </div>
                    <div class="m-a mt-48">
                        <a href="{{ route('other.diagnose.new') }}" class="diagnose_button mt-48" title="診断を始める">診断を始める</a>
                    </div>
                    <div class="mt-24 d-f">
                        <a class="btn_tw btn mr-8" href="https://twitter.com/intent/tweet?url={{ route('other.diagnose.index') }}&amp;text=武将診断テスト&amp;hashtags=戦国,武将,診断テスト,信長の野望" target="_blank_" rel="nofollow"><i class="fab fa-twitter fa-fw"></i>シェア</a>
                        <a class="btn_fb btn" href="http://www.facebook.com/share.php?u={{ route('other.diagnose.index') }}" target="_blank_" rel="nofollow"><i class="fab fa-facebook fa-fw"></i>シェア</a>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'other.diagnose.index'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection