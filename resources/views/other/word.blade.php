@extends('layouts.layout')

@section('title', '戦国時代用語集 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="戦国時代に出てくる用語集です。随時更新中です。">
    <meta property="og:description" content="戦国時代に出てくる用語集です。随時更新中です。" />
    <meta property="og:title" content="戦国時代用語集 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "戦国時代用語集 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <div class="article_contents">
                    <div class="d-f sp_block j-sb">
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年10月31日</span>
                            {{-- <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年10月27日</span> --}}
                        </time>
                    </div>
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            <img src="{{ asset('img/common/diagnose.png')}}" alt="用語集アイコン" width="30px" height="30px" class="mr-8">
                            <p>戦国時代用語集</p>
                        </div>
                    </h1>
                    <p class="mt-24">戦国時代に出てくる用語集です。<br>※随時更新中</p>
                    <table class="font_small ta-c mt-24 w-100">
                        <tr>
                            <th style="width: 60px;">用語</th>
                            <th style="width: 80px;">読み</th>
                            <th>説明</th>
                        </tr>
                        @foreach ($Words as $Word)
                            <tr>
                                <td class="back_cloud_blue">{{ $Word->name }}</td>
                                <td>{{ $Word->name_kana }}</td>
                                <td class="ta-l">{!! $Word->body !!}</td>
                            </tr>                
                        @endforeach
                        <tr>
                            <th>用語</th>
                            <th>読み</th>
                            <th>説明</th>
                        </tr>
                    </table>
                @include('components.breadcrumbs', ['slug' => 'other.word'])
                </div>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection