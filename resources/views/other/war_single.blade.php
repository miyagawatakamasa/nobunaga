@extends('layouts.layout')

@section('title', $Disturbance->name . ' | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="{{ $Disturbance->body }}">
    <meta property="og:description" content="{{ $Disturbance->body }}" />
    <meta property="og:title" content="{{ $Disturbance->name }} | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ Storage::disk('s3')->url('img/disturbances/' . $Disturbance->id . '.png') }}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/war.css') }}">
    <link rel="stylesheet" href="{{ asset('css/samurai.css') }}">
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "{{ $Disturbance->name }} | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    {{-- <div class="mt-36">
                        <img src="{{ asset('img/sozai/war.jpg')}}" alt="{{ $Disturbance->name }}" width="100%" height="100%">
                    </div> --}}
                    {{-- <div class="d-f sp_block j-sb">
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年04月04日</span>
                            <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年11月10日</span>
                        </time>
                    </div> --}}
                    <h1 class="mt-12">
                        <div class="d-f ai-c">
                            {{-- <img src="{{ asset('img/common/timeline.png')}}" alt="戦国年表アイコン" width="30px" height="30px" class="mr-8"> --}}
                            <p>{{ $Disturbance->name }}</p>
                        </div>
                    </h1>
                    <div class="article_body">
                        <p>{{ $Disturbance->start_year }}年@if ($Disturbance->start_month){{ $Disturbance->start_month }}月@endif</p>
                        <p>{!! $Disturbance->body_html !!}</p>
                        <div class="table_of_contents_area mt-24">
                            <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                            <div id="table-of-content" class="mt-12 active toc_contents"></div>
                        </div>
                        @foreach ($Disturbance->wars as $war)
                            <h2>{{ $war->name }}（{{ $war->kana }}）</h2>
                            @include('components.war', ['war' => $war])
                        @endforeach
                        <p class="mt-36"><a href="{{ route('other.war') }}">戦国時代の合戦一覧に戻る</a></p>
                    </div>
                    <div class="mt-48">
                        {{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2958289145034418"
                            crossorigin="anonymous"></script> --}}
                        <!-- 合戦登場武将上 -->
                        <ins class="adsbygoogle"
                            style="display:block"
                            data-ad-client="ca-pub-2958289145034418"
                            data-ad-slot="6137753113"
                            data-ad-format="auto"
                            data-full-width-responsive="true"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <section class="mt-48">
                        <h2 class="rich_h2">この合戦に登場する武将</h2>
                        <ul class="samurai_ul mt-12">
                            @foreach ($UniqueWarSamurais as $UniqueWarSamurai)
                                @if ($UniqueWarSamurai->samurai?->name)
                                    <li class="samurai_li active">
                                        <a href="{{ route('database.samurais.redirect_show', [$UniqueWarSamurai->samurai->id]) }}" title="{{ $UniqueWarSamurai->samurai->name }}" class="samurai_anchor">
                                            <div class="img_area">
                                                <img src="{{ My_func::return_min_samurai_img_url($UniqueWarSamurai->samurai->id, $GameTitle->slug) }}" alt="{{ $UniqueWarSamurai->samurai->name }}" width="80px" height="80px" class="samurai_img">
                                            </div>
                                            <div class="text_area">
                                                <p class="hover_white">{{ $UniqueWarSamurai->samurai->name }} <span class="font_small hover_white">（{{ $UniqueWarSamurai->samurai->furigana }}）</span></p>
                                                <p class="mt-8 font_small hover_white">{!! $UniqueWarSamurai->samurai->retuden !!}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </section>
                    @include('components.breadcrumbs', ['slug' => 'other.war.single', 'argument1' => $Disturbance])
                </article>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection