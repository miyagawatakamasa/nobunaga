@extends('layouts.layout')

@section('title', '合戦一覧 | 信長の野望 徹底攻略')

@section('meta')
	<meta name="description" content="戦国時代に絞った合戦一覧です。随時更新中です。">
    <meta property="og:description" content="戦国時代に絞った合戦一覧です。随時更新中です。" />
    <meta property="og:title" content="合戦一覧 | 信長の野望 徹底攻略">
    <meta property="og:image" content="{{ asset('img/top/nobu.png') }}">
@endsection

@section('css')
@endsection

@section('js')
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "合戦一覧 | 信長の野望 徹底攻略",
            "url": "{{ url()->current() }}"
        }
    </script>
    <script type="text/javascript" src="{{ asset('js/table_of_contents.js') }}"></script>
@endsection

@section('content')
    <div class="inner inner_wrapper">
        <div class="single_contents">
            @include('components.game_aside')
            <div class="left_contents">
                <article class="article_contents">
                    <div class="d-f sp_block j-sb">
                        <time>
                            <i class="far fa-calendar-check"></i><span class="calender">2022年04月04日</span>
                            <i class="fas fa-sync-alt reflesh_icon"></i><span class="calender">2022年11月11日</span>
                        </time>
                    </div>
                    <h1 class="mt-36">
                        <div class="d-f ai-c">
                            {{-- <img src="{{ asset('img/common/timeline.png')}}" alt="戦国年表アイコン" width="30px" height="30px" class="mr-8"> --}}
                            <p>戦国時代の合戦一覧</p>
                        </div>
                    </h1>
                    <div class="mt-12">
                        <img src="{{ asset('img/sozai/war.jpg')}}" alt="戦国時代の合戦一覧" width="100%" height="100%">
                    </div>
                    <div class="table_of_contents_area mt-24">
                        <p class="ta-c table_of_contents_title">目次<span id="toc-toggle" class="font_small ml-24">[<a href="" id="toc-toggle-text">非表示</a>]</span></p>
                        <div id="table-of-content" class="mt-12 active toc_contents"></div>
                    </div>
                    <div class="article_body">
                        @foreach ($Disturbances as $Disturbance)
                            <h2>{{ $Disturbance->name }}</h2>
                            <p>{{ $Disturbance->start_year }}年{{ $Disturbance->start_month }}月</p>
                            <p>{!! $Disturbance->body_html !!}</p>
                            <p class="rich_h3">具体的な合戦</p>
                            <table class="font_small mt-24 w-100 ta-c">
                                <tr>
                                    <th style="width: 150px;">開戦日</th>
                                    <th>戦名</th>
                                </tr>
                                @foreach ($Disturbance->wars as $War)
                                <tr>
                                    <td>
                                        @if ($War->start_year){{ $War->start_year . '年' }}@endif
                                        @if ($War->start_month){{ $War->start_month . '月' }}@endif
                                        @if ($War->start_day){{ $War->start_day . '日' }}@endif
                                    </td>
                                    <td>{{ $War->name }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <th>開戦日</th>
                                    <th>戦名</th>
                                </tr>
                            </table>
                            <p class="ta-r mt-8"><a href="{{ route('other.war.single', $Disturbance->id) }}">>> 詳細を見る</a></p>
                        @endforeach
                    <p>参照</p>
                    <p>戦国合戦辞典 存亡を賭けた戦国864の戦い</p>
                    </div>
                    @include('components.breadcrumbs', ['slug' => 'other.war'])
                </article>
            </div>
            @include('components.aside')
        </div>
    </div>
@endsection