$(function(){
	let column_no = 0;
	let column_no_prev = 0;
	let first_access = 1;

	window.addEventListener('load', function () {
		// $('.sinsei_leadership').trigger("click");
		// document.getElementById("sinsei-leadership").click();
		$('.sort_th').on('click',function(){
			// document.getElementById("sinsei-brave").click();
			$('.sort_th').removeClass('asc');
			$('.sort_th').removeClass('desc');

			column_no = this.cellIndex;
			let table = this.parentNode.parentNode.parentNode;
			let sortType = 0;
			let sortArray = new Array;
			for (let r = 1; r < table.rows.length; r++) {
				let column = new Object;
				column.row = table.rows[r];
				column.value = table.rows[r].cells[column_no].textContent;
				sortArray.push(column);
				if (isNaN(Number(column.value))) {
					sortType = 1;
				}
			}
			this.classList.remove("asc");
			this.classList.remove("desc");
			if (sortType == 0) {
				// 数値のソート
				if (column_no_prev == column_no) {
					sortArray.sort(compareNumber);
					this.classList.add("desc");
				} else {
					sortArray.sort(compareNumberDesc);
					this.classList.add("asc");
				}
			} else {
				// 文字列のソート
				if (column_no_prev == column_no) {
					sortArray.sort(compareString);
					this.classList.add("desc");
				} else {
					sortArray.sort(compareStringDesc);
					this.classList.add("asc");
				}
			}
			let tbody = this.parentNode.parentNode;
			for (let i = 0; i < sortArray.length; i++) {
				tbody.appendChild(sortArray[i].row);
			}
			if (column_no_prev == column_no) {
				column_no_prev = -1;
			} else {
				column_no_prev = column_no;
			}
			if (!first_access) {
				$('.samurai_tr').removeClass('active');
			}
			first_access = 0;
		});

		let samurai_id = getParam('samurai-id');
		let sort = getParam('sort');
		$('.samurai_id_' + samurai_id).addClass('active');

		$('#' + sort).trigger("click");

		const position = $('.samurai_id_' + samurai_id).offset().top - 50;

		const speed = 600;
		if (samurai_id) {
			$("html,body").animate({scrollTop:position},speed);
		}
	});
	function compareNumber(a, b)
	{
		return a.value - b.value;
	}
	function compareNumberDesc(a, b)
	{
		return b.value - a.value;
	}
	function compareString(a, b) {
		if (a.value < b.value) {
			return -1;
		} else {
			return 1;
		}
	}
	function compareStringDesc(a, b) {
		if (a.value > b.value) {
			return -1;
		} else {
			return 1;
		}
	}
	function getParam(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
});
