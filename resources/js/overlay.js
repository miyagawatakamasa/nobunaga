$(function(){
    $('.old_country_name_area').on('click',function(){
        $('.overlay_content').fadeIn();
        $('.overlay_back').fadeIn();
        $('.old_country_name_list_area').fadeIn();
        $('.old_country_name_list_area').toggleClass('sp_flex');
        $(window).resize();
        return false;
    });

    $('#close').on('click',function(){
        $('.overlay_content').fadeOut();
        $('.overlay_back').fadeOut();
        $('.old_country_name_list_area').fadeOut();
        $('.old_country_name_list_area').toggleClass('sp_flex');
        $(window).resize();
    });
    $('.overlay_back').on('click',function(){
        $('.overlay_content').fadeOut();
        $('.overlay_back').fadeOut();
        $('.old_country_name_list_area').fadeOut();
        $('.old_country_name_list_area').toggleClass('sp_flex');
        $(window).resize();
    });

    // $(document).on({
    //     'mouseenter' : function() {
    //         $("#old-country-color").attr('src', 'http://nobunaga.localhost.com/img/common/color_map/color_map_8.png');
    //         $(".old_country_tr_8").fadeIn('fast');
    //         $(".old_country_anchor_8").toggleClass('hover');
    //     },
    //     'mouseleave' : function(){
    //         $("#old-country-color").attr('src', $oldCountry);
    //         $(".old_country_tr_8").fadeOut('fast');
    //         $(".old_country_anchor_8").toggleClass('hover');
    //     }
    // }, '.old_country_8');

    $('.page_top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    // サイドバー 都道府県
    $('.prefectures').click(function () {
        $(this).children('.category_arrow').toggleClass('active');
        $('.prefecture').toggleClass('active');
        return false;
    });
});