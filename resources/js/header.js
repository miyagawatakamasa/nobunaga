$(function(){
    $(window).on('load scroll', function() {
        var value = $(this).scrollTop();
        if ( value > 120 ) {
            $('#sp-slide-header').addClass('active');
        } else {
            $('#sp-slide-header').removeClass('active');
            $('#sp-humberger-overlay').removeClass('active');
            $('.openbtn').removeClass('active');
        }
    });

    $('.openbtn').on('click', function() {
        $(this).toggleClass('active');
        $('#humberger-nav').toggleClass('active');
        $('#sp-humberger-overlay').toggleClass('active');
    });

    $('#sp-humberger-overlay').on('click',function(){
        $('#sp-humberger-overlay').toggleClass('active');
        $('.openbtn').toggleClass('active');
    });
});