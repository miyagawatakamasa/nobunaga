document.addEventListener('DOMContentLoaded', () => {
    const heads = document.querySelectorAll('div.article_body > h2, h3, h4, h5, h6');
    const count = heads.length;
    let loopCount = 0;
    let before_head = '';
    let h2_count = 0;
    let h3_count = 0;
    let h4_count = 0;
    let before_head_number = 0;

    if (heads && heads.length) {
        let contents = '<ul class="table_of_contents_ol">';
        heads.forEach((head, i) => {
            loopCount ++;
            const thisTag = head.tagName;
            const thisSliceNumber = Number(thisTag.slice(1));

            if (before_head == thisTag) {
                contents += `</li>`;
            } else {
                if (thisSliceNumber >= before_head_number) {
                } else if (thisSliceNumber == 2 && before_head_number == 2) {
                    contents += `</ol>q`;
                    h3_count = 0;
                    h4_count = 0;
                } else if (before_head_number == 3) {
                    contents += `</ol></ol>`;
                    h3_count = 0;
                    h4_count = 0;
                } else if (before_head_number == 4) {
                    contents += `</ol></ol></ol>`;
                    h3_count = 0;
                    h4_count = 0;
                } 
            }
            if (thisTag == "H2") {
                h2_count++;
                contents += `<li class="table_of_contents_list"><a href="#head${i}">${h2_count}. ${head.textContent}</a>`;
                before_head_number = 2;
            } else if (thisTag == "H3") {
                h3_count++;
                if (before_head == thisTag) {
                    contents += `<li class="table_of_contents_list"><a href="#head${i}">${h2_count}-${h3_count}. ${head.textContent}</a>`;
                } else {
                    contents += `<ol class="table_of_contents_ol"><li class="table_of_contents_list"><a href="#head${i}">${h2_count}-${h3_count}. ${head.textContent}</a></li>`;
                }
                before_head_number = 3;
            } else if (thisTag == "H4") {
                h4_count++;
                if (before_head == thisTag) {
                    contents += `<li class="table_of_contents_list"><a href="#head${i}">${h2_count}-${h3_count}-${h4_count}. ${head.textContent}</a>`;
                } else {
                    contents += `<ol class="table_of_contents_ol"><li class="table_of_contents_list"><a href="#head${i}">${h2_count}-${h3_count}-${h4_count}. ${head.textContent}</a></li>`;
                }
                before_head_number = 4;
            }
            before_head = thisTag;
            head.id = `head${i}`;
            if (count == loopCount) {
                contents += `</li></ul>`;
            }
        })
        document.querySelector('#table-of-content').innerHTML += `${contents}`;

    }
});
$(function(){
    $('#toc-toggle').on('click',function(){
        $('#table-of-content').toggleClass('active');

        if ($('#table-of-content').hasClass('active')) {
            $('#toc-toggle-text').text('非表示');
        } else {
            $('#toc-toggle-text').text('表示');
        }
        return false;
    });
});
