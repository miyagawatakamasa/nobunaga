$(function(){
	var boinGroup = 'あ';
	$('.samurai_li').each(function(i, elem) {
		if ($(elem).data('kana') == 'が') {
			$(elem).data('kana', 'か');
		} else if ($(elem).data('kana') == 'ぐ') {
			$(elem).data('kana', 'ぐ');
		} else if ($(elem).data('kana') == 'ぎ') {
			$(elem).data('kana', 'き');
		} else if ($(elem).data('kana') == 'ご') {
			$(elem).data('kana', 'こ');
		} else if ($(elem).data('kana') == 'じ') {
			$(elem).data('kana', 'し');
		} else if ($(elem).data('kana') == 'ぞ') {
			$(elem).data('kana', 'そ');
		} else if ($(elem).data('kana') == 'だ') {
			$(elem).data('kana', 'た');
		} else if ($(elem).data('kana') == 'ど') {
			$(elem).data('kana', 'と');
		} else if ($(elem).data('kana') == 'ば') {
			$(elem).data('kana', 'は');
		} else if ($(elem).data('kana') == 'べ') {
			$(elem).data('kana', 'へ');
		}
	});

    $('.kana_anchor').on('click',function(){
			$('.kana_anchor').removeClass('active');
			$(this).addClass('active');

			boinGroup = $(this).text();

			$('.shiin_li').removeClass('main');
			$('.shiin_li').children('.shiin_anchor').removeClass('active');

			$('.shiin_li').each(function(i, elem) {
				if ($(elem).data('boin-group') == boinGroup) {
					$(elem).addClass('main');
				}
				if ($(elem).data('shiin') == boinGroup) {
					$(elem).children('.shiin_anchor').addClass('active');
				}
			});

			$('.samurai_li').removeClass('active');
			$('.samurai_li').each(function(i, elem) {
				if ($(elem).data('kana') == boinGroup) {
					$(elem).addClass('active');
				}
			});

			const position = $('#title').offset().top;
			const speed = 500;
			$("html, body").animate({scrollTop:position}, speed, "swing");

        return false;
    });

	$('.shiin_anchor').on('click',function(){
		$('.shiin_anchor').removeClass('active');
		$(this).addClass('active');
		$('.samurai_li').removeClass('active');

		boinGroup = $(this).text();

		$('.samurai_li').each(function(i, elem) {
			if ($(elem).data('kana') == boinGroup) {
				$(elem).addClass('active');
			}
		});

		const position = $('#title').offset().top;
		const speed = 500;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});