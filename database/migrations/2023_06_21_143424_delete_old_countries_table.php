<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('old_countries');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('old_countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('region');
            $table->string('region_slug');
            $table->string('name');
            $table->string('kana');
            $table->string('old_kana');
            $table->integer('harvest');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
