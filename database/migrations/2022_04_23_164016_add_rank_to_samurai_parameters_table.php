<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('samurai_parameters', function (Blueprint $table) {
            $table->integer('leadership_rank')->nullable()->after('leadership');
            $table->integer('brave_rank')->nullable()->after('brave');
            $table->integer('wisdom_rank')->nullable()->after('wisdom');
            $table->integer('domestic_affairs_rank')->nullable()->after('domestic_affairs');
            $table->integer('foreign_affairs_rank')->nullable()->after('foreign_affairs');
            $table->integer('all_rank')->nullable()->after('all');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('samurai_parameters', function (Blueprint $table) {
            $table->dropColumn('leadership_rank');
            $table->dropColumn('brave_rank');
            $table->dropColumn('wisdom_rank');
            $table->dropColumn('domestic_affairs_rank');
            $table->dropColumn('all_rank');
        });
    }
};
