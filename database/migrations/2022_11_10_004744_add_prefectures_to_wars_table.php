<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('wars');

        Schema::create('wars', function (Blueprint $table) {
            $table->id();
            $table->integer('disturbance_id');
            $table->integer('start_year')->default(null)->nullable();
            $table->integer('start_month')->default(null)->nullable();
            $table->integer('start_day')->default(null)->nullable();
            $table->integer('end_year')->default(null)->nullable();
            $table->integer('end_month')->default(null)->nullable();
            $table->integer('end_day')->default(null)->nullable();
            $table->string('prefectures')->default(null)->nullable();
            $table->string('city');
            $table->string('town');
            $table->string('name');
            $table->string('kana');
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars');

        Schema::create('wars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('start_year')->default(null)->nullable();
            $table->string('start_month')->default(null)->nullable();
            $table->string('end_year')->default(null)->nullable();
            $table->string('end_month')->default(null)->nullable();
            $table->string('place')->nullable();
            $table->string('name');
            $table->string('kana');
            $table->timestamps();
            $table->softDeletes();
        });
    }
};
