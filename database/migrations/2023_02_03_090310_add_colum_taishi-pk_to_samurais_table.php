<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('samurais', function (Blueprint $table) {
            $table->boolean('taishi-pk')->default(0)->after('taishi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('samurais', function (Blueprint $table) {
            $table->dropColumn('taishi-pk');
        });
    }
};
