<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('samurais', function (Blueprint $table) {
            $table->string('father')->nullable()->after('retuden_html');
            $table->string('adoptive_father')->nullable()->after('father');
            $table->string('mother')->nullable()->after('adoptive_father');
            $table->string('adoptive_mother')->nullable()->after('mother');
            $table->string('spouse')->nullable()->after('adoptive_mother');
            $table->string('concubine_1')->nullable()->after('spouse');
            $table->string('concubine_2')->nullable()->after('concubine_1');
            $table->string('concubine_3')->nullable()->after('concubine_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('samurais', function (Blueprint $table) {
            $table->dropColumn('father');
            $table->dropColumn('adoptive_father');
            $table->dropColumn('mother');
            $table->dropColumn('adoptive_mother');
            $table->dropColumn('spouse');
            $table->dropColumn('concubine_1');
            $table->dropColumn('concubine_2');
            $table->dropColumn('concubine_3');
        });
    }
};
