<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shinsei_pk_samurai_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->string('name');
            $table->string('furigana');
            $table->integer('leadership');
            $table->integer('leadership_rank');
            $table->integer('brave');
            $table->integer('brave_rank');
            $table->integer('wisdom');
            $table->integer('wisdom_rank');
            $table->integer('affairs');
            $table->integer('affairs_rank');
            $table->integer('all');
            $table->integer('all_rank');
            $table->string('policy');
            $table->string('tactics');
            $table->string('characteristic1');
            $table->string('characteristic2')->nullable();
            $table->string('characteristic3')->nullable();
            $table->string('head_of_house');
            $table->string('magistrate');
            $table->string('nobugen_age')->nullable();
            $table->string('owarito_age')->nullable();
            $table->string('oke_age')->nullable();
            $table->string('tenka_age')->nullable();
            $table->string('houi_age')->nullable();
            $table->string('mikata_age')->nullable();
            $table->string('yume_age')->nullable();
            $table->string('tenkabuzi_age')->nullable();
            $table->string('seki_age')->nullable();
            $table->string('osaka_age')->nullable();
            $table->string('nobugen_shiro')->nullable();
            $table->string('owarito_shiro')->nullable();
            $table->string('oke_shiro')->nullable();
            $table->string('tenka_shiro')->nullable();
            $table->string('houi_shiro')->nullable();
            $table->string('mikata_shiro')->nullable();
            $table->string('yume_shiro')->nullable();
            $table->string('tenkabuzi_shiro')->nullable();
            $table->string('seki_shiro')->nullable();
            $table->string('osaka_shiro')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shinsei_pk_samurai_parameters');
    }
};
