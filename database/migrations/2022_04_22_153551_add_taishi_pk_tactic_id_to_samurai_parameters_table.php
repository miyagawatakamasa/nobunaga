<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('samurai_parameters', function (Blueprint $table) {
            $table->integer('all')->nullable()->after('foreign_affairs');
            $table->integer('taishi_pk_will_id')->nullable()->after('all');
            $table->integer('taishi_pk_tactic_id')->nullable()->after('taishi_pk_will_id');
            $table->integer('taishi_pk_assistant')->nullable()->after('taishi_pk_tactic_id');
            $table->integer('taishi_pk_strategy_id')->nullable()->after('taishi_pk_assistant');
            $table->string('taishi_pk_temperament')->nullable()->after('taishi_pk_strategy_id');
            $table->integer('taishi_pk_ambition')->nullable()->after('taishi_pk_temperament');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('samurai_parameters', function (Blueprint $table) {
            $table->dropColumn('all');
            $table->dropColumn('taishi_pk_will_id');
            $table->dropColumn('taishi_pk_tactic_id');
            $table->dropColumn('taishi_pk_assistant');
            $table->dropColumn('taishi_pk_strategy_id');
            $table->dropColumn('taishi_pk_temperament');
            $table->dropColumn('taishi_pk_ambition');
        });
    }
};
