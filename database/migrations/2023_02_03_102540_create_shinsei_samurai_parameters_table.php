<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shinsei_samurai_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->integer('leadership');
            $table->integer('leadership_rank');
            $table->integer('brave');
            $table->integer('brave_rank');
            $table->integer('wisdom');
            $table->integer('wisdom_rank');
            $table->integer('affairs');
            $table->integer('affairs_rank');
            $table->integer('all');
            $table->integer('all_rank');
            $table->integer('shinsei_doctrine_id');
            $table->integer('shinsei_tactic_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shinsei_samurai_parameters');
    }
};
