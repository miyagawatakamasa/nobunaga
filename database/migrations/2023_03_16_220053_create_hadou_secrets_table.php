<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hadou_secrets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_kana');
            $table->string('system');
            $table->string('conditions');
            $table->string('effect1');
            $table->string('effect2')->default(null)->nullable();
            $table->string('effect3')->default(null)->nullable();
            $table->string('effect4')->default(null)->nullable();
            $table->string('effect5')->default(null)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadou_secrets');
    }
};
