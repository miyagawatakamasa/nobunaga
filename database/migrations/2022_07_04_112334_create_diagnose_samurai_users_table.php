<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnose_samurai_users', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->integer('leadership');
            $table->integer('brave');
            $table->integer('wisdom');
            $table->integer('affairs');
            $table->integer('betrayal');
            $table->integer('bright');
            $table->integer('intuition');
            $table->integer('precise');
            $table->integer('attentive');
            $table->integer('justice');
            $table->integer('revolution');
            $table->integer('dream');
            $table->integer('patience');
            $table->integer('advancement');
            $table->integer('psychopath');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnose_samurai_users');
    }
};
