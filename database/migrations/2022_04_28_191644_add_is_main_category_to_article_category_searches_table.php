<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_category_searches', function (Blueprint $table) {
            $table->boolean('is_main_category')->default(0)->after('article_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_category_searches', function (Blueprint $table) {
            $table->dropColumn('is_main_category');
        });
    }
};
