<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hadou_samurai_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->string('rarity');
            $table->integer('strength');
            $table->integer('talent');
            $table->integer('leadership');
            $table->integer('leadership_rank');
            $table->integer('brave');
            $table->integer('brave_rank');
            $table->integer('wisdom');
            $table->integer('wisdom_rank');
            $table->integer('affairs');
            $table->integer('affairs_rank');
            $table->integer('all');
            $table->integer('all_rank');
            $table->string('spear');
            $table->string('horsemanship');
            $table->string('bow');
            $table->string('tactics')->default(null)->nullable();
            $table->text('tactics_body')->default(null)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadou_samurai_parameters');
    }
};
