<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('war_samurais', function (Blueprint $table) {
            $table->id();
            $table->string('position');
            $table->string('w_or_l');
            $table->integer('war_id');
            $table->integer('samurai_id')->default(null)->nullable();
            $table->string('samurai_name')->default(null)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('war_samurais');
    }
};
