<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shinnobu_samurai_parameters', function (Blueprint $table) {
            $table->string('rank')->after('rarity');
            $table->text('comment')->after('anecdote');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shinnobu_samurai_parameters', function (Blueprint $table) {
            $table->dropColumn('rank');
            $table->dropColumn('comment');
        });
    }
};
