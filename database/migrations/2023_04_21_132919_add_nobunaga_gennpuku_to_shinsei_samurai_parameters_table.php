<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shinsei_samurai_parameters', function (Blueprint $table) {
            $table->string('nobunaga_gennpuku')->after('shinsei_tactic_id');
            $table->string('owari_touitsu')->after('nobunaga_gennpuku');
            $table->string('okehazama')->after('owari_touitsu');
            $table->string('houimou')->after('okehazama');
            $table->string('yumemaboroshi')->after('houimou');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shinsei_samurai_parameters', function (Blueprint $table) {
            $table->dropColumn('nobunaga_gennpuku');
            $table->dropColumn('owari_touitsu');
            $table->dropColumn('okehazama');
            $table->dropColumn('houimou');
            $table->dropColumn('yumemaboroshi');
        });
    }
};
