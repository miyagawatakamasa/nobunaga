<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSamuraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('samurais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('furigana');
            $table->integer('birth_year');
            $table->integer('death_year');
            $table->string('sex');
            $table->boolean('zennkoku')->default(0);
            $table->boolean('sgunnyuudenn')->default(0);
            $table->boolean('bhuuunnroku')->default(0);
            $table->boolean('haoudenn')->default(0);
            $table->boolean('tennsyouki')->default(0);
            $table->boolean('syouseiroku')->default(0);
            $table->boolean('reppuuden')->default(0);
            $table->boolean('ranseiki')->default(0);
            $table->boolean('soutennroku')->default(0);
            $table->boolean('tsousei')->default(0);
            $table->boolean('kakushinn')->default(0);
            $table->boolean('tenndou')->default(0);
            $table->boolean('souzou')->default(0);
            $table->boolean('taishi')->default(0);
            $table->boolean('shinsei')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('samurais');
    }
}
