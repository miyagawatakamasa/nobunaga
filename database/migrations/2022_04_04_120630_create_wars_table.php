<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('start_year')->default(null)->nullable();
            $table->string('start_month')->default(null)->nullable();
            $table->string('end_year')->default(null)->nullable();
            $table->string('end_month')->default(null)->nullable();
            $table->string('place')->nullable();
            $table->string('name');
            $table->string('kana');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars');
    }
}
