<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('samurai_names', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->string('childhood_name')->nullable();
            $table->string('spiritual_name')->nullable();
            $table->string('kana')->nullable();
            $table->string('nickname')->nullable();
            $table->string('legal_name')->nullable();
            $table->string('alias')->nullable();
            $table->string('search_word')->nullable();
            $table->string('search_word_kana')->nullable();
            $table->boolean('is_search');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('samurai_names');
    }
};
