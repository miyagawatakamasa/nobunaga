<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('disturbances', function (Blueprint $table) {
            $table->integer('end_year')->nullable()->after('start_month');
            $table->integer('end_month')->nullable()->after('end_year');
            $table->integer('start_month')->nullable()->change();
            $table->text('body')->nullable()->change();
        });

        Schema::table('wars', function (Blueprint $table) {
            $table->text('body')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wars', function (Blueprint $table) {
            $table->text('body')->change();
        });

        Schema::table('disturbances', function (Blueprint $table) {
            $table->dropColumn('end_year');
            $table->dropColumn('end_month');
            $table->integer('start_month')->change();
            $table->text('body')->change();
        });
    }
};
