<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSamuraiParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('samurai_parameters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('samurai_id');
            $table->integer('game_title_id');
            $table->integer('leadership');
            $table->integer('brave');
            $table->integer('wisdom');
            $table->integer('domestic_affairs');
            $table->integer('foreign_affairs');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('samurai_parameters');
    }
}
