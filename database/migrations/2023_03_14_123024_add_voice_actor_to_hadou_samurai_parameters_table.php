<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hadou_samurai_parameters', function (Blueprint $table) {
            $table->string('voice_actor')->after('bow');
            $table->text('tactics_reason')->default(null)->nullable()->after('tactics_body');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hadou_samurai_parameters', function (Blueprint $table) {
            $table->dropColumn('tactics_reason');
            $table->dropColumn('voice_actor');
        });
    }
};
