<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taishi_pk_buildings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('system');
            $table->string('ability');
            $table->string('individuality_first');
            $table->string('individuality_secound');
            $table->integer('cost');
            $table->integer('level');
            $table->integer('level_limit');
            $table->string('conditions');
            $table->string('effect');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taishi_pk_buildings');
    }
};
