<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shinnobu_samurai_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('samurai_id');
            $table->string('rarity');
            $table->string('type');
            $table->string('appropriate_position');
            $table->string('appropriate_army');
            $table->integer('leadership');
            $table->integer('leadership_rank');
            $table->integer('brave');
            $table->integer('brave_rank');
            $table->integer('wisdom');
            $table->integer('wisdom_rank');
            $table->integer('affairs');
            $table->integer('affairs_rank');
            $table->integer('all');
            $table->integer('all_rank');
            $table->integer('combat_power');
            $table->string('heading');
            $table->text('history');
            $table->text('anecdote');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shinnobu_samurai_parameters');
    }
};
