<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shinnobu_samurai_parameters', function (Blueprint $table) {
            $table->text('anecdote')->default(null)->nullable()->change();
            $table->text('comment')->default(null)->nullable()->change();
        });

        Schema::table('taishi_pk_buildings', function (Blueprint $table) {
            $table->string('conditions')->default(null)->nullable()->change();
        });

        Schema::table('taishi_tactics', function (Blueprint $table) {
            $table->string('terms')->default(null)->nullable()->change();
            $table->string('effect')->default(null)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shinnobu_samurai_parameters', function (Blueprint $table) {
            $table->text('anecdote')->default(null)->nullable()->change();
            $table->text('comment')->default(null)->nullable()->change();
        });

        Schema::table('taishi_pk_buildings', function (Blueprint $table) {
            $table->string('conditions')->default(null)->nullable()->change();
        });

        Schema::table('taishi_tactics', function (Blueprint $table) {
            $table->string('terms')->default(null)->nullable()->change();
            $table->string('effect')->default(null)->nullable()->change();
        });
    }
};
