<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Disturbance;
use App\Traits\SeedingFromCsv;

class DisturbanceTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "disturbances";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new Disturbance();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
