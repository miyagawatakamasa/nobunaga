<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ShinseiCharacteristicSearch;
use App\Traits\SeedingFromCsv;

class ShinseiCharacteristicSearchTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "shinsei_characteristic_searches";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new ShinseiCharacteristicSearch();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
