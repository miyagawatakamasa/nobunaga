<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\TaishiTaimei;
use App\Traits\SeedingFromCsv;

class TaishiTaimeiTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "taishi_taimeis";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new TaishiTaimei();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
