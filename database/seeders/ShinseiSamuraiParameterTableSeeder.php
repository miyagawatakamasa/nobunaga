<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ShinseiSamuraiParameter;
use App\Traits\SeedingFromCsv;

class ShinseiSamuraiParameterTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "shinsei_samurai_parameters";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new ShinseiSamuraiParameter();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
