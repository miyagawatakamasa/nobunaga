<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\HadouSkillSearch;
use App\Traits\SeedingFromCsv;

class HadouSkillSearchTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "hadou_skill_searches";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new HadouSkillSearch();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
