<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SamuraiName;
use App\Traits\SeedingFromCsv;

class SamuraiNameTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "samurai_names";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new SamuraiName();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
