<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ShinnobuSamuraiParameter;
use App\Traits\SeedingFromCsv;

class ShinnobuSamuraiParameterTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "shinnobu_samurai_parameters";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new ShinnobuSamuraiParameter();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
