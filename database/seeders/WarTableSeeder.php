<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\War;
use App\Traits\SeedingFromCsv;

class WarTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "wars";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new War();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
