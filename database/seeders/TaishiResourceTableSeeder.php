<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\TaishiResource;
use App\Traits\SeedingFromCsv;

class TaishiResourceTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "taishi_resources";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new TaishiResource();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
