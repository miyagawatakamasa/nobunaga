<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\TaishiPkHeirloomSamurai;
use App\Traits\SeedingFromCsv;

class TaishiPkHeirloomSamuraiTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "taishi_pk_heirloom_samurais";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new TaishiPkHeirloomSamurai();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
