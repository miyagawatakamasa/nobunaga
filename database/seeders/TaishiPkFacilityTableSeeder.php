<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\TaishiPkFacility;
use App\Traits\SeedingFromCsv;

class TaishiPkFacilityTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "taishi_pk_facilities";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new TaishiPkFacility();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
