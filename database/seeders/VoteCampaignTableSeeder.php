<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\VoteCampaign;
use App\Traits\SeedingFromCsv;

class VoteCampaignTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "vote_campaigns";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new VoteCampaign();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
