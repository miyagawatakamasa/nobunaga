<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Scenario;
use App\Traits\SeedingFromCsv;

class ScenarioTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "scenarios";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new Scenario();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
