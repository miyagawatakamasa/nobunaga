<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\ShinseiDoctrine;
use App\Traits\SeedingFromCsv;

class ShinseiDoctrineTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "shinsei_doctrines";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new ShinseiDoctrine();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
