<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArticleCategoryTableSeeder::class);
        $this->call(ArticleCategorySearchTableSeeder::class);
        // $this->call(DiagnoseSamuraiTableSeeder::class);
        $this->call(DisturbanceTableSeeder::class);
        $this->call(GameTitleTableSeeder::class);
        $this->call(InfoTableSeeder::class);
        $this->call(HadouSamuraiParameterTableSeeder::class);
        $this->call(HadouSkillSearchTableSeeder::class);
        $this->call(HadouSkillTableSeeder::class);
        $this->call(HadouSecretTableSeeder::class);
        $this->call(HadouSecretSearchTableSeeder::class);
        $this->call(SamuraiTableSeeder::class);
        $this->call(SamuraiNameTableSeeder::class);
        // $this->call(SamuraiParameterTableSeeder::class);  //エラーなので一時コメントアウト
        $this->call(SamuraiRelatedArticleTableSeeder::class);
        // $this->call(ScenarioTableSeeder::class);
        // $this->call(ShinnobuSamuraiParameterTableSeeder::class);
        $this->call(ShinseiSamuraiParameterTableSeeder::class);  //エラーなので一時コメントアウト
        $this->call(ShinseiCharacteristicSearchTableSeeder::class);
        $this->call(ShinseiCharacteristicTableSeeder::class);
        $this->call(ShinseiDoctrineTableSeeder::class);
        $this->call(ShinseiTacticTableSeeder::class);
        $this->call(ShinseiPolicyTableSeeder::class);
        $this->call(ShinseiPkSamuraiParameterTableSeeder::class); 
        $this->call(ShinseiPkSkillTableSeeder::class); 
        // $this->call(TaimeiTableSeeder::class);
        // $this->call(TaishiPkBuildingTableSeeder::class);
        // $this->call(TaishiPkFacilityTableSeeder::class);
        // $this->call(TaishiPkHeirloomSamuraiTableSeeder::class);
        // $this->call(TaishiPkHeirloomTableSeeder::class);
        // $this->call(TaishiPkStrategyTableSeeder::class);
        // $this->call(TaishiPkWillTableSeeder::class);
        // $this->call(TaishiPkTacticTableSeeder::class);
        // $this->call(TaishiResourceTableSeeder::class);
        // $this->call(TaishiStrategyTableSeeder::class);
        // $this->call(TaishiTacticTableSeeder::class);
        // $this->call(TaishiTaimeiTableSeeder::class);
        // $this->call(TaishiTensyuTableSeeder::class);
        $this->call(TimelineTableSeeder::class);
        // $this->call(VoteCampaignTableSeeder::class);
        $this->call(WarTableSeeder::class);   //エラーなので一時コメントアウト
        $this->call(WarSamuraiTableSeeder::class);
        // $this->call(WordTableSeeder::class);
    }
}
