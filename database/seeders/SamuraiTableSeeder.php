<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Samurai;
use App\Traits\SeedingFromCsv;

class SamuraiTableSeeder extends Seeder
{

  use SeedingFromCsv;
  private $table = "samurais";
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {

    $model = new Samurai();
    DB::statement('TRUNCATE TABLE ' . $this->table . ';');
    DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1;');
    $this->insertFromCsv($this->table, $model, $this);
  }
}
