<?php

namespace App\Lib;

use Illuminate\Support\Facades\File;
use App\Models\Samurai;
use Storage;

class My_func
{
    public static function return_article_main_img_url($articleId)
    {
        if (File::exists('img/article/' . $articleId . '/main.png')) {
            return url('img/article/' . $articleId . '/main.png');
        } else {
            return url('img/top/nobu.png');
        }
    }

    public static function return_article_main_img_webp_url($articleId)
    {
        if (File::exists('img/article/' . $articleId . '/main.webp')) {
            return url('img/article/' . $articleId . '/main.webp');
        } else {
            return url('img/top/nobu.png');
        }
    }

    public static function return_vote_main_img_url($voteId)
    {
        if (File::exists('img/vote/' . $voteId . '/main.png')) {
            return url('img/vote/' . $voteId . '/main.png');
        } else {
            return url('img/top/nobu.png');
        }
    }

    public static function return_min_samurai_img_url($samuraiId, $gameTitle)
    {
        if ($gameTitle == 'shinsei') {
            $path = 'img/shinsei/samurai/';
        } else if ($gameTitle == 'shinsei-pk') {
            $path = 'img/shinsei-pk/shinsei/samurai/';
        } else {
            $path = 'img/samurai/min/';
        }
        if (Storage::disk('s3')->get($path . $samuraiId . '.jpg')) {
            return Storage::disk('s3')->url($path . $samuraiId . '.jpg');
        } elseif (Storage::disk('s3')->get('img/samurai/min/' . $samuraiId . '.jpg')) {
            return Storage::disk('s3')->url('img/samurai/min/' . $samuraiId . '.jpg');
        } elseif (Storage::disk('s3')->get('img/shinsei/samurai/' . $samuraiId . '.jpg')) {
            return Storage::disk('s3')->url('img/shinsei/samurai/' . $samuraiId . '.jpg');
        } else {
            return Storage::disk('s3')->url('img/samurai/min/00.jpg');
        }
    }

    public static function return_min_samurai_img_webp_url($samuraiId, $gameTitle)
    {
        if ($gameTitle == 'shinsei') {
            $path = 'img/shinsei/samurai_webp/';
        } else if ($gameTitle == 'shinsei-pk') {
            $path = 'img/shinsei-pk/shinsei/min_webp/';
        } else {
            $path = 'img/samurai/min_webp/';
        }

        if (Storage::disk('s3')->get($path . $samuraiId . '.webp')) {
            return Storage::disk('s3')->url($path . $samuraiId . '.webp');
        } elseif (Storage::disk('s3')->get('img/samurai/min_webp/' . $samuraiId . '.webp')) {
            return Storage::disk('s3')->url('img/samurai/min_webp/' . $samuraiId . '.webp');
        } elseif (Storage::disk('s3')->get('img/shinsei/samurai_webp/' . $samuraiId . '.webp')) {
            return Storage::disk('s3')->url('img/shinsei/samurai_webp/' . $samuraiId . '.webp');
        } else {
            return Storage::disk('s3')->url('img/shinsei/samurai_webp/00.webp');
        }
    }

    public static function return_entry_status($samuraiSecnarioStatus)
    {
        if ($samuraiSecnarioStatus == 'b') {
            return '未登場';
        } elseif ($samuraiSecnarioStatus == 'g') {
            return '元服前';
        } elseif ($samuraiSecnarioStatus == 'd') {
            return '死亡';
        } else {
            return $samuraiSecnarioStatus;
        }
    }

    public static function return_samurai_name_or_string($string)
    {
        $samurai = Samurai::find($string);

        if ($samurai) {
            return "<a href='/database/samurai/" . $samurai->id . "'>" . $samurai->name . "</a>";
        } else {
            return $string;
        }
    }

    public static function return_wiki_img_html_tag($Samurai)
    {
        $path = 'img/samurai/wiki/' . $Samurai->id . '.jpg';
        if (Storage::disk('s3')->get($path)) {
            return "<img src='" . Storage::disk('s3')->url($path) . "' alt='" . $Samurai->name .'の自画像' . "' width='80%' height='auto' class='m-a'>";
        } else {
            return;
        }
    }
}
