<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiagnoseSamuraiUser extends Model
{
  use SoftDeletes;

  protected $table = 'diagnose_samurai_users';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'leadership',
    'brave',
    'wisdom',
    'affairs',
    'betrayal',
    'bright',
    'intuition',
    'precise',
    'attentive',
    'justice',
    'revolution',
    'dream',
    'patience',
    'advancement',
    'psychopath',
  ];

  /**
   * belongsTo
   */
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
    return $query->where($this->table.'.samurai_id', $samuraiId);
  }
  public function scopeIsPsychopath(Builder $query)
  {
    return $query->where($this->table.'.psychopath', '>=', 10);
  }
}
