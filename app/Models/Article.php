<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
  use SoftDeletes;

  protected $table = 'articles';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'additioned_at',
    'deleted_at'
  ];

  protected $fillable = [
    'title',
    'body',
    'description',
    'recommend_order',
    'is_updated',
    'view_count',
  ];

  /**
   * hasMany/hasOne
   */
  public function articleCategorySearches()
  {
      return $this->hasMany('App\Models\ArticleCategorySearch');
  }
  public function samuraiRelatedArticles()
  {
      return $this->hasMany('App\Models\SamuraiRelatedArticle');
  }

  public function scopeIsRecommend(Builder $query)
  {
      return $query->where($this->table . '.recommend_order', '>', '0');
  }
}
