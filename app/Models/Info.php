<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Info extends Model
{
  use SoftDeletes;

  protected $table = 'infos';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'date',
    'deleted_at'
  ];

  protected $fillable = [
    'date',
    'contents',
    'is_display',
  ];

  /**
   * @param Builder $query
   * @param $userId
   * @return \Illuminate\Database\Query\Builder
   */

  public function scopeIsDisplay(Builder $query)
  {
   return $query->where($this->table.'.is_display', 1);
  }
}
