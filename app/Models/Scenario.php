<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scenario extends Model
{
  use SoftDeletes;

  protected $table = 'scenarios';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'title',
    'year',
    'month',
    'game_title_id',
  ];

  /**
  * belongsTo
  */
  public function gameTille()
  {
    return $this->belongsTo('App\Models\GameTitle');
  }

  public function scopeGameTitleId(Builder $query, $gameTitleId)
  {
   return $query->where($this->table.'.game_title_id', $gameTitleId);
  }
}
