<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiPkStrategy extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_pk_strategies';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'cost',
    'description',
    'kana',
    'terrain',
    'required_units',
  ];

  /**
   * hasMany/hasOne
   */
  public function samuraiParameters()
  {
      return $this->hasMany('App\Models\SamuraiParameter');
  }
}
