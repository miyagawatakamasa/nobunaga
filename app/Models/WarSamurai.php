<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarSamurai extends Model
{
  use SoftDeletes;

  protected $table = 'war_samurais';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'position',
    'w_or_l',
    'war_id',
    'samurai_id',
    'samurai_name',
  ];

  public function war()
  {
    return $this->belongsTo('App\Models\War')->orderBy('start_year', 'asc');
  }

  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeWarId(Builder $query, $warId)
  {
    return $query->where($this->table.'.war_id', $warId);
  }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
    return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
