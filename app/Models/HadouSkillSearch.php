<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class HadouSkillSearch extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'hadou_skill_searches';
  
    protected $primaryKey = 'id';
  
    protected $guarded = [];
  
    protected $dates = [
      'deleted_at'
    ];
  
    protected $fillable = [
      'hadou_samurai_parameter_id',
      'hadou_skill_id',
    ];
  
    /**
     * belongsTo
     */
    public function hadouSamuraiParameter()
    {
      return $this->belongsTo('App\Models\HadouSamuraiParameter');
    }
    public function hadouSkill()
    {
      return $this->belongsTo('App\Models\HadouSkill');
    }
  
    public function scopeHadouSamuraiParameterId(Builder $query, $hadou_samurai_parameter_id)
    {
    return $query->where($this->table.'.hadou_samurai_parameter_id', $hadou_samurai_parameter_id);
    }
}
