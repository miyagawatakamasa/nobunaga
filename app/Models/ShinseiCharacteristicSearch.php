<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiCharacteristicSearch extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_characteristic_searches';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'shinsei_characteristic_id',
    'shinsei_samurai_parameter_id',
  ];

  public function shinseiCharacteristic()
  {
    return $this->belongsTo('App\Models\ShinseiCharacteristic');
  }
  public function shinseiSamuraiParameter()
  {
    return $this->belongsTo('App\Models\ShinseiSamuraiParameter');
  }

  public function scopeShinseiSamuraiParameterId(Builder $query, $shinseiSamuraiParameterId)
  {
    return $query->where($this->table.'.shinsei_samurai_parameter_id', $shinseiSamuraiParameterId);
  }
}
