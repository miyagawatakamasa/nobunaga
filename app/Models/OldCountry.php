<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class OldCountry extends Model
{
  use SoftDeletes;

  protected $table = 'old_countries';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'region',
    'region_slug',
    'name',
    'kana',
    'old_kana',
    'harvest',
  ];
}
