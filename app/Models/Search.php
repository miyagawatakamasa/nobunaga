<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends Model
{
  use SoftDeletes;

  protected $table = 'searches';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $fillable = [
    'text',
    'count',
  ];

  public function scopeText(Builder $query, $text)
  {
      return $query->where($this->table . '.text', $text);
  }
}
