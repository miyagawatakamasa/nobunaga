<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class SamuraiParameter extends Model
{
  use SoftDeletes;

  protected $table = 'samurai_parameters';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'game_title_id',
    'leadership',
    'leadership_rank',
    'brave',
    'brave_rank',
    'wisdom',
    'wisdom_rank',
    'domestic_affairs',
    'domestic_affairs_rank',
    'foreign_affairs',
    'foreign_affairs_rank',
    'all',
    'all_rank',
    'taishi_pk_will_id',
    'taishi_pk_tactic_id',
    'taishi_pk_assistant',
    'taishi_pk_strategy_id',
    'taishi_pk_temperament',
    'taishi_pk_ambition',
  ];

  /**
   * belongsTo
   */
  public function gameTitle()
  {
    return $this->belongsTo('App\Models\GameTitle');
  }
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }
  public function taishiPkWill()
  {
    return $this->belongsTo('App\Models\TaishiPkWill');
  }
  public function taishiPkStrategy()
  {
    return $this->belongsTo('App\Models\TaishiPkStrategy');
  }

  public function scopeGameTitleId(Builder $query, $gameTitleId)
  {
  return $query->where($this->table.'.game_title_id', $gameTitleId);
  }
  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
