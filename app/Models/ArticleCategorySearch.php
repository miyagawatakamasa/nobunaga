<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategorySearch extends Model
{
  use SoftDeletes;

  protected $table = 'article_category_searches';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'article_id',
    'article_category_id',
    'disturbance_id',
    'is_main_category',
  ];

  /**
   * belongsTo
   */
  public function article()
  {
    return $this->belongsTo('App\Models\Article');
  }
  public function articleCategory()
  {
    return $this->belongsTo('App\Models\ArticleCategory');
  }
  public function disturbance()
  {
    return $this->belongsTo('App\Models\Disturbance');
  }

  /**
   * scope
   */
  public function scopeArticleId(Builder $query, $articleId)
  {
      return $query->where($this->table . '.article_id', $articleId);
  }
  public function scopeArticleCategoryId(Builder $query, $articleCategoryId)
  {
      return $query->where($this->table . '.article_category_id', $articleCategoryId);
  }
  public function scopeDisturbanceId(Builder $query, $articleId)
  {
      return $query->where($this->table . '.disturbance_id', $articleId);
  }
}
