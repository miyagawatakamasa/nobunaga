<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class HadouSecretSearch extends Model
{
  use SoftDeletes;

  protected $table = 'hadou_secret_searches';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'hadou_samurai_parameter_id',
    'hadou_secret_id',
  ];

  /**
   * belongsTo
   */
  public function hadouSamuraiParameter()
  {
    return $this->belongsTo('App\Models\HadouSamuraiParameter');
  }
  public function hadouSecret()
  {
    return $this->belongsTo('App\Models\HadouSecret');
  }

  public function scopeHadouSamuraiParameterId(Builder $query, $hadou_samurai_parameter_id)
  {
  return $query->where($this->table.'.hadou_samurai_parameter_id', $hadou_samurai_parameter_id);
  }
}
