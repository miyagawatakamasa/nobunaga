<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class HadouSamuraiParameter extends Model
{
    use SoftDeletes;

    protected $table = 'hadou_samurai_parameters';
  
    protected $primaryKey = 'id';
  
    protected $guarded = [];
  
    protected $dates = [
      'deleted_at'
    ];
  
    protected $fillable = [
      'samurai_id',
      'rarity',
      'strength',
      'talent',
      'leadership',
      'leadership_rank',
      'brave',
      'brave_rank',
      'wisdom',
      'wisdom_rank',
      'affairs',
      'affairs_rank',
      'all',
      'all_rank',
      'spear',
      'horsemanship',
      'bow',
      'voice_actor',
      'tactics',
      'tactics_body',
      'tactics_reason',
    ];
  
    /**
     * belongsTo
     */
    public function samurai()
    {
      return $this->belongsTo('App\Models\Samurai');
    }
  
    /**
     * hasMany/hasOne
     */
    public function hadouSkillSearches()
    {
      return $this->hasMany('App\Models\HadouSkillSearch');
    }
    public function hadouSecretSearches()
    {
      return $this->hasMany('App\Models\HadouSecretSearch');
    }
  
    public function scopeSamuraiId(Builder $query, $samuraiId)
    {
      return $query->where($this->table.'.samurai_id', $samuraiId);
    }
    public function scopeRarity(Builder $query, $rarity)
    {
      return $query->where($this->table.'.rarity', $rarity);
    }
  
}