<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiSamuraiParameter extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_samurai_parameters';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'leadership',
    'leadership_rank',
    'brave',
    'brave_rank',
    'wisdom',
    'wisdom_rank',
    'affairs',
    'affairs_rank',
    'all',
    'all_rank',
    'shinsei_doctrine_id',
    'shinsei_tactic_id',
    'nobunaga_gennpuku',
    'owari_touitsu',
    'okehazama',
    'houimou',
    'yumemaboroshi',
  ];

  public function shinseiCharacteristicSearches()
  {
      return $this->hasMany('App\Models\ShinseiCharacteristicSearch');
  }
  
  /**
   * belongsTo
   */
  public function gameTitle()
  {
    return $this->belongsTo('App\Models\GameTitle');
  }
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }
  public function shinseiDoctrine()
  {
    return $this->belongsTo('App\Models\ShinseiDoctrine');
  }
  public function shinseiTactic()
  {
    return $this->belongsTo('App\Models\ShinseiTactic');
  }

  public function scopeGameTitleId(Builder $query, $gameTitleId)
  {
  return $query->where($this->table.'.game_title_id', $gameTitleId);
  }
  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
