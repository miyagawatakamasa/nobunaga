<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taimei extends Model
{
  use SoftDeletes;

  protected $table = 'taimeis';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'description',
    'period',
    'agriculture',
    'commercial',
    'military',
    'discussion',
    'taimei',
  ];
}
