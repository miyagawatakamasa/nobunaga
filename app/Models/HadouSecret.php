<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class HadouSecret extends Model
{
  use SoftDeletes;

  protected $table = 'hadou_secrets';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'name_kana',
    'system',
    'conditions',
    'effect1',
    'effect2',
    'effect3',
    'effect4',
    'effect5',
  ];

  /**
   * hasMany/hasOne
   */
  public function hadouSecretSearches()
  {
      return $this->hasMany('App\Models\HadouSecretSearch');
  }
}
