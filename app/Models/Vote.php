<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vote extends Model
{
  use SoftDeletes;

  protected $table = 'votes';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'vote_campaign_id',
    'samurai_id',
    'ip',
    'reason',
  ];

  /**
   * belongsTo
   */
  public function voteCampaign()
  {
    return $this->belongsTo('App\Models\VoteCampaign');
  }
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeVoteCampaignId(Builder $query, $voteCampaignId)
  {
   return $query->where($this->table . '.vote_campaign_id', $voteCampaignId);
  }
  public function scopeIp(Builder $query, $ip)
  {
   return $query->where($this->table . '.ip', $ip);
  }
  public function scopeIssetReason(Builder $query)
  {
   return $query->where($this->table . '.reason', '<>', null );
  }
}
