<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiTactic extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_tactics';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'description',
  ];

  public function shinseiSamuraiParameters()
  {
      return $this->hasMany('App\Models\ShinseiSamuraiParameter');
  }
}
