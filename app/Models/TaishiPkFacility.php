<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiPkFacility extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_pk_facilities';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'cost',
    'period',
    'effect',
  ];
}
