<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class SamuraiRelatedArticle extends Model
{
  use SoftDeletes;

  // 名前おかしいけどいいか
  protected $table = 'samurai_related_articles';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'article_id',
  ];

  /**
   * belongsTo
   */
  public function article()
  {
    return $this->belongsTo('App\Models\Article');
  }
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
  public function scopeArticleId(Builder $query, $articleId)
  {
  return $query->where($this->table.'.article_id', $articleId);
  }
}
