<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class SamuraiName extends Model
{
  use SoftDeletes;

  protected $table = 'samurai_names';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'childhood_name',
    'spiritual_name',
    'kana',
    'nickname',
    'legal_name',
    'alias',
    'search_word',
    'search_word_kana',
    'is_search',
  ];

  /**
   * belongsTo
   */
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
    return $query->where($this->table.'.samurai_id', $samuraiId);
  }
  public function scopeActive(Builder $query)
  {
    return $query->where($this->table.'.is_search', '1');
  }
  public function scopeIsSetChildhoodName(Builder $query)
  {
    return $query->where($this->table.'.childhood_name', '!=', null);
  }
  public function scopeIsSetSpiritualName(Builder $query)
  {
    return $query->where($this->table.'.spiritual_name', '!=', null);
  }
  public function scopeIsSetKana(Builder $query)
  {
    return $query->where($this->table.'.kana', '!=', null);
  }
  public function scopeIsSetNickname(Builder $query)
  {
    return $query->where($this->table.'.nickname', '!=', null);
  }
  public function scopeIsSetLegalName(Builder $query)
  {
    return $query->where($this->table.'.legal_name', '!=', null);
  }
  public function scopeIsSetAlias(Builder $query)
  {
    return $query->where($this->table.'.alias', '!=', null);
  }

}
