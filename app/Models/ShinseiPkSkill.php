<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiPkSkill extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_pk_skills';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'type',
    'name',
    'furi',
    'koyuu',
    'description',
  ];

  public function scopeTactics(Builder $query)
  {
    return $query->where($this->table.'.type', "tactics");
  }
  public function scopeCharacteristic(Builder $query)
  {
    return $query->where($this->table.'.type', "characteristic");
  }
  public function scopeHeadOfHouse(Builder $query)
  {
    return $query->where($this->table.'.type', "head_of_house");
  }
  public function scopeMagistrate(Builder $query)
  {
    return $query->where($this->table.'.type', "magistrate");
  }
  public function scopeName(Builder $query, $name)
  {
    return $query->where($this->table.'.name', $name);
  }
}
