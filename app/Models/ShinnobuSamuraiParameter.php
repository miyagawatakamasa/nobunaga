<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinnobuSamuraiParameter extends Model
{
  use SoftDeletes;

  protected $table = 'shinnobu_samurai_parameters';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'rarity',
    'rank',
    'type',
    'appropriate_position',
    'appropriate_army',
    'leadership',
    'leadership_rank',
    'brave',
    'brave_rank',
    'wisdom',
    'wisdom_rank',
    'affairs',
    'affairs_rank',
    'all',
    'all_rank',
    'combat_power',
    'heading',
    'history',
    'anecdote',
    'comment',
  ];

  /**
   * belongsTo
   */
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
  public function scopeRarity(Builder $query, $rarity)
  {
  return $query->where($this->table.'.rarity', $rarity);
  }
  public function scopeAppropriatePosition(Builder $query, $rarity)
  {
  return $query->where($this->table.'.appropriate_position', $rarity);
  }
  public function scopeAppropriateArmy(Builder $query, $rarity)
  {
  return $query->where($this->table.'.appropriate_army', $rarity);
  }
}
