<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class War extends Model
{
  use SoftDeletes;

  protected $table = 'wars';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'disturbance_id',
    'start_year',
    'start_month',
    'start_day',
    'end_year',
    'end_month',
    'end_day',
    'prefectures',
    'city',
    'town',
    'name',
    'kana',
    'body',
    'body_html',
  ];

  public function disturbance()
  {
    return $this->belongsTo('App\Models\Disturbance');
  }

  public function warSamurais()
  {
    return $this->hasMany('App\Models\WarSamurai');
  }
}
