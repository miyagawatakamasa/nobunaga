<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Timeline extends Model
{
  use SoftDeletes;

  protected $table = 'timelines';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'year',
    'month',
    'body',
    'day',
    'priolity',
  ];

  public function scopeIsToday(Builder $query)
  {
    $now = Carbon::now();

    return $query
      ->where($this->table.'.month', $now->month)
      ->where($this->table.'.day', $now->day);
  }
}
