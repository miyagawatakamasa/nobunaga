<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoteCampaign extends Model
{
  use SoftDeletes;

  protected $table = 'vote_campaigns';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'title',
    'body',
  ];

  /**
   * hasMany/hasOne
   */
  public function votes()
  {
      return $this->hasMany('App\Models\Vote');
  }
}
