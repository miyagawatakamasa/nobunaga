<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategory extends Model
{
  use SoftDeletes;

  protected $table = 'article_categories';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'slug',
    'order',
    'parent_category_id',
  ];

  /**
   * hasMany/hasOne
   */
  public function articleCategorySearches()
  {
      return $this->hasMany('App\Models\ArticleCategorySearch');
  }
  public function scopeSlug(Builder $query, $slug)
  {
      return $query->where($this->table . '.slug', $slug);
  }
}
