<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiCharacteristic extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_characteristics';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'description',
  ];

  public function shinseiCharacteristicSearches()
  {
      return $this->hasMany('App\Models\ShinseiCharacteristicSearch');
  }
}
