<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SamuraiName;

class Samurai extends Model
{
  use SoftDeletes;

  protected $table = 'samurais';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'furigana',
    'order',
    'birth_year',
    'death_year',
    'sex',
    'retuden',
    'retuden_html',
    'father',
    'adoptive_father',
    'mother',
    'adoptive_mother',
    'spouse',
    'concubine_1',
    'concubine_2',
    'concubine_3',
    'zennkoku',
    'sgunnyuudenn',
    'bhuuunnroku',
    'haoudenn',
    'tennsyouki',
    'syouseiroku',
    'reppuuden',
    'ranseiki',
    'soutennroku',
    'tsousei',
    'kakushinn',
    'tenndou',
    'souzou',
    'taishi',
    'taishi-pk',
    'shinsei',
  ];

  /**
   * hasMany/hasOne
   */
  public function samuraiParameters()
  {
    return $this->hasMany('App\Models\SamuraiParameter');
  }
  public function shinnobuSamuraiParameter()
  {
    return $this->hasOne('App\Models\ShinnobuSamuraiParameter');
  }
  public function votes()
  {
    return $this->hasMany('App\Models\Vote');
  }
  public function samuraiRelatedArticles()
  {
    return $this->hasMany('App\Models\SamuraiRelatedArticle');
  }
  public function taishiPkHeirloomSamurais()
  {
    return $this->hasMany('App\Models\TaishiPkHeirloomSamurai');
  }
  public function warSamurais()
  {
    return $this->hasMany('App\Models\WarSamurai');
  }
  public function hadouSamuraiParameters()
  {
    return $this->hasMany('App\Models\HadouSamuraiParameter');
  }
  public function SamuraiNames()
  {
    return $this->hasMany('App\Models\SamuraiName');
  }

  public function scopeTaishi(Builder $query)
  {
    return $query->where($this->table.'.taishi', 1);
  }
  public function scopeTaishiPk(Builder $query)
  {
    return $query->where($this->table.'.taishi-pk', 1);
  }
  public function scopeShinsei(Builder $query)
  {
    return $query->where($this->table.'.shinsei', 1);
  }
  public function scopeShinseiPk(Builder $query)
  {
    return $query->where($this->table.'.shinsei-pk', 1);
  }
  public function scopeActive(Builder $query)
  {
    return $query->where(function($q) {
      $q->where($this->table.'.taishi', 1)
            ->orWhere($this->table.'.taishi-pk', 1)
            ->orWhere($this->table.'.shinsei', 1);
    });
  }

  public static function childhoodNames($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetChildhoodName()->get();
      return $SamuraiNames;
  }
  public static function spiritualNames($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetSpiritualName()->get();
      return $SamuraiNames;
  }
  public static function kanas($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetKana()->get();
      return $SamuraiNames;
  }
  public static function nicknames($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetNickname()->get();
      return $SamuraiNames;
  }
  public static function legalNames($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetLegalName()->get();
      return $SamuraiNames;
  }
  public static function aliases($samuraiId)
  {
      $SamuraiNames = SamuraiName::samuraiId($samuraiId)->isSetAlias()->get();
      return $SamuraiNames;
  }
}
