<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disturbance extends Model
{
  use SoftDeletes;

  protected $table = 'disturbances';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'start_year',
    'start_month',
    'end_year',
    'end_month',
    'body',
    'body_html',
  ];

  public function wars()
  {
    return $this->hasMany('App\Models\War');
  }
  public function articleCategorySearches()
  {
    return $this->hasMany('App\Models\ArticleCategorySearch');
  }
}
