<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class HadouSkill extends Model
{
  use SoftDeletes;

  protected $table = 'hadou_skills';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'effect1',
    'effect2',
    'effect3',
    'effect4',
    'effect5',
  ];

  /**
   * hasMany/hasOne
   */
  public function hadouSkillSearches()
  {
      return $this->hasMany('App\Models\HadouSkillSearch');
  }
}
