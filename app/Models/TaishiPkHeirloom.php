<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiPkHeirloom extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_pk_heirlooms';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
  ];

  /**
   * hasMany/hasOne
   */
  public function taishiPkHeirloomSamurais()
  {
      return $this->hasMany('App\Models\TaishiPkHeirloomSamurai');
  }
}
