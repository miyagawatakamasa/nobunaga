<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameTitle extends Model
{
  use SoftDeletes;

  protected $table = 'game_titles';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'slug',
    'order',
  ];

  /**
  * hasMany
  */
  public function samuraiParameters()
  {
    return $this->hasMany('App\Model\SamuraiParameter');
  }
  public function senarios()
  {
    return $this->hasMany('App\Model\Scenario');
  }

  public function scopeSlug(Builder $query, $slug)
  {
  return $query->where($this->table.'.slug', $slug);
  }
}
