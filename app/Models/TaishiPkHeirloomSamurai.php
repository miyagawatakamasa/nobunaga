<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiPkHeirloomSamurai extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_pk_heirloom_samurais';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'taishi_pk_heirloom_id',
    'samurai_id',
  ];

  /**
   * belongsTo
   */
  public function taishiPkHeirloom()
  {
    return $this->belongsTo('App\Models\TaishiPkHeirloom');
  }
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }


  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
