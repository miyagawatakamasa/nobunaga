<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiPkBuilding extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_pk_buildings';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'system',
    'ability',
    'individuality_first',
    'individuality_secound',
    'cost',
    'level',
    'level_limit',
    'conditions',
    'effect',
  ];
}
