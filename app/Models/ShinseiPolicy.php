<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiPolicy extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_policies';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'description',
    'daimyou',
    'samurai_id',
  ];

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
    return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
