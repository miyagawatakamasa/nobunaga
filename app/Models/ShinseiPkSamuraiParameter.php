<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShinseiPkSamuraiParameter extends Model
{
  use SoftDeletes;

  protected $table = 'shinsei_pk_samurai_parameters';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'samurai_id',
    'name',
    'furigana',
    'leadership',
    'leadership_rank',
    'brave',
    'brave_rank',
    'wisdom',
    'wisdom_rank',
    'affairs',
    'affairs_rank',
    'all',
    'all_rank',
    'policy',
    'tactics',
    'characteristic1',
    'characteristic2',
    'characteristic3',
    'head_of_house',
    'magistrate',
    'nobugen_age',
    'owarito_age',
    'oke_age',
    'tenka_age',
    'houi_age',
    'mikata_age',
    'yume_age',
    'tenkabuzi_age',
    'seki_age',
    'osaka_age',
    'nobugen_shiro',
    'owarito_shiro',
    'oke_shiro',
    'tenka_shiro',
    'houi_shiro',
    'mikata_shiro',
    'yume_shiro',
    'tenkabuzi_shiro',
    'seki_shiro',
    'osaka_shiro',
  ];

  // public function shinseiCharacteristicSearches()
  // {
  //     return $this->hasMany('App\Models\ShinseiCharacteristicSearch');
  // }

  /**
   * belongsTo
   */
  public function samurai()
  {
    return $this->belongsTo('App\Models\Samurai');
  }
  // public function shinseiDoctrine()
  // {
  //   return $this->belongsTo('App\Models\ShinseiDoctrine');
  // }
  // public function shinseiTactic()
  // {
  //   return $this->belongsTo('App\Models\ShinseiTactic');
  // }

  public function scopeSamuraiId(Builder $query, $samuraiId)
  {
  return $query->where($this->table.'.samurai_id', $samuraiId);
  }
}
