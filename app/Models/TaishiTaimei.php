<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaishiTaimei extends Model
{
  use SoftDeletes;

  protected $table = 'taishi_taimeis';

  protected $primaryKey = 'id';

  protected $guarded = [];

  protected $dates = [
    'deleted_at'
  ];

  protected $fillable = [
    'name',
    'description',
    'period',
    'agriculture',
    'commercial',
    'military',
    'discussion',
    'taimei',
  ];
}
