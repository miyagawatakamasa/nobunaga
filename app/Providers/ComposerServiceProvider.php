<?php
namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $gameTitle = config('const.gameTitle');

        View::composer('*', function($view, $gameTitle) {
            $view->with('gameTitle', $gameTitle);
        });
    }
}