<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    /**
     * not_undefinedのバリデーション
     *
     * @return bool
     */
        Validator::extend(
            'not_undefined',
            function ($attribute, $value, $parameters, $validator) {
                return preg_match('/^0$|^-?[1-9][0-9]*$/', $value);
            }
        );
    }
}