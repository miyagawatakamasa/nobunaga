<?php

namespace App\Http\Controllers\Tactics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\TaishiTactic;
use App\Models\TaishiPkTactic;

class TacticsController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();

        if ($GameTitle->slug == 'taishi') {
            $TaishiTactics = TaishiTactic::get();
            return view('tactics.index')->with([
                'GameTitle' => $GameTitle,
                'TaishiTactics' => $TaishiTactics,
            ]);
        } elseif ($GameTitle->slug == 'taishi-pk') {
            $TaishiPkTactics = TaishiPkTactic::get();
            return view('tactics.index')->with([
                'GameTitle' => $GameTitle,
                'TaishiPkTactics' => $TaishiPkTactics,
            ]);
        }
    }
}
