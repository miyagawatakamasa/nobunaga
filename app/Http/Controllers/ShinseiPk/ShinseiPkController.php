<?php

namespace App\Http\Controllers\ShinseiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\HadouSamuraiParameter;
use App\Models\Samurai;
use App\Models\SamuraiRelatedArticle;
use App\Models\ShinnobuSamuraiParameter;
use App\Models\ShinseiSamuraiParameter;
use App\Models\ShinseiPkSamuraiParameter;
use App\Models\ShinseiPkSkill;
use App\Models\WarSamurai;


class ShinseiPkController extends Controller
{
    public function samuraiIndex()
    {
        $ShinseiPkSamuraiParameters = ShinseiPkSamuraiParameter::with('samurai')->get();
        return view('shinsei-pk.samurai_index')->with([
            'ShinseiPkSamuraiParameters' => $ShinseiPkSamuraiParameters,
        ]);
    }

    public function show(Request $request)
    {
        $GameTitle = GameTitle::slug('shinsei-pk')->first();
        $ShinseiPkSamuraiParameter = ShinseiPkSamuraiParameter::with(['samurai'])->samuraiId($request->samurai_id)->first();

        $SamuraiRelatedArticles = SamuraiRelatedArticle::with(['article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->samuraiId($ShinseiPkSamuraiParameter->samurai->id)->get();

        $NextSamurai = null;
        $BeforeSamurai = null;
        if ($ShinseiPkSamuraiParameter->id != 2216) {
            $NextSamurai = ShinseiPkSamuraiParameter::find($request->samurai_id + 1);
        }
        if ($ShinseiPkSamuraiParameter->id != 1) {
            $BeforeSamurai = ShinseiPkSamuraiParameter::find($request->samurai_id - 1);
        }

        // $imgCount = count(glob("img/shinsei/samurai/" . $Samurai->id . "_" . "*.jpg"));

        $ShinnobuSamuraiParameter = ShinnobuSamuraiParameter::samuraiId($request->samurai_id)->first();
        $TaishiPkSamuraiParameter = Samurai::taishiPk()->find($request->samurai_id);
        $HadouSamuraiParameter = HadouSamuraiParameter::samuraiId($request->samurai_id)->first();
        $ShinseiSamuraiParameter = ShinseiSamuraiParameter::samuraiId($request->samurai_id)->first();
        $WarSamurais = WarSamurai::with(['war', 'war.disturbance', 'samurai', 'war.warSamurais', 'war.warSamurais.samurai'])->samuraiId($request->samurai_id)->get()->sortBy([
            ['war.start_year', true],
            ['war.start_month', true],
            ['war.start_day', true],
        ]);

        $tactics = ShinseiPkSkill::tactics()->name($ShinseiPkSamuraiParameter->tactics)->first();
        $characteristic1 = ShinseiPkSkill::characteristic()->name($ShinseiPkSamuraiParameter->characteristic1)->first();
        $characteristic2 = ShinseiPkSkill::characteristic()->name($ShinseiPkSamuraiParameter->characteristic2)->first();
        $characteristic3 = ShinseiPkSkill::characteristic()->name($ShinseiPkSamuraiParameter->characteristic3)->first();
        $headOfHouse = ShinseiPkSkill::headOfHouse()->name($ShinseiPkSamuraiParameter->head_of_house)->first();
        $magistrate = ShinseiPkSkill::magistrate()->name($ShinseiPkSamuraiParameter->magistrate)->first();


        return view('shinsei-pk.samurai')->with([
            'BeforeSamurai' => $BeforeSamurai,
            'GameTitle' => $GameTitle,
            // 'imgCount' => $imgCount,
            'NextSamurai' => $NextSamurai,
            'ShinseiPkSamuraiParameter' => $ShinseiPkSamuraiParameter,
            // 'Samurais' => $Samurais,
            'HadouSamuraiParameter' => $HadouSamuraiParameter,
            'ShinseiSamuraiParameter' => $ShinseiSamuraiParameter,
            'ShinnobuSamuraiParameter' => $ShinnobuSamuraiParameter,
            'TaishiPkSamuraiParameter' => $TaishiPkSamuraiParameter,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            'WarSamurais' => $WarSamurais,

            'tactics' => $tactics,
            'characteristic1' => $characteristic1,
            'characteristic2' => $characteristic2,
            'characteristic3' => $characteristic3,
            'headOfHouse' => $headOfHouse,
            'magistrate' => $magistrate,
        ]);
    }

}
