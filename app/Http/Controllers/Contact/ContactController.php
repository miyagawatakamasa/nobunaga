<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactPost;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function send(ContactPost $request)
    {
        $data = $request->all();
        $Contact = Contact::create($data);
        $Contact->save();

        return view('contact.complete');
    }

}
