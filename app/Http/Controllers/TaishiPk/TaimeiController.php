<?php

namespace App\Http\Controllers\TaishiPk;

use App\Http\Controllers\Controller;
use App\Models\Taimei;
use Illuminate\Http\Request;

class TaimeiController extends Controller
{
    public function index()
    {
        $Taimeis = Taimei::all();
        return view('taishi-pk.taimei')->with([
            'Taimeis' => $Taimeis,
          ]);
    }
}
