<?php

namespace App\Http\Controllers\TaishiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\TaishiPkFacility;

class FacilityController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug('taishi-pk')->first();
        if (!isset($GameTitle)) {
            return redirect()->route('top');
        }

        $TaishiPkFacilities = TaishiPkFacility::all();

        return view('taishi-pk.facility')->with([
            'GameTitle' => $GameTitle,
            'TaishiPkFacilities' => $TaishiPkFacilities,
          ]);
    }

}
