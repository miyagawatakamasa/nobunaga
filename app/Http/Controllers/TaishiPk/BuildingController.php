<?php

namespace App\Http\Controllers\TaishiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\TaishiPkBuilding;

class BuildingController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug('taishi-pk')->first();
        if (!isset($GameTitle)) {
            return redirect()->route('top');
        }

        $TaishiPkBuildings = TaishiPkBuilding::all();

        return view('taishi-pk.building')->with([
            'GameTitle' => $GameTitle,
            'TaishiPkBuildings' => $TaishiPkBuildings,
          ]);
    }

}
