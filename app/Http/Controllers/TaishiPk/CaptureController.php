<?php

namespace App\Http\Controllers\TaishiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CaptureController extends Controller
{
    public function military(Request $request)
    {
        return view('taishi-pk.capture.military');
    }
    public function march(Request $request)
    {
        return view('taishi-pk.capture.march');
    }
    public function decisiveBattle(Request $request)
    {
        return view('taishi-pk.capture.decisive_battle');
    }
    public function siege(Request $request)
    {
        return view('taishi-pk.capture.siege');
    }

    public function plot(Request $request)
    {
        return view('taishi-pk.capture.plot');
    }

    public function generalContract(Request $request)
    {
        return view('taishi-pk.capture.general_contract');
    }

    public function humanResources(Request $request)
    {
        return view('taishi-pk.capture.human_resources');
    }

    public function will(Request $request)
    {
        return view('taishi-pk.capture.will');
    }

    public function domesticAffairs(Request $request)
    {
        return view('taishi-pk.capture.domestic_affairs');
    }

    public function diplomacy(Request $request)
    {
        return view('taishi-pk.capture.diplomacy');
    }

    public function commercial(Request $request)
    {
        return view('taishi-pk.capture.commercial');
    }

    public function agriculture(Request $request)
    {
        return view('taishi-pk.capture.agriculture');
    }
}
