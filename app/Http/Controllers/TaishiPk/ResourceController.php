<?php

namespace App\Http\Controllers\TaishiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TaishiResource;

class ResourceController extends Controller
{
    public function index(Request $request)
    {
        $TaishiResources = TaishiResource::get();
        return view('taishi-pk.resource')->with([
                'TaishiResources' => $TaishiResources,
            ]);
    }
}
