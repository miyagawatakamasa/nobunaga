<?php

namespace App\Http\Controllers\TaishiPk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\TaishiStrategy;
use App\Models\TaishiPkStrategy;

class StrategyController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug("taishi-pk")->first();

        // if ($GameTitle->slug == 'taishi') {
        //     $TaishiStrategies = TaishiStrategy::get();
        //     return view('strategy.index')->with([
        //             'GameTitle' => $GameTitle,
        //             'TaishiStrategies' => $TaishiStrategies,
        //     ]);
        // } elseif ($GameTitle->slug == 'taishi-pk') {
            $TaishiPkStrategies = TaishiPkStrategy::get();
            return view('strategy.index')->with([
                    'GameTitle' => $GameTitle,
                    'TaishiPkStrategies' => $TaishiPkStrategies,
            ]);
        // }
    }
}
