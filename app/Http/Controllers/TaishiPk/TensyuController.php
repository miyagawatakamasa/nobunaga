<?php

namespace App\Http\Controllers\TaishiPk;

use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\TaishiTensyu;
use Illuminate\Http\Request;

class TensyuController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug('taishi-pk')->first();
        $TaishiTensyus = TaishiTensyu::all();
        return view('taishi-pk.tensyu')->with([
            'GameTitle' => $GameTitle,
            'TaishiTensyus' => $TaishiTensyus,
          ]);
    }
}
