<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        return view('about.index');
    }

    public function release()
    {
        return view('about.release');
    }

    public function administrator()
    {
        return view('about.administrator');
    }
}
