<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleCategorySearch;
use App\Models\Disturbance;
use App\Models\HadouSamuraiParameter;
use App\Models\Samurai;
use App\Models\VoteCampaign;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SitemapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sitemap()
    {
        $now = Carbon::now();

        $Articles = Article::with('articleCategorySearches')->get();
        $ArticleCategories = ArticleCategory::get();

        $TaishiSamurais = Samurai::taishiPk()->get();
        $ShinseiSamurais = Samurai::shinsei()->get();
        $ShinseiPkSamurais = Samurai::shinseiPk()->get();
        $HadouSamuraiParameters = HadouSamuraiParameter::get();

        $VoteCampaigns = VoteCampaign::get();
        $Disturbances = Disturbance::get();

        return response()->view('sitemap', [
            'Articles' => $Articles,
            'ArticleCategories' => $ArticleCategories,
            'Disturbances' => $Disturbances,
            'HadouSamuraiParameters' => $HadouSamuraiParameters,
            'now' => $now,
            'TaishiSamurais' => $TaishiSamurais,
            'ShinseiSamurais' => $ShinseiSamurais,
            'ShinseiPkSamurais' => $ShinseiPkSamurais,
            'VoteCampaigns' => $VoteCampaigns,
        ])->header('Content-Type', 'text/xml');
    }
}
