<?php

namespace App\Http\Controllers\Privacy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivacyController extends Controller
{
    public function index()
    {
        return view('privacy_policy.index');
    }
}
