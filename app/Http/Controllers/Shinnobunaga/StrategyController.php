<?php

namespace App\Http\Controllers\Shinnobunaga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShinnobuSamuraiParameter;

class StrategyController extends Controller
{
    public function beginner()
    {
        return view('shinnobunaga.strategy.beginner');
    }

    public function resemara()
    {
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->orderBy('rank', 'desc')->get();

        return view('shinnobunaga.strategy.resemara')->with([
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }

    public function mukakin()
    {
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::get();

        return view('shinnobunaga.strategy.mukakin')->with([
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
        ]);
    }

    public function heirloom()
    {
        return view('shinnobunaga.strategy.heirloom');
    }
}
