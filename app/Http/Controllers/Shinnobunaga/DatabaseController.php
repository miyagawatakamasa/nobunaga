<?php

namespace App\Http\Controllers\Shinnobunaga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HadouSamuraiParameter;
use App\Models\Samurai;
use App\Models\SamuraiParameter;
use App\Models\SamuraiRelatedArticle;
use App\Models\ShinnobuSamuraiParameter;
use App\Models\WarSamurai;

class DatabaseController extends Controller
{
    public function samuraiIndex()
    {
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->get();

        return view('shinnobunaga.database.samurai.index')->with([
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }

    public function samuraiShow(Request $request)
    {
        $ShinnobuSamuraiParameter = ShinnobuSamuraiParameter::with('samurai')->find($request->samurai_id);
        $Samurai = Samurai::find($ShinnobuSamuraiParameter->samurai_id);
        $SamuraiRelatedArticles = SamuraiRelatedArticle::with(['article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->samuraiId($Samurai->id)->get();

        $NextShinnobuSamuraiParameter = ShinnobuSamuraiParameter::with('samurai')->find($request->samurai_id + 1);
        $BeforeShinnobuSamuraiParameter = ShinnobuSamuraiParameter::with('samurai')->find($request->samurai_id - 1);

        $TaishiPkSamuraiParameter = Samurai::taishiPk()->find($Samurai->id);
        $ShinseiSamuraiParameter = Samurai::shinsei()->find($Samurai->id);
        $HadouSamuraiParameter = HadouSamuraiParameter::with('samurai')->samuraiId($Samurai->id)->first();
        $ShinseiPkSamuraiParameter = Samurai::shinseiPk()->find($Samurai->id);

        $WarSamurais = WarSamurai::with(['war', 'war.disturbance'])->samuraiId($Samurai->id)->get()->sortBy([
            ['war.start_year', true],
            ['war.start_month', true],
            ['war.start_day', true],
        ]);

        return view('shinnobunaga.database.samurai.show')->with([
            'BeforeShinnobuSamuraiParameter' => $BeforeShinnobuSamuraiParameter,
            'NextShinnobuSamuraiParameter' => $NextShinnobuSamuraiParameter,
            'ShinnobuSamuraiParameter' => $ShinnobuSamuraiParameter,
            'Samurai' => $Samurai,
            'HadouSamuraiParameter' => $HadouSamuraiParameter,
            'ShinseiPkSamuraiParameter' => $ShinseiPkSamuraiParameter,
            'TaishiPkSamuraiParameter' => $TaishiPkSamuraiParameter,
            'ShinseiSamuraiParameter' => $ShinseiSamuraiParameter,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            'WarSamurais' => $WarSamurais,
          ]);
    }

    public function samuraiRanking()
    {
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->orderBy('rank', 'desc')->get();

        return view('shinnobunaga.database.samurai.ranking')->with([
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }

    public function samuraiRarity(Request $request)
    {
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->rarity($request->rarity)->get();

        return view('shinnobunaga.database.samurai.rarity')->with([
            'rarity' => $request->rarity,
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }

    public function appropriatePosition(Request $request)
    {
        $appropriate_position_hiragana = '';

        if ($request->appropriate_position == 'captain') {
            $appropriate_position_hiragana = '主将';
        } elseif ($request->appropriate_position == 'staff') {
            $appropriate_position_hiragana = '参謀';
        } elseif ($request->appropriate_position == 'pioneer') {
            $appropriate_position_hiragana = '先鋒';
        }
        
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->appropriatePosition($appropriate_position_hiragana)->get();

        return view('shinnobunaga.database.samurai.appropriate_position')->with([
            'appropriate_position' => $request->appropriate_position,
            'appropriate_position_hiragana' => $appropriate_position_hiragana,
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }

    public function appropriateArmy(Request $request)
    {
        $appropriate_army_hiragana = '';

        if ($request->appropriate_army == 'spear-soldier') {
            $appropriate_army_hiragana = '槍兵';
        } elseif ($request->appropriate_army == 'cavalry') {
            $appropriate_army_hiragana = '騎兵';
        } elseif ($request->appropriate_army == 'ninja') {
            $appropriate_army_hiragana = '忍者';
        } elseif ($request->appropriate_army == 'gun') {
            $appropriate_army_hiragana = '鉄砲';
        } elseif ($request->appropriate_army == 'odzu') {
            $appropriate_army_hiragana = '大筒';
        } elseif ($request->appropriate_army == 'farmer') {
            $appropriate_army_hiragana = '農兵';
        }
        $ShinnobuSamuraiParameters = ShinnobuSamuraiParameter::with('samurai')->appropriateArmy($appropriate_army_hiragana)->get();

        return view('shinnobunaga.database.samurai.appropriate_army')->with([
            'appropriate_army' => $request->appropriate_army,
            'appropriate_army_hiragana' => $appropriate_army_hiragana,
            'ShinnobuSamuraiParameters' => $ShinnobuSamuraiParameters,
          ]);
    }
}
