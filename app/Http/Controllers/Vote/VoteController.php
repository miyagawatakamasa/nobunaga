<?php

namespace App\Http\Controllers\Vote;

use App\Http\Controllers\Controller;
use App\Http\Requests\VotePost;
use App\Models\Samurai;
use App\Models\SamuraiRelatedArticle;
use App\Models\Vote;
use App\Models\VoteCampaign;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    public function index()
    {
        $VoteCampaigns = VoteCampaign::get();
        $totalVotes = count(Vote::get());

        return view('vote.index')->with([
            'totalVotes' => $totalVotes,
            'VoteCampaigns' => $VoteCampaigns,
          ]);
    }
    public function show(Request $request)
    {
        $VoteCampaign = VoteCampaign::find($request->vote_campaign_id);
        $Votes = Vote::voteCampaignId($request->vote_campaign_id)->orderBy('samurai_id', 'asc')->get();
        $totalVotes = count($Votes);
        $VoteResults = [];

        foreach ($Votes as $key => $Vote) {
            if (isset($VoteResults[$Vote->samurai_id]['count'])) {
                ++$VoteResults[$Vote->samurai_id]['count'];
            } else {
                $VoteResults[$Vote->samurai_id]['count'] = 1;
            }
            $VoteResults[$Vote->samurai_id]['samurai'] = $Vote->samurai;
        }

        uasort($VoteResults, function ($value1, $value2) {
            return $value2['count'] - $value1['count'];
        });

        $VoteReasons = Vote::voteCampaignId($request->vote_campaign_id)->issetReason()->orderBy('updated_at', 'desc')->get();

        return view('vote.campaign')->with([
            'totalVotes' => $totalVotes,
            'VoteCampaign' => $VoteCampaign,
            'VoteResults' => $VoteResults,
            'VoteReasons' => $VoteReasons,
        ]);
    }
    public function vote(Request $request)
    {
        $Samurais = samurai::get();
        $voteCampaignId = $request->vote_campaign_id;
        $VoteCampaign = VoteCampaign::find($voteCampaignId);

        return view('vote.vote')->with([
            'Samurais' => $Samurais,
            'voteCampaignId' => $voteCampaignId,
            'VoteCampaign' => $VoteCampaign,
          ]);
    }
    public function post(VotePost $request)
    {
        $CampaignVotes = Vote::voteCampaignId($request->vote_campanign_id)->ip($request->ip())->get();
        foreach ($CampaignVotes as $CampaignVote) {
            if ($CampaignVote->updated_at->isToday()) {
                return redirect()->route('vote_campaign.vote', [$request->vote_campaign_id])->with([
                    'errorMessage' => '1日に1度のみ投票できます。また投票できるようになるので明日お願いします！
                    他のお題も見てみよう。',
                  ]);
            }
        }

        $data['vote_campaign_id'] = $request->vote_campanign_id;
        $data['samurai_id'] = $request->samurai_id;
        $data['ip'] = $request->ip();
        if (isset($request->reason)) {
            $data['reason'] = $request->reason;
        }

        $Vote = Vote::create($data);
        $Vote->save();

        return redirect()->route('vote_campaign.complete', [$request->vote_campaign_id])->with([
            'samurai_id' => $request->samurai_id,
        ]);
    }
    public function complete(Request $request)
    {
        $SamuraiRelatedArticles = SamuraiRelatedArticle::samuraiId($request->samurai_id)->get();

        $VoteCampaign = VoteCampaign::find($request->vote_campaign_id);
        return view('vote.complete')->with([
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            'VoteCampaign' => $VoteCampaign,
          ]);
    }
}
