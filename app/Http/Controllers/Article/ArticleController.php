<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleCategorySearch;
use App\Models\SamuraiRelatedArticle;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $Articles = Article::with(['articleCategorySearches', 'articleCategorySearches.articleCategory'])->orderBy('created_at', 'desc')->with('articleCategorySearches')->paginate(20);

        return view('article.index')->with([
            'Articles' => $Articles,
        ]);
    }

    public function categoryIndex(Request $request)
    {
        $ArticleCategory = ArticleCategory::with(['articleCategorySearches', 'articleCategorySearches.article'])->slug($request->category_slug)->first();
        $ArticleCategorySearches = ArticleCategorySearch::with(['articleCategory', 'article'])->articleCategoryId($ArticleCategory->id)->orderBy('created_at', 'desc')->paginate(20);
        return view('article.category_index')->with([
            'ArticleCategory' => $ArticleCategory,
            'ArticleCategorySearches' => $ArticleCategorySearches,
        ]);
    }

    public function single(Request $request)
    {
        $Article = Article::with('articleCategorySearches')->find($request->article_id);
        $ArticleCategorySearches = ArticleCategorySearch::with(['articleCategory', 'article'])->articleId($Article->id)->with('articleCategory')->get();

        foreach ($ArticleCategorySearches as $ArticleCategorySearch) {
            if ($ArticleCategorySearch->is_main_category) {
                $MainArticleCategorySearch = $ArticleCategorySearch;
                break;
            }
        }
        $RelatedArticleCategorySearches = ArticleCategorySearch::with(['articleCategory', 'article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->articleCategoryId($MainArticleCategorySearch->article_category_id)->orderBy('created_at', 'desc')->take(10)->get();

        $Article['view_count'] = $Article->view_count + 1;
        $Article->save();

        $SamuraiRelatedArticles = SamuraiRelatedArticle::with('samurai')->articleId($request->article_id)->get();

        return view('article.single')->with([
            'Article' => $Article,
            'ArticleCategorySearches' => $ArticleCategorySearches,
            'MainArticleCategorySearch' => $MainArticleCategorySearch,
            'RelatedArticleCategorySearches' => $RelatedArticleCategorySearches,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
        ]);
    }

    public function example()
    {
        return view('article.example');
    }
}
