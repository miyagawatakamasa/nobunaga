<?php

namespace App\Http\Controllers\Other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DiagnoseSamurai;
use App\Models\DiagnoseSamuraiUser;

class DiagnoseController extends Controller
{
    public function index(Request $request)
    {
        $DiagnoseSamurais = DiagnoseSamurai::with('samurai')->get();

        $countData = [];

        foreach ($DiagnoseSamurais as $DiagnoseSamurai) {
            $DiagnoseSamuraiUsers = DiagnoseSamuraiUser::samuraiId($DiagnoseSamurai->samurai_id)->get();
            $countData[$DiagnoseSamurai->id] = count($DiagnoseSamuraiUsers);
        }
        $DiagnoseSamuraiUsers = DiagnoseSamuraiUser::get();
        $DiagnoseSamurai = DiagnoseSamurai::find($request->diagnose_samurai_id);

        $psychopathDiagnoseSamuraiUsersCount = count(DiagnoseSamuraiUser::isPsychopath()->get());

        return view('other.diagnose.index')->with([
            'DiagnoseSamurais' => $DiagnoseSamurais,
            'countData' => $countData,
            'DiagnoseSamurai' => $DiagnoseSamurai,
            'DiagnoseSamuraiUsers' => $DiagnoseSamuraiUsers,
            'isPsychopath' => $request->psychopath,
            'psychopathDiagnoseSamuraiUsersCount' => $psychopathDiagnoseSamuraiUsersCount,

        ]);
    }

    public function new(Request $request)
    {
        $DiagnoseSamurais = DiagnoseSamurai::get();
        return view('other.diagnose.new')->with([
            'DiagnoseSamurais' => $DiagnoseSamurais,
        ]);
    }

    public function create(Request $request)
    {
        $data = [
            'leadership' => 0,
            'brave' => 0,
            'wisdom' => 0,
            'affairs' => 0,
            'betrayal' => 0,
            'bright' => 0,
            'intuition' => 0,
            'precise' => 0,
            'attentive' => 0,
            'justice' => 0,
            'revolution' => 0,
            'dream' => 0,
            'patience' => 0,
            'advancement' => 0,
            'psychopath' => 0,
        ];
        if ($request->q1 == 1) {
            $data['dream'] += 10;
            $data['advancement'] += 1;
        } elseif ($request->q1 == 2) {
            $data['dream'] += 6;
        } elseif ($request->q1 == 3) {
            $data['dream'] += 4;
        } elseif ($request->q1 == 4) {
            $data['dream'] += 3;
        } elseif ($request->q1 == 5) {
            $data['dream'] += 2;
        }
        if ($request->q2 == 1) {
            $data['psychopath'] += 5;
            $data['justice'] += 5;
        } elseif ($request->q2 == 2) {
            $data['psychopath'] += 3;
        } elseif ($request->q2 == 3) {
            $data['psychopath'] += 2;
        } elseif ($request->q2 == 4) {
            $data['psychopath'] += 1;
        } elseif ($request->q2 == 5) {
        }
        if ($request->q3 == 1) {
            $data['advancement'] += 4;
            $data['psychopath'] += 5;
        } elseif ($request->q3 == 2) {
            $data['advancement'] += 3;
            $data['psychopath'] += 3;
        } elseif ($request->q3 == 3) {
            $data['advancement'] += 1;
        } elseif ($request->q3 == 4) {
        } elseif ($request->q3 == 5) {
        }
        if ($request->q4 == 1) {
            $data['bright'] += 15;
        } elseif ($request->q4 == 2) {
            $data['bright'] += 6;
        } elseif ($request->q4 == 3) {
            $data['bright'] += 3;
        } elseif ($request->q4 == 4) {
            $data['bright'] += 2;
        } elseif ($request->q4 == 5) {
            $data['bright'] += 1;
        }
        if ($request->q5 == 1) {
            $data['justice'] += 10;
        } elseif ($request->q5 == 2) {
            $data['justice'] += 4;
        } elseif ($request->q5 == 3) {
            $data['justice'] += 2;
        } elseif ($request->q5 == 4) {
            $data['justice'] += 1;
        } elseif ($request->q5 == 5) {
            $data['justice'] += 0;
        }
        if ($request->q6 == 1) {
            $data['attentive'] += 10;
        } elseif ($request->q6 == 2) {
            $data['attentive'] += 4;
        } elseif ($request->q6 == 3) {
            $data['attentive'] += 2;
        } elseif ($request->q6 == 4) {
            $data['attentive'] += 1;
        } elseif ($request->q6 == 5) {
            $data['attentive'] += 0;
        }
        if ($request->q7 == 1) {
            $data['patience'] += 0;
        } elseif ($request->q7 == 2) {
            $data['patience'] += 2;
        } elseif ($request->q7 == 3) {
            $data['patience'] += 6;
        } elseif ($request->q7 == 4) {
            $data['patience'] += 8;
        } elseif ($request->q7 == 5) {
            $data['patience'] += 15;
        }
        if ($request->q8 == 1) {
            $data['wisdom'] += 10;
        } elseif ($request->q8 == 2) {
            $data['wisdom'] += 4;
        } elseif ($request->q8 == 3) {
            $data['wisdom'] += 3;
        } elseif ($request->q8 == 4) {
            $data['wisdom'] += 2;
        } elseif ($request->q8 == 4) {
            $data['wisdom'] += 1;
        }
        if ($request->q9 == 1) {
            $data['intuition'] += 15;
        } elseif ($request->q9 == 2) {
            $data['intuition'] += 8;
        } elseif ($request->q9 == 3) {
            $data['intuition'] += 3;
        } elseif ($request->q9 == 4) {
            $data['intuition'] += 2;
        } elseif ($request->q9 == 4) {
            $data['intuition'] += 1;
        }
        if ($request->q10 = 1) {
            $data['brave'] += 10;
        } elseif ($request->q10 == 2) {
            $data['brave'] += 8;
        } elseif ($request->q10 == 3) {
            $data['brave'] += 3;
        } elseif ($request->q10 == 4) {
            $data['brave'] += 1;
        } elseif ($request->q10 == 4) {
            $data['brave'] += 0;
        }
        if ($request->q11 == 1) {
            $data['precise'] += 12;
        } elseif ($request->q11 == 2) {
            $data['precise'] += 8;
        } elseif ($request->q11 == 3) {
            $data['precise'] += 4;
        } elseif ($request->q11 == 4) {
            $data['precise'] += 2;
        } elseif ($request->q11 == 4) {
            $data['precise'] += 1;
        }
        if ($request->q12 == 1) {
            $data['advancement'] += 10;
        } elseif ($request->q12 == 2) {
            $data['advancement'] += 6;
        } elseif ($request->q12 == 3) {
            $data['advancement'] += 3;
        } elseif ($request->q12 == 4) {
            $data['advancement'] += 2;
        } elseif ($request->q12 == 4) {
            $data['advancement'] += 1;
        }
        if ($request->q13 == 1) {
            $data['affairs'] += 10;
        } elseif ($request->q13 == 2) {
            $data['affairs'] += 6;
        } elseif ($request->q13 == 3) {
            $data['affairs'] += 4;
        } elseif ($request->q13 == 4) {
            $data['affairs'] += 3;
        } elseif ($request->q13 == 4) {
            $data['affairs'] += 2;
        }
        if ($request->q14 == 1) {
            $data['leadership'] += 5;
        } elseif ($request->q14 == 2) {
            $data['leadership'] += 4;
        } elseif ($request->q14 == 3) {
            $data['leadership'] += 3;
        } elseif ($request->q14 == 4) {
            $data['leadership'] += 2;
        } elseif ($request->q14 == 4) {
            $data['leadership'] += 1;
        }
        if ($request->q15 == 1) {
            $data['revolution'] += 10;
        } elseif ($request->q14 == 2) {
            $data['revolution'] += 8;
        } elseif ($request->q14 == 3) {
            $data['revolution'] += 3;
        } elseif ($request->q14 == 4) {
            $data['revolution'] += 2;
        } elseif ($request->q14 == 4) {
            $data['revolution'] += 1;
        }
        if ($request->q16 == 1) {
            $data['betrayal'] += 15;
        } elseif ($request->q14 == 2) {
            $data['betrayal'] += 8;
        } elseif ($request->q14 == 3) {
            $data['betrayal'] += 3;
        } elseif ($request->q14 == 4) {
            $data['betrayal'] += 1;
        } elseif ($request->q14 == 4) {
            $data['betrayal'] += 0;
        }

        $dataPoint = [];
        $DiagnoseSamurais = DiagnoseSamurai::get();

        foreach ($DiagnoseSamurais as $DiagnoseSamurai) {
            $dataPoint[$DiagnoseSamurai->id] = 0;

            foreach ($data as $key => $datum) {
                $dataPoint[$DiagnoseSamurai->id] += sqrt(($datum - $DiagnoseSamurai[$key]) * ($datum - $DiagnoseSamurai[$key]));
            }
        }

        $yourDiagnoseSamuraiId = array_search(min($dataPoint), $dataPoint);

        $DiagnoseSamurai = DiagnoseSamurai::find($yourDiagnoseSamuraiId);
        $data['samurai_id'] = $DiagnoseSamurai->samurai_id;

        $isPsychopath = 0;
        if ($data['psychopath'] >= 10) {
            $isPsychopath = 1;
        }

        // 登録
        $DiagnoseSamuraiUser = DiagnoseSamuraiUser::create($data);
        $DiagnoseSamuraiUser->save();
        
        if ($isPsychopath) {
            return redirect()->route('other.diagnose.complete', $DiagnoseSamurai->id . '?psychopath=' . $isPsychopath);
        } else {
            return redirect()->route('other.diagnose.complete', $DiagnoseSamurai->id);
        }
    }

    public function complete(Request $request)
    {
        $DiagnoseSamurais = DiagnoseSamurai::with('samurai')->get();

        $countData = [];

        foreach ($DiagnoseSamurais as $DiagnoseSamurai) {
            $DiagnoseSamuraiUsers = DiagnoseSamuraiUser::samuraiId($DiagnoseSamurai->samurai_id)->get();
            $countData[$DiagnoseSamurai->id] = count($DiagnoseSamuraiUsers);
        }
        $DiagnoseSamuraiUsers = DiagnoseSamuraiUser::get();
        $DiagnoseSamurai = DiagnoseSamurai::with('samurai')->find($request->diagnose_samurai_id);

        $psychopathDiagnoseSamuraiUsersCount = count(DiagnoseSamuraiUser::isPsychopath()->get());

        return view('other.diagnose.complete')->with([
            'countData' => $countData,
            'DiagnoseSamurai' => $DiagnoseSamurai,
            'DiagnoseSamurais' => $DiagnoseSamurais,
            'DiagnoseSamuraiUsers' => $DiagnoseSamuraiUsers,
            'isPsychopath' => $request->psychopath,
            'psychopathDiagnoseSamuraiUsersCount' => $psychopathDiagnoseSamuraiUsersCount,
        ]);
    }
}
