<?php

namespace App\Http\Controllers\Other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Disturbance;
use App\Models\GameTitle;
use App\Models\Timeline;
use App\Models\War;
use App\Models\WarSamurai;
use App\Models\Word;


class OtherController extends Controller
{
    public function timeline(Request $request)
    {
        $Timelines = Timeline::orderBy('year', 'asc')->orderBy('month', 'asc')->get();
        return view('other.timeline')->with([
                'Timelines' => $Timelines,
            ]);
    }

    public function gameTitle()
    {
        $GameTitles = GameTitle::orderby('year', 'asc')->orderBy('month', 'asc')->get();
        return view('other.game_title')->with([
                'GameTitles' => $GameTitles,
            ]);
    }

    public function real()
    {
        return view('other.tactics');
    }

    public function war()
    {
        $Disturbances = Disturbance::with('wars')->orderBy('start_year', 'asc')->orderBy('start_month', 'asc')->get();

        return view('other.war')->with([
                'Disturbances' => $Disturbances,
            ]);
    }

    public function warSingle(Request $request)
    {
        $Disturbance = Disturbance::with('wars')->find($request->disturbance_id);

        $UniqueWarSamurais = collect([]);
        foreach ($Disturbance->wars as $war) {
            $WarSamurai = WarSamurai::with('samurai')->warId($war->id)->get();
            $UniqueWarSamurais = $UniqueWarSamurais->mergeRecursive($WarSamurai);
        }
        $UniqueWarSamurais = $UniqueWarSamurais->unique('samurai_id');
        return view('other.war_single')->with([
                'Disturbance' => $Disturbance,
                'UniqueWarSamurais' => $UniqueWarSamurais,
            ]);
    }

    public function vassals()
    {
        return view('other.vassals');
    }

    public function word()
    {
        $Words = Word::orderBy('name_kana', 'asc')->get();

        return view('other.word')->with([
                'Words' => $Words,
            ]);
    }
}
