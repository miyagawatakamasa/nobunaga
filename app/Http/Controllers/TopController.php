<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ArticleCategorySearch;
use App\Models\Info;
use App\Models\Article;
use App\Models\GameTitle;
use App\Models\VoteCampaign;

class TopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $RankingArticleCategorySearches = ArticleCategorySearch::with(['articleCategory', 'article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->articleCategoryId(1)->orderBy('updated_at', 'desc')->get();
        $Infos = Info::isDisplay()->orderBy('updated_at', 'desc')->get();
        $VoteCampaigns = VoteCampaign::orderBy('updated_at', 'desc')->get();
        $NewArticles = Article::orderBy('created_at', 'desc')->with(['articleCategorySearches','articleCategorySearches.articleCategory'])->take(8)->get();

        return view('index')->with([
            'RankingArticleCategorySearches' => $RankingArticleCategorySearches,
            'Infos' => $Infos,
            'NewArticles' => $NewArticles,
            'VoteCampaigns' => $VoteCampaigns
          ]);
    }
}
