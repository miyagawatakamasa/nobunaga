<?php

namespace App\Http\Controllers\Shinsei;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\HadouSamuraiParameter;
use App\Models\Samurai;
use App\Models\SamuraiRelatedArticle;
use App\Models\ShinnobuSamuraiParameter;
use App\Models\ShinseiPolicy;
use App\Models\ShinseiSamuraiParameter;
use App\Models\WarSamurai;


class ShinseiController extends Controller
{
    public function fight()
    {
        $GameTitle = GameTitle::slug('shinsei')->first();
        return view('shinsei.fight')->with([
            'GameTitle' => $GameTitle,
          ]);
    }
    public function scenario()
    {
        $GameTitle = GameTitle::slug('shinsei')->first();

        return view('shinsei.strategy')->with([
            'GameTitle' => $GameTitle,
          ]);
    }
    // public function domestic()
    // {
    //     $GameTitle = GameTitle::slug('shinsei')->first();

    //     return view('shinsei.domestic')->with([
    //         'GameTitle' => $GameTitle,
    //       ]);
    // }
    // public function battle()
    // {
    //     $GameTitle = GameTitle::slug('shinsei')->first();

    //     return view('shinsei.battle')->with([
    //         'GameTitle' => $GameTitle,
    //       ]);
    // }

    public function info()
    {
        return view('shinsei.info');
    }

    public function beginner()
    {
        return view('shinsei.beginner');
    }

    public function tensyou()
    {
        return view('shinsei.tensyou');
    }

    public function policy()
    {
        $ShinseiPolicies = ShinseiPolicy::get();
        return view('shinsei.policy')->with([
            'ShinseiPolicies' => $ShinseiPolicies,
          ]);;
    }

    public function show(Request $request)
    {
        $GameTitle = GameTitle::slug('shinsei')->first();
        $Samurai = Samurai::find($request->samurai_id);
        $ShinseiSamuraiParameter = ShinseiSamuraiParameter::with(['shinseiCharacteristicSearches', 'shinseiCharacteristicSearches.shinseiCharacteristic'])->samuraiId($Samurai->id)->first();
        // $SamuraiPkTactics = TaishiPkTactic::get();

        $SamuraiRelatedArticles = SamuraiRelatedArticle::with(['article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->samuraiId($Samurai->id)->get();

        $Samurais = Samurai::shinsei()->get();

        for ($i=1; $i < (count($Samurais) + 1) ; $i++) { 
            $NextSamurai = Samurai::shinsei()->find($request->samurai_id + $i);

            if (isset($NextSamurai)) {
                break;
            }
        }
        for ($i=1; $i < (count($Samurais) + 1) ; $i++) { 
            $BeforeSamurai = Samurai::shinsei()->find($request->samurai_id - $i);

            if (isset($BeforeSamurai)) {
                break;
            }
        }

        // $TaishiPkHeirloomSamurais = TaishiPkHeirloomSamurai::samuraiId($request->samurai_id)->get();

        $imgCount = count(glob("img/shinsei/samurai/" . $Samurai->id . "_" . "*.jpg"));

        $ShinnobuSamuraiParameter = ShinnobuSamuraiParameter::samuraiId($Samurai->id)->first();
        $TaishiPkSamuraiParameter = Samurai::taishiPk()->find($Samurai->id);
        $HadouSamuraiParameter = HadouSamuraiParameter::samuraiId($Samurai->id)->first();
        $ShinseiPkSamuraiParameter = Samurai::shinseiPk()->find($Samurai->id);

        $WarSamurais = WarSamurai::with(['war', 'war.disturbance', 'samurai', 'war.warSamurais', 'war.warSamurais.samurai'])->samuraiId($Samurai->id)->get()->sortBy([
            ['war.start_year', true],
            ['war.start_month', true],
            ['war.start_day', true],
        ]);

        $ShinseiPolicy = ShinseiPolicy::samuraiId($Samurai->id)->first();

        return view('shinsei.samurai')->with([
            'BeforeSamurai' => $BeforeSamurai,
            'GameTitle' => $GameTitle,
            'imgCount' => $imgCount,
            'NextSamurai' => $NextSamurai,
            'Samurai' => $Samurai,
            'Samurais' => $Samurais,
            'HadouSamuraiParameter' => $HadouSamuraiParameter,
            'ShinseiSamuraiParameter' => $ShinseiSamuraiParameter,
            'ShinseiPkSamuraiParameter' => $ShinseiPkSamuraiParameter,
            'ShinnobuSamuraiParameter' => $ShinnobuSamuraiParameter,
            'ShinseiPolicy' => $ShinseiPolicy,
            'TaishiPkSamuraiParameter' => $TaishiPkSamuraiParameter,

            // 'TaishiPkTactics' => $SamuraiPkTactics,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            // 'TaishiPkHeirloomSamurais' => $TaishiPkHeirloomSamurais,
            'WarSamurais' => $WarSamurais,
        ]);
    }

}
