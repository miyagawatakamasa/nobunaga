<?php

namespace App\Http\Controllers\Scenario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\Scenario;

class ScenarioController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();
        if (!isset($GameTitle)) {
            return redirect()->route('top');
        }

        $Scenarios = Scenario::gameTitleId($GameTitle->id)->orderBy('year', 'asc')->get();

        return view('scenario.index')->with([
            'GameTitle' => $GameTitle,
            'Scenarios' => $Scenarios,
          ]);
    }

    public function recommend(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();

        return view('scenario.recommend')->with([
            'GameTitle' => $GameTitle,
          ]);
    }
}
