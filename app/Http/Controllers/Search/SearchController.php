<?php

namespace App\Http\Controllers\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Samurai;
use App\Models\Word;
use App\Models\Article;
use App\Models\Search;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $q = '';
        if ($request->q) {
            $q = $request->q;
        }

        // 部将
        $samuraiQuery = Samurai::query()->orderBy('furigana', 'asc');
        $Samurais = [];

        if ($q) {
            $samuraiQuery->where('name', 'like', '%' . $q . '%')
            ->orWhere('furigana', 'like', '%' . $q . '%');
            $Samurais = $samuraiQuery->get();
        }

        // 用語
        $wordQuery = Word::query()->orderBy('name_kana', 'asc');
        $Words = [];

        if ($q) {
            $wordQuery->where('name', 'like', '%' . $q . '%')
            ->orWhere('name_kana', 'like', '%' . $q . '%');
            $Words = $wordQuery->get();
        }

        // 記事
        $articleQuery = Article::query()->orderBy('title', 'asc');
        $Articles = [];

        if ($q) {
            $articleQuery->where('title', 'like', '%' . $q . '%');
            $Articles = $articleQuery->get();
        }

        if ($q) {
            $insertData = [];
            $insertData['text'] = $q;

            $TheSearch = Search::text($q)->first();

            if (isset($TheSearch)) {
                $TheSearch->count = $TheSearch->count + 1;
                $TheSearch->update();
            } else {
                $Search = Search::create($insertData);
                $Search->save();
            }
        }

        return view('search.index')->with([
            'q' => $q,
            'Samurais' => $Samurais,
            'Words' => $Words,
            'Articles' => $Articles,
        ]);
    }
}
