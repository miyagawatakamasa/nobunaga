<?php

namespace App\Http\Controllers\Hadou;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\Samurai;
use App\Models\SamuraiRelatedArticle;
use App\Models\HadouSamuraiParameter;
use App\Models\HadouSecretSearch;
use App\Models\HadouSkillSearch;
use App\Models\ShinnobuSamuraiParameter;
use App\Models\WarSamurai;

class HadouController extends Controller
{
    public function samuraiIndex()
    {

        $HadouSamuraiParameters = HadouSamuraiParameter::with('samurai')->get();

        return view('hadou.samurai_index')->with([
            'HadouSamuraiParameters' => $HadouSamuraiParameters,
        ]);
    }

    public function samuraiShow(Request $request)
    {
        $GameTitle = GameTitle::slug('shinsei')->first();
        $Samurai = Samurai::find($request->samurai_id);
        $HadouSamuraiParameter = HadouSamuraiParameter::with('samurai')->samuraiId($Samurai->id)->first();
        $HadouSkillSearches = HadouSkillSearch::with('hadouSkill')->hadouSamuraiParameterId($HadouSamuraiParameter->id)->get();
        $HadouSecretSearch = HadouSecretSearch::hadouSamuraiParameterId($HadouSamuraiParameter->id)->first();

        // $SamuraiPkTactics = TaishiPkTactic::get();

        $SamuraiRelatedArticles = SamuraiRelatedArticle::with(['article', 'article.articleCategorySearches', 'article.articleCategorySearches.articleCategory'])->samuraiId($Samurai->id)->get();

        // $Samurais = Samurai::shinsei()->get();

        $NextSamurai = HadouSamuraiParameter::with('samurai')->find($HadouSamuraiParameter->id + 1);
        $BeforeSamurai = HadouSamuraiParameter::with('samurai')->find($HadouSamuraiParameter->id - 1);

        $ShinnobuSamuraiParameter = ShinnobuSamuraiParameter::samuraiId($Samurai->id)->first();
        $TaishiPkSamuraiParameter = Samurai::taishiPk()->find($Samurai->id);
        $ShinseiSamuraiParameter = Samurai::shinsei()->find($Samurai->id);
        $ShinseiPkSamuraiParameter = Samurai::shinseiPk()->find($Samurai->id);

        $WarSamurais = WarSamurai::with(['war', 'war.disturbance', 'samurai', 'war.warSamurais', 'war.warSamurais.samurai'])->samuraiId($Samurai->id)->get()->sortBy([
            ['war.start_year', true],
            ['war.start_month', true],
            ['war.start_day', true],
        ]);

        return view('hadou.samurai_show')->with([
            'BeforeSamurai' => $BeforeSamurai,
            'GameTitle' => $GameTitle,
            'NextSamurai' => $NextSamurai,
            'Samurai' => $Samurai,
            // 'Samurais' => $Samurais,
            'ShinseiSamuraiParameter' => $ShinseiSamuraiParameter,
            'ShinnobuSamuraiParameter' => $ShinnobuSamuraiParameter,
            'TaishiPkSamuraiParameter' => $TaishiPkSamuraiParameter,
            'HadouSamuraiParameter' => $HadouSamuraiParameter,
            'HadouSecretSearch' => $HadouSecretSearch,
            'HadouSkillSearches' => $HadouSkillSearches,
            'ShinseiPkSamuraiParameter' => $ShinseiPkSamuraiParameter,
            
            // 'TaishiPkTactics' => $SamuraiPkTactics,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            // 'TaishiPkHeirloomSamurais' => $TaishiPkHeirloomSamurais,
            'WarSamurais' => $WarSamurais,
        ]);
    }
    public function samuraiRarity(Request $request)
    {
        $HadouSamuraiParameters = HadouSamuraiParameter::with('samurai')->rarity($request->rarity)->get();

        return view('hadou.rarity')->with([
            'rarity' => $request->rarity,
            'HadouSamuraiParameters' => $HadouSamuraiParameters,
          ]);

    }

    public function abilityScore(Request $request)
    {
        $HadouSamuraiParameters = HadouSamuraiParameter::get();

        return view('hadou.ability_score')->with([
            'HadouSamuraiParameters' => $HadouSamuraiParameters,
        ]);
    }
}
