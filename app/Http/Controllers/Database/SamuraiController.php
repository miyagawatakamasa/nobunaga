<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;
use App\Models\Samurai;
use App\Models\SamuraiParameter;
use App\Models\HadouSamuraiParameter;
use App\Models\ShinnobuSamuraiParameter;
use App\Models\TaishiStrategy;
use App\Models\TaishiPkHeirloomSamurai;
use App\Models\TaishiPkTactic;
use App\Models\SamuraiRelatedArticle;
use App\Models\ShinseiSamuraiParameter;
use App\Models\WarSamurai;

class SamuraiController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();
        if (!isset($GameTitle)) {
            return redirect()->route('top');
        }

        $Samurais = Samurai::orderBy('furigana', 'asc')->get();

        if ($request->game_series_slug == 'taishi-pk') {
            $Samurais = Samurai::taishiPk()->orderBy('furigana', 'asc')->get();
        }
        if ($request->game_series_slug == 'shinsei') {
            $Samurais = Samurai::shinsei()->orderBy('furigana', 'asc')->get();
        }

        return view('database.samurais.index')->with([
            'Samurais' => $Samurais,
            'GameTitle' => $GameTitle,
          ]);
    }

    public function show(Request $request)
    {
        // 落ち着いたら削除
        if ($request->game_series_slug == 'taishi') {
            return redirect()->route('database.samurais.show', ['taishi-pk', $request->samurai_id]);
        }
        if ($request->game_series_slug == 'taishi_pk') {
            return redirect()->route('database.samurais.show', ['taishi-pk', $request->samurai_id]);
        }
        if ($request->game_series_slug == 'shinsei') {
            return redirect()->route('shinsei.database.samurais.show', [$request->samurai_id]);
        }
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();
        $Samurai = Samurai::find($request->samurai_id);
        $SamuraiParameter = SamuraiParameter::gameTitleId($GameTitle->id)->samuraiId($Samurai->id)->first();
        $SamuraiPkTactics = TaishiPkTactic::get();

        $SamuraiRelatedArticles = SamuraiRelatedArticle::samuraiId($Samurai->id)->get();

        $Samurais = Samurai::taishiPk()->get();
        for ($i=1; $i < (count($Samurais) + 1) ; $i++) { 
            $NextSamurai = Samurai::taishiPk()->find($request->samurai_id + $i);

            if (isset($NextSamurai)) {
                break;
            }
        }
        for ($i=1; $i < (count($Samurais) + 1) ; $i++) { 
            $BeforeSamurai = Samurai::taishiPk()->find($request->samurai_id - $i);

            if (isset($BeforeSamurai)) {
                break;
            }
        }

        $TaishiPkHeirloomSamurais = TaishiPkHeirloomSamurai::samuraiId($request->samurai_id)->get();

        $imgCount = count(glob("img/samurai/min/" . $Samurai->id . "_" . "*.jpg"));

        $ShinnobuSamuraiParameter = ShinnobuSamuraiParameter::samuraiId($Samurai->id)->first();
        $ShinseiSamuraiParameter = Samurai::shinsei()->find($request->samurai_id);
        $HadouSamuraiParameter = HadouSamuraiParameter::samuraiId($Samurai->id)->first();
        $ShinseiPkSamuraiParameter = Samurai::shinseiPk()->find($Samurai->id);

        $WarSamurais = WarSamurai::samuraiId($Samurai->id)->get()->sortBy([
            ['war.start_year', true],
            ['war.start_month', true],
            ['war.start_day', true],
        ]);

        return view('database.samurais.samurai')->with([
            'BeforeSamurai' => $BeforeSamurai,
            'GameTitle' => $GameTitle,
            'imgCount' => $imgCount,
            'NextSamurai' => $NextSamurai,
            'Samurai' => $Samurai,
            'Samurais' => $Samurais,
            'SamuraiParameter' => $SamuraiParameter,
            'HadouSamuraiParameter' => $HadouSamuraiParameter,
            'ShinseiPkSamuraiParameter' => $ShinseiPkSamuraiParameter,
            'ShinnobuSamuraiParameter' => $ShinnobuSamuraiParameter,
            'ShinseiSamuraiParameter' => $ShinseiSamuraiParameter,
            'TaishiPkTactics' => $SamuraiPkTactics,
            'SamuraiRelatedArticles' => $SamuraiRelatedArticles,
            'TaishiPkHeirloomSamurais' => $TaishiPkHeirloomSamurais,
            'WarSamurais' => $WarSamurais,
        ]);
    }

    public function redirectShow(Request $request)
    {
        $GameTitle = GameTitle::slug(config('const.gameTitle'))->first();

        $shinseiSamurai = Samurai::shinsei()->find($request->samurai_id);
        $taishiPkSamurai = Samurai::taishiPk()->find($request->samurai_id);

        if (isset($shinseiSamurai)) {
            return redirect()->route('shinsei.database.samurais.show', [$request->samurai_id]);
        } elseif (isset($taishiPkSamurai)) {
            return redirect()->route('database.samurais.show', [$GameTitle->slug, $request->samurai_id]);
        } else {
            return redirect()->route('top');
        }
    }

    public function ranking(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();
        if ($GameTitle->slug == 'taishi-pk') {
            $SamuraiParameters = SamuraiParameter::with('samurai')->get();
        } elseif ($GameTitle->slug == 'shinsei') {
            $SamuraiParameters = ShinseiSamuraiParameter::with('samurai')->get();
        }
        // $SamuraiParameters = SamuraiParameter::with('samurai')->get();

        return view('database.samurais.ranking')->with([
            'GameTitle' => $GameTitle,
            'SamuraiParameters' => $SamuraiParameters,
          ]);
    }
}
