<?php

namespace App\Http\Controllers\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GameTitle;

class WillController extends Controller
{
    public function index(Request $request)
    {
        $GameTitle = GameTitle::slug($request->game_series_slug)->first();

        return view('database.will.index')->with([
            'GameTitle' => $GameTitle,
          ]);
    }
}
