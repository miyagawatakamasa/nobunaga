<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VotePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'samurai_id' => 'required|not_undefined',
            // 'vote_campaign_id' => 'required|integer',
        ];
    }
}
