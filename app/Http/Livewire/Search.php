<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Samurai;
use App\Models\SamuraiName;

class Search extends Component
{
    public $search = '';

    public function render()
    {
        $samuraiQuery = Samurai::query()->active()->orderBy('furigana', 'asc');
        $Samurais = [];
        if ($this->search) {
            $samuraiQuery->where('name', 'like', '%' . $this->search . '%')
            ->orWhere('furigana', 'like', '%' . $this->search . '%');
            $Samurais = $samuraiQuery->take(15)->get();
        }

        $samuraiNameQuery = SamuraiName::query()->active()->orderBy('search_word_kana', 'asc');
        $SamuraiNames = [];
        if ($this->search) {
            $samuraiNameQuery->where('search_word', 'like', '%' . $this->search . '%')
            ->orWhere('search_word_kana', 'like', '%' . $this->search . '%');
            $SamuraiNames = $samuraiNameQuery->take((15 - count($Samurais)))->get();
        }

        return view('livewire.search')
        ->with([
            'Samurais' => $Samurais,
            'SamuraiNames' => $SamuraiNames,
        ]);
    }
}
