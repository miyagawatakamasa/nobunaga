<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Samurai;
use App\Models\SamuraiName;
use App\Models\ShinseiPkSamuraiParameter;
use App\Models\ShinseiPkSkill;

class ShinseiPkSamurai extends Component
{
    public $search = '';
    public $leadership_min = '';
    public $leadership_max = '';
    public $brave_min = '';
    public $brave_max = '';
    public $wisdom_min = '';
    public $wisdom_max = '';
    public $affairs_min = '';
    public $affairs_max = '';
    public $all_min = '';
    public $all_max = '';
    public $characteristic = '';
    public $tactics = '';
    public $head_of_house = '';
    public $magistrate = '';

    public function render()
    {
        $samuraiQuery = ShinseiPkSamuraiParameter::query()->orderBy('furigana', 'asc');
        $Samurais = [];
        if ($this->search) {
            $samuraiQuery->where('name', 'like', '%' . $this->search . '%')
            ->orWhere('furigana', 'like', '%' . $this->search . '%');
        }

        if ($this->leadership_min) {
            $samuraiQuery->where('leadership', '>=', $this->leadership_min);
        }
        if ($this->leadership_max) {
            $samuraiQuery->where('leadership', '<=', $this->leadership_max);
        }
        if ($this->brave_min) {
            $samuraiQuery->where('brave', '>=', $this->brave_min);
        }
        if ($this->brave_max) {
            $samuraiQuery->where('brave', '<=', $this->brave_max);
        }
        if ($this->wisdom_min) {
            $samuraiQuery->where('wisdom', '>=', $this->wisdom_min);
        }
        if ($this->wisdom_max) {
            $samuraiQuery->where('wisdom', '<=', $this->wisdom_max);
        }
        if ($this->affairs_min) {
            $samuraiQuery->where('affairs', '>=', $this->affairs_min);
        }
        if ($this->affairs_max) {
            $samuraiQuery->where('affairs', '<=', $this->affairs_max);
        }
        if ($this->all_min) {
            $samuraiQuery->where('all', '>=', $this->all_min);
        }
        if ($this->all_max) {
            $samuraiQuery->where('all', '<=', $this->all_max);
        }
        if ($this->characteristic) {
            $samuraiQuery->where(function ($query) {
                $query->where('characteristic1', $this->characteristic)
                    ->orWhere('characteristic2', $this->characteristic)
                    ->orWhere('characteristic3', $this->characteristic);
            });
        }
        if ($this->tactics) {
            $samuraiQuery->where('tactics', $this->tactics);
        }
        if ($this->head_of_house) {
            $samuraiQuery->where('head_of_house', $this->head_of_house);
        }
        if ($this->magistrate) {
            $samuraiQuery->where('magistrate', $this->magistrate);
        }

        $Samurais = $samuraiQuery->take(15)->get();

        // $samuraiNameQuery = SamuraiName::query()->active()->orderBy('search_word_kana', 'asc');
        // $SamuraiNames = [];
        // if ($this->search) {
        //     $samuraiNameQuery->where('search_word', 'like', '%' . $this->search . '%')
        //     ->orWhere('search_word_kana', 'like', '%' . $this->search . '%');
        //     $SamuraiNames = $samuraiNameQuery->take((15 - count($Samurais)))->get();
        // }

        $ShinseiPkSkills = ShinseiPkSkill::get();

        return view('livewire.shinsei-pk-samurai')
        ->with([
            'Samurais' => $Samurais,
            // 'SamuraiNames' => $SamuraiNames,
            'ShinseiPkSkills' => $ShinseiPkSkills,
        ]);
    }
}
