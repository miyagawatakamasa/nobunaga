<?php

namespace App\Http\Livewire;

use Livewire\Component;

class OldCountry extends Component
{
    public function render()
    {
        return view('livewire.old-country');
    }
}
