<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SpMenu extends Component
{
    public $isActive = 0;

    public function isActive(){
        if ($this->isActive) {
            $this->isActive = 0;
        } else {
            $this->isActive = 1;
        }
    }
    public function render()
    {
        return view('livewire.sp-menu');
    }
}
