<?php

namespace App\Http\Middleware;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\GameTitle;
use App\Models\Samurai;
use App\Models\Timeline;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\View\Factory;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function __construct(Factory $viewFactory, AuthManager $authManager)
    {
        $this->viewFactory = $viewFactory;
        $this->authManager = $authManager;
    }

    public function handle(Request $request, Closure $next)
    {
        $GameTitle = GameTitle::slug(config('const.gameTitle'))->first();
        $OldContries = config('const.oldCountry');
        $NewArticles = Article::with('articleCategorySearches')->orderBy('created_at', 'desc')->take(8)->get();
        $RankingArticles = Article::with('articleCategorySearches')->orderBy('view_count', 'desc')->take(10)->get();
        $RandomSamurai = Samurai::inRandomOrder()->first();
        $RecommendArticles = Article::with(['articleCategorySearches', 'articleCategorySearches.articleCategory'])->isRecommend()->orderBy('recommend_order', 'asc')->with('articleCategorySearches')->get();
        $TextSliderTimelines = Timeline::isToday()->get();
        $ArticleCategories = ArticleCategory::orderBy('order', 'asc')->with('articleCategorySearches')->get();

        $this->viewFactory->share('GameTitle', $GameTitle);
        $this->viewFactory->share('OldContries', $OldContries);
        $this->viewFactory->share('NewArticles', $NewArticles);
        $this->viewFactory->share('RankingArticles', $RankingArticles);
        $this->viewFactory->share('RandomSamurai', $RandomSamurai);
        $this->viewFactory->share('RecommendArticles', $RecommendArticles);
        $this->viewFactory->share('TextSliderTimelines', $TextSliderTimelines);
        $this->viewFactory->share('ArticleCategories', $ArticleCategories);

        return $next($request);
    }
}
