const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/header.js', 'public/js')
    .js('resources/js/adsense.js', 'public/js')
    .js('resources/js/overlay.js', 'public/js')
    .js('resources/js/samurai_kana.js', 'public/js')
    .js('resources/js/sort.js', 'public/js')
    .js('resources/js/table_of_contents.js', 'public/js')
    .sass('resources/sass/common.scss', 'public/css')
    .sass('resources/sass/contact.scss', 'public/css')
    .sass('resources/sass/diagnose.scss', 'public/css')
    .sass('resources/sass/form.scss', 'public/css')
    .sass('resources/sass/reset.scss', 'public/css')
    .sass('resources/sass/samurai.scss', 'public/css')
    .sass('resources/sass/single.scss', 'public/css')
    .sass('resources/sass/table.scss', 'public/css')
    .sass('resources/sass/top.scss', 'public/css')
    .sass('resources/sass/vote.scss', 'public/css')
    .sass('resources/sass/war.scss', 'public/css');

mix.styles([
    'public/css/reset.css',
    'public/css/common.css',
    'public/css/table.css',
    'public/css/single.css'
    ], 'public/css/all.css');

mix.js([
    'public/js/overlay.js',
    'public/js/header.js',
    'public/js/adsense.js'
    ], 'public/js/all.js');