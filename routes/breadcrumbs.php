<?php

// Top
Breadcrumbs::for('top', function ($trail) {
    $trail->push('ホーム', route('top'));
});

Breadcrumbs::for('privacy', function ($trail) {
    $trail->parent('about');
    $trail->push('プライバシーポリシー', route('privacy'));
});

Breadcrumbs::for('contact', function ($trail) {
    $trail->parent('top');
    $trail->push('お問い合わせ', route('contact'));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('top');
    $trail->push('このサイトについて', route('about'));
});

Breadcrumbs::for('release', function ($trail) {
    $trail->parent('about');
    $trail->push('リリース情報', route('release'));
});

Breadcrumbs::for('administrator', function ($trail) {
    $trail->parent('about');
    $trail->push('管理人について', route('administrator'));
});

Breadcrumbs::for('search', function ($trail, $q) {
    $trail->parent('top');
    $trail->push('「' . $q . '」の検索結果', route('search'));
});

Breadcrumbs::for('other.timeline', function ($trail) {
    $trail->parent('top');
    $trail->push('戦国年表', route('other.timeline'));
});

Breadcrumbs::for('other.game_title', function ($trail) {
    $trail->parent('top');
    $trail->push('ゲームタイトル一覧', route('other.game_title'));
});

Breadcrumbs::for('other.war', function ($trail) {
    $trail->parent('top');
    $trail->push('戦国時代の合戦一覧' , route('other.war'));
});
Breadcrumbs::for('other.war.single', function ($trail, $Disturbance) {
    $trail->parent('other.war');
    $trail->push($Disturbance->name , route('other.war.single', $Disturbance->id));
});

Breadcrumbs::for('other.tactics', function ($trail) {
    $trail->parent('top');
    $trail->push('戦法一覧' , route('other.tactics'));
});

Breadcrumbs::for('other.vassals', function ($trail) {
    $trail->parent('top');
    $trail->push('家臣団総称' , route('other.vassals'));
});

// 武将診断
Breadcrumbs::for('other.diagnose.index', function ($trail) {
    $trail->parent('top');
    $trail->push('武将診断メーカー' , route('other.diagnose.index'));
});
Breadcrumbs::for('other.diagnose.new', function ($trail) {
    $trail->parent('other.diagnose.index');
    $trail->push('武将診断テスト' , route('other.diagnose.new'));
});
Breadcrumbs::for('other.diagnose.complete', function ($trail, $DiagnoseSamurai) {
    $trail->parent('other.diagnose.index');
    $trail->push('武将診断結果', route('other.diagnose.complete', $DiagnoseSamurai->id));
});

Breadcrumbs::for('other.word', function ($trail) {
    $trail->parent('top');
    $trail->push('戦国時代用語集' , route('other.word'));
});

Breadcrumbs::for('article.index', function ($trail) {
    $trail->parent('top');
    $trail->push('記事一覧', route('article.index'));
});

Breadcrumbs::for('article.category.index', function ($trail, $ArticleCategory) {
    $trail->parent('article.index');
    $trail->push('[' . $ArticleCategory->name . ']記事一覧', route('article.category.index', $ArticleCategory->slug));
});

Breadcrumbs::for('article.single', function ($trail, $ArticleCategory, $Article) {
    $trail->parent('article.category.index', $ArticleCategory);
    $trail->push($Article->title, route('article.single', $Article->id));
});

// 新信長の野望 攻略
Breadcrumbs::for('shinnobunaga.strategy.beginner', function ($trail) {
    $trail->parent('top');
    $trail->push('序盤の進め方【新信長の野望】' , route('shinnobunaga.strategy.beginner'));
});
// Breadcrumbs::for('shinnobunaga.strategy.quiz', function ($trail) {
//     $trail->parent('top');
//     $trail->push('序盤の進め方【新信長の野望】' , route('shinnobunaga.strategy.quiz'));
// });
Breadcrumbs::for('shinnobunaga.gacha.kokushimusou', function ($trail) {
    $trail->parent('top');
    $trail->push('国士無双ガチャおすすめ【新信長の野望】' , route('shinnobunaga.gacha.kokushimusou'));
});
Breadcrumbs::for('shinnobunaga.strategy.resemara', function ($trail) {
    $trail->parent('top');
    $trail->push('リセマラ当たりランキング【新信長の野望】' , route('shinnobunaga.strategy.resemara'));
});
Breadcrumbs::for('shinnobunaga.strategy.mukakin', function ($trail) {
    $trail->parent('top');
    $trail->push('無課金向けキャラランキング【新信長の野望】' , route('shinnobunaga.strategy.mukakin'));
});
Breadcrumbs::for('shinnobunaga.strategy.heirloom', function ($trail) {
    $trail->parent('top');
    $trail->push('家宝システム【新信長の野望】' , route('shinnobunaga.strategy.heirloom'));
});
Breadcrumbs::for('shinnobunaga.database.samurais', function ($trail) {
    $trail->parent('top');
    $trail->push('武将名鑑【新信長の野望】' , route('shinnobunaga.database.samurais'));
});
Breadcrumbs::for('shinnobunaga.database.samurais.show', function ($trail, $ShinnobuSamuraiParameter) {
    $trail->parent('shinnobunaga.database.samurais');
    $trail->push($ShinnobuSamuraiParameter->samurai->name . '【新信長の野望】' , route('shinnobunaga.database.samurais.show', $ShinnobuSamuraiParameter->id));
});
Breadcrumbs::for('shinnobunaga.database.samurais.show.rarity', function ($trail, $rarity) {
    $trail->parent('shinnobunaga.database.samurais');
    $trail->push(strtoupper($rarity) . 'キャラ一覧【新信長の野望】' , route('shinnobunaga.database.samurais.show.rarity', $rarity));
});


// 覇道 攻略
Breadcrumbs::for('hadou.database.samurais', function ($trail) {
    $trail->parent('top');
    $trail->push('武将名鑑【覇道】' , route('hadou.database.samurais'));
});
Breadcrumbs::for('hadou.database.samurais.show', function ($trail, $HadouSamuraiParameter) {
    $trail->parent('hadou.database.samurais');
    $trail->push($HadouSamuraiParameter->samurai->name . '【覇道】' , route('hadou.database.samurais.show', $HadouSamuraiParameter->id));
});
Breadcrumbs::for('hadou.database.samurais.show.rarity', function ($trail, $rarity) {
    $trail->parent('hadou.database.samurais');
    $trail->push(strtoupper($rarity) . 'キャラ一覧【覇道】' , route('hadou.database.samurais.show.rarity', $rarity));
});


Breadcrumbs::for('database.samurais', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('武将名鑑【' . $GameTitle->name . '】' , route('database.samurais', $GameTitle->slug));
});

Breadcrumbs::for('database.ranking', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('能力値ランキング【' . $GameTitle->name . '】' , route('database.ranking', $GameTitle->slug));
});

Breadcrumbs::for('scenario.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('シナリオ【' . $GameTitle->name . '】' , route('scenario.index', $GameTitle->slug));
});

Breadcrumbs::for('scenario.recommend', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('おすすめシナリオ【' . $GameTitle->name . '】' , route('scenario.recommend', $GameTitle->slug));
});


// 大志PK 攻略
Breadcrumbs::for('capture.military', function ($trail) {
    $trail->parent('top');
    $trail->push('軍事のコツ【大志（パワーアップキット）】' , route('capture.military'));
});
Breadcrumbs::for('capture.march', function ($trail) {
    $trail->parent('top');
    $trail->push('行軍のコツ【大志（パワーアップキット）】' , route('capture.march'));
});
Breadcrumbs::for('capture.decisive_battle', function ($trail) {
    $trail->parent('top');
    $trail->push('決戦のコツ【大志（パワーアップキット）】' , route('capture.decisive_battle'));
});
Breadcrumbs::for('capture.siege', function ($trail) {
    $trail->parent('top');
    $trail->push('攻城戦のコツ【大志（パワーアップキット）】' , route('capture.siege'));
});
Breadcrumbs::for('capture.domestic_affairs', function ($trail) {
    $trail->parent('top');
    $trail->push('内政のコツ【大志（パワーアップキット）】' , route('capture.domestic_affairs'));
});
Breadcrumbs::for('capture.commercial', function ($trail) {
    $trail->parent('top');
    $trail->push('商業のコツ【大志（パワーアップキット）】' , route('capture.commercial'));
});
Breadcrumbs::for('capture.agriculture', function ($trail) {
    $trail->parent('top');
    $trail->push('農業のコツ【大志（パワーアップキット）】' , route('capture.agriculture'));
});
Breadcrumbs::for('capture.diplomacy', function ($trail) {
    $trail->parent('top');
    $trail->push('外交のコツ【大志（パワーアップキット）】' , route('capture.diplomacy'));
});
Breadcrumbs::for('capture.plot', function ($trail) {
    $trail->parent('top');
    $trail->push('調略のコツ【大志（パワーアップキット）】' , route('capture.plot'));
});
Breadcrumbs::for('capture.general_contract', function ($trail) {
    $trail->parent('top');
    $trail->push('普請のコツ【大志（パワーアップキット）】' , route('capture.general_contract'));
});

Breadcrumbs::for('capture.human_resources', function ($trail) {
    $trail->parent('top');
    $trail->push('人事のコツ【大志（パワーアップキット）】' , route('capture.human_resources'));
});
Breadcrumbs::for('capture.will', function ($trail) {
    $trail->parent('top');
    $trail->push('志について【大志（パワーアップキット）】' , route('capture.will'));
});

Breadcrumbs::for('taimei.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('大命【' . $GameTitle->name . '】' , route('taimei.index'));
});

Breadcrumbs::for('taishi-pk.strategy.index', function ($trail) {
    $trail->parent('top');
    $trail->push('作戦【大志（パワーアップキット）】' , route('taishi-pk.strategy.index'));
});

Breadcrumbs::for('tactics.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('戦法【' . $GameTitle->name . '】' , route('tactics.index', $GameTitle->slug));
});

Breadcrumbs::for('resource.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('資源【' . $GameTitle->name . '】' , route('resource.index'));
});

Breadcrumbs::for('tensyu.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('天守【' . $GameTitle->name . '】' , route('tensyu.index', [$GameTitle->slug]));
});

Breadcrumbs::for('facility.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('設備【' . $GameTitle->name . '】' , route('facility.index', [$GameTitle->slug]));
});

Breadcrumbs::for('building.index', function ($trail, $GameTitle) {
    $trail->parent('top');
    $trail->push('施設【' . $GameTitle->name . '】' , route('building.index', [$GameTitle->slug]));
});

Breadcrumbs::for('database.samurais.show', function ($trail, $GameTitle, $Samurai) {
    $trail->parent('database.samurais', $GameTitle);
    $trail->push($Samurai->name . '【' . $GameTitle->name . '】' , route('database.samurais.show', [$GameTitle->slug, $Samurai->id]));
});

Breadcrumbs::for('vote_campaign.index', function ($trail) {
    $trail->parent('top');
    $trail->push('投票キャンペーン', route('vote_campaign.index'));
});

Breadcrumbs::for('vote_campaign.show', function ($trail, $VoteCampaign) {
    $trail->parent('vote_campaign.index');
    $trail->push($VoteCampaign->title, route('vote_campaign.show', $VoteCampaign->id));
});

Breadcrumbs::for('vote_campaign.vote', function ($trail, $VoteCampaign) {
    $trail->parent('vote_campaign.show', $VoteCampaign);
    $trail->push('投票', route('vote_campaign.vote', $VoteCampaign->id));
});

Breadcrumbs::for('vote_campaign.complete', function ($trail, $VoteCampaign) {
    $trail->parent('vote_campaign.show', $VoteCampaign);
    $trail->push('投票完了', route('vote_campaign.complete', $VoteCampaign->id));
});

Breadcrumbs::for('shinsei.info', function ($trail) {
    $trail->parent('top');
    $trail->push('信長の野望 新生 最新情報まとめ', route('shinsei.info'));
});
Breadcrumbs::for('shinsei.tensyou', function ($trail) {
    $trail->parent('top');
    $trail->push('天正猿芝居のシナリオ予想【信長の野望 新生】', route('shinsei.tensyou'));
});
Breadcrumbs::for('shinsei.beginner', function ($trail) {
    $trail->parent('top');
    $trail->push('信長の野望 新生 ゲームの始め方', route('shinsei.beginner'));
});
Breadcrumbs::for('shinsei.samurais.show', function ($trail, $GameTitle, $Samurai) {
    $trail->parent('database.samurais', $GameTitle);
    $trail->push($Samurai->name . '【' . $GameTitle->name . '】' , route('database.samurais.show', [$GameTitle->slug, $Samurai->id]));
});

// 新生PK
Breadcrumbs::for('shinsei-pk.database.samurais', function ($trail) {
    $trail->parent('top');
    $trail->push('武将名鑑【信長の野望 PK】' , route('shinsei-pk.database.samurais'));
});
Breadcrumbs::for('shinsei-pk.database.show', function ($trail, $GameTitle, $ShinseiPkSamuraiParameter) {
    $trail->parent('shinsei-pk.database.samurais');
    $trail->push($ShinseiPkSamuraiParameter->name . '【' . $GameTitle->name . '】' , route('shinsei-pk.database.show', [$ShinseiPkSamuraiParameter->samurai->id]));
});