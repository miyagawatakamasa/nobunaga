<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['before']], function () {
    Route::get('/', 'TopController@index')->name('top');

    Route::get('/privacy-policy', 'Privacy\PrivacyController@index')->name('privacy');
    Route::get('/about', 'About\AboutController@index')->name('about');
    Route::get('/release', 'About\AboutController@release')->name('release');
    Route::get('/administrator', 'About\AboutController@administrator')->name('administrator');
    Route::get('/contact', 'Contact\ContactController@index')->name('contact');
    Route::post('/contact', 'Contact\ContactController@send')->name('contact.send');
    Route::get('/search', 'Search\SearchController@index')->name('search');

    // 攻略


    // 記事
    Route::get('/article', 'Article\ArticleController@index')->name('article.index');
    Route::get('/article/category/{category_slug}', 'Article\ArticleController@categoryIndex')->name('article.category.index');
    Route::get('/article/example', 'Article\ArticleController@example')->name('article.example');
    Route::get('/article/{article_id}', 'Article\ArticleController@single')->name('article.single');


    // 後で消す
    Route::get('/vote_campaign', 'Vote\VoteController@index');
    Route::get('/vote_campaign/{vote_campaign_id}', 'Vote\VoteController@show');
    Route::get('/vote_campaign/{vote_campaign_id}/vote', 'Vote\VoteController@vote');

    // 投票
    Route::get('/vote-campaign', 'Vote\VoteController@index')->name('vote_campaign.index');
    Route::get('/vote-campaign/{vote_campaign_id}', 'Vote\VoteController@show')->name('vote_campaign.show');
    Route::get('/vote-campaign/{vote_campaign_id}/vote', 'Vote\VoteController@vote')->name('vote_campaign.vote');
    Route::post('/vote-campaign/{vote_campaign_id}/vote', 'Vote\VoteController@post')->name('vote_campaign.post');
    Route::get('/vote-campaign/{vote_campaign_id}/complete', 'Vote\VoteController@complete')->name('vote_campaign.complete');


    // その他のコンテンツ
    Route::get('/contents/timeline', 'Other\OtherController@timeline')->name('other.timeline');
    Route::get('/contents/game-title', 'Other\OtherController@gameTitle')->name('other.game_title');
    Route::get('/contents/real', 'Other\OtherController@real')->name('other.tactics');
    Route::get('/contents/war', 'Other\OtherController@war')->name('other.war');
    Route::get('/contents/war/{disturbance_id}', 'Other\OtherController@warSingle')->name('other.war.single');

    Route::get('/contents/vassals', 'Other\OtherController@vassals')->name('other.vassals');
    Route::get('/contents/diagnose', 'Other\DiagnoseController@index')->name('other.diagnose.index');
    Route::get('/contents/diagnose/new', 'Other\DiagnoseController@new')->name('other.diagnose.new');
    Route::post('/contents/diagnose/new', 'Other\DiagnoseController@create')->name('other.diagnose.create');
    Route::get('/contents/diagnose/complete/{diagnose_samurai_id}', 'Other\DiagnoseController@complete')->name('other.diagnose.complete');
    Route::get('/contents/word', 'Other\OtherController@word')->name('other.word');


    // 新信長
    Route::namespace('Shinnobunaga')->prefix('shinnobunaga')->name('shinnobunaga.')->group(function () {

        // 攻略
        Route::prefix('strategy')->name('strategy.')->group(function () {
            // 序盤の進め方
            Route::get('/beginner', 'StrategyController@beginner')->name('beginner');
            // クイズの答え
            // Route::get('/strategy/quiz', 'StrategyController@quiz')->name('strategy.quiz');
            // リセマラランキング
            Route::get('/resemara', 'StrategyController@resemara')->name('resemara');
            // 無課金向けキャラランキング
            Route::get('/mukakin', 'StrategyController@mukakin')->name('mukakin');
            // 家宝について
            Route::get('/heirloom', 'StrategyController@heirloom')->name('heirloom');
        });

        // データベース
        Route::prefix('database')->name('database.')->group(function () {

            // 最強ランキング
            Route::get('/samurai/ranking', 'DatabaseController@samuraiRanking')->name('samurais.ranking');

            // 武将名鑑
            Route::get('/samurai', 'DatabaseController@samuraiIndex')->name('samurais');
            Route::get('/samurai/rarity/{rarity}', 'DatabaseController@samuraiRarity')->name('samurais.show.rarity');
            Route::get('/samurai/appropriate-position/{appropriate_position}', 'DatabaseController@appropriatePosition')->name('samurais.show.appropriate_position');
            Route::get('/samurai/appropriate-army/{appropriate_army}', 'DatabaseController@appropriateArmy')->name('samurais.show.appropriate_army');
            Route::get('/samurai/{samurai_id}', 'DatabaseController@samuraiShow')->name('samurais.show');
        });

        // ガチャ
        Route::prefix('gacha')->name('gacha.')->group(function () {
            Route::get('/kokushimusou', 'GachaController@kokushimusou')->name('kokushimusou');
        });
    });

    // 覇道
    Route::namespace('Hadou')->prefix('hadou')->name('hadou.')->group(function () {
        // データベース
        Route::prefix('database')->name('database.')->group(function () {

            // 最強ランキング
            // Route::get('/samurai/ranking', 'DatabaseController@samuraiRanking')->name('samurais.ranking');

            // 武将名鑑
            Route::get('/samurai', 'HadouController@samuraiIndex')->name('samurais');
            Route::get('/samurai/{samurai_id}', 'HadouController@samuraiShow')->name('samurais.show');
            Route::get('/samurai/rarity/{rarity}', 'HadouController@samuraiRarity')->name('samurais.show.rarity');
            Route::get('/samurai/ranking/ability-score', 'HadouController@abilityScore')->name('database.ranking.ability_score');
        });
    });


    // 大志PK
    Route::namespace('TaishiPk')->prefix('taishi-pk')->group(function () {
        // 資源
        Route::get('/resource', 'ResourceController@index')->name('resource.index');
        // 設備
        Route::get('/facility', 'FacilityController@index')->name('facility.index');
        // 天守
        Route::get('/tensyu', 'TensyuController@index')->name('tensyu.index');
        // 施設
        Route::get('/building', 'BuildingController@index')->name('building.index');
        // 大命
        Route::get('/taimei', 'TaimeiController@index')->name('taimei.index');

        // 攻略
        // 軍事のコツ
        Route::get('/capture/military', 'CaptureController@military')->name('capture.military');
        // 行軍のコツ
        Route::get('/capture/march', 'CaptureController@march')->name('capture.march');
        // 決戦のコツ
        Route::get('/capture/decisive-battle', 'CaptureController@decisiveBattle')->name('capture.decisive_battle');
        // 攻城戦のコツ
        Route::get('/capture/siege', 'CaptureController@siege')->name('capture.siege');
        // 調略のコツ
        Route::get('/capture/plot', 'CaptureController@plot')->name('capture.plot');
        // 普請のコツ
        Route::get('/capture/general-contract', 'CaptureController@generalContract')->name('capture.general_contract');
        // 人事のコツ
        Route::get('/capture/human-resources', 'CaptureController@humanResources')->name('capture.human_resources');
        // 志について
        Route::get('/capture/will', 'CaptureController@will')->name('capture.will');
        // 内政のコツ
        Route::get('/capture/domestic-affairs', 'CaptureController@domesticAffairs')->name('capture.domestic_affairs');
        // 商業のコツ
        Route::get('/capture/commercial', 'CaptureController@commercial')->name('capture.commercial');
        // 農業のコツ
        Route::get('/capture/agriculture', 'CaptureController@agriculture')->name('capture.agriculture');
        // 外交のコツ
        Route::get('/capture/diplomacy', 'CaptureController@diplomacy')->name('capture.diplomacy');

        // 作戦
        Route::get('/strategy', 'StrategyController@index')->name('taishi-pk.strategy.index');

    });



    // 戦法
    Route::get('/{game_series_slug}/tactics', 'Tactics\TacticsController@index')->name('tactics.index');

    // シナリオ
    Route::get('/{game_series_slug}/scenario', 'Scenario\ScenarioController@index')->name('scenario.index');
    Route::get('/{game_series_slug}/scenario/recommend', 'Scenario\ScenarioController@recommend')->name('scenario.recommend');


    // 新生
    Route::namespace('Shinsei')->prefix('shinsei')->name('shinsei.')->group(function () {
        Route::get('/info', 'ShinseiController@info')->name('info');
        Route::get('/scenario', 'ShinseiController@scenario')->name('scenario');
        Route::get('/beginner', 'ShinseiController@beginner')->name('beginner');
        Route::get('/tensyou', 'ShinseiController@tensyou')->name('tensyou');
        Route::get('/policy', 'ShinseiController@policy')->name('policy');

        Route::get('/database/samurai/{samurai_id}', 'ShinseiController@show')->name('database.samurais.show');

        // Route::get('/fight', 'ShinseiController@fight')->name('fight');
        // Route::get('/domestic', 'ShinseiController@domestic')->name('domestic');
        // Route::get('/battle', 'ShinseiController@battle')->name('battle');
    });

    // 新生PK
    // Route::namespace('ShinseiPk')->prefix('shinsei-pk')->group(function () {

    Route::namespace('ShinseiPk')->prefix('shinsei-pk')->name('shinsei-pk.')->group(function () {
        Route::get('/database/samurai', 'ShinseiPkController@samuraiIndex')->name('database.samurais');
        Route::get('/database/samurai/{samurai_id}', 'ShinseiPkController@show')->name('database.show');
    });

    // データベース
    Route::get('/{game_series_slug}/database/samurai', 'Database\SamuraiController@index')->name('database.samurais');
    Route::get('/{game_series_slug}/database/samurai/{samurai_id}', 'Database\SamuraiController@show')->name('database.samurais.show');
    Route::get('/database/samurai/{samurai_id}', 'Database\SamuraiController@redirectShow')->name('database.samurais.redirect_show');
    Route::get('/{game_series_slug}/ranking/all', 'Database\SamuraiController@ranking')->name('database.ranking');

    Route::get('/{game_series_slug}/database/will', 'Database\WillController@index')->name('database.will');


    Route::get('sitemap.xml', 'SitemapController@sitemap')->name('sitemap');


    // 検索系は?q=aaa
});